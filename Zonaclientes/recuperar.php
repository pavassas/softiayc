<?php
	include('data/Conexion.php');
	
	if($_GET['recuperar'] == "si")
	{
		header("Cache-Control: no-store, no-cache, must-revalidate");
		sleep(1);
		$dat = $_GET['dat'];
		$con = mysqli_query($conectar,"select * from usuario where usu_usuario = '".$dat."' or usu_email = '".$dat."'");
		$dato = mysqli_fetch_array($con);
		$usucla = $dato['usu_clave_int'];
		$usu = $dato['usu_usuario'];
		$ema = $dato['usu_email'];
		$act = $dato['usu_sw_activo'];
		$clave = $dato['usu_clave'];
		
		$con = mysqli_query($conectar,"select * from recuperar where usu_clave_int = '".$usucla."' and rec_estado = 0");
		$num = mysqli_num_rows($con);
		
		if($num > 0)
		{
			$dato = mysqli_fetch_array($con);
			$random = $dato['rec_codigo'];
		}
		else
		{
			$length = 50;
			$random = "";
			$characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"; // change to whatever characters you want
			while ($length > 0) {
				$random .= $characters[mt_rand(0,strlen($characters)-1)];
				$length -= 1;
			}
			$con = mysqli_query($conectar,"insert into recuperar(rec_codigo,usu_clave_int,rec_estado) values('".$random."','".$usucla."','0')");
		}
		
		if($dat != '')
		{
			if(($usu == $dat) || ($ema == $dat))
			{
				// asunto del email
				$subject = "Recuperacion Clave HelpDesk";
				
				// Cuerpo del mensaje
				$mensaje = "------------------------------------------- \n";
				$mensaje.= " Solicitud de recuperación                  \n";
				$mensaje.= "------------------------------------------- \n\n";
				$mensaje.= "Restablecer Contraseña: http://pavas.com.co/Zonaclientes/restablecer.php?codigo=".$random."\n";
				$mensaje.= "FECHA:    ".date("d/m/Y")."\n";
				$mensaje.= "Enviado desde http://www.pavastecnologia.com \n";
				
				// headers del email
				$headers = "From: adminpavas@pavas.com.co\r\n";
				
				// Enviamos el mensaje
				if (mail($ema, $subject, $mensaje, $headers)) {
					echo "<div class='ok'>Su contrase&ntilde;a a sido recuperada satisfactoriamente<br> Por favor revise su correo $ema</div>";
					echo "<script> form.contrasena.value = ''; </script>";
				} else {
					echo "<div class='validaciones'>Error de envió</div>";
				}
			}
			else
			{
				echo "<div class='validaciones'>Usuario o Correo incorrecto</div>";
			}
		}
		else
		{
			echo "<div class='validaciones'>Usuario o Correo incorrecto</div>";
		}
		exit();
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<script type="text/javascript" src="llamadas3.js"></script>
<script type="text/javascript" src="js/jquery-1.6.min.js"></script>
<title>Recuperar Contraseña</title>
<style>
.reveal-modal {
	top: 100px; 
	left: 50%;
	margin-left: -300px;
	width: 520px;
	background: #eee url(terminos/modal-gloss.png) no-repeat -200px -80px;
	position: absolute;
	z-index: 101;
	padding: 30px 40px 34px;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	border-radius: 5px;
	-moz-box-shadow: 0 0 10px rgba(0,0,0,.4);
	-webkit-box-shadow: 0 0 10px rgba(0,0,0,.4);
	-box-shadow: 0 0 10px rgba(0,0,0,.4);
	}
</style>
</head>

<body>
<!-- Opción de recuperar contraseña -->
<div id="recuperar" class="reveal-modal" style="left: 56%; top: 100px; height: 230px; width: 350px;">
	<table style="width: 100%; text-align:center">
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style1" colspan="2"><p class="style42">Restablecer Contrase&ntilde;a</p></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style1" colspan="2"><span lang="es-co" class="style44"><strong>Correo electr&oacute;nico o nombre 
			de usuario:</strong></span></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style1" colspan="2">
			<input name="recuperarcontrasena" id="recuperarcontrasena" class="resplandor" type="text" style="width: 280px; height: 20px;" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style1" colspan="2">
			<input class="boton" name="recuperar" onclick="RECUPERAR()" type="button" value="Enviar" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2"><div id="recu" class="auto-style1"></div></td>
			<td>&nbsp;</td>
		</tr>
	</table>
</div>
</body>

</html>
