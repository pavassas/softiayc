<?php
	include('data/Conexion.php');
	header('Content-Type: text/html; charset=UTF-8');
	date_default_timezone_set('America/Bogota');
	
	if($_GET['varContrasena'] == 1 || $_GET['varContrasena'] == 2)
	{
		echo "<script>alert('Usuario o Contrase�a incorrecta');</script>";
	}
	elseif($_GET['varContrasena'] == 3)
	{
		echo "<script>alert('Su cuenta esta inactiva');</script>";
	}
	if($_GET['recuperar'] == "si")
	{
		header("Cache-Control: no-store, no-cache, must-revalidate");
		sleep(1);
		$dat = $_GET['dat'];
		$con = mysqli_query($conectar,"select * from usuario where usu_usuario = '".$dat."' or usu_email = '".$dat."'");
		$dato = mysqli_fetch_array($con);
		$usucla = $dato['usu_clave_int'];
		$usu = $dato['usu_usuario'];
		$ema = $dato['usu_email'];
		$act = $dato['usu_sw_activo'];
		$clave = $dato['usu_clave'];
		
		$con = mysqli_query($conectar,"select * from recuperar where usu_clave_int = '".$usucla."' and rec_estado = 0");
		$num = mysqli_num_rows($con);
		
		if($num > 0)
		{
			$dato = mysqli_fetch_array($con);
			$random = $dato['rec_codigo'];
		}
		else
		{
			$length = 50;
			$random = "";
			$characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"; // change to whatever characters you want
			while ($length > 0) {
				$random .= $characters[mt_rand(0,strlen($characters)-1)];
				$length -= 1;
			}
			$con = mysqli_query($conectar,"insert into recuperar(rec_codigo,usu_clave_int,rec_estado) values('".$random."','".$usucla."','0')");
		}
		
		if($dat != '')
		{
			if(($usu == $dat) || ($ema == $dat))
			{
				// asunto del email
				$subject = "Recuperacion Clave HelpDesk";
				
				// Cuerpo del mensaje
				$mensaje = "------------------------------------------- \n";
				$mensaje.= " Solicitud de recuperacion                  \n";
				$mensaje.= "------------------------------------------- \n\n";
				$mensaje.= "Para restablecer su contrasena, solo debes presionar clic en el siguiente enlace:\n";
				$mensaje.= "Restablecer Contrasena: http://pavas.co/Zonaclientes/restablecer.php?codigo=".$random."\n";
				$mensaje.= "Si no puedes hacer clic en el enlace, entonces debes copiarlo y pegarlo en su navegador";
				
				$mensaje.= "FECHA:    ".date("d/m/Y")."\n";
				$mensaje.= "Enviado desde http://www.pavas.com.co \n";
				
				// headers del email
				$headers = "From: adminpavas@pavas.com.co\r\n";
				
				// Enviamos el mensaje
				if (mail($ema, $subject, $mensaje, $headers)) {
					echo "<div class='ok'>Su contrase&ntilde;a a sido recuperada satisfactoriamente<br> Por favor revise su correo $ema</div>";
					echo "<script> form.contrasena.value = ''; </script>";
				} else {
					echo "<div class='validaciones'>Error de envió</div>";
				}
			}
			else
			{
				echo "<div class='validaciones'>Usuario o Correo incorrecto</div>";
			}
		}
		else
		{
			echo "<div class='validaciones'>Usuario o Correo incorrecto</div>";
		}
		exit();
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<meta content="IE=9" http-equiv="X-UA-Compatible" /> 
<meta name="google-translate-customization" content="772a7b5186d5fd3a-4fb5c63ee60b8eac-gc125beb8de852df9-e"></meta>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta charset="utf-8" />
<title>ZONA CLIENTES</title>

<?php //VENTANA EMERGENTE ?>
<link rel="stylesheet" href="css/reveal.css" />
<script type="text/javascript" src="js/jquery-1.6.min.js"></script>
<script type="text/javascript" src="js/jquery.reveal.js"></script>


<?php //VALIDACIONES ?>
<script type="text/javascript" src="llamadas.js"></script>

<style type="text/css">@import "css/login.css";
.auto-style1 {
	text-align: center;
}
.auto-style2 {
	color: #DEDEDE;
	font-size: xx-small;
	opacity:.8;
}
.auto-style3 {
	color: #110CDE;
}
.resplandor{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    /*transition: all 0.75s ease-in-out;*/
    /*-webkit-transition: all 0.75s ease-in-out;*/
    /*-moz-transition: all 0.75s ease-in-out;*/
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.2);
    color:gray;
    background-color:#eee;
    padding: 3px;
    box-shadow: 0 0 10px #aaa;
    -webkit-box-shadow: 0 0 10px #aaa;
    -moz-box-shadow: 0 0 10px #aaa;
    border:1px solid #999;
    background-color:white;
}
.validaciones{
	font-size:14px;
	font-family: Arial, helvetica;
	outline:none;
	/*-webkit-transition: all 0.75s ease-in-out;*/
	-moz-transition: all 0.75s ease-in-out;
	border-radius:3px;
	-webkit-border-radius:6px;
	-moz-border-radius:3px;
	border:1px solid rgba(0,0,0, 0.2);
	color:maroon;
	background-color:#eee;
	padding: 3px;
}
</style>
<script language="JavaScript" src="js/jquery-1.6.1.min.js"></script>

</head>
<body>
<form name="form" id="form-login" action="data/validarUsuarios.php" method="post" autocomplete="on" style="width: 340px">
<div id="envoltura" style="left: 48%; top: 50%; width: 400px;">
<div id="mensaje">Mensaje...</div>
<div id="contenedor" class="curva" style="width: 400px">
	<div id="cabecera" class="tac">
		<img src="images/logoblanco.png" alt="Form Login" height="50" width="150" />
	</div>
	<div id="cuerpo">
	
        <p><label for="usuario">Usuario:</label></p>
        <p class="mb10">
		<input name="usuario" type="text" id="usuario" autofocus required style="width: 330px" /></p>
		<p><label for="contrasenia">Contrase&ntilde;a:</label></p>
		<p class="mb10">
		<input name="contrasena" type="password" id="contrasenia" required style="width: 330px" /></p>
		<p><input name="submit" type="submit" id="submit" value="Ingresar" class="boton" /> 
		<span class="auto-style3"> 
		<a data-reveal-id="recuperar" data-animation="fade" style="cursor:pointer">¿Olvidaste tu contraseña?</a></span></p>
	
	</div>
	<div id="pie" class="tac">Sistema de Gesti&oacute;n Documental</div>
</div>
	<div id="nota" class="auto-style1" style="height: 26px">
		<span class="auto-style2">PAVAS TECNOLOGÍA S.A.S.<br>Copyright © Todos los derechos reservados</span></div>
</div>
<div id="recuperar" class="reveal-modal" style="left: 57%; top: 200px; height: 150px; width: 350px;">
	<table style="width: 100%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style1" colspan="2"><p class="style42">Restablecer Contraseña</p></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style1" colspan="2"><span lang="es-co" class="style44"><strong>Correo electrónico o nombre 
			de usuario:</strong></span></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style1" colspan="2">
			<input name="recuperarcontrasena" id="recuperarcontrasena" class="resplandor" type="text" style="width: 280px; height: 20px;" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style1" colspan="2">
			<input class="boton" name="recuperar" onclick="RECUPERAR()" type="button" value="Enviar" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2"><div id="recu" class="auto-style1"></div></td>
			<td>&nbsp;</td>
		</tr>
	</table>
	<a class="close-reveal-modal">&#215;</a>
</div>
</form>
</body>
</html>