<?php
	error_reporting(0);
	include('data/Conexion.php');
	header('Content-Type: text/html; charset=UTF-8');
	date_default_timezone_set('America/Bogota');
	
	if($_GET['varContrasena'] == 1 || $_GET['varContrasena'] == 2)
	{
		echo "<script>alert('Usuario o Contrase a incorrecta');</script>";
	}
	elseif($_GET['varContrasena'] == 3)
	{
		echo "<script>alert('Su cuenta esta inactiva');</script>";
	}
	if($_GET['recuperar'] == "si")
	{
		header("Cache-Control: no-store, no-cache, must-revalidate");
		sleep(1);
		$dat = $_GET['dat'];
		$con = mysqli_query($conectar,"select * from usuario where usu_usuario = '".$dat."' or usu_email = '".$dat."'");
		$dato = mysqli_fetch_array($con);
		$usucla = $dato['usu_clave_int'];
		$usu = $dato['usu_usuario'];
		$ema = $dato['usu_email'];
		$act = $dato['usu_sw_activo'];
		$clave = $dato['usu_clave'];
		
		$con = mysqli_query($conectar,"select * from recuperar where usu_clave_int = '".$usucla."' and rec_estado = 0");
		$num = mysqli_num_rows($con);
		
		if($num > 0)
		{
			$dato = mysqli_fetch_array($con);
			$random = $dato['rec_codigo'];
		}
		else
		{
			$length = 50;
			$random = "";
			$characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"; // change to whatever characters you want
			while ($length > 0) {
				$random .= $characters[mt_rand(0,strlen($characters)-1)];
				$length -= 1;
			}
			$con = mysqli_query($conectar,"insert into recuperar(rec_codigo,usu_clave_int,rec_estado) values('".$random."','".$usucla."','0')");
		}
		
		if($dat != '')
		{
			if(($usu == $dat) || ($ema == $dat))
			{
				// asunto del email
				$subject = "Recuperacion Clave Zona Clientes";
				
				// Cuerpo del mensaje
				$mensaje = "------------------------------------------- \n";
				$mensaje.= " Solicitud de recuperacion                  \n";
				$mensaje.= "------------------------------------------- \n\n";
				$mensaje.= "Para restablecer su contrasena, solo debes presionar clic en el siguiente enlace:\n";
				$mensaje.= "Restablecer Contrasena: http://pavas.com.co/Zonaclientes/restablecer.php?codigo=".$random."\n";
				$mensaje.= "Si no puedes hacer clic en el enlace, entonces debes copiarlo y pegarlo en su navegador";
				
				$mensaje.= "FECHA:    ".date("d/m/Y")."\n";
				$mensaje.= "Enviado desde http://www.pavas.com.co/ \n";
				
				// headers del email
				$headers = "From: adminpavas@pavas.com.co\r\n";
				
				// Enviamos el mensaje
				if (mail($ema, $subject, $mensaje, $headers)) {
					echo "<div class='ok'>Su contrase&ntilde;a a sido recuperada satisfactoriamente<br> Por favor revise su correo $ema</div>";
					echo "<script> form.contrasena.value = ''; </script>";
				} else {
					echo "<div class='validaciones'>Error de envió</div>";
				}
			}
			else
			{
				echo "<div class='validaciones'>Usuario o Correo incorrecto</div>";
			}
		}
		else
		{
			echo "<div class='validaciones'>Usuario o Correo incorrecto</div>";
		}
		exit();
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title>ZONA CLIENTES</title>
<?php //VENTANA EMERGENTE ?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<link rel="stylesheet" href="css/reveal.css" />
<script type="text/javascript" src="js/jquery.reveal.js"></script>
<script type="text/javascript" src="llamadas3.js"></script>

<link rel="stylesheet" href="css/index.css" />
<script>
window.onload = function() {
 
    function bgadj(){
             
        var element = document.getElementById("bg");
         
        var ratio =  element.width / element.height;   
         
        if ((window.innerWidth / window.innerHeight) < ratio){
         
            element.style.width = 'auto';
            element.style.height = '100%';
             
            <!-- si la imagen es mas ancha que la ventana la centro -->
            if (element.width > window.innerWidth){
             
                var ajuste = (window.innerWidth - element.width)/2;
                 
                element.style.left = ajuste+'px';
             
            }
         
        }
        else{  
         
            element.style.width = '100%';
            element.style.height = '100%';
            element.style.left = '0';
 
        }
         
    }
<!-- llamo a la función bgadj() por primera vez al terminar de cargar la página -->
    bgadj();
    <!-- vuelvo a llamar a la función  bgadj() al redimensionar la ventana -->
    window.onresize = function() {
        bgadj();
 
    }
 
}
</script>
<style type="text/css">
.auto-style1 {
	margin-left: 0px;
}
.auto-style2 {
	text-align: center;
}
</style>
</head>

<body>
<form name="form" id="form-login" action="data/validarUsuarios.php" method="post" autocomplete="on" style="width: 340px">
<div class="auto-style2">
<?php echo '<img id="bg" src="images/fondo.jpg"  alt="background" />'; ?>
<table class="bordes">
	<tr>
		<td style="width: 5px">&nbsp;</td>
		<td><img src="images/logo.jpg" height="94" width="221" /></td>
		<td style="width: 5px">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3"><hr style="opacity: 0.3;filter: alpha(opacity=30); /* For IE8 and earlier */"/></td>
	</tr>
	<tr>
		<td style="width: 5px">&nbsp;</td>
		<td>
		<table style="width: 100%; height: 41px;">
			<tr>
				<td style="width: 45px">
				<img src="images/usser.png" height="28" /></td>
				<td>
				<input name="usuario" id="usuario" required style="width:100%;height:100%;border:0px;text-decoration:none" placeholder="Usuario" type="text" class="auto-style1" />
				</td>
			</tr>
		</table>
		</td>
		<td style="width: 5px">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3"><hr style="opacity: 0.3;filter: alpha(opacity=30); /* For IE8 and earlier */"/></td>
	</tr>
	<tr>
		<td style="width: 5px; height: 16px;"></td>
		<td style="height: 16px">
		<table style="width: 100%">
			<tr>
				<td style="width: 45px">
				<img src="images/pass.png" height="30" /></td>
				<td>
				<input name="contrasena" id="contrasenia" required style="width:100%;height:100%;border:0px;text-decoration:none" placeholder="Contraseña" type="password" class="auto-style1" />
				</td>
			</tr>
		</table>
		</td>
		<td style="width: 5px; height: 16px;"></td>
	</tr>
	<tr>
		<td colspan="3"><hr style="opacity: 0.3;filter: alpha(opacity=30); /* For IE8 and earlier */"/></td>
	</tr>
	<tr>
		<td colspan="3">
		<div style="background-color:#DEDEDE" class="bordesboton">
		<table style="width: 98%; height: 30px; text-align:center" align="center">
			<tr>
				<td>
				<input name="Submit1" type="submit" class="boton" value="INGRESAR" style="background-color:#5A5AF3;color:white;width:100%; cursor:pointer; height: 26px;" /></td>
				<td align="right" style="font-size:small;color:#5A5AF3; height: 26px;"><a data-reveal-id="recuperar" data-animation="fade" style="cursor:pointer"><strong>¿Olvidaste tu contraseña?</strong></a></td>
			</tr>
			<tr>
				<td colspan="2" style="font-size:x-small;color:#5A5AF3">
				<strong><br />
				Sistema de Gestión Documental
				</strong>
				</td>
			</tr>
		</table>
		</div>
		</td>
	</tr>
	</table>

</div>
<div id="recuperar" class="reveal-modal" style="left: 60%; top: 200px; height: 150px; width: 350px;">
	<table style="width: 100%;text-align:center" align="center">
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style1" colspan="2"><p class="style42">Restablecer Contraseña</p></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style1" colspan="2"><span lang="es-co" class="style44"><strong>Correo electrónico o nombre 
			de usuario:</strong></span></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style1" colspan="2">
			<input name="recuperarcontrasena" id="recuperarcontrasena" class="resplandor" type="text" style="width: 280px; height: 20px;" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style1" colspan="2">
			<input name="recuperar" onclick="RECUPERAR()" type="button" value="Enviar" style="cursor:pointer" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2"><div id="recu" class="auto-style1"></div></td>
			<td>&nbsp;</td>
		</tr>
	</table>
	<a class="close-reveal-modal">&#215;</a>
</div>

</form>
</body>

</html>
