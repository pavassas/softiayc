function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function EDITAR(v)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('editarciudad').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#editarciudad").html("<img alt='cargando' src='img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?editarnot=si&notedi="+v,true);
	ajax.send(null);
	OCULTARSCROLL();
	/*REFRESCARLISTA1();
	setTimeout("REFRESCARLISTA1();",500);
	setTimeout("REFRESCARLISTA1();",1000);
	setTimeout("REFRESCARLISTA1();",1500);
	setTimeout("REFRESCARLISTA1();",2000);*/
}
function GUARDAR(id)
{
	var tip = form1.tipo1.value;
	var todobr = form1.todasobras1.value;
	
	var obras = "";
	var objCBarray = document.getElementsByName('multiselect_obras1');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	obras += objCBarray[i].value + ",";
	    }
	}
	var todusu = form1.todos1.value;
	var act = form1.activo1.checked;
	
	var usuarios = "";
	var objCBarray = document.getElementsByName('multiselect_usuarios1');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	usuarios += objCBarray[i].value + ",";
	    }
	}
	
	var perfiles = "";
	var objCBarray = document.getElementsByName('multiselect_perfiles1');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	perfiles += objCBarray[i].value + ",";
	    }
	}
	var diacom = form1.diacomite1.value;
	var diasdes = form1.diasdespuescomite1.value;
	var diassubcom = form1.diassubircomite1.value;
	var diassub = form1.diassubir1.value;
	var diasmes = form1.diasdelmes1.value;
	
	//alert("TIP: "+tip+" TODOBR: "+todobr+" OBRAS: "+obras+" TODUSU: "+todusu+" USUARIOS: "+usuarios+" PERFILES: "+perfiles);
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('datos').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#datos").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?guardarnot=si&tip="+tip+"&todobr="+todobr+"&obr="+obras+"&todusu="+todusu+"&usuarios="+usuarios+"&perfiles="+perfiles+"&act="+act+"&diacom="+diacom+"&diasdes="+diasdes+"&diassubcom="+diassubcom+"&diassub="+diassub+"&diasmes="+diasmes+"&n="+id,true);
	ajax.send(null);
	if(tip != '')
	{
		setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
	}
}
function NUEVO()
{
	var tip = form1.tipo.value;
	var todobr = form1.todasobras.value;
	
	var obras = "";
	var objCBarray = document.getElementsByName('multiselect_obras');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	obras += objCBarray[i].value + ",";
	    }
	}
	var todusu = form1.todos.value;
	var act = form1.activo.checked;
	
	var usuarios = "";
	var objCBarray = document.getElementsByName('multiselect_usuarios');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	usuarios += objCBarray[i].value + ",";
	    }
	}
	
	var perfiles = "";
	var objCBarray = document.getElementsByName('multiselect_perfiles');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	perfiles += objCBarray[i].value + ",";
	    }
	}
	
	var diacom = form1.diacomite.value;
	var diasdes = form1.diasdespuescomite.value;
	var diassubcom = form1.diassubircomite.value;
	var diassub = form1.diassubir.value;
	var diasmes = form1.diasdelmes.value;
	//alert("TIP: "+tip+" TODOBR: "+todobr+" OBRAS: "+obras+" TODUSU: "+todusu+" USUARIOS: "+usuarios+" PERFILES: "+perfiles);
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('datos1').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#datos1").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?nuevanotificacion=si&tip="+tip+"&todobr="+todobr+"&obr="+obras+"&todusu="+todusu+"&usuarios="+usuarios+"&perfiles="+perfiles+"&act="+act+"&diacom="+diacom+"&diasdes="+diasdes+"&diassubcom="+diassubcom+"&diassub="+diassub+"&diasmes="+diasmes,true);
	ajax.send(null);
	if(tip != '')
	{
		form1.tipo.value = '';
		setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
	}
	OCULTARSCROLL();
}
function CONSULTAMODULO(v)
{
	if(v == 'TODOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('ciudades').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#ciudades").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si",true);
		ajax.send(null);
		setTimeout("OCULTARSCROLL();",1000);
	}
}
function BUSCAR()
{	
	var tip = form1.bustipo.value;
	var obr = form1.busobra.value;
	var act = form1.buscaractivos.value;
			
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('ciudades').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#ciudades").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?buscarnot=si&tip="+tip+"&obr="+obr+"&act="+act,true);
	ajax.send(null);
	setTimeout("OCULTARSCROLL();",1000);
}
function VERUSUARIOS(o)
{
	if(o == 'usuarios')
	{
		var obras = "";
		var objCBarray = document.getElementsByName('multiselect_obras');
		
		for (i = 0; i < objCBarray.length; i++) 
		{
			if (objCBarray[i].checked) 
			{
		    	obras += objCBarray[i].value + ",";
		    }
		}
		var tod = form1.todos.value;
		if(tod == 1)
		{
			document.getElementById("usuariosseleccionados").style.display = "none";
			document.getElementById("perfilesseleccionados").style.display = "none";
		}
		else
		{
			document.getElementById("usuariosseleccionados").style.display = "block";
			document.getElementById("perfilesseleccionados").style.display = "none";
		}
	}
	else
	{
		var obras = "";
		var objCBarray = document.getElementsByName('multiselect_obras1');
		
		for (i = 0; i < objCBarray.length; i++) 
		{
			if (objCBarray[i].checked) 
			{
		    	obras += objCBarray[i].value + ",";
		    }
		}
		var tod = form1.todos1.value;
		if(tod == 1)
		{
			document.getElementById("usuariosseleccionados1").style.display = "none";
			document.getElementById("perfilesseleccionados1").style.display = "none";
		}
		else
		{
			document.getElementById("usuariosseleccionados1").style.display = "block";
			document.getElementById("perfilesseleccionados1").style.display = "none";
		}
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
	    	if(o == 'usuarios')
			{
   		     	document.getElementById('usuariosseleccionados').innerHTML=ajax.responseText;
   		    }
   		    else
   		    {
   		     	document.getElementById('usuariosseleccionados1').innerHTML=ajax.responseText;
   		    }
	    }
	}
	if(o == 'usuarios')
	{
		jQuery("#usuariosseleccionados").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
	}
	else
	{
		jQuery("#usuariosseleccionados1").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
	}
	ajax.open("GET","?mostrarusuarios=si&obr="+obras+"&o="+o,true);
	ajax.send(null);
	setTimeout("OCULTARSCROLL();",1000);
	if(o == 'usuarios')
	{
		REFRESCARLISTA();
		setTimeout("REFRESCARLISTA();",500);
		setTimeout("REFRESCARLISTA();",1000);
		setTimeout("REFRESCARLISTA();",1500);
		setTimeout("REFRESCARLISTA();",2000);
	}
	else
	{
		REFRESCARLISTA1();
		setTimeout("REFRESCARLISTA1();",500);
		setTimeout("REFRESCARLISTA1();",1000);
		setTimeout("REFRESCARLISTA1();",1500);
		setTimeout("REFRESCARLISTA1();",2000);
	}
}
function VERPERFILES(o)
{
	if(o == 'perfiles')
	{
		var obras = "";
		var objCBarray = document.getElementsByName('multiselect_obras');
		
		for (i = 0; i < objCBarray.length; i++) 
		{
			if (objCBarray[i].checked) 
			{
		    	obras += objCBarray[i].value + ",";
		    }
		}
		var tod = form1.todos.value;
		if(tod == 2)
		{
			document.getElementById("perfilesseleccionados").style.display = "none";
			document.getElementById("usuariosseleccionados").style.display = "none";
		}
		else
		{
			document.getElementById("perfilesseleccionados").style.display = "block";
			document.getElementById("usuariosseleccionados").style.display = "none";
		}
	}
	else
	{
		var obras = "";
		var objCBarray = document.getElementsByName('multiselect_obras1');
		
		for (i = 0; i < objCBarray.length; i++) 
		{
			if (objCBarray[i].checked) 
			{
		    	obras += objCBarray[i].value + ",";
		    }
		}
		var tod = form1.todos1.value;
		if(tod == 2)
		{
			document.getElementById("perfilesseleccionados1").style.display = "none";
			document.getElementById("usuariosseleccionados1").style.display = "none";
		}
		else
		{
			document.getElementById("perfilesseleccionados1").style.display = "block";
			document.getElementById("usuariosseleccionados1").style.display = "none";
		}
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
	    	if(o == 'perfiles')
			{
   		     	document.getElementById('perfilesseleccionados').innerHTML=ajax.responseText;
   		    }
   		    else
   		    {
   		     	document.getElementById('perfilesseleccionados1').innerHTML=ajax.responseText;
   		    }
	    }
	}
	if(o == 'perfiles')
	{
		jQuery("#perfilesseleccionados").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
	}
	else
	{
		jQuery("#perfilesseleccionados1").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
	}
	ajax.open("GET","?mostrarperfiles=si&obr="+obras+"&o="+o,true);
	ajax.send(null);
	setTimeout("OCULTARSCROLL();",1000);
	if(o == 'perfiles')
	{
		REFRESCARLISTAPERFILES();
		setTimeout("REFRESCARLISTAPERFILES();",500);
		setTimeout("REFRESCARLISTAPERFILES();",1000);
		setTimeout("REFRESCARLISTAPERFILES();",1500);
		setTimeout("REFRESCARLISTAPERFILES();",2000);
	}
	else
	{
		REFRESCARLISTAPERFILES1();
		setTimeout("REFRESCARLISTAPERFILES1();",500);
		setTimeout("REFRESCARLISTAPERFILES1();",1000);
		setTimeout("REFRESCARLISTAPERFILES1();",1500);
		setTimeout("REFRESCARLISTAPERFILES1();",2000);
	}
}
function VEROBRAS(o)
{
	if(o == 'obras')
	{
		var todusu = form1.todos.value;
		var tod = form1.todasobras.value;
		if(tod == 1)
		{
			document.getElementById("obrasseleccionadas").style.display = "none";
		}
		else
		{
			document.getElementById("obrasseleccionadas").style.display = "block";
		}
	}
	else
	{
		var todusu = form1.todos1.value;
		var tod = form1.todasobras1.value;
		if(tod == 1)
		{
			document.getElementById("obrasseleccionadas1").style.display = "none";
		}
		else
		{
			document.getElementById("obrasseleccionadas1").style.display = "block";
		}
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
	    	if(o == 'obras')
			{
   		     	document.getElementById('obrasseleccionadas').innerHTML=ajax.responseText;
   		    }
   		    else
   		    {
   		     	document.getElementById('obrasseleccionadas1').innerHTML=ajax.responseText;
   		    }
	    }
	}
	if(o == 'obras')
	{
		jQuery("#obrasseleccionadas").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
	}
	else
	{
		jQuery("#obrasseleccionadas1").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
	}
	ajax.open("GET","?mostrarobras=si&o="+o+"&todusu="+todusu,true);
	ajax.send(null);
	setTimeout("OCULTARSCROLL();",1000);
	if(o == 'obras')
	{
		REFRESCARLISTAOBRAS();
		setTimeout("REFRESCARLISTAOBRAS();",500);
		setTimeout("REFRESCARLISTAOBRAS();",1000);
		setTimeout("REFRESCARLISTAOBRAS();",1500);
		setTimeout("REFRESCARLISTAOBRAS();",2000);
	}
	else
	{
		REFRESCARLISTAOBRAS1();
		setTimeout("REFRESCARLISTAOBRAS1();",500);
		setTimeout("REFRESCARLISTAOBRAS1();",1000);
		setTimeout("REFRESCARLISTAOBRAS1();",1500);
		setTimeout("REFRESCARLISTAOBRAS1();",2000);
	}
}
function VERIFICARTIPO(v)
{
	if(v == 6)
	{
		document.getElementById("diacomite").disabled = false;
		document.getElementById("diasdespuescomite").disabled = false;
		document.getElementById("diassubircomite").disabled = false;
		document.getElementById("diassubir").disabled = true;
		document.getElementById("diasdelmes").disabled = true;
		form1.diassubir.value = '';
		form1.diasdelmes.value = '';
	}
	else
	if(v == 7 || v == 5)
	{
		document.getElementById("diacomite").disabled = true;
		document.getElementById("diasdespuescomite").disabled = true;
		document.getElementById("diassubircomite").disabled = true;
		document.getElementById("diassubir").disabled = false;
		document.getElementById("diasdelmes").disabled = false;
		form1.diacomite.value = '';
		form1.diasdespuescomite.value = '';
		form1.diassubircomite.value = '';
	}
	else
	{
		document.getElementById("diacomite").disabled = true;
		document.getElementById("diasdespuescomite").disabled = true;
		document.getElementById("diassubircomite").disabled = true;
		document.getElementById("diassubir").disabled = true;
		document.getElementById("diasdelmes").disabled = true;
		form1.diacomite.value = '';
		form1.diasdespuescomite.value = '';
		form1.diassubircomite.value = '';
		form1.diassubir.value = '';
		form1.diasdelmes.value = '';
	}
}
function VERIFICARTIPO1(v)
{
	if(v == 6)
	{
		document.getElementById("diacomite1").disabled = false;
		document.getElementById("diasdespuescomite1").disabled = false;
		document.getElementById("diassubircomite1").disabled = false;
		document.getElementById("diassubir1").disabled = true;
		document.getElementById("diasdelmes1").disabled = true;
		form1.diassubir1.value = '';
		form1.diasdelmes1.value = '';
	}
	else
	if(v == 7 || v == 5)
	{
		document.getElementById("diacomite1").disabled = true;
		document.getElementById("diasdespuescomite1").disabled = true;
		document.getElementById("diassubircomite").disabled = true;
		document.getElementById("diassubir1").disabled = false;
		document.getElementById("diasdelmes1").disabled = false;
		form1.diacomite1.value = '';
		form1.diasdespuescomite1.value = '';
		form1.diassubircomite1.value = '';
	}
	else
	{
		document.getElementById("diacomite1").disabled = true;
		document.getElementById("diasdespuescomite1").disabled = true;
		document.getElementById("diassubircomite").disabled = true;
		document.getElementById("diassubir1").disabled = true;
		document.getElementById("diasdelmes1").disabled = true;
		form1.diacomite1.value = '';
		form1.diasdespuescomite1.value = '';
		form1.diassubircomite1.value = '';
		form1.diassubir1.value = '';
		form1.diasdelmes1.value = '';
	}
}