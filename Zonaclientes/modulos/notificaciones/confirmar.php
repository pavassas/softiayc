<?php
	error_reporting(0);
	include('../../data/Conexion.php');
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_SESSION['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
		
	// verifica si no se ha loggeado
	if(!isset($_SESSION["persona"]))
	{
	  session_destroy();
	  header("LOCATION:index.php");
	}else{
	}
?>
<!DOCTYPE HTML>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

	
<title>PAVAS TECNOLOGÍA S.A.S.</title>
<meta name="description" content="Service Desk">
<meta name="author" content="InvGate S.R.L.">

<link rel="shortcut icon" href="img/favicon.ico">
<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">

<link rel="stylesheet" href="css/clean.7e4f11af0c688412dd8aa25b28b0db84.css" type="text/css">
<link rel="stylesheet" href="css/fonts.d87620aa29437ec1a4c2364858fa3607.css" type="text/css">
<link rel="stylesheet" href="css/toolbar.e73e3ba4378b1f2b1de350ef323d2c10.css" type="text/css">
<link rel="stylesheet" href="css/forms.7ba5dbaa170d790e06dc3e64d16df6ec.css" type="text/css">
<link rel="stylesheet" href="css/dropdowns.a3e95e26feafcd09b87732e381224c45.css" type="text/css">
<link rel="stylesheet" href="css/tables.d22651b7bc6ede923c3140e803446613.css" type="text/css">
<link rel="stylesheet" href="css/plugins/validationengine/validationengine.2c0953b5103027332146a5934721da3d.css" type="text/css">
<link rel="stylesheet" href="css/plugins/colorbox/colorbox.bf6245d8fbdbd182e63b3d30efb331bc.css" type="text/css">
<link rel="stylesheet" href="css/plugins/gritter/jquery.gritter.9561c37e5099df25b9dbd6b50002fae9.css" type="text/css">
<link rel="stylesheet" href="css/plugins/invgate/popup.bcac3d52c26fe8813f96890668274834.css" type="text/css">
<link rel="stylesheet" href="css/plugins/invgate/inputlist.a4daf29140fb7fd16c87914a472d76b4.css" type="text/css">
<link rel="stylesheet" href="css/plugins/invgate/multipleselector.537b0771c40de9f3a64e193ba36a57c7.css" type="text/css">
<link rel="stylesheet" href="css/plugins.26ab202f1d786ec744d7f587444f18f3.css" type="text/css">
<link rel="stylesheet" href="css/gamification/main.74d81eedeec7466de677ed5cbb818d69.css" type="text/css">
<link rel="stylesheet" href="css/scrollbar.da62bc7ef54aebbdb4e52272528a44d0.css" type="text/css">
<link rel="stylesheet" href="css/main.e41c890c8f72625912855cdc11dffd33.css" type="text/css">

<link href="css/requests/list.b2b381c1e243bbdeb809bdd354fa55a7.css" media="screen" rel="stylesheet" type="text/css" >
<!--[if lte IE 7]>
<link rel="stylesheet" href="css/ie.c32bc18afe0e2883ee4912a51f86c119.css" type="text/css" />
<![endif]-->

<style type="text/css">
.auto-style1 {
	text-align: center;
}
.auto-style2 {
	font-size: 12px;
	font-family: Arial, helvetica;
	outline: none;
/*transition: all 0.75s ease-in-out;*/ /*-webkit-transition: all 0.75s ease-in-out;*/ /*-moz-transition: all 0.75s ease-in-out;*/border-radius: 3px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 3px;
	border: 1px solid rgba(0,0,0, 0.2);
	
	background-color: #eee;
	padding: 3px;
	box-shadow: 0 0 10px #aaa;
	-webkit-box-shadow: 0 0 10px #aaa;
	-moz-box-shadow: 0 0 10px #aaa;
	border: 1px solid #999;
	background-color: white;
	text-align: center;
}
.auto-style3 {
	text-align: left;
}
.inputs{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.5);
    padding: 3px;
}
.validaciones{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.2);
    color:maroon;
    background-color:#eee;
    padding: 3px;
    text-align:center;
}
.ok{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.2);
    color:green;
    background-color:#eee;
    padding: 3px;
    text-align:center;
}
</style>

<script type="text/javascript" language="javascript">
	selecteds=0;
	
	function CheckUncheck(total,check){
		checkbox=null;
		for(i=1;i<=total;i++){
			checkbox=document.getElementById("idcat"+i);
			//alert(checkbox.value);
			checkbox.checked=check.checked;
		}
		
		if(check.checked){
			selecteds=total;
		}else{
			selecteds=0;
		}
		
	}
	
	function contadorVals(check){
		if(check.checked){
			selecteds=selecteds+1;
		}else{
			selecteds=selecteds-1;
		}
	}
	
	function selectedVals(){
		if(selecteds==0){
			alert("Seleccione al menos un registro.");
			return false;
		}else{
			return true;
		}
	}
	var IE = document.all ? true : false;
if (!IE) {
    document.captureEvents(Event.MOUSEMOVE);
}
document.onmousemove = getMouseXY;
var tempX = 0;
var tempY = 0;
//esta funcion no necesitas entender solo asigna la posicion del raton a tempX y tempY
function getMouseXY(e){
    if (IE) { //para IE
        tempX = event.clientX + document.body.scrollLeft;
        tempY = event.clientY + document.body.scrollTop;
    }
    else { //para netscape
        tempX = e.pageX;
        tempY = e.pageY;
    }
    if (tempX < 0) {
        tempX = 0;
    }
    if (tempY < 0) {
        tempY = 0;
    }
    return true;
}
function VentanaFlotante(mensaje, x, y){
        //creo el objeto div
        var div_fl = document.createElement('DIV');
        //le asigno que su posicion sera abosoluta
        div_fl.style.position = 'absolute';
        //le asigno el ide a la ventana
        div_fl.id = 'Miventana';
        //digo en que posicion left y top se creara a partir de la posicion del raton tempx tempy
        div_fl.style.left = tempX + 'px';
        div_fl.style.top = tempY + 'px';
        //asigno el ancho del div pasado por parametro
        div_fl.style.width = x + 'px';
        //asigno el alto del div pasado por parametro
        div_fl.style.height = y + 'px';
        //digo con css que el borde sera de grosor 1px solido y de color negro
        div_fl.style.border = "1px solid #000000";
        //asigno el color de fondo
        div_fl.style.backgroundColor = "#cccccc";
        //el objeto añado a la estrutura principal el document.body
        document.body.appendChild(div_fl);
        //el mensaje pasado por parametro muestro dentro del div
        div_fl.innerHTML = mensaje;
}
function quitarDiv()
{
//creo el objeto del div
var mv = document.getElementById('Miventana');
//elimino el objeto
document.body.removeChild(mv);
}
</script>

<?php //VALIDACIONES ?>
<script type="text/javascript" src="llamadas.js"></script>
<script type="text/javascript" src="js/jquery-1.6.min.js"></script>

<?php //VENTANA EMERGENTE ?>
<link rel="stylesheet" href="css/reveal.css" />
<script type="text/javascript" src="js/jquery.reveal.js"></script>
</head>


<body>
<?php
$rows=mysqli_query($conectar,"select * from notificar_h");
$total=mysqli_num_rows($rows);
$idcats=$_POST['idcat'];
?>
<form action="accionconfirmar.php?accion=<?php echo $_POST['Accion']; ?>" method="post">
					<input  type="hidden" name="idcat[]" id="idcat" value="<?php echo $row['not_clave_int'];?>" />
	<input  type="hidden" name="idcat[]" id="idcat" value="<?php echo $row['not_clave_int'];?>" />
<!--[if lte IE 7]>
<div class="ieWarning">Este navegador no es compatible con el sistema. Por favor, use Chrome, Safari, Firefox o Internet Explorer 8 o superior.</div>
<![endif]-->
<div class="requestListFiltersButton requestListFiltersButtonClear">
	<table style="width: 105%">
		<tr>
			<td class="auto-style2" style="width: 60px; cursor:pointer">
			Todos
			<?php
				$con = mysqli_query($conectar,"select COUNT(*) cant from notificar_h");
				$dato = mysqli_fetch_array($con);
				echo $dato['cant'];
			?>
			</td>
			<td class="auto-style1" style="width: 117px; cursor:pointer">
			&nbsp;</td>
			<td class="auto-style1" style="width: 65px; cursor:pointer">
			&nbsp;</td>
			<td class="auto-style1" style="width: 70px; cursor:pointer">
			&nbsp;</td>
			<td class="auto-style1">&nbsp;</td>
			<td class="auto-style2" style="width: 55px; cursor:pointer">
			<table style="width: 100%">
				<tr>
					<td><a data-reveal-id="nuevousuario" data-animation="fade" style="cursor:pointer"><img alt="" src="../../images/add2.png"></a></td>
					<td><a data-reveal-id="nuevousuario" data-animation="fade" style="cursor:pointer">Añadir</a></td>
				</tr>
			</table>
			</td>
			<td class="auto-style1" style="width: 55px"></td>
		</tr>
		<tr>
			<td colspan="6" class="auto-style2">
			<?php
			if(isset($_POST['Accion']))
			{
				$accion = $_POST['Accion'];
			}
			if($accion <> 'Agregar')
			{
			?>
			<table style="width: 100%">
				<tr>
					<td class="auto-style3" style="width: 221px">&nbsp;</td>
					<td class="auto-style3" style="width: 221px">&nbsp;</td>
					<td class="auto-style3" style="width: 221px">&nbsp;</td>
					<td class="auto-style3" style="width: 221px">&nbsp;</td>
					<td class="auto-style3" style="width: 221px">&nbsp;</td>
					<td class="auto-style3" style="width: 221px">&nbsp;</td>
					<td class="auto-style3" style="width: 3px">&nbsp;</td>
					<td class="auto-style3">&nbsp;</td>
				</tr>
				<tr>
					<td class="auto-style3" style="width: 221px"><strong>Notificación</strong></td>
					<td class="auto-style3" style="width: 221px"><strong>Todas Las Obras</strong></td>
					<td class="auto-style3" style="width: 221px"><strong>Todos Los Usuarios</strong></td>
					<td class="auto-style3" style="width: 221px"><strong>Todos Los Perfiles</strong></td>
					<td class="auto-style3" style="width: 221px"><strong>Actualizado Por</strong></td>
					<td class="auto-style3" style="width: 221px"><strong>Fecha Actualización</strong></td>
					<td class="auto-style3" style="width: 3px"><strong>Activo</strong></td>
					<td class="auto-style3">&nbsp;</td>
				</tr>
				<?php
					if(isset($_POST['Accion']))
					{
						$accion = $_POST['Accion'];
					}
					if(is_array($idcats)){
					if($accion == 'Activar')
					{
					?>
				    <p class="auto-style3">
				    	
				    	<strong>Esta seguro de Activar las siguientes Notificaciones? 
				    	<input type="submit" value="SI" name="Accion" onmouseover="this.style.backgroundColor='#445B74';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="userNameIcon" /></strong>
				    	<a href="notificaciones.php"><strong>
						<input type="button" value="NO" name="Accion" onmouseover="this.style.backgroundColor='#445B74';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="userNameIcon" /></strong></a>
				    </p>
				    <?php
				    }
				    else
				    if($accion == 'Inactivar')
					{
					
					?>
				    <p class="auto-style3">
				    	
				    	<strong>Esta seguro de Inactivar las siguientes Notificaciones?
				    	<input type="submit" value="SI" name="Accion" onmouseover="this.style.backgroundColor='#445B74';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="userNameIcon" /></strong>
				    	<a href="notificaciones.php"><strong>
						<input type="button" value="NO" name="Accion" onmouseover="this.style.backgroundColor='#445B74';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="userNameIcon" /></strong></a>
				    </p>
				    <?php
				    }
				    else
				    if($accion == 'Eliminar')
					{
					
					?>
				    <p class="auto-style3">
				    	
				    	<strong>Esta seguro de Eliminar las siguientes Notificaciones?
				    	<input type="submit" value="SI" name="Accion" onmouseover="this.style.backgroundColor='#445B74';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="userNameIcon" /></strong>
				    	<a href="notificaciones.php"><strong>
						<input type="button" value="NO" name="Accion" onmouseover="this.style.backgroundColor='#445B74';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="userNameIcon" /></strong></a>
				    </p>
				    <?php
				    }
				    ?>
				    <?php
			        	for($i=0;$i<count($idcats);$i++)
			        	{
							$rows=mysqli_query($conectar,"select * from notificar_h where not_clave_int= '".$idcats[$i]."' order by not_tipo");
							if(mysqli_num_rows($rows)){
							$row=mysqli_fetch_array($rows);
							$clanot = $row['not_clave_int'];
							$tip = $row['not_tipo'];
							if($tip == 1){ $tip = 'Nueva carga'; }elseif($tip == 2){ $tip = 'Actualizar carga'; }elseif($tip == 3){ $tip = 'Eliminar carga'; }elseif($tip == 4){ $tip = 'Aprobar carga'; }elseif($tip == 5){ $tip = 'Aprobar Informe Mensual'; }elseif($tip == 6){ $tip = 'Recordatorio Comité De Obra'; }elseif($tip == 7){ $tip = 'Recordatorio Informe Mensual'; }
							$swtod = $row['not_sw_todos'];
							$act = $row['not_sw_activo'];
							$usuact = $row['not_usu_actualiz'];
							$fecact = $row['not_fec_actualiz'];
			        ?>
				<tr>
					<input  type="hidden" name="idcat[]" id="idcat" value="<?php echo $row['not_clave_int'];?>" />
					<td class="auto-style3" style="width: 221px"><?php echo strtoupper($tip); ?></td>
					<td class="auto-style3" style="width: 221px">
					<?php 
					if($swtodobr == 1)
					{ 
						echo "SI"; 
					}
					else
					{ 
						$conobr = mysqli_query($conectar,"select * from notificar_d n inner join obra o on (o.obr_clave_int = n.nod_registro) where not_clave_int = '".$clanot."' and nod_tipo = 1");
						$numobr = mysqli_num_rows($conobr);
					?>
					<div style="cursor:pointer" onmouseover="javascript: VentanaFlotante('<?php for($j = 0; $j < $numobr; $j++){ $datoobr = mysqli_fetch_array($conobr); echo $datoobr['obr_nombre']."<br>"; } ?>', 300, 15*<?php echo $numobr; ?>)" onmouseout="javascript: quitarDiv();">NO</div>
					<?php 
					} 
					?>
					</td>
					<td class="auto-style3" style="width: 221px">
					<?php 
					if($swtodusu == 1)
					{ 
						echo "SI"; 
					}
					else
					{ 
						$conusu = mysqli_query($conectar,"select * from notificar_d n inner join usuario u on (u.usu_clave_int = n.nod_registro) where not_clave_int = '".$clanot."' and nod_tipo = 2");
						$numusu = mysqli_num_rows($conusu);
					?>
					<div style="cursor:pointer" onmouseover="javascript: VentanaFlotante('<?php if($numusu <= 0){ echo 'Sin Registros'; }else{ for($j = 0; $j < $numusu; $j++){ $datousu = mysqli_fetch_array($conusu); echo $datousu['usu_nombre']."<br>"; } } ?>', 300, 15*<?php if($numusu <= 0){ echo '1'; }else{ echo $numusu; } ?>)" onmouseout="javascript: quitarDiv();">NO</div>
					<?php 
					} 
					?>
					</td>
					<td class="auto-style3" style="width: 221px">
					<?php 
					if($swtodper == 1)
					{ 
						echo "SI"; 
					}
					else
					{ 
						$conprf = mysqli_query($conectar,"select * from notificar_d n inner join perfil p on (p.prf_clave_int = n.nod_registro) where not_clave_int = '".$clanot."' and nod_tipo = 3");
						$numprf = mysqli_num_rows($conprf);
					?>
					<div style="cursor:pointer" onmouseover="javascript: VentanaFlotante('<?php if($numprf <= 0){ echo 'Sin Registros'; }else{ for($j = 0; $j < $numprf; $j++){ $datoprf = mysqli_fetch_array($conprf); echo $datoprf['prf_descripcion']."<br>"; } } ?>', 300, 15*<?php if($numprf <= 0){ echo '1'; }else{ echo $numprf; } ?>)" onmouseout="javascript: quitarDiv();">NO</div>
					<?php 
					} 
					?>
					</td>
					<td class="auto-style3" style="width: 221px"><?php echo $row['not_usu_actualiz']; ?></td>
					<td class="auto-style3" style="width: 221px"><?php echo $row['not_fec_actualiz']; ?></td>
					<td class="auto-style1" style="width: 3px">
					<input name="activarinactivar" id="activarinactivar" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" ></td>
					<td class="auto-style1">
					&nbsp;</td>
				</tr>
				<?php
							}
						}
		        	}
		        ?>
				<tr>
					<td class="auto-style3" style="width: 221px">&nbsp;</td>
					<td class="auto-style3" style="width: 221px">&nbsp;</td>
					<td class="auto-style3" style="width: 221px">&nbsp;</td>
					<td class="auto-style3" style="width: 221px">&nbsp;</td>
					<td class="auto-style3" style="width: 221px">&nbsp;</td>
					<td class="auto-style3" style="width: 221px">&nbsp;</td>
					<td class="auto-style3" style="width: 3px">&nbsp;</td>
					<td class="auto-style3">&nbsp;</td>
				</tr>
			</table>
			<?php
			}
			?>
			</td>
			<td style="width: 55px">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 60px">&nbsp;</td>
			<td style="width: 117px">&nbsp;</td>
			<td style="width: 65px">&nbsp;</td>
			<td style="width: 70px">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td style="width: 55px">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 60px">&nbsp;</td>
			<td style="width: 117px">&nbsp;</td>
			<td style="width: 65px">&nbsp;</td>
			<td style="width: 70px">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td style="width: 55px">&nbsp;</td>
		</tr>
	</table>
</div>
</form>
</body>
</html>