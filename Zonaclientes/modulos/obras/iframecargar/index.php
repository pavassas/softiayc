﻿<?php
error_reporting(0);
include('../../../data/Conexion.php');
session_start();
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_SESSION['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];

// verifica si no se ha loggeado
if(!isset($_SESSION["persona"]))
{
  session_destroy();
  header("LOCATION:index.php");
}else{
}
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");

$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
$dato = mysqli_fetch_array($con);
$claveperfil = $dato['prf_clave_int'];
$claveusuario = $dato['usu_clave_int'];
$ediclacar = $dato['car_clave_int'];

$obr = $_GET['obr'];
?>
<!DOCTYPE html>
<html>
<head>
	<title>Subir archivos con dropzone</title>
	<meta charset="utf-8">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script type="text/javascript" src="bower_components/dropzone/downloads/dropzone.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function()
	{
		Dropzone.autoDiscover = false;
		$("#dropzone").dropzone({
			url: "uploads.php?obr=<?php echo $obr; ?>",
			addRemoveLinks: true,
			maxFileSize: 1000,
			dictResponseError: "Ha ocurrido un error en el server",
			acceptedFiles: '.png,.jpg,.jpeg,.gif',
			complete: function(file)
			{
				if(file.status == "success")
				{
					alert("El siguiente archivo ha subido correctamentee: " + file.name);
				}
			},
			error: function(file)
			{
				alert("No se puede cargar el tipo de archivo seleccionado: " + file.name);
			},
			removedfile: function(file, serverFileName)
			{
				var name = file.name;
				$.ajax({
					type: "POST",
					url: "uploads.php?delete=true&obr=<?php echo $obr; ?>",
					data: "filename="+name,
					success: function(data)
					{
						var json = JSON.parse(data);
						if(json.res == true)
						{
							var element;
							(element = file.previewElement) != null ? 
							element.parentNode.removeChild(file.previewElement) : 
							false;
							alert("El elemento fué eliminado: " + name); 
						}
					}
				});
			}
		});
	});
	</script>
	<script type="text/javascript" src="llamadas.js"></script>
	<link rel="stylesheet" type="text/css" href="bower_components/dropzone/downloads/css/dropzone.css">
</head>
<body>
	<div id="dropzone" class="dropzone"></div>
</body>
</html>