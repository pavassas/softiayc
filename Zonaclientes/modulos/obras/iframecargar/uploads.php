<?php
include('../../../data/Conexion.php');
session_start();
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_SESSION['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];

// verifica si no se ha loggeado
if(!isset($_SESSION["persona"]))
{
  session_destroy();
  header("LOCATION:index.php");
}else{
}
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");

$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
$dato = mysqli_fetch_array($con);
$claveperfil = $dato['prf_clave_int'];
$claveusuario = $dato['usu_clave_int'];
$ediclacar = $dato['car_clave_int'];

$obr = $_GET['obr'];

if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
{
	if(isset($_GET["delete"]) && $_GET["delete"] == true)
	{
		$name = $_POST["filename"];
		/*if(file_exists('./uploads/'.$name))
		{*/
			//unlink('./uploads/'.$name);
			mysqli_query($conectar,"DELETE FROM obra_foto WHERE ofo_nombre_original = '$name' and obr_clave_int = ".$obr." LIMIT 1");
			echo json_encode(array("res" => true));
		/*}
		else
		{
			echo json_encode(array("res" => false));
		}*/
	}
	else
	{
		$file = $_FILES["file"]["name"];
		$archivo = basename($_FILES['file']['name']);
						
		$array_nombre = explode('.',$archivo);
		$cuenta_arr_nombre = count($array_nombre);
		$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				
		$filetype = $_FILES["file"]["type"];
		$filesize = $_FILES["file"]["size"];

		if(!is_dir("uploads/"))
			mkdir("uploads/", 0777);
		
		$con = mysqli_query($conectar,"select ofo_clave_int from obra_foto order by ofo_clave_int DESC LIMIT 1");
		$dato = mysqli_fetch_array($con);
		$clacaf = $dato['ofo_clave_int'];
		
		if($clacaf == '' or $clacaf == 0)
		{
			$clacaf = 1;
		}
		else
		{
			$clacaf = $clacaf+1;
		}
		
		$destino =  "uploads/".$clacaf.".".$extension;
		
		if($file && move_uploaded_file($_FILES["file"]["tmp_name"], $destino))
		{
			mysqli_query($conectar,"INSERT INTO obra_foto(ofo_clave_int,obr_clave_int,ofo_foto,ofo_nombre_original,ofo_usu_actualiz,ofo_fec_actualiz) values(null,'".$obr."','".$destino."','".$file."','".$usuario."','".$fecha."')");
			if($extension == 'jpg' or $extension == 'jpeg')
			{
				//Reduzco el tamano de la imagen
				$img_origen = imagecreatefromjpeg($destino);
				$ancho_origen = imagesx($img_origen);//Se obtiene el ancho de la imagen
				$alto_origen = imagesy($img_origen);//Se obtiene el alto de la imagen
				$ancho_limite = 850;
				
				if($ancho_origen > $ancho_limite)//Para foto horizontal
				{
					$ancho_origen = $ancho_limite;
					$alto_origen = $ancho_limite*imagesy($img_origen)/imagesx($img_origen);
				}
				else//Para fotos verticales
				{
					$alto_origen = $ancho_limite;
					$ancho_origen = $ancho_limite*imagesx($img_origen)/imagesy($img_origen);
				}
				$img_destino = imagecreatetruecolor($ancho_origen, $alto_origen);//Se crea la imagen segun las dimensiones dadas
				imagecopyresized($img_destino, $img_origen, 0, 0, 0, 0, $ancho_origen, $alto_origen, imagesx($img_origen), imagesy($img_origen));
				imagejpeg($img_destino,$destino);//Se guarda la nueva foto
			}
			else
			if($extension == 'png')
			{
				//Reduzco el tamano de la imagen
				$img_origen = imagecreatefrompng($destino);
				$ancho_origen = imagesx($img_origen);//Se obtiene el ancho de la imagen
				$alto_origen = imagesy($img_origen);//Se obtiene el alto de la imagen
				$ancho_limite = 850;
				
				if($ancho_origen > $ancho_limite)//Para foto horizontal
				{
					$ancho_origen = $ancho_limite;
					$alto_origen = $ancho_limite*imagesy($img_origen)/imagesx($img_origen);
				}
				else//Para fotos verticales
				{
					$alto_origen = $ancho_limite;
					$ancho_origen = $ancho_limite*imagesx($img_origen)/imagesy($img_origen);
				}
				$img_destino = imagecreatetruecolor($ancho_origen, $alto_origen);//Se crea la imagen segun las dimensiones dadas
				imagecopyresized($img_destino, $img_origen, 0, 0, 0, 0, $ancho_origen, $alto_origen, imagesx($img_origen), imagesy($img_origen));
				imagepng($img_destino,$destino);//Se guarda la nueva foto
			}
			else
			if($extension == 'gif')
			{
				//Reduzco el tamano de la imagen
				$img_origen = imagecreatefromgif($destino);
				$ancho_origen = imagesx($img_origen);//Se obtiene el ancho de la imagen
				$alto_origen = imagesy($img_origen);//Se obtiene el alto de la imagen
				$ancho_limite = 850;
				
				if($ancho_origen > $ancho_limite)//Para foto horizontal
				{
					$ancho_origen = $ancho_limite;
					$alto_origen = $ancho_limite*imagesy($img_origen)/imagesx($img_origen);
				}
				else//Para fotos verticales
				{
					$alto_origen = $ancho_limite;
					$ancho_origen = $ancho_limite*imagesx($img_origen)/imagesy($img_origen);
				}
				$img_destino = imagecreatetruecolor($ancho_origen, $alto_origen);//Se crea la imagen segun las dimensiones dadas
				imagecopyresized($img_destino, $img_origen, 0, 0, 0, 0, $ancho_origen, $alto_origen, imagesx($img_origen), imagesy($img_origen));
				imagegif($img_destino,$destino);//Se guarda la nueva foto
			}
			else
			if($extension == 'gif')
			{
				//Reduzco el tamano de la imagen
				$img_origen = imagecreatefromgif($destino);
				$ancho_origen = imagesx($img_origen);//Se obtiene el ancho de la imagen
				$alto_origen = imagesy($img_origen);//Se obtiene el alto de la imagen
				$ancho_limite = 850;
				
				if($ancho_origen > $ancho_limite)//Para foto horizontal
				{
					$ancho_origen = $ancho_limite;
					$alto_origen = $ancho_limite*imagesy($img_origen)/imagesx($img_origen);
				}
				else//Para fotos verticales
				{
					$alto_origen = $ancho_limite;
					$ancho_origen = $ancho_limite*imagesx($img_origen)/imagesy($img_origen);
				}
				$img_destino = imagecreatetruecolor($ancho_origen, $alto_origen);//Se crea la imagen segun las dimensiones dadas
				imagecopyresized($img_destino, $img_origen, 0, 0, 0, 0, $ancho_origen, $alto_origen, imagesx($img_origen), imagesy($img_origen));
				imagegif($img_destino,$destino);//Se guarda la nueva foto
			}
		}
	}
}
?>