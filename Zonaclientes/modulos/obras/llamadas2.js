function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function EDITAR(v,m)
{	
	var coordenadas = $("#btnedit"+  v).position();
	var top = coordenadas.top;
	$('#editarobra').css('top',top);


	var Heightparent = parent.document.getElementById('iframe2').contentWindow.document .body.scrollHeight; 
	console.log(Heightparent);

	console.log("Top" + top);
	if(m == 'OBRA')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('editarobra').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#editarobra").html("<img alt='cargando' src='img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?editarobr=si&obredi="+v,true);
		ajax.send(null);
	}
	setTimeout(OCULTARSCROLL(),5000);
}
function GUARDAR(v,id)
{
	if(v == 'OBRA')
	{
		var obr = form1.obra1.value;
		var lo = obr.length;
		var ciu = form1.ciudad1.value;
		var dir = form1.direccion1.value;
		var res = form1.responsable1.value;
		var act = form1.activo1.checked;
		
		if(obr == '')
		{
			alert('Debe ingresar la Obra');
		}
		else
		if(ciu == '')
		{
			alert('Debe elegir la Ciudad de la Obra');
		}
		else
		if(dir == '')
		{
			alert('Debe ingresar la Direccion de la Obra');
		}
		else
		if(res == '')
		{
			alert('Debe elegir el Responsable de la Obra');
		}
		else
		{
			var nuevadireccion = CORREGIRTEXTO(dir);
			var ajax;
			ajax=new ajaxFunction();
			ajax.onreadystatechange=function()
			{
				if(ajax.readyState==4)
			    {
		   		     document.getElementById('datos').innerHTML=ajax.responseText;
			    }
			}
			jQuery("#datos").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
			ajax.open("GET","?guardarobr=si&obr="+obr+"&ciu="+ciu+"&dir="+nuevadireccion+"&res="+res+"&act="+act+"&lo="+lo+"&o="+id,true);
			ajax.send(null);
			if(obr != '' && ciu != '' && dir != '' && res != '' && lo >= 3)
			{
				setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
			}
		}
	}
}
function NUEVO(v)
{
	if(v == 'OBRA')
	{
		var obr = form1.obra.value;
		var lo = obr.length;
		var ciu = form1.ciudad.value;
		var dir = form1.direccion.value;
		var res = form1.responsable.value;	
		var act = form1.activo.checked;
		
		if(obr == '')
		{
			alert('Debe ingresar la Obra');
		}
		else
		if(ciu == '')
		{
			alert('Debe elegir la Ciudad de la Obra');
		}
		else
		if(dir == '')
		{
			alert('Debe ingresar la Direccion de la Obra');
		}
		else
		if(res == '')
		{
			alert('Debe elegir el Responsable de la Obra');
		}
		else
		{
			var nuevadireccion = CORREGIRTEXTO(dir);
			var ajax;
			ajax=new ajaxFunction();
			ajax.onreadystatechange=function()
			{
				if(ajax.readyState==4)
			    {
		   		     document.getElementById('datos1').innerHTML=ajax.responseText;
			    }
			}
			jQuery("#datos1").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
			ajax.open("GET","?nuevaobra=si&obr="+obr+"&ciu="+ciu+"&dir="+nuevadireccion+"&res="+res+"&act="+act+"&lo="+lo,true);
			ajax.send(null);
			if(obr != '' && ciu != '' && dir != '' && res != '' && lo >= 3)
			{
				form1.obra.value = '';
				form1.ciudad.value = '';
				form1.direccion.value = '';
				form1.responsable.value = '';
				setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
			}
		}
	}
}
function CORREGIRTEXTO(v)
{
	var res = v.replace('#','REEMPLAZARNUMERAL');
	var res = res.replace('+','REEMPLAZARMAS');
	
	return res;
}
function CONSULTAMODULO(v)
{
	if(v == 'TODOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('obras').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#obras").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si",true);
		ajax.send(null);
	}
}
function BUSCAR(m)
{	
	if(m == 'OBRA')
	{
		var obr = form1.obra2.value;
		var ciu = form1.busciudad.value;
		var dir = form1.busdireccion.value;
		var res = form1.busresponsable.value;
		var act = form1.buscaractivos.value;
				
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('obras').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#obras").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?buscarobr=si&obr="+obr+"&ciu="+ciu+"&dir="+dir+"&res="+res+"&act="+act,true);
		ajax.send(null);
	}
}
function MOSTRARMOVIMIENTO(v)
{
	var div = document.getElementById('movimiento'+v);
    div.style.display = 'block';
    
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('movimiento'+v).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#movimiento"+v).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarmovimiento=si&claobr="+v,true);
	ajax.send(null);
}
function OCULTARMOVIMIENTO(v)
{
	var div = document.getElementById('movimiento'+v);
    div.style.display = 'none';
}
function ELIMINARFOTO(v)
{
	if(confirm('Esta seguro/a de Eliminar esta imagen?'))
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     //document.getElementById('miVentana').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#fotoeliminada"+v).html("<img alt='cargando' src='img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?eliminarfot