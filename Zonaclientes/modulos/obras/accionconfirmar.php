<?php include('../../data/Conexion.php');
session_start();
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_SESSION['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];
	
// verifica si no se ha loggeado
if(!isset($_SESSION["persona"]))
{
  session_destroy();
  header("LOCATION:index.php");
}else{
}
$con = mysqli_query($conectar,"select obr_clave_int from usuario where usu_usuario = '".$usuario."'");
$dato = mysqli_fetch_array($con);
$ultimaobra = $dato['obr_clave_int'];
	
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");
$idcats=$_POST['idcat'];
$contador=0;
if(is_array($idcats)){
        for($i=0;$i<count($idcats);$i++){
        	if($_GET['accion'] == 'Activar')
        	{
				$rows=mysqli_query($conectar,"update obra set obr_sw_activo = 1, obr_usu_actualiz = '".$usuario."', obr_fec_actualiz = '".$fecha."' where obr_clave_int=".$idcats[$i]);
				mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_encabezado,obr_clave_int,loa_usu_actualiz,loa_fec_actualiz) values(null,17,55,'".$idcats[$i]."','".$ultimaobra."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 55=Actualización obra
        	}
        	elseif($_GET['accion'] == 'Inactivar')
        	{
        		$rows=mysqli_query($conectar,"update obra set obr_sw_activo = 0, obr_usu_actualiz = '".$usuario."', obr_fec_actualiz = '".$fecha."' where obr_clave_int=".$idcats[$i]);
				mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_encabezado,obr_clave_int,loa_usu_actualiz,loa_fec_actualiz) values(null,17,55,'".$idcats[$i]."','".$ultimaobra."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 55=Actualización obra
        	}
        	else if($_GET['accion'] == 'Eliminar')
        	{
        		if($idcats[$i] != '')
        		{
	        		/* mysqli_query($conectar,"select * from carga where obr_clave_int = '".$idcats[$i]."'");
	        		$numinv = mysqli_num_rows($con);
	        		$con = mysqli_query($conectar,"select * from carga_archivo where obr_clave_int = '".$idcats[$i]."'");
	        		$numdes = mysqli_num_rows($con);
			
	        		if($nume > 0 || $numdes > 0)
	        		{
						$men = "ERROR";
	        		}
	        		else
	        		{*/
	        			$rows1=mysqli_query($conectar,"update obra set obr_sw_activo = 2 where obr_clave_int=".$idcats[$i]);
						mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_encabezado,obr_clave_int,loa_usu_actualiz,loa_fec_actualiz) values(null,17,56,'".$idcats[$i]."','".$ultimaobra."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 56=Eliminación obra
	        		//}
	        	}
        	}
        	$contador++;
		}
		//echo "Se han eliminado $contador Registros de la base de datos";
		if($_GET['accion'] == 'Activar')
        {
        	echo '<script>alert("Las Obras seleccionadas han sido Activadas correctamente"); window.location.href="obras.php";</script>';
        }
    	elseif($_GET['accion'] == 'Inactivar')
    	{
    		echo '<script>alert("Las Obras seleccionadas han sido Inactivadas correctamente"); window.location.href="obras.php";</script>';
    	}
    	elseif($_GET['accion'] == 'Eliminar')
    	{
    		if($men != '')
    		{
    			echo '<script>alert("No es posible eliminar las obras seleccionadas porque ya tienen movimientos"); window.location.href="obras.php";</script>';
    		}
    		else
    		{
    			echo '<script>alert("Las Obras seleccionadas han sido Eliminados correctamente"); window.location.href="obras.php";</script>';
    		}
    	}
}else{
	echo '<script>alert("No se enviaron registros para eliminar"); window.location.href="obras.php";</script>';
}

?>