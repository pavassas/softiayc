<?php
	include('../../data/Conexion.php');
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_SESSION['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
		
	// verifica si no se ha loggeado
	if(!isset($_SESSION["persona"]))
	{
	  session_destroy();
	  header("LOCATION:index.php");
	}else{
	}
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");
	
	$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
	$dato = mysqli_fetch_array($con);
	$perfil = $dato['prf_descripcion'];
	$claveperfil = $dato['prf_clave_int'];
	$claveusuario = $dato['usu_clave_int'];
	$ultimaobra = $dato['obr_clave_int'];
	
	$con = mysqli_query($conectar,"select per_metodo from permiso where prf_clave_int = '".$claveperfil."' and ven_clave_int = 4");
	$dato = mysqli_fetch_array($con);
	$metodo = $dato['per_metodo'];
	
	if($_GET['editarciu'] == 'si')
	{
		$ciuedi = $_GET['ciuedi'];
		$con = mysqli_query($conectar,"select * from tipo_informe where tii_clave_int = '".$ciuedi."'"); 
		$dato = mysqli_fetch_array($con); 
		$ciu = $dato['tii_nombre'];
		$act = $dato['tii_sw_activo'];
		
		$png = $dato['tii_png'];
		$jpg = $dato['tii_jpg'];
		$jpeg = $dato['tii_jpeg'];
		$gif = $dato['tii_gif'];
		
		$dwg = $dato['tii_dwg'];
		$mpp = $dato['tii_mpp'];
		$tif = $dato['tii_tif'];
		
		$pdf = $dato['tii_pdf'];
		$docx = $dato['tii_docx'];
		$xls = $dato['tii_xls'];
		$xlsx = $dato['tii_xlsx'];
?>
		<table style="width: 38%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Tipo informe:</td>
			<td class="auto-style3">
			<input name="ciudad1" id="ciudad1" value="<?php echo $ciu; ?>" class="inputs" type="text" style="width: 200px" />&nbsp;
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3" colspan="2">
			<table style="width: 100%">
				<tr>
					<td style="width: 10px">
					<input name="png1" id="png1" <?php if($png == 1){ echo 'checked="checked"'; } ?> type="checkbox">
					</td>
					<td><label for="png1">PNG</label></td>
					<td style="width: 10px">
					<input name="jpg1" id="jpg1" <?php if($jpg == 1){ echo 'checked="checked"'; } ?> type="checkbox"></td>
					<td><label for="jpg1">JPG</label></td>
					<td style="width: 10px">
					<input name="jpeg1" id="jpeg1" <?php if($jpeg == 1){ echo 'checked="checked"'; } ?> type="checkbox"></td>
					<td><label for="jpeg1">JPEG</label></td>
				</tr>
				<tr>
					<td style="width: 10px">
					<input name="gif1" id="gif1" <?php if($gif == 1){ echo 'checked="checked"'; } ?> type="checkbox"></td>
					<td><label for="gif1">GIF</label></td>
					<td style="width: 10px">
					<input name="dwg1" id="dwg1" <?php if($dwg == 1){ echo 'checked="checked"'; } ?> type="checkbox">
					</td>
					<td><label for="dwg1">DWG</label></td>
					<td style="width: 10px">
					<input name="mpp1" id="mpp1" <?php if($mpp == 1){ echo 'checked="checked"'; } ?> type="checkbox">
					</td>
					<td><label for="mpp1">MPP</label></td>
				</tr>
				<tr>
					<td style="width: 10px">
					<input name="tif1" id="tif1" <?php if($tif == 1){ echo 'checked="checked"'; } ?> type="checkbox">
					</td>
					<td><label for="tif1">TIF</label></td>
					<td style="width: 10px">
					<input name="pdf1" id="pdf1" <?php if($pdf == 1){ echo 'checked="checked"'; } ?> type="checkbox">
					</td>
					<td><label for="pdf1">PDF</label></td>
					<td style="width: 10px">
					<input name="docx1" id="docx1" <?php if($docx == 1){ echo 'checked="checked"'; } ?> type="checkbox">
					</td>
					<td><label for="docx1">DOCX</label></td>
				</tr>
				<tr>
					<td style="width: 10px">
					<input name="xls1" id="xls1" <?php if($xls == 1){ echo 'checked="checked"'; } ?> type="checkbox"></td>
					<td><label for="xls1">XLS</label></td>
					<td style="width: 10px">
					<input name="xlsx1" id="xlsx1" <?php if($xlsx == 1){ echo 'checked="checked"'; } ?> type="checkbox"></td>
					<td><label for="xlsx1">XLSX</label></td>
					<td style="width: 10px">
					</td>
					<td>&nbsp;</td>
				</tr>
				</table>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3" colspan="2">Activa:<input class="inputs" <?php if($act == 1){ echo 'checked="checked"'; } ?> name="activo1" type="checkbox" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4">
			<input name="submit" type="button" value="Guardar" onclick="GUARDAR('CIUDAD','<?php echo $ciuedi; ?>')"  style="width: 348px; height: 25px; cursor:pointer" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">
			<div id="datos">
			</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
<?php
		exit();
	}
	
	if($_GET['guardarciu'] == 'si')
	{
		sleep(1);
		$ciu = $_GET['ciu'];
		$act = $_GET['act'];
		$lc = $_GET['lc'];
		
		$png = $_GET['png'];
		$jpg = $_GET['jpg'];
		$jpeg = $_GET['jpeg'];
		$gif = $_GET['gif'];
		$dwg = $_GET['dwg'];
		$mpp = $_GET['mpp'];
		$tif = $_GET['tif'];
		$pdf = $_GET['pdf'];
		$docx = $_GET['docx'];
		$xls = $_GET['xls'];
		$xlsx = $_GET['xlsx'];
		
		$c = $_GET['c'];
		
		$fecha=date("Y/m/d H:i:s");
		
		$sql = mysqli_query($conectar,"select * from tipo_informe where (UPPER(tii_nombre) = UPPER('".$ciu."')) AND tii_clave_int <> '".$c."'");
		$dato = mysqli_fetch_array($sql);
		$conciu = $dato['tii_nombre'];
		
		if($ciu == '')
		{
			echo "<div class='validaciones'>Debe ingresar la Ciudad</div>";
		}
		else
		if(STRTOUPPER($conciu) == STRTOUPPER($ciu))
		{
			echo "<div class='validaciones'>El tipo de informe ingresado ya existe</div>";
		}
		else
		if($lc < 3)
		{
			echo "<div class='validaciones'>La Ciudad debe ser mí­nimo de 3 dijitos</div>";
		}
		else
		{			
			if($act == 'false'){ $swact = 0; }elseif($act == 'true'){ $swact = 1; }
			
			if($png == 'false'){ $png = 0; }elseif($png == 'true'){ $png = 1; }
			if($jpg == 'false'){ $jpg = 0; }elseif($jpg == 'true'){ $jpg = 1; }
			if($jpeg == 'false'){ $jpeg = 0; }elseif($jpeg == 'true'){ $jpeg = 1; }
			if($gif == 'false'){ $gif = 0; }elseif($gif == 'true'){ $gif = 1; }
			if($dwg == 'false'){ $dwg = 0; }elseif($dwg == 'true'){ $dwg = 1; }
			if($mpp == 'false'){ $mpp = 0; }elseif($mpp == 'true'){ $mpp = 1; }
			if($tif == 'false'){ $tif = 0; }elseif($tif == 'true'){ $tif = 1; }
			if($pdf == 'false'){ $pdf = 0; }elseif($pdf == 'true'){ $pdf = 1; }
			if($docx == 'false'){ $docx = 0; }elseif($docx == 'true'){ $docx = 1; }
			if($xls == 'false'){ $xls = 0; }elseif($xls == 'true'){ $xls = 1; }
			if($xlsx == 'false'){ $xlsx = 0; }elseif($xlsx == 'true'){ $xlsx = 1; }
			
			$con = mysqli_query($conectar,"update tipo_informe set tii_nombre = '".$ciu."', tii_sw_activo = '".$swact."', tii_usu_actualiz = '".$usuario."', tii_fec_actualiz = '".$fecha."',tii_png = '".$png."',tii_jpg = '".$jpg."',tii_jpeg = '".$jpeg."',tii_gif = '".$gif."',tii_dwg = '".$dwg."',tii_mpp = '".$mpp."',tii_tif = '".$tif."',tii_pdf = '".$pdf."',tii_docx = '".$docx."',tii_xls = '".$xls."',tii_xlsx = '".$xlsx."' where tii_clave_int = '".$c."'");
			
			if($con >= 1)
			{
				mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_encabezado,obr_clave_int,loa_usu_actualiz,loa_fec_actualiz) values(null,18,58,'".$c."','".$ultimaobra."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 58=Actualización ciudad
				echo "<div class='ok'>Datos grabados correctamente</div>";
			}
			else
			{
				echo "<div class='validaciones'>No se han podido guardar los datos</div>";
			}	
		}
		exit();
	}
	
	if($_GET['nuevaciudad'] == 'si')
	{
		sleep(1);
		$ciu = $_GET['ciu'];
		$act = $_GET['act'];
		$lc = $_GET['lc'];
		
		$png = $_GET['png'];
		$jpg = $_GET['jpg'];
		$jpeg = $_GET['jpeg'];
		$gif = $_GET['gif'];
		
		$dwg = $_GET['dwg'];
		$mpp = $_GET['mpp'];
		$tif = $_GET['tif'];
		
		$pdf = $_GET['pdf'];
		$docx = $_GET['docx'];
		$xls = $_GET['xls'];
		$xlsx = $_GET['xlsx'];
		
		$fecha=date("Y/m/d H:i:s");
		
		$sql = mysqli_query($conectar,"select * from tipo_informe where (UPPER(tii_nombre) = UPPER('".$ciu."'))");
		$dato = mysqli_fetch_array($sql);
		$conciu = $dato['tii_nombre'];
		
		if($ciu == '')
		{
			echo "<div class='validaciones'>Debe ingresar la ciudad</div>";
		}
		else
		if(STRTOUPPER($conciu) == STRTOUPPER($ciu))
		{
			echo "<div class='validaciones'>El tipo de informe ingresado ya existe</div>";
		}
		else
		if($lc < 3)
		{
			echo "<div class='validaciones'>La ciudad debe ser mí­nimo de 3 dijitos</div>";
		}
		else
		{
			if($act == 'false'){ $swact = 0; }elseif($act == 'true'){ $swact = 1; }
			
			if($png == 'false'){ $png = 0; }elseif($png == 'true'){ $png = 1; }
			if($jpg == 'false'){ $jpg = 0; }elseif($jpg == 'true'){ $jpg = 1; }
			if($jpeg == 'false'){ $jpeg = 0; }elseif($jpeg == 'true'){ $jpeg = 1; }
			if($gif == 'false'){ $gif = 0; }elseif($gif == 'true'){ $gif = 1; }
			
			if($dwg == 'false'){ $dwg = 0; }elseif($dwg == 'true'){ $dwg = 1; }
			if($mpp == 'false'){ $mpp = 0; }elseif($mpp == 'true'){ $mpp = 1; }
			if($tif == 'false'){ $tif = 0; }elseif($tif == 'true'){ $tif = 1; }
			
			if($pdf == 'false'){ $pdf = 0; }elseif($pdf == 'true'){ $pdf = 1; }
			if($docx == 'false'){ $docx = 0; }elseif($docx == 'true'){ $docx = 1; }
			if($xls == 'false'){ $xls = 0; }elseif($xls == 'true'){ $xls = 1; }
			if($xlsx == 'false'){ $xlsx = 0; }elseif($xlsx == 'true'){ $xlsx = 1; }
			
			$con = mysqli_query($conectar,"insert into tipo_informe(tii_nombre,tii_sw_activo,tii_png,tii_jpg,tii_jpeg,tii_gif,tii_dwg,tii_mpp,tii_tif,tii_pdf,tii_docx,tii_xls,tii_xlsx,tii_usu_actualiz,tii_fec_actualiz) values('".$ciu."','".$swact."','".$png."','".$jpg."','".$jpeg."','".$gif."','".$dwg."','".$mpp."','".$tif."','".$pdf."','".$docx."','".$xls."','".$xlsx."','".$usuario."','".$fecha."')");				
			
			if($con >= 1)
			{
				$con = mysqli_query($conectar,"select tii_clave_int from tipo_informe where UPPER(tii_nombre) = UPPER('".$ciu."')");
				$dato = mysqli_fetch_array($con);
				$claciu = $dato['tii_clave_int'];
				mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_encabezado,obr_clave_int,loa_usu_actualiz,loa_fec_actualiz) values(null,18,57,'".$claciu."','".$ultimaobra."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 57=Creación ciudad
				echo "<div class='ok'>Datos grabados correctamente</div>";
			}
			else
			{
				echo "<div class='validaciones'>No se han podido guardar los datos</div>";
			}
		}
		exit();
	}
	if($_GET['todos'] == 'si')
	{
		sleep(1);
		$rows=mysqli_query($conectar,"select * from tipo_informe");
		$total=mysqli_num_rows($rows);
?>
		<table style="width: 100%;border-collapse:collapse">
		<tr>
			<td class="auto-style3" style="width: 27px">
				<input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<?php echo $total;?>,this);" class="auto-style6" /><span class="auto-style6">
				</span>
			</td>
			<td class="auto-style3" colspan="16">
			<?php 
			if($metodo == 1)
			{
			?>
			<table style="width: 30%">
				<tr>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../images/activo.png" alt="" class="auto-style6" /><input type="submit" value="Activar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../images/inactivo.png" alt="" class="auto-style6" /><input type="submit" value="Inactivar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../images/eliminar.png" alt="" class="auto-style6" /><input type="submit" value="Eliminar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
				</tr>
			</table>
			<?php
			}
			?>
			</td>
		</tr>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px"><strong>Tipo informe</strong></td>
			<td class="auto-style8" style="width: 45px"><strong>PNG</strong></td>
			<td class="auto-style8" style="width: 45px"><strong>JPG</strong></td>
			<td class="auto-style8" style="width: 45px"><strong>JPEG</strong></td>
			<td class="auto-style8" style="width: 45px"><strong>GIF</strong></td>
			<td class="auto-style8" style="width: 45px"><strong>DWG</strong></td>
			<td class="auto-style8" style="width: 45px"><strong>MPP</strong></td>
			<td class="auto-style8" style="width: 45px"><strong>TIF</strong></td>
			<td class="auto-style8" style="width: 45px"><strong>PDF</strong></td>
			<td class="auto-style8" style="width: 45px"><strong>DOCX</strong></td>
			<td class="auto-style8" style="width: 45px"><strong>XLS</strong></td>
			<td class="auto-style8" style="width: 45px"><strong>XLSX</strong></td>
			<td class="auto-style5" style="width: 98px"><strong>Usuario</strong></td>
			<td class="auto-style5" style="width: 127px"><strong>
				Actualización</strong></td>
			<td class="auto-style5" style="width: 29px"><strong>
			Activo</strong></td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
		<?php
			$contador=0;
			$con = mysqli_query($conectar,"select * from tipo_informe order by tii_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$ciu = $dato['tii_nombre'];
				$act = $dato['tii_sw_activo'];
				
				$png = $dato['tii_png'];
				$jpg = $dato['tii_jpg'];
				$jpeg = $dato['tii_jpeg'];
				$gif = $dato['tii_gif'];
				$dwg = $dato['tii_dwg'];
				$mpp = $dato['tii_mpp'];
				$tif = $dato['tii_tif'];
				$pdf = $dato['tii_pdf'];
				$docx = $dato['tii_docx'];
				$xls = $dato['tii_xls'];
				$xlsx = $dato['tii_xlsx'];
					
				$usuact = $dato['tii_usu_actualiz'];
				$fecact = $dato['tii_fec_actualiz'];
				$contador=$contador+1;
				if($i == 0)
				{
		?>
		<tr>
			<td class="auto-style5" colspan="17"><hr></td>
		</tr>
		<?php
				}
		?>
		<tr style="<?php if($i % 2 == 0){ echo 'background-color:#8596B2"'; } ?>">
			<td class="auto-style3" style="width: 27px">
				<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<?php echo $contador;?>" value="<?php echo $dato['tii_clave_int'];?>" class="auto-style6" /></td>
			<td class="auto-style5" style="width: 200px"><?php echo $ciu; ?></td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox17" disabled="disabled" <?php if($png == 1){ echo 'checked="checked"'; } ?> type="checkbox"></td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox18" disabled="disabled" <?php if($jpg == 1){ echo 'checked="checked"'; } ?> type="checkbox"></td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox19" disabled="disabled" <?php if($jpeg == 1){ echo 'checked="checked"'; } ?> type="checkbox"></td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox20" disabled="disabled" <?php if($gif == 1){ echo 'checked="checked"'; } ?> type="checkbox"></td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox4" disabled="disabled" <?php if($dwg == 1){ echo 'checked="checked"'; } ?> type="checkbox">
				</td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox4" disabled="disabled" <?php if($mpp == 1){ echo 'checked="checked"'; } ?> type="checkbox">
				</td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox4" disabled="disabled" <?php if($tif == 1){ echo 'checked="checked"'; } ?> type="checkbox">
				</td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox21" disabled="disabled" <?php if($pdf == 1){ echo 'checked="checked"'; } ?> type="checkbox"></td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox22" disabled="disabled" <?php if($docx == 1){ echo 'checked="checked"'; } ?> type="checkbox"></td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox23" disabled="disabled" <?php if($xls == 1){ echo 'checked="checked"'; } ?> type="checkbox"></td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox24" disabled="disabled" <?php if($xlsx == 1){ echo 'checked="checked"'; } ?> type="checkbox"></td>
			<td class="auto-style5" style="width: 98px"><?php echo $usuact; ?></td>
			<td class="auto-style5" style="width: 127px"><?php echo $fecact; ?></td>
			<td class="auto-style1" style="width: 30px">
			<input name="activarinactivar" id="activarinactivar" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" class="auto-style6" ></td>
			<td class="auto-style5">
			<?php 
			if($metodo == 1)
			{
			?>
			<a data-reveal-id="editarciudad" data-animation="fade" style="cursor:pointer" onclick="EDITAR('<?php echo $dato['tii_clave_int']; ?>','CIUDAD')"><img src="../../images/editar.png" alt="" height="22" width="21" /></a>
			<?php
			}
			?>
			</td>
		</tr>
		<?php
			}
		?>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 45px">&nbsp;</td>
			<td class="auto-style5" style="width: 45px">&nbsp;</td>
			<td class="auto-style5" style="width: 45px">&nbsp;</td>
			<td class="auto-style5" style="width: 45px">&nbsp;</td>
			<td class="auto-style5" style="width: 45px">&nbsp;</td>
			<td class="auto-style5" style="width: 45px">&nbsp;</td>
			<td class="auto-style5" style="width: 45px">&nbsp;</td>
			<td class="auto-style5" style="width: 45px">&nbsp;</td>
			<td class="auto-style5" style="width: 45px">&nbsp;</td>
			<td class="auto-style5" style="width: 45px">&nbsp;</td>
			<td class="auto-style5" style="width: 45px">&nbsp;</td>
			<td class="auto-style5" style="width: 98px">&nbsp;</td>
			<td class="auto-style5" style="width: 127px">&nbsp;</td>
			<td class="auto-style5" style="width: 29px">&nbsp;</td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
	</table>
<?php
		exit();
	}
?>
<?php
	if($_GET['buscarciu'] == 'si')
	{
		$ciu = $_GET['ciu'];
		$act = $_GET['act'];

		$rows=mysqli_query($conectar,"select * from tipo_informe where (tii_nombre LIKE REPLACE('%".$ciu."%',' ','%') OR '".$ciu."' IS NULL OR '".$ciu."' = '') and (tii_sw_activo = '".$act."' OR '".$act."' IS NULL OR '".$act."' = '')");
		$total=mysqli_num_rows($rows);
?>
		<table style="width: 100%;border-collapse:collapse">
		<tr>
			<td class="auto-style3" style="width: 27px">
				<input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<?php echo $total;?>,this);" class="auto-style6" /><span class="auto-style6">
				</span>
			</td>
			<td class="auto-style3" colspan="16">
			<?php 
			if($metodo == 1)
			{
			?>
			<table style="width: 30%">
				<tr>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../images/activo.png" alt="" class="auto-style6" /><input type="submit" value="Activar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../images/inactivo.png" alt="" class="auto-style6" /><input type="submit" value="Inactivar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../images/eliminar.png" alt="" class="auto-style6" /><input type="submit" value="Eliminar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
				</tr>
			</table>
			<?php
			}
			?>
			</td>
		</tr>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px"><strong>Tipo informe</strong></td>
			<td class="auto-style8" style="width: 45px"><strong>PNG</strong></td>
			<td class="auto-style8" style="width: 45px"><strong>JPG</strong></td>
			<td class="auto-style8" style="width: 45px"><strong>JPEG</strong></td>
			<td class="auto-style8" style="width: 45px"><strong>GIF</strong></td>
			<td class="auto-style8" style="width: 45px"><strong>DWG</strong></td>
			<td class="auto-style8" style="width: 45px"><strong>MPP</strong></td>
			<td class="auto-style8" style="width: 45px"><strong>TIF</strong></td>
			<td class="auto-style8" style="width: 45px"><strong>PDF</strong></td>
			<td class="auto-style8" style="width: 45px"><strong>DOCX</strong></td>
			<td class="auto-style8" style="width: 45px"><strong>XLS</strong></td>
			<td class="auto-style8" style="width: 45px"><strong>XLSX</strong></td>
			<td class="auto-style5" style="width: 98px"><strong>Usuario</strong></td>
			<td class="auto-style5" style="width: 127px"><strong>
				Actualización</strong></td>
			<td class="auto-style5" style="width: 29px"><strong>
			Activo</strong></td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
		<?php
			$contador=0;
			$con = mysqli_query($conectar,"select * from tipo_informe where (tii_nombre LIKE REPLACE('%".$ciu."%',' ','%') OR '".$ciu."' IS NULL OR '".$ciu."' = '') and (tii_sw_activo = '".$act."' OR '".$act."' IS NULL OR '".$act."' = '') order by tii_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$ciu = $dato['tii_nombre'];
				$act = $dato['tii_sw_activo'];
				
				$png = $dato['tii_png'];
				$jpg = $dato['tii_jpg'];
				$jpeg = $dato['tii_jpeg'];
				$gif = $dato['tii_gif'];
				$dwg = $dato['tii_dwg'];
				$mpp = $dato['tii_mpp'];
				$tif = $dato['tii_tif'];
				$pdf = $dato['tii_pdf'];
				$docx = $dato['tii_docx'];
				$xls = $dato['tii_xls'];
				$xlsx = $dato['tii_xlsx'];
					
				$usuact = $dato['tii_usu_actualiz'];
				$fecact = $dato['tii_fec_actualiz'];
				$contador=$contador+1;
				if($i == 0)
				{
		?>
		<tr>
			<td class="auto-style5" colspan="17"><hr></td>
		</tr>
		<?php
				}
		?>
		<tr style="<?php if($i % 2 == 0){ echo 'background-color:#8596B2"'; } ?>">
			<td class="auto-style3" style="width: 27px">
				<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<?php echo $contador;?>" value="<?php echo $dato['tii_clave_int'];?>" class="auto-style6" /></td>
			<td class="auto-style5" style="width: 200px"><?php echo $ciu; ?></td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox9" disabled="disabled" <?php if($png == 1){ echo 'checked="checked"'; } ?> type="checkbox"></td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox10" disabled="disabled" <?php if($jpg == 1){ echo 'checked="checked"'; } ?> type="checkbox"></td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox11" disabled="disabled" <?php if($jpeg == 1){ echo 'checked="checked"'; } ?> type="checkbox"></td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox12" disabled="disabled" <?php if($gif == 1){ echo 'checked="checked"'; } ?> type="checkbox"></td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox4" disabled="disabled" <?php if($dwg == 1){ echo 'checked="checked"'; } ?> type="checkbox">
				</td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox4" disabled="disabled" <?php if($mpp == 1){ echo 'checked="checked"'; } ?> type="checkbox">
				</td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox4" disabled="disabled" <?php if($tif == 1){ echo 'checked="checked"'; } ?> type="checkbox">
				</td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox13" disabled="disabled" <?php if($pdf == 1){ echo 'checked="checked"'; } ?> type="checkbox"></td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox14" disabled="disabled" <?php if($docx == 1){ echo 'checked="checked"'; } ?> type="checkbox"></td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox15" disabled="disabled" <?php if($xls == 1){ echo 'checked="checked"'; } ?> type="checkbox"></td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox16" disabled="disabled" <?php if($xlsx == 1){ echo 'checked="checked"'; } ?> type="checkbox"></td>
			<td class="auto-style5" style="width: 98px"><?php echo $usuact; ?></td>
			<td class="auto-style5" style="width: 127px"><?php echo $fecact; ?></td>
			<td class="auto-style1" style="width: 30px">
			<input name="activarinactivar" id="activarinactivar" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" class="auto-style6" ></td>
			<td class="auto-style5">
			<?php 
			if($metodo == 1)
			{
			?>
			<a data-reveal-id="editarciudad" data-animation="fade" style="cursor:pointer" onclick="EDITAR('<?php echo $dato['tii_clave_int']; ?>','CIUDAD')"><img src="../../images/editar.png" alt="" height="22" width="21" /></a>
			<?php
			}
			?>
			</td>
		</tr>
		<?php
			}
		?>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 45px">&nbsp;</td>
			<td class="auto-style5" style="width: 45px">&nbsp;</td>
			<td class="auto-style5" style="width: 45px">&nbsp;</td>
			<td class="auto-style5" style="width: 45px">&nbsp;</td>
			<td class="auto-style5" style="width: 45px">&nbsp;</td>
			<td class="auto-style5" style="width: 45px">&nbsp;</td>
			<td class="auto-style5" style="width: 45px">&nbsp;</td>
			<td class="auto-style5" style="width: 45px">&nbsp;</td>
			<td class="auto-style5" style="width: 45px">&nbsp;</td>
			<td class="auto-style5" style="width: 45px">&nbsp;</td>
			<td class="auto-style5" style="width: 45px">&nbsp;</td>
			<td class="auto-style5" style="width: 98px">&nbsp;</td>
			<td class="auto-style5" style="width: 127px">&nbsp;</td>
			<td class="auto-style5" style="width: 29px">&nbsp;</td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
	</table>
<?php
		exit();
	}
?>
<!DOCTYPE HTML>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

	
<title>CONTROL DE OBRAS</title>
<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
<!--[if lte IE 7]>
<link rel="stylesheet" href="css/ie.c32bc18afe0e2883ee4912a51f86c119.css" type="text/css" />
<![endif]-->

<style type="text/css">
.auto-style1 {
	text-align: center;
}
.auto-style2 {
	font-size: 12px;
	font-family: Arial, helvetica;
	outline: none;
/*transition: all 0.75s ease-in-out;*/ /*-webkit-transition: all 0.75s ease-in-out;*/ /*-moz-transition: all 0.75s ease-in-out;*/border-radius: 3px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 3px;
	border: 1px solid rgba(0,0,0, 0.2);
	background-color: #eee;
	padding: 3px;
	box-shadow: 0 0 10px #aaa;
	-webkit-box-shadow: 0 0 10px #aaa;
	-moz-box-shadow: 0 0 10px #aaa;
	border: 1px solid #999;
	background-color: white;
	text-align: center;
}
.auto-style3 {
	text-align: left;
}
.inputs{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.5);
    padding: 3px;
}
.validaciones{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.2);
    color:maroon;
    background-color:#eee;
    padding: 3px;
    text-align:center;
}
.ok{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.2);
    color:green;
    background-color:#eee;
    padding: 3px;
    text-align:center;
}
.auto-style4 {
	visibility: hidden;
	top: 100px;
	left: 50%;
	margin-left: -300px;
	width: 520px;
	background: #eee url('../../css/terminos/modal-gloss.png') no-repeat -200px -80px;
	position: absolute;
	z-index: 101;
	padding: 30px 40px 34px;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	border-radius: 5px;
	-moz-box-shadow: 0 0 10px rgba(0,0,0,.4);
	text-align: left;
}
.auto-style5 {
	text-align: left;
	font-family: Arial, Helvetica, sans-serif;
}
.auto-style6 {
	font-family: Arial, Helvetica, sans-serif;
}
.auto-style7 {
	text-align: center;
	font-size: large;
}
.auto-style8 {
	text-align: center;
	font-family: Arial, Helvetica, sans-serif;
}
</style>

<script type="text/javascript" language="javascript">
	selecteds=0;
	
	function CheckUncheck(total,check){
		checkbox=null;
		for(i=1;i<=total;i++){
			checkbox=document.getElementById("idcat"+i);
			//alert(checkbox.value);
			checkbox.checked=check.checked;
		}
		
		if(check.checked){
			selecteds=total;
		}else{
			selecteds=0;
		}
		
	}
	
	function contadorVals(check){
		if(check.checked){
			selecteds=selecteds+1;
		}else{
			selecteds=selecteds-1;
		}
	}
	
	function selectedVals(){
		if(selecteds==0){
			alert("Seleccione al menos un registro.");
			return false;
		}else{
			return true;
		}
	}
</script>
<?php //VALIDACIONES ?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="llamadas.js"></script>

<?php //VENTANA EMERGENTE ?>
<link rel="stylesheet" href="../../css/reveal.css" />
<script type="text/javascript" src="../../js/jquery.reveal.js"></script>
<script type="text/javascript" language="javascript">
function OCULTARSCROLL()
{
	setTimeout("parent.autoResize('iframe8')",500);
	setTimeout("parent.autoResize('iframe8')",1000);
	setTimeout("parent.autoResize('iframe8')",2000);
	setTimeout("parent.autoResize('iframe8')",3000);
	setTimeout("parent.autoResize('iframe8')",4000);
	setTimeout("parent.autoResize('iframe8')",5000);
	setTimeout("parent.autoResize('iframe8')",6000);
	setTimeout("parent.autoResize('iframe8')",7000);
	setTimeout("parent.autoResize('iframe8')",8000);
	setTimeout("parent.autoResize('iframe8')",9000);
	setTimeout("parent.autoResize('iframe8')",10000);
	setTimeout("parent.autoResize('iframe8')",11000);
	setTimeout("parent.autoResize('iframe8')",12000);
	setTimeout("parent.autoResize('iframe8')",13000);
	setTimeout("parent.autoResize('iframe8')",14000);
	setTimeout("parent.autoResize('iframe8')",15000);
	setTimeout("parent.autoResize('iframe8')",16000);
	setTimeout("parent.autoResize('iframe8')",17000);
	setTimeout("parent.autoResize('iframe8')",18000);
	setTimeout("parent.autoResize('iframe8')",19000);
	setTimeout("parent.autoResize('iframe8')",20000);
}
setTimeout("parent.autoResize('iframe8')",500);
setTimeout("parent.autoResize('iframe8')",1000);
setTimeout("parent.autoResize('iframe8')",2000);
setTimeout("parent.autoResize('iframe8')",3000);
setTimeout("parent.autoResize('iframe8')",4000);
setTimeout("parent.autoResize('iframe8')",5000);
setTimeout("parent.autoResize('iframe8')",6000);
setTimeout("parent.autoResize('iframe8')",7000);
setTimeout("parent.autoResize('iframe8')",8000);
setTimeout("parent.autoResize('iframe8')",9000);
setTimeout("parent.autoResize('iframe8')",10000);
setTimeout("parent.autoResize('iframe8')",11000);
setTimeout("parent.autoResize('iframe8')",12000);
setTimeout("parent.autoResize('iframe8')",13000);
setTimeout("parent.autoResize('iframe8')",14000);
setTimeout("parent.autoResize('iframe8')",15000);
setTimeout("parent.autoResize('iframe8')",16000);
setTimeout("parent.autoResize('iframe8')",17000);
setTimeout("parent.autoResize('iframe8')",18000);
setTimeout("parent.autoResize('iframe8')",19000);
setTimeout("parent.autoResize('iframe8')",20000);
</script>

</head>
<body>
<?php
$rows=mysqli_query($conectar,"select * from tipo_informe");
$total=mysqli_num_rows($rows);
?>
<form name="form1" id="form1" action="confirmar.php" method="post" onsubmit="return selectedVals();">
<!--[if lte IE 7]>
<div class="ieWarning">Este navegador no es compatible con el sistema. Por favor, use Chrome, Safari, Firefox o Internet Explorer 8 o superior.</div>
<![endif]-->
<table style="width: 100%">
	<tr>
		<td class="auto-style2" onclick="CONSULTAMODULO('TODOS')" onmouseover="this.style.backgroundColor='#092451';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="width: 60px; cursor:pointer">
		Todos
		<?php
			$con = mysqli_query($conectar,"select COUNT(*) cant from tipo_informe");
			$dato = mysqli_fetch_array($con);
			echo $dato['cant'];
		?>
		</td>
		<td class="auto-style7" style="cursor:pointer" colspan="4">
		MAESTRA TIPO INFORME</td>
		<td class="auto-style2" onmouseover="this.style.backgroundColor='#CCCCCC';this.style.color='#0F213C';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="width: 55px; cursor:pointer">
		<a data-reveal-id="nuevaciudad" data-animation="fade" style="cursor:pointer">
		<table style="width: 100%; display:none">
			<tr>
				<td style="width: 14px">
				<?php 
				if($metodo == 1)
				{
				?>
				<a data-reveal-id="nuevaciudad" data-animation="fade" style="cursor:pointer">
				<img alt="" src="../../images/add2.png"></a>
				<?php
				}
				?>
				</td>
				<td>Añadir</td>
			</tr>
		</table></a>
		</td>
	</tr>
	<tr>
		<td class="auto-style2" colspan="6">
		<div id="filtro">
			<table style="width: 33%">
				<tr>
					<td class="auto-style1"><strong>Filtro:<img src="../../images/buscar.png" alt="" height="18" width="15" /></strong></td>
					<td class="auto-style1">
			<input class="inputs" onkeyup="BUSCAR('CIUDAD')" name="ciudad2" maxlength="70" type="text" placeholder="Tipo Informe" style="width: 150px" /></td>
					<td class="auto-style1">
					<strong>Activo:</strong></td>
					<td class="auto-style1">
					<select name="buscaractivos" class="inputs" onchange="BUSCAR('CIUDAD')">
					<option value="">Todos</option>
					<option value="1">Activos</option>
					<option value="0">Inactivo</option>
					</select></td>
				</tr>
			</table>
		</div>
		</td>
	</tr>
	<tr>
		<td colspan="6" class="auto-style2">
		<div id="editarciudad" class="reveal-modal" style="left: 57%; top: 10px; height: 180px; width: 350px;">
			<table style="width: 38%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Nombre:</td>
			<td class="auto-style3"><input class="inputs" name="nombre1" maxlength="50" value="<?php echo $nom; ?>" type="text" style="width: 200px" />
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Activo:</td>
			<td class="auto-style3"><input class="inputs" <?php if($act == 1){ echo 'checked="checked"'; } ?> name="activo1" type="checkbox" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4">
			<input name="submit" type="button" value="Guardar" onclick="GUARDAR('CIUDAD','<?php echo $ubiedi; ?>')"  style="width: 348px; height: 25px; cursor:pointer" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">
			<div id="datos">
			</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
			<a class="close-reveal-modal">&#215;</a>
		</div>
		<div id="ciudades">
		<table style="width: 100%;border-collapse:collapse">
			<tr>
				<td class="auto-style3" style="width: 27px">
					<input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<?php echo $total;?>,this);" />
				</td>
				<td class="auto-style3" colspan="16">
				<?php 
				if($metodo == 1)
				{
				?>
				<table style="width: 30%">
					<tr>
						<td class="auto-style1"><p style="cursor:pointer"><img src="../../images/activo.png" alt="" /><input type="submit" value="Activar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" /></p></td>
						<td class="auto-style1"><p style="cursor:pointer"><img src="../../images/inactivo.png" alt="" /><input type="submit" value="Inactivar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" /></p></td>
						<td class="auto-style1"><p style="cursor:pointer"><img src="../../images/eliminar.png" alt="" /><input type="submit" value="Eliminar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" /></p></td>
					</tr>
				</table>
				<?php
				}
				?>
				</td>
			</tr>
			<tr>
				<td class="auto-style3" style="width: 27px">&nbsp;</td>
				<td class="auto-style3" style="width: 200px" ><strong>Tipo informe</strong></td>
				<td class="auto-style1" style="width: 45px"><strong>PNG</strong></td>
				<td class="auto-style1" style="width: 45px"><strong>JPG</strong></td>
				<td class="auto-style1" style="width: 45px"><strong>JPEG</strong></td>
				<td class="auto-style1" style="width: 45px"><strong>GIF</strong></td>
				<td class="auto-style1" style="width: 45px"><strong>DWG</strong></td>
				<td class="auto-style1" style="width: 45px"><strong>MPP</strong></td>
				<td class="auto-style1" style="width: 45px"><strong>TIF</strong></td>
				<td class="auto-style1" style="width: 45px"><strong>PDF</strong></td>
				<td class="auto-style1" style="width: 45px"><strong>DOCX</strong></td>
				<td class="auto-style1" style="width: 45px"><strong>XLS</strong></td>
				<td class="auto-style1" style="width: 45px"><strong>XLSX</strong></td>
				<td class="auto-style3" style="width: 98px"><strong>Usuario</strong></td>
				<td class="auto-style3" style="width: 127px"><strong>
				Actualización</strong></td>
				<td class="auto-style3" style="width: 29px"><strong>
				Activo</strong></td>
				<td class="auto-style3">&nbsp;</td>
			</tr>
			<?php
				$contador=0;
				$con = mysqli_query($conectar,"select * from tipo_informe order by tii_nombre");
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$claciu = $dato['tii_clave_int'];
					$ciu = $dato['tii_nombre'];
					$act = $dato['tii_sw_activo'];
					
					$png = $dato['tii_png'];
					$jpg = $dato['tii_jpg'];
					$jpeg = $dato['tii_jpeg'];
					$gif = $dato['tii_gif'];
					$dwg = $dato['tii_dwg'];
					$mpp = $dato['tii_mpp'];
					$tif = $dato['tii_tif'];
					$pdf = $dato['tii_pdf'];
					$docx = $dato['tii_docx'];
					$xls = $dato['tii_xls'];
					$xlsx = $dato['tii_xlsx'];
					
					$usuact = $dato['tii_usu_actualiz'];
					$fecact = $dato['tii_fec_actualiz'];
					$contador=$contador+1;
					if($i == 0)
					{
			?>
			<tr>
				<td class="auto-style3" colspan="17"><hr></td>
			</tr>
			<?php
					}
			?>
			<tr style="<?php if($i % 2 == 0){ echo 'background-color:#8596B2"'; } ?>">
				<td class="auto-style3" style="width: 27px">
					<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<?php echo $contador;?>" value="<?php echo $dato['tii_clave_int'];?>" /></td>
				<td class="auto-style3" style="width: 200px" ><?php echo $ciu; ?></td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox1" disabled="disabled" <?php if($png == 1){ echo 'checked="checked"'; } ?> type="checkbox"></td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox2" disabled="disabled" <?php if($jpg == 1){ echo 'checked="checked"'; } ?> type="checkbox"></td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox3" disabled="disabled" <?php if($jpeg == 1){ echo 'checked="checked"'; } ?> type="checkbox"></td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox4" disabled="disabled" <?php if($gif == 1){ echo 'checked="checked"'; } ?> type="checkbox"></td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox4" disabled="disabled" <?php if($dwg == 1){ echo 'checked="checked"'; } ?> type="checkbox">
				</td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox4" disabled="disabled" <?php if($mpp == 1){ echo 'checked="checked"'; } ?> type="checkbox">
				</td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox4" disabled="disabled" <?php if($tif == 1){ echo 'checked="checked"'; } ?> type="checkbox">
				</td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox5" disabled="disabled" <?php if($pdf == 1){ echo 'checked="checked"'; } ?> type="checkbox"></td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox6" disabled="disabled" <?php if($docx == 1){ echo 'checked="checked"'; } ?> type="checkbox"></td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox7" disabled="disabled" <?php if($xls == 1){ echo 'checked="checked"'; } ?> type="checkbox"></td>
				<td class="auto-style1" style="width: 45px">
				<input name="Checkbox8" disabled="disabled" <?php if($xlsx == 1){ echo 'checked="checked"'; } ?> type="checkbox"></td>
				<td class="auto-style3" style="width: 98px"><?php echo $usuact; ?></td>
				<td class="auto-style3" style="width: 127px"><?php echo $fecact; ?></td>
				<td class="auto-style1" style="width: 30px">
				<input name="activarinactivar" id="activarinactivar" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" ></td>
				<td class="auto-style3">
				<?php 
				if($metodo == 1)
				{
				?>
				<a data-reveal-id="editarciudad" data-animation="fade" style="cursor:pointer" onclick="EDITAR('<?php echo $dato['tii_clave_int']; ?>','CIUDAD')"><img src="../../images/editar.png" alt="" height="22" width="21" /></a>
				<?php
				}
				?>
				</td>
				<?php
					}
				?>
			</tr>
			<tr>
				<td class="auto-style3" style="width: 27px">&nbsp;</td>
				<td class="auto-style3" style="width: 200px" >&nbsp;</td>
				<td class="auto-style3" style="width: 45px">&nbsp;</td>
				<td class="auto-style3" style="width: 45px">&nbsp;</td>
				<td class="auto-style3" style="width: 45px">&nbsp;</td>
				<td class="auto-style3" style="width: 45px">&nbsp;</td>
				<td class="auto-style3" style="width: 45px">&nbsp;</td>
				<td class="auto-style3" style="width: 45px">&nbsp;</td>
				<td class="auto-style3" style="width: 45px">&nbsp;</td>
				<td class="auto-style3" style="width: 45px">&nbsp;</td>
				<td class="auto-style3" style="width: 45px">&nbsp;</td>
				<td class="auto-style3" style="width: 45px">&nbsp;</td>
				<td class="auto-style3" style="width: 45px">&nbsp;</td>
				<td class="auto-style3" style="width: 98px">&nbsp;</td>
				<td class="auto-style3" style="width: 127px">&nbsp;</td>
				<td class="auto-style3" style="width: 29px">&nbsp;</td>
				<td class="auto-style3">&nbsp;</td>
			</tr>
		</table>
		</div>
<div id="nuevaciudad" class="auto-style4" style="left: 57%; top: 10px; height: 180px; width: 350px;">
	<table style="width: 38%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td>Tipo informe:</td>
			<td>
			<input name="ciudad" id="ciudad"  type="text" style="width: 200px" class="inputs" />
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">
			<table style="width: 100%">
				<tr>
					<td style="width: 10px">
					<input name="png" id="png" type="checkbox">
					</td>
					<td><label for="png">PNG</label></td>
					<td style="width: 10px">
					<input name="jpg" id="jpg" type="checkbox"></td>
					<td><label for="jpg">JPG</label></td>
					<td style="width: 10px">
					<input name="jpeg" id="jpeg" type="checkbox"></td>
					<td><label for="jpeg">JPEG</label></td>
				</tr>
				<tr>
					<td style="width: 10px">
					<input name="gif" id="gif" type="checkbox"></td>
					<td><label for="gif">GIF</label></td>
					<td style="width: 10px">
					<input name="dwg" id="dwg" type="checkbox">
					</td>
					<td><label for="dwg">DWG</label></td>
					<td style="width: 10px">
					<input name="mpp" id="mpp" type="checkbox">
					</td>
					<td><label for="mpp">MPP</label></td>
				</tr>
				<tr>
					<td style="width: 10px">
					<input name="tif" id="tif" type="checkbox">
					</td>
					<td><label for="tif">TIF</label></td>
					<td style="width: 10px">
					<input name="pdf" id="pdf" type="checkbox"></td>
					<td><label for="pdf">PDF</label></td>
					<td style="width: 10px">
					<input name="docx" id="docx" type="checkbox"></td>
					<td><label for="docx">DOCX</label></td>
				</tr>
				<tr>
					<td style="width: 10px">
					<input name="xls" id="xls" type="checkbox"></td>
					<td><label for="xls">XLS</label></td>
					<td style="width: 10px">
					<input name="xlsx" id="xlsx" type="checkbox"></td>
					<td><label for="xlsx">XLSX</label></td>
					<td style="width: 10px">&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Activo:</td>
			<td><input class="inputs" name="activo" checked="checked" type="checkbox" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4">
			<input name="nuevo1" type="button" value="Guardar" onclick="NUEVO('CIUDAD')"  style="width: 348px; height: 25px; cursor:pointer" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">
			<div id="datos1">
			</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
</div>
		</td>
	</tr>
	<tr>
		<td style="width: 60px">&nbsp;</td>
		<td style="width: 117px">&nbsp;</td>
		<td style="width: 65px">&nbsp;</td>
		<td style="width: 70px">&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td style="width: 60px">&nbsp;</td>
		<td style="width: 117px">&nbsp;</td>
		<td style="width: 65px">&nbsp;</td>
		<td style="width: 70px">&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
</form>
</body>
</html>