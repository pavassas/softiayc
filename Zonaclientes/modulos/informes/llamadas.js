function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }

function VISOR(c,v)
{
	var url = "www.pavas.com.co/Zonaclientes/" +c;
    var mdl =  "<div class='embed-responsive embed-responsive-16by9'><iframe class='embed-responsive-item' src='https://docs.google.com/viewer?url="+url+"&embedded=true' frameborder='0' allowfullscreen></iframe></div>";
    $('#' + v).html(mdl);

}

function CONSULTAMODULO(v)
{
	if(v == 'INFORMEINVENTARIO')
	{
		var div = document.getElementById('filtro');
    	div.style.display = 'none';
		window.location.href = "inventario.php";
	}
	else
	if(v == 'TODOS')
	{
		var div = document.getElementById('filtro');
    	div.style.display = 'block';
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('asignados').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#asignados").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si",true);
		ajax.send(null);
	}
	OCULTARSCROLL();
}
function BUSCAR(v)
{
	var obras = "";
	var objCBarray = document.getElementsByName('multiselect_busobra');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	obras += objCBarray[i].value + ",";
	    }
	}
		
	var nomarc = form1.busnombrearchivo.value;
	var nomane = form1.busnombreanexo.value;
	var ley = form1.busleyenda.value;
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('resultadobusqueda').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#resultadobusqueda").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?buscar=si&obras="+obras+"&nomarc="+nomarc+"&nomane="+nomane+"&ley="+ley,true);
	ajax.send(null);
	OCULTARSCROLL();
}
function EXPORTAR()
{
	var actividades = "";
	var objCBarray = document.getElementsByName('multiselect_busactividad');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	actividades += objCBarray[i].value + ",";
	    }
	}
	
	var usuarios = "";
	var objCBarray = document.getElementsByName('multiselect_bususuario');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	usuarios += objCBarray[i].value + ",";
	    }
	}
	
	var ventanas = "";
	var objCBarray = document.getElementsByName('multiselect_busventana');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	ventanas += objCBarray[i].value + ",";
	    }
	}
	
	var fi = form1.busfecini.value;
	var ff = form1.busfecfin.value;
	
	window.location.href = "informes/informeexcel.php?act="+actividades+"&usu="+usuarios+"&ven="+ventanas+"&fi="+fi+"&ff="+ff;
}
function VER(o)
{
/*	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('asignados').innerHTML=ajax.responseText;
	    }
	}*/
	$("#asignados").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
	//ajax.open("GET","?verinforme=si&tipinf="+o,true);
	//ajax.send(null);
	$.post('../../funciones/informes/fnInformes.php', {
		opcion:"VERFILTROS",
		id:o
	}, function (data) {
		$('#asignados').html(data);
	})
	/*OCULTARSCROLL();
	setTimeout("VERFILTRO();",500);
	setTimeout("VERFILTRO();",1000);
	setTimeout("VERFILTRO();",1500);
	setTimeout("VERFILTRO();",2000);
	setTimeout("VERFILTRO();",2500);
	setTimeout("VERFILTRO();",3000);*/
}
function MOSTRARMOVIMIENTO(v)
{
	var ane = jQuery('#ocultofotos'+v).val();
	if(ane == 0)
	{	    
		jQuery('#ocultofotos'+v).val('1');
    	var div = document.getElementById('movimiento'+v);
    	div.style.display = 'block'; 
    	jQuery("#verocultofotos"+v).html("OCULTAR FOTOS");
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('movimiento'+v).innerHTML=ajax.responseText;
		    }
		}
		jQuery("#movimiento"+v).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?mostrarmovimiento=si&clacar="+v,true);
		ajax.send(null);
	}
	else
	{
		jQuery('#ocultofotos'+v).val('0');
    	var div = document.getElementById('movimiento'+v);
    	div.style.display = 'none';
    	jQuery("#verocultofotos"+v).html("VER FOTOS");
	}
}
function OCULTARMOVIMIENTO(v)
{
	var div = document.getElementById('movimiento'+v);
    div.style.display = 'none';
}
function MOSTRARMOVIMIENTOANEXO(v)
{
	var ane = jQuery('#ocultoanexos'+v).val();
    if(ane == 0)
    {
    	jQuery('#ocultoanexos'+v).val('1');
    	var div = document.getElementById('movimientoanexo'+v);
    	div.style.display = 'block'; 
    	jQuery("#verocultaranexo"+v).html("OCULTAR ANEXOS");    
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('movimientoanexo'+v).innerHTML=ajax.responseText;
		    }
		}
		jQuery("#movimientoanexo"+v).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?mostrarmovimientoanexo=si&clacaa="+v,true);
		ajax.send(null);
	}
	else
	{
		jQuery('#ocultoanexos'+v).val('0');
    	var div = document.getElementById('movimientoanexo'+v);
    	div.style.display = 'none';
    	jQuery("#verocultaranexo"+v).html("VER ANEXOS");
	}
}
function OCULTARMOVIMIENTOANEXO(v)
{
	var div = document.getElementById('movimientoanexo'+v);
    div.style.display = 'none';
}

function CRUDINFORMES(opt,tipo){
	if (opt == "VERINFORMES"){
		//if (tipo == "" || tipo == null || tipo == undefined) {
			tipo =  $('#tbtinformes').attr('data-tipo');
		//}
		$('#resultadobusqueda').html("");
		$.post('../../funciones/informes/fnInformes.php', {
			opcion:opt,
			tipo:tipo
		}, function (data) {
			$('#resultadobusqueda').html(data);
		})
	}else if (opt == "VERFOTOS"){
		$.post('../../funciones/informes/fnInformes.php', {
			opcion:opt,
			id:tipo
		}, function (data) {
			$('#fotos_'+tipo).html(data);
		})

	}
}