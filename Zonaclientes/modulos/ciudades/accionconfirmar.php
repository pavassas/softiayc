<?php include('../../data/Conexion.php');
session_start();
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_SESSION['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];
	
// verifica si no se ha loggeado
if(!isset($_SESSION["persona"]))
{
  session_destroy();
  header("LOCATION:index.php");
}else{
}
$con = mysqli_query($conectar,"select obr_clave_int from usuario where usu_usuario = '".$usuario."'");
$dato = mysqli_fetch_array($con);
$ultimaobra = $dato['obr_clave_int'];
	
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");
$idcats=$_POST['idcat'];
$contador=0;
if(is_array($idcats)){
        for($i=0;$i<count($idcats);$i++){
        	if($_GET['accion'] == 'Activar')
        	{
				$rows=mysqli_query($conectar,"update ciudad set ciu_sw_activo = 1, ciu_usu_actualiz = '".$usuario."', ciu_fec_actualiz = '".$fecha."' where ciu_clave_int=".$idcats[$i]);
				mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_encabezado,obr_clave_int,loa_usu_actualiz,loa_fec_actualiz) values(null,18,58,'".$idcats[$i]."','".$ultimaobra."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 58=Actualización ciudad
        	}
        	elseif($_GET['accion'] == 'Inactivar')
        	{
        		$rows=mysqli_query($conectar,"update ciudad set ciu_sw_activo = 0, ciu_usu_actualiz = '".$usuario."', ciu_fec_actualiz = '".$fecha."' where ciu_clave_int=".$idcats[$i]);
				mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_encabezado,obr_clave_int,loa_usu_actualiz,loa_fec_actualiz) values(null,18,58,'".$idcats[$i]."','".$ultimaobra."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 58=Actualización ciudad
        	}
        	elseif($_GET['accion'] == 'Eliminar')
        	{
        		if($idcats[$i] != '')
        		{
	        		$con = mysqli_query($conectar,"select * from obra where ciu_clave_int = '".$idcats[$i]."'");
	        		$nume = mysqli_num_rows($con);
			
	        		if($nume > 0)
	        		{
	        			for($k = 0; $k < $nume; $k++)
						{
							$dato = mysqli_fetch_array($con); 
							$men = $men."".$dato['obr_nombre'].",";
						}
	        		}
	        		else
	        		{
	        			$rows1=mysqli_query($conectar,"delete from ciudad where ciu_clave_int=".$idcats[$i]);
						mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_encabezado,obr_clave_int,loa_usu_actualiz,loa_fec_actualiz) values(null,18,59,'".$idcats[$i]."','".$ultimaobra."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 59=Eliminación ciudad
	        		}
	        	}
        	}
        	$contador++;
		}
		//echo "Se han eliminado $contador Registros de la base de datos";
		if($_GET['accion'] == 'Activar')
        {
        	echo '<script>alert("Las Ciudades seleccionadas han sido Activados correctamente"); window.location.href="ciudades.php";</script>';
        }
    	elseif($_GET['accion'] == 'Inactivar')
    	{
    		echo '<script>alert("Las Ciudades seleccionadas han sido Inactivados correctamente"); window.location.href="ciudades.php";</script>';
    	}
    	elseif($_GET['accion'] == 'Eliminar')
    	{
    		if($men != '')
    		{
    			echo '<script>alert("Las Ciudades seleccionadas han sido Eliminados correctamente, Excepto Las Ciudades que estan asignadas a las siguientes obras: '.$men.'"); window.location.href="ciudades.php";</script>';
    		}
    		else
    		{
    			echo '<script>alert("Las Ciudades seleccionadas han sido Eliminados correctamente"); window.location.href="ciudades.php";</script>';
    		}
    	}
}else{
	echo '<script>alert("No se enviaron registros para eliminar"); window.location.href="ciudades.php";</script>';
}

?>