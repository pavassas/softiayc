<?php
	include('../../data/Conexion.php');
	require_once('../../Classes/PHPMailer-master/class.phpmailer.php');
	date_default_timezone_set('America/Bogota');
	session_start();
	$usuario= $_SESSION['usuario'];
	$fecha=date("Y/m/d H:i:s");
	
	$con = mysqli_query($conectar,"select usu_clave_int from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
	$dato = mysqli_fetch_array($con);
	$claveusuario = $dato['usu_clave_int'];
	
	$service = $_POST['id'];
	
	$conimg = mysqli_query($conectar,"select ana_nombre from anexos_archivo where ana_clave_int = '".$service."'");
	$datoimg = mysqli_fetch_array($conimg);
	$nomane = $datoimg['ana_nombre'];
	
	$con = mysqli_query($conectar,"select usu_nombre,usu_email,tii_nombre,o.obr_nombre,caa_nombre from carga_archivo c inner join usuario_obra uo on (uo.obr_clave_int = c.obr_clave_int) inner join usuario u on (u.usu_clave_int = uo.usu_clave_int) inner join obra o on (o.obr_clave_int = c.obr_clave_int) inner join tipo_informe ti on (ti.tii_clave_int = c.tii_clave_int) where c.caa_clave_int = (select caa_clave_int from anexos_archivo where ana_clave_int = '".$service."') and c.caa_estado = 1 and u.usu_clave_int <> '".$claveusuario."'");
	$num = mysqli_num_rows($con);
	for($i = 0; $i < $num; $i++)
	{
		$mail = new PHPMailer();
		
		$dato1 = mysqli_fetch_array($con);
		$mail->Body = '';
		$ema = '';
		$nom = $dato1['usu_nombre'];
		$ema = $dato1['usu_email'];
		$tii = $dato1['tii_nombre'];
		$obr = $dato1['obr_nombre'];
		$nomarc = $dato1['caa_nombre'];
		
		$mail->From = "adminpavas@pavas.com.co";
		$mail->FromName = "I,A & C";
		$mail->AddAddress($ema, "Destino");
		$mail->Subject = "Anexo eliminado de ".$tii.". Obra: ".$obr;
		
		// Cuerpo del mensaje
		$mail->Body .= "Hola ".$nom."!\n\n";	
		$mail->Body .= "ZONA CLIENTES registra que se ha eliminado un anexo de ".$tii." de su obra: ".$obr.".\n";
		$mail->Body .= "NOMBRE ARCHIVO: ".$nomarc."\n";
		$mail->Body .= "NOMBRE ANEXO: ".$nomane."\n";
		$mail->Body .= date("d/m/Y H:m:s")."\n\n";
		$mail->Body .= "Este mensaje es generado automáticamente por ZONA CLIENTES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Zonaclientes \n";
		
		if(!$mail->Send())
		{
			//echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
			//echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
		}
		else
		{
		}
	}
	mysqli_query($conectar,"DELETE FROM anexos_archivo WHERE ana_clave_int='".$service."'");
?>