<?php
	include('../../data/Conexion.php');
	date_default_timezone_set('America/Bogota');
	session_start();
	$usuario= $_SESSION['usuario'];
	$fecha=date("Y/m/d H:i:s");
	
	require ("zipfile.php");
	$zipfile = new zipfile();
	$clacaa = $_GET['clacaa'];
	
	function extension_archivo($ruta)
	{
	    $res = explode(".", $ruta);
	    $extension = $res[count($res)-1];
	    return $extension;
	} 

	$con = mysqli_query($conectar,"select ca.caa_ruta,ca.caa_nombre,ti.tii_nombre from carga_archivo ca inner join tipo_informe ti on (ti.tii_clave_int = ca.tii_clave_int) where caa_clave_int = '".$clacaa."'");
	$dato = mysqli_fetch_array($con);
	$rut = "iframecargar/".$dato['caa_ruta'];
	$nomarc = $dato['caa_nombre'];
	$tii = $dato['tii_nombre'];
	
	$ext = extension_archivo($rut);
	$zipfile->add_file(implode("",file($rut)), "ARCHIVO/".$nomarc.".".$ext);
	
	$con = mysqli_query($conectar,"select ana_ruta,ana_nombre from anexos_archivo where caa_clave_int = '".$clacaa."'");
	$num = mysqli_num_rows($con);
	for($i = 0; $i < $num; $i++)
	{
		$dato = mysqli_fetch_array($con);
		$rut = "iframecargar/".$dato['ana_ruta'];
		$nomarc = $dato['ana_nombre'];
		$ext = extension_archivo($rut);
		$zipfile->add_file(implode("",file($rut)), "ANEXOS/".$nomarc.".".$ext);
	}
	 
	header("Content-type: application/octet-stream");
	header("Content-disposition: attachment; filename=".$tii.".zip");
	echo $zipfile->file();
?>