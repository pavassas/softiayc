﻿<?php
error_reporting(0);
include('../../../data/Conexion.php');
$clacar = $_GET['clacar'];
$claobr = $_GET['claobr'];
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<![endif]-->
<meta charset="utf-8">
<title>jQuery Image Gallery Demo</title>
<meta name="description" content="jQuery Image Gallery displays images with the touch-enabled, responsive and customizable blueimp Gallery carousel in the dialog component of jQuery UI. It features swipe, mouse and keyboard navigation, transition effects and on-demand content loading and can be extended to display additional content types.">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/themes/south-street/jquery-ui.css" id="theme">
<link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
</head>
<body>
<?php
if($clacar <> '' and $clacar <> 0)
{
	$con = mysqli_query($conectar,"select caf_nombre,caf_leyenda from carga_foto where car_clave_int = '".$clacar."' order by caf_orden");
}
else
{
	$con = mysqli_query($conectar,"select ofo_foto from obra_foto where obr_clave_int = '".$claobr."'");
}
$num = mysqli_num_rows($con);
for($i = 0; $i < $num; $i++)
{
	$dato = mysqli_fetch_array($con);
	if($clacar <> '' and $clacar <> 0)
	{
		$fot = $dato['caf_nombre'];
		$ley = $dato['caf_leyenda'];
	}
	else
	{
		$fot = $dato['ofo_foto'];
		$ley = 'GALERIA DE IMAGENES';
	}
	if($clacar <> '' and $clacar <> 0)
	{
?>
	<a href="<?php echo '../iframecargar/'.$fot; ?>" title="<?php echo $ley; ?>" data-dialog=""><img src="<?php echo '../iframecargar/'.$fot; ?>" style="width:79px;height:79px"></a>
<?php
	}
	else
	{
	?>
	<a href="<?php echo '../../obras/iframecargar/'.$fot; ?>" title="<?php echo $ley; ?>" data-dialog=""><img src="<?php echo '../../obras/iframecargar/'.$fot; ?>" style="width:79px;height:79px"></a>
	<?php
	}
}
?>
<!-- The dialog widget -->
<div id="blueimp-gallery-dialog" data-show="fade" data-hide="fade">
    <!-- The Gallery widget  -->
    <div class="blueimp-gallery blueimp-gallery-carousel blueimp-gallery-controls">
        <div class="slides"></div>
        <a class="prev"></a>
        <a class="next"></a>
        <a class="play-pause"></a>
    </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
<script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<script src="js/jquery.image-gallery.js"></script>
</body> 
</html>
