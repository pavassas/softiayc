function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function NUEVO()
{
	var nom = form1.nombre.value;
	var obr = form1.obra.value;
	var tii = form1.tipoinforme.value;

	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('fotos').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#fotos").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?nuevacarga=si&nom="+nom+"&obr="+obr+"&tii="+tii,true);
	ajax.send(null);
	if(nom != '' && obr != '' && tii != '')
	{
		setTimeout("REFRESCARTODOS();",500);
		setTimeout("REFRESBOTONGUARDAR();",500);
	}
	OCULTARSCROLL();
}
function REFRESBOTONGUARDAR()
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('guardar').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#guardar").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?botonguardar=si",true);
	ajax.send(null);
}
function ACTUALIZARCARGA(v)
{
	var nom = form1.nombre1.value;
	var obr = form1.obra1.value;
	var tii = form1.tipoinforme1.value;

	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('estadoactualizar').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#estadoactualizar").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?actualizarcarga=si&nom="+nom+"&obr="+obr+"&tii="+tii+"&cc="+v,true);
	ajax.send(null);
}
function ELIMINARCARGA(v,o,i)
{
	if(confirm('Esta seguro/a de Eliminar este registro?'))
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
		    	if(o != 'LISTA')
				{
	   		     	document.getElementById('estadoactualizar').innerHTML=ajax.responseText;
	   		    }
		    }
		}
		if(o != 'LISTA')
		{
			jQuery("#estadoactualizar").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		}
		ajax.open("GET","?eliminarcarga=si&clacar="+v,true);
		ajax.send(null);
		if(o == 'LISTA')
		{
			$('#imagenes'+v).fadeOut("slow");
			$('#movimiento'+v).fadeOut("slow");
		}
		else
		{
			setTimeout("document.form1.submit();",2000);
		}
		setTimeout("REFRESCARTODOS();",2000);
	}
}
function ELIMINARARCHIVO(v,o,i,na)
{
	if(confirm('Esta seguro/a de Eliminar este registro?'))
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     //document.getElementById('estadoactualizar').innerHTML=ajax.responseText;
		    }
		}
		//jQuery("#estadoactualizar").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?eliminararchivo=si&clacaa="+v,true);
		ajax.send(null);
		if(o == 'LISTA')
		{
			$('#'+na+v).fadeOut("slow");
			$('#movimientoanexo'+v).fadeOut("slow");
		}
		else
		{
			setTimeout("document.form1.submit();",2000);
		}
		setTimeout("REFRESCARTODOS();",2000);
	}
}
function ACTUALIZARARCHIVO(v)
{
	var obr = form1.obra1.value;
	var tii = form1.tipoinforme1.value;

	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('estadoactualizar').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#estadoactualizar").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?actualizararchivo=si&obr="+obr+"&tii="+tii+"&cc="+v,true);
	ajax.send(null);
}
function ACTUALIZARCARGAGUARDADA(v)
{
	var nom = form1.nombre.value;
	var obr = form1.obra.value;
	var tii = form1.tipoinforme.value;

	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('estadoactualizar').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#estadoactualizar").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?actualizarcarga=si&nom="+nom+"&obr="+obr+"&tii="+tii+"&cc="+v,true);
	ajax.send(null);
}
function REFRESCARTODOS()
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('estados').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#estados").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?refrescartodos=si",true);
	ajax.send(null);
}
function NUEVACARGA()
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('opcion').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#opcion").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?crearnuevacarga=si",true);
	ajax.send(null);
	OCULTARSCROLL();
	REFRESCARTODOS();
	setTimeout("REFRESCARTODOS();",500);
}
function VERTODOS(v)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('opcion').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#opcion").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?vertodos=si&est="+v,true);
	ajax.send(null);
	OCULTARSCROLL();
	REFRESCARTODOS();
	setTimeout("REFRESCARTODOS();",500);
	VERFILTRO();
	setTimeout("VERFILTRO();",500);
	setTimeout("VERFILTRO();",1000);
	setTimeout("VERFILTRO();",1500);
	setTimeout("VERFILTRO();",2000);
	setTimeout("VERFILTRO();",2500);
	setTimeout("VERFILTRO();",3000);
	setTimeout("VERFILTRO();",3500);
	setTimeout("VERFILTRO();",4000);
	setTimeout("VERFILTRO();",4500);
	setTimeout("VERFILTRO();",5000);
	setTimeout("VERFILTRO();",5500);
	setTimeout("VERFILTRO();",6000);
	setTimeout("VERFILTRO();",6500);
	setTimeout("VERFILTRO();",7000);
	setTimeout("VERFILTRO();",7500);
	setTimeout("VERFILTRO();",8000);
	setTimeout("VERFILTRO();",8500);
	setTimeout("VERFILTRO();",9000);
	setTimeout("VERFILTRO();",9500);
	setTimeout("VERFILTRO();",10000);
	setTimeout("VERFILTRO();",10500);
	setTimeout("VERFILTRO();",11000);
	setTimeout("VERFILTRO();",11500);
	setTimeout("VERFILTRO();",12000);
	setTimeout("VERFILTRO();",12500);
	setTimeout("VERFILTRO();",13000);
	setTimeout("VERFILTRO();",13500);
	setTimeout("VERFILTRO();",14000);
	setTimeout("VERFILTRO();",14500);
	setTimeout("VERFILTRO();",15000);
}
/*
var ane = form1.ocultoanexos.value;
    if(ane == 0)
    {
    	form1.ocultoanexos.value = 1;
    	var div = document.getElementById('movimientoanexo'+v);
    	div.style.display = 'block'; 
    	jQuery("#verocultaranexo"+v).html("OCULTAR ANEXOS");
    	var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('movimientoanexo'+v).innerHTML=ajax.responseText;
		    }
		}
		jQuery("#movimientoanexo"+v).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?mostrarmovimientoanexo=si&clacaa="+v,true);
		ajax.send(null);
    }
    else
    { 
    	form1.ocultoanexos.value = 0; 
    	var div = document.getElementById('movimientoanexo'+v);
    	div.style.display = 'none';
    	jQuery("#verocultaranexo"+v).html("VER ANEXOS");
    }
    */
function MOSTRARMOVIMIENTO(v)
{
	var ane = jQuery('#ocultofotos'+v).val();
	if(ane == 0)
	{	    
		jQuery('#ocultofotos'+v).val('1');
    	var div = document.getElementById('movimiento'+v);
    	div.style.display = 'block'; 
    	jQuery("#verocultofotos"+v).html("OCULTAR FOTOS");
	    
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('movimiento'+v).innerHTML=ajax.responseText;
		    }
		}
		jQuery("#movimiento"+v).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?mostrarmovimiento=si&clacar="+v,true);
		ajax.send(null);
		setTimeout("FOTOS();",2000);
	}
	else
	{
		jQuery('#ocultofotos'+v).val('0');
    	var div = document.getElementById('movimiento'+v);
    	div.style.display = 'none';
    	jQuery("#verocultofotos"+v).html("VER FOTOS");
	}
}
function OCULTARMOVIMIENTO(v)
{
	var div = document.getElementById('movimiento'+v);
    div.style.display = 'none';
}
function EDITAR(v)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('opcion').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#opcion").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?editarcarga=si&clacar="+v,true);
	ajax.send(null);
	OCULTARSCROLL();
	SUBIRSCROLL();
}
function EDITARARCHIVO(v)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('opcion').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#opcion").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?editarcargaarchivo=si&clacaa="+v,true);
	ajax.send(null);
	OCULTARSCROLL();
	setTimeout("REFRESCARBOTONACTUALIZAR1('1','"+v+"');",2000);
	setTimeout("REFRESCARBOTONACTUALIZAR2('2','"+v+"');",2000);
	VERINFO();
	SUBIRSCROLL();
}
function eliminar(v)
{
	if(confirm('Esta seguro/a de Eliminar esta imagen?'))
	{
		var dataString = 'id='+v;
	
		$.ajax({
	        type: "POST",
	        url: "delete.php",
	        data: dataString,
	        success: function() {
				//$('#delete-ok').empty();
				//$('#delete-ok').append('<div class="correcto">Se ha eliminado correctamente la orden con id='+v+'.</div>').fadeIn("slow");
				$('#service'+v).fadeOut("slow");
				//$('#'+v).remove();
	        }
	    });
	}
}
function eliminaranexo(v)
{
	if(confirm('Esta seguro/a de Eliminar este anexo?'))
	{
		var dataString = 'id='+v;
	
		$.ajax({
	        type: "POST",
	        url: "deleteanexo.php",
	        data: dataString,
	        success: function() {
				//$('#delete-ok').empty();
				//$('#delete-ok').append('<div class="correcto">Se ha eliminado correctamente la orden con id='+v+'.</div>').fadeIn("slow");
				$('#service'+v).fadeOut("slow");
				//$('#'+v).remove();
	        }
	    });
	}
}
function VALIDAR(v)
{
	if(v == '')
	{
		var v = $('#tipoinforme').val();
	}
	if(v == 1)
	{
		form1.nombre.disabled = false;
		var div = document.getElementById('botonguardar');
    	div.style.display = 'block';
	}
	else
	if(v > 1)
	{
		form1.nombre.disabled = true;
		form1.nombre.value = '';
		var div = document.getElementById('botonguardar');
    	div.style.display = 'none';
    	VERINFO();
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('fotos').innerHTML=ajax.responseText;
	    }
	}
	if(v > 1)
	{
		//document.getElementById("opcioncarga1").style.display = "block";
		//document.getElementById("opcioncarga2").style.display = "block";
		jQuery("#fotos").html(""); //loading gif will be overwrited when ajax have success
		OPCION2();
		//AGREGAR();
	}
	else
	{
		//document.getElementById("opcioncarga1").style.display = "none";
		//document.getElementById("opcioncarga2").style.display = "none";
		jQuery("#fotos").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?tipoinforme=si&tipinf="+v,true);
	}
	ajax.send(null);
	
}
function AGREGAR()
{
	var agg = form1.agregado.value;
	if(agg == 0)
	{
		agg = 1;
		form1.agregado.value = agg;
	}
	else
	{
		agg = parseInt(agg)+1;
		form1.agregado.value = agg;
	}

	var ti = $('#tipoinforme').val();
	if(ti == 2 || ti == 3)
	{
		$('#fotos').append('<div><br><table style=\"width: 50%\" align=\"center\"><tr><td style=\"height: 130px\"><fieldset name=\"Group1\"><legend align=\"center\"><strong>ACTA DE COMITE<\/strong><\/legend><table align=\"center\"><tr><td><input type=\"text\" class=\"inputs\" placeholder=\"\" style=\"width:180px\" name=\"nombre'+agg+'\" id=\"nombre'+agg+'\" spellcheck="false" \/><\/td><\/tr><tr><td align="center"><input name=\"fileUpload1'+agg+'\" id=\"fileUpload1'+agg+'\" onchange="OCULTARSCROLL()" onmousemove="OCULTOSELECCIONADO('+agg+')" type=\"file\" \/><tr><td><div id=\"resultadoadjunto'+agg+'\" style=\"width:100%\"><\/div><\/td><\/tr><\/table><\/fieldset><\/td><td style=\"height: 130px\"><fieldset name=\"Group1\"><legend align=\"center\"><strong>ANEXOS<\/strong><\/legend><table align=\"center\"><tr><td>&nbsp;<\/td><\/tr><tr><td onmousemove="OCULTOSELECCIONADO('+agg+')"><input name=\"fileUpload'+agg+'\" id=\"fileUpload'+agg+'\" onmousemove="OCULTOSELECCIONADO('+agg+')" type=\"file\" \/><\/td><\/tr><tr><td><\/td><\/tr><tr><td><\/td><\/tr><\/table><\/fieldset><\/td><td><fieldset name=\"Group1\"><legend align=\"center\"><strong>COMENTARIOS<\/strong><\/legend><table align=\"center\"><tr><td><textarea class=\"inputs\" name=\"comentario'+agg+'\" id=\"comentario'+agg+'\" style=\"height: 50px; width: 250px;\"><\/textarea><\/td><td>&nbsp;<\/td><\/tr><tr><td>&nbsp;<\/td><td>&nbsp;<\/td><\/tr><\/table><\/fieldset><\/td><\/tr><tr><td colspan=\"3\"><div id=\"ocultarbotonsubir'+agg+'\"><a><input type=\"button\" onclick=ADJUNTAR('+agg+',"NO","") onmousemove="OCULTOSELECCIONADO('+agg+')" value=\"Subir\" style=\"width:100%;cursor:pointer\" \/><input name=\"ocultoanexo'+agg+'\" id=\"ocultoanexo'+agg+'\" value=\"0\" type=\"hidden\" \/><\/a><\/div><\/td><\/tr><\/table><\/div>');
	}
	else
	{
		$('#fotos').append('<div><br><table style=\"width: 50%\" align=\"center\"><tr><td style=\"height: 130px\"><fieldset name=\"Group1\"><legend align=\"center\"><strong>INFORME<\/strong><\/legend><table align=\"center\"><tr><td><input type=\"text\" class=\"inputs\" placeholder=\"\" style=\"width:180px\" name=\"nombre'+agg+'\" id=\"nombre'+agg+'\" spellcheck="false" \/><\/td><\/tr><tr><td align="center"><input name=\"fileUpload1'+agg+'\" id=\"fileUpload1'+agg+'\" onchange="OCULTARSCROLL()" onmousemove="OCULTOSELECCIONADO('+agg+')" type=\"file\" \/><tr><td><div id=\"resultadoadjunto'+agg+'\" style=\"width:100%\"><\/div><\/td><\/tr><\/table><\/fieldset><\/td><td style=\"height: 130px\"><fieldset name=\"Group1\"><legend align=\"center\"><strong>ANEXOS<\/strong><\/legend><table align=\"center\"><tr><td>&nbsp;<\/td><\/tr><tr><td onmousemove="OCULTOSELECCIONADO('+agg+')"><input name=\"fileUpload'+agg+'\" id=\"fileUpload'+agg+'\" onmousemove="OCULTOSELECCIONADO('+agg+')" type=\"file\" \/><\/td><\/tr><tr><td><\/td><\/tr><tr><td><\/td><\/tr><\/table><\/fieldset><\/td><td><fieldset name=\"Group1\"><legend align=\"center\"><strong>COMENTARIOS<\/strong><\/legend><table align=\"center\"><tr><td><textarea class=\"inputs\" name=\"comentario'+agg+'\" id=\"comentario'+agg+'\" style=\"height: 50px; width: 250px;\"><\/textarea><\/td><td>&nbsp;<\/td><\/tr><tr><td>&nbsp;<\/td><td>&nbsp;<\/td><\/tr><\/table><\/fieldset><\/td><\/tr><tr><td colspan=\"3\"><div id=\"ocultarbotonsubir'+agg+'\"><a><input type=\"button\" onclick=ADJUNTAR('+agg+',"NO","") onmousemove="OCULTOSELECCIONADO('+agg+')" value=\"Subir\" style=\"width:100%;cursor:pointer\" \/><input name=\"ocultoanexo'+agg+'\" id=\"ocultoanexo'+agg+'\" value=\"0\" type=\"hidden\" \/><\/a><\/div><\/td><\/tr><\/table><\/div>');
	}
	REFRESCARBOTON(agg);
	REFRESCARBOTON1(agg);
	OCULTARSCROLL();
}
function OPCION2()
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('fotos').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#fotos").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostraropcion2=si",true);
	ajax.send(null);
	//VERINFO();
	OCULTARSCROLL();
}
function MOSTRARULTIMOARCHIVO()
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
	    	 window.frames['iframeopc2'].document.getElementById('ultimoregistro').innerHTML=ajax.responseText;
   		     //document.getElementById('ultimoregistro').innerHTML=ajax.responseText;
	    }
	}
	//jQuery("#ultimoregistro").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","iframeopcion2/index.php?mostrarultimoarchivo=si",true);
	ajax.send(null);
	OCULTARSCROLL();
}
function MOSTRARRUTA(v)
{
	var nomadj = $('#adjunto'+v).val();
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('resultadoadjunto'+v).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#resultadoadjunto"+v).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarruta=si&nomadj="+nomadj,true);
	ajax.send(null);
	OCULTARSCROLL();
}
function RESULTADOADJUNTO(v)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('resultadoadjunto'+v).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#resultadoadjunto"+v).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?estadoadjunto=si",true);
	ajax.send(null);
	OCULTARSCROLL();
}
function GUARDARNOMBREANEXO(v)
{	
	var cant = $('#ocultoanexo'+v).val();
	var nom = "";
	for(i = 1; i <= cant; i++)
	{
		var str = form1.ocultoeliminados.value;
		var res = str.split(",");
		for(j = 0; j < res.length; j++)
		{
			var ele = res[j];
			if(ele == 'anexo'+v+''+i)
			{
				swvalidar = 1;
			}
		}
		if(swvalidar == 0)
		{
			var nom = nom+$('#anexo'+v+''+i).val() + ",";
			var div = document.getElementById('anexo'+v+''+i);
    		div.style.display = 'none';
    	}
    	swvalidar = 0;
	}
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     //document.getElementById('result').innerHTML=ajax.responseText;
	    }
	}
	//jQuery("#result").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?guardarnombreanexo=si&nomane="+nom,true);
	ajax.send(null);
	form1.ocultoeliminados.value = '';
}
function GUARDARNOMBREANEXOACTUALIZADO(v,ci)
{	
	var cant = $('#ocultoanexo'+v).val();
	var nom = "";
	for(i = 1; i <= cant; i++)
	{
		var str = form1.ocultoeliminados.value;
		var res = str.split(",");
		for(j = 0; j < res.length; j++)
		{
			var ele = res[j];
			if(ele == 'anexo'+v+''+i)
			{
				swvalidar = 1;
			}
		}
		if(swvalidar == 0)
		{
			var nom = nom+$('#anexo'+v+''+i).val() + ",";
			var div = document.getElementById('anexo'+v+''+i);
    		div.style.display = 'none';
    	}
    	swvalidar = 0;
	}
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     //document.getElementById('result').innerHTML=ajax.responseText;
	    }
	}
	//jQuery("#result").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?guardarnombreanexoactualizado=si&nomane="+nom+"&ci="+ci,true);
	ajax.send(null);
	form1.ocultoeliminados.value = '';
}
function OCULTOSELECCIONADO(v)
{
	$('#ocultoseleccionado').val(v);
	form1.ocultoseleccionado.value = v;
}
function MOSTRARMOVIMIENTOANEXO(v)
{
    var ane = jQuery('#ocultoanexos'+v).val();
    if(ane == 0)
    {
    	jQuery('#ocultoanexos'+v).val('1');
    	var div = document.getElementById('movimientoanexo'+v);
    	div.style.display = 'block'; 
    	jQuery("#verocultaranexo"+v).html("OCULTAR ANEXOS");
    	var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('movimientoanexo'+v).innerHTML=ajax.responseText;
		    }
		}
		jQuery("#movimientoanexo"+v).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?mostrarmovimientoanexo=si&clacaa="+v,true);
		ajax.send(null);
    }
    else
    { 
    	jQuery('#ocultoanexos'+v).val('0');
    	var div = document.getElementById('movimientoanexo'+v);
    	div.style.display = 'none';
    	jQuery("#verocultaranexo"+v).html("VER ANEXOS");
    }
}
function APROBARREGISTROFOTOGRAFICO(v)
{
	if(confirm('Esta seguro/a de Aprobar este registro fotografico?'))
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('estados').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#estados").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?aprobarregistrofotografico=si&clacar="+v,true);
		ajax.send(null);
		$('#imagenes'+v).fadeOut("slow");
	}
}
function APROBARCOMITE(v,na)
{
	if(confirm('Esta seguro/a de Aprobar este informe de comite?'))
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('estados').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#estados").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?aprobarcomite=si&clacaa="+v,true);
		ajax.send(null);
		$('#'+na+v).fadeOut("slow");
	}
}
function APROBARINFORMESMASIVO()
{
	if(confirm('Esta seguro/a de Aprobar los informes seleccionados?'))
	{
		var cont = $('#contador1').val();
		var informes = "";		
		var objCBarray = document.getElementsByName('idcatid');
		for (i = 0; i < cont; i++) 
		{
			if (objCBarray[i].checked) 
			{
		    	informes += objCBarray[i].value + ",";
		    }
		}
	
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('estadoaprobacion').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#estadoaprobacion").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?aprobarinformesmasivo=si&inf="+informes,true);
		ajax.send(null);
		VERCANTIDADES();
		
		for (i = 0; i < cont; i++) 
		{
			if (objCBarray[i].checked) 
			{
				var tip = objCBarray[i].value;
				
				if(tip.substring(0, 1) == 'O')
				{
		    		$('#obra'+tip.substring(8, 14)).fadeOut("slow");
		    	}
		    	else
		    	if(tip.substring(0, 1) == 'C')
				{
		    		$('#tecnico'+tip.substring(8, 14)).fadeOut("slow");
		    	}
		    	else
		    	if(tip.substring(0, 1) == 'I')
				{
		    		$('#mensual'+tip.substring(8, 14)).fadeOut("slow");
		    	}
		    	else
		    	if(tip.substring(0, 1) == 'F')
				{
		    		$('#imagenes'+tip.substring(4, 8)).fadeOut("slow");
		    	}
		    }
		}
	}
}
function ELIMINARINFORMESMASIVO()
{
	if(confirm('Esta seguro/a de Eliminar los informes seleccionados?'))
	{
		var cont = $('#contador1').val();
		var informes = "";
		var objCBarray = document.getElementsByName('idcatid');
		for (i = 0; i < cont; i++) 
		{
			if (objCBarray[i].checked) 
			{
		    	informes += objCBarray[i].value + ",";
		    }
		}
	
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('estadoaprobacion').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#estadoaprobacion").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?eliminarinformesmasivo=si&inf="+informes,true);
		ajax.send(null);
		VERCANTIDADES();
		
		for (i = 0; i < cont; i++) 
		{
			if (objCBarray[i].checked) 
			{
				var tip = objCBarray[i].value;
				
				if(tip.substring(0, 1) == 'O')
				{
		    		$('#obra'+tip.substring(8, 14)).fadeOut("slow");
		    	}
		    	else
		    	if(tip.substring(0, 1) == 'C')
				{
		    		$('#tecnico'+tip.substring(8, 14)).fadeOut("slow");
		    	}
		    	else
		    	if(tip.substring(0, 1) == 'I')
				{
		    		$('#mensual'+tip.substring(8, 14)).fadeOut("slow");
		    	}
		    	else
		    	if(tip.substring(0, 1) == 'F')
				{
		    		$('#imagenes'+tip.substring(4, 8)).fadeOut("slow");
		    	}
		    }
		}
	}
}
function VERCANTIDADES()
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('estados').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#estados").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?vercantidades=si",true);
	ajax.send(null);
}
function GUARDARLEYENDA(c,l)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     //document.getElementById('estados').innerHTML=ajax.responseText;
	    }
	}
	//jQuery("#estados").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?guardarleyenda=si&clacaf="+c+"&ley="+l,true);
	ajax.send(null);
}
function GUARDARNOMBREANEXOAUTOMATICO(c,na)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     //document.getElementById('estados').innerHTML=ajax.responseText;
	    }
	}
	//jQuery("#estados").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?guardarnombreanexoautomatico=si&claana="+c+"&nomane="+na,true);
	ajax.send(null);
}
function VERFOTOSAGREGADAS(v)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('fotosagregadas').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#fotosagregadas").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarfotosagregadas=si&clacar="+v,true);
	ajax.send(null);
	OCULTARSCROLL();
}
function ACTUALIZARARCHIVO(nom,obr,ti,com,ci)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('resultadoactualizar').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#resultadoactualizar").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?actualizararchivoeditado=si&nom="+nom+"&obr="+obr+"&ti="+ti+"&com="+com+"&ci="+ci,true);
	ajax.send(null);
	OCULTARSCROLL();
}
function VERANEXOS(file,nomane)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('veranexos').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#veranexos").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?veranexos=si&file="+file+"&nomane="+nomane,true);
	ajax.send(null);
	OCULTARSCROLL();
}
function AGREGARNOMBREANEXO(file,nomane)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     //document.getElementById('veranexos').innerHTML=ajax.responseText;
	    }
	}
	//jQuery("#veranexos").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?agregarnombreanexo=si&file="+file+"&nomane="+nomane,true);
	ajax.send(null);
	OCULTARSCROLL();
}
function BUSCAR()
{
	var obras = "";
	var objCBarray = document.getElementsByName('multiselect_busobra');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	obras += objCBarray[i].value + ",";
	    }
	}
	
	var informes = "";
	var objCBarray = document.getElementsByName('multiselect_bustipoinforme');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	informes += objCBarray[i].value + ",";
	    }
	}
	
	var nomarc = $('#busnombrearchivo').val();
	var nomane = $('#busnombreanexo').val();
	var nomley = $('#busleyenda').val();
	var nomcom = $('#buscomentario').val();
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('resultadobusqueda').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#resultadobusqueda").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?buscar=si&obras="+obras+"&informes="+informes+"&nomarc="+nomarc+"&nomane="+nomane+"&nomley="+nomley+"&nomcom="+nomcom,true);
	ajax.send(null);
	OCULTARSCROLL();
}
function INSERTARDATOS(nom,obr,ti,com)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     //document.getElementById('resultadoadjunto'+v).innerHTML=ajax.responseText;
	    }
	}
	//jQuery("#resultadoadjunto"+v).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?insrtardatos=si&nom="+nom+"&obr="+obr+"&ti="+ti+"&com="+com,true);
	ajax.send(null);
	OCULTARSCROLL();
}