<?php
define('WP_MEMORY_LIMIT', '128M');
ini_set('memory_limit', '128M');
include('../../data/Conexion.php');
session_start();
error_reporting(E_ALL);
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_SESSION['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];
$ci = $_GET['ci'];

$fecha=date("Y/m/d H:i:s");
if (!empty($_FILES)) {
	$tempFile = $_FILES['Filedata']['tmp_name'];
	$targetPath = $_SERVER['DOCUMENT_ROOT'] . $_GET['folder'] . '/';
	$targetFile =  str_replace('//','/',$targetPath) . $_FILES['Filedata']['name'];
	$file = $_FILES["Filedata"]["name"];
	
	/****mi codigo****/
	$archivo = basename($_FILES['Filedata']['name']);
	
	$array_nombre = explode('.',$archivo);
	$cuenta_arr_nombre = count($array_nombre);
	$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
	
	$con = mysqli_query($conectar,"select ana_clave_int from anexos_archivo order by ana_clave_int DESC LIMIT 1");
	$dato = mysqli_fetch_array($con);
	$claana = $dato['ana_clave_int'];
	
	if($claana == '' or $claana == 0)
	{
		$claana = 1;
	}
	else
	{
		$claana = $claana+1;
	}
	
	$destino =  "iframecargar/uploads/archivoanexo".$claana.".".$extension;
	$ruta = "uploads/archivoanexo".$claana.".".$extension;
	/****Termina mi codigo****/
	
	// Uncomment the following line if you want to make the directory if it doesn't exist
	// mkdir(str_replace('//','/',$targetPath), 0755, true);
	if(move_uploaded_file($tempFile,$destino))
	{
		if($ci > 0 and $ci <> '')
		{
			$clacaa = $ci;
		}
		else
		{
			$con = mysqli_query($conectar,"select caa_clave_int from carga_archivo where caa_usu_actualiz = '".$usuario."' order by caa_clave_int DESC LIMIT 1");
			$dato = mysqli_fetch_array($con);
			$clacaa = $dato['caa_clave_int'];			
		}
		mysqli_query($conectar,"INSERT INTO anexos_archivo(ana_clave_int,caa_clave_int,ana_ruta,ana_ruta_original,ana_usu_actualiz,ana_fec_actualiz) values(null,'".$clacaa."','".$ruta."','".$file."','".$usuario."','".$fecha."')");

		if($extension == 'jpg' or $extension == 'jpeg' or $extension == 'JPG' or $extension == 'JPEG')
		{
			//Reduzco el tamano de la imagen
			$img_origen = imagecreatefromjpeg($destino);
			$ancho_origen = imagesx($img_origen);//Se obtiene el ancho de la imagen
			$alto_origen = imagesy($img_origen);//Se obtiene el alto de la imagen
			$ancho_limite = 850;
			
			if($ancho_origen > $ancho_limite)//Para foto horizontal
			{
				$ancho_origen = $ancho_limite;
				$alto_origen = $ancho_limite*imagesy($img_origen)/imagesx($img_origen);
			}
			else//Para fotos verticales
			{
				$alto_origen = $ancho_limite;
				$ancho_origen = $ancho_limite*imagesx($img_origen)/imagesy($img_origen);
			}
			$img_destino = imagecreatetruecolor($ancho_origen, $alto_origen);//Se crea la imagen segun las dimensiones dadas
			imagecopyresized($img_destino, $img_origen, 0, 0, 0, 0, $ancho_origen, $alto_origen, imagesx($img_origen), imagesy($img_origen));
			imagejpeg($img_destino,$destino);//Se guarda la nueva foto
		}
		else
		if($extension == 'png' or $extension == 'PNG')
		{
			//Reduzco el tamano de la imagen
			$img_origen = imagecreatefrompng($destino);
			$ancho_origen = imagesx($img_origen);//Se obtiene el ancho de la imagen
			$alto_origen = imagesy($img_origen);//Se obtiene el alto de la imagen
			$ancho_limite = 850;
			
			if($ancho_origen > $ancho_limite)//Para foto horizontal
			{
				$ancho_origen = $ancho_limite;
				$alto_origen = $ancho_limite*imagesy($img_origen)/imagesx($img_origen);
			}
			else//Para fotos verticales
			{
				$alto_origen = $ancho_limite;
				$ancho_origen = $ancho_limite*imagesx($img_origen)/imagesy($img_origen);
			}
			$img_destino = imagecreatetruecolor($ancho_origen, $alto_origen);//Se crea la imagen segun las dimensiones dadas
			imagecopyresized($img_destino, $img_origen, 0, 0, 0, 0, $ancho_origen, $alto_origen, imagesx($img_origen), imagesy($img_origen));
			imagepng($img_destino,$destino);//Se guarda la nueva foto
		}
		else
		if($extension == 'gif' or $extension == 'GIF')
		{
			//Reduzco el tamano de la imagen
			$img_origen = imagecreatefromgif($destino);
			$ancho_origen = imagesx($img_origen);//Se obtiene el ancho de la imagen
			$alto_origen = imagesy($img_origen);//Se obtiene el alto de la imagen
			$ancho_limite = 850;
			
			if($ancho_origen > $ancho_limite)//Para foto horizontal
			{
				$ancho_origen = $ancho_limite;
				$alto_origen = $ancho_limite*imagesy($img_origen)/imagesx($img_origen);
			}
			else//Para fotos verticales
			{
				$alto_origen = $ancho_limite;
				$ancho_origen = $ancho_limite*imagesx($img_origen)/imagesy($img_origen);
			}
			$img_destino = imagecreatetruecolor($ancho_origen, $alto_origen);//Se crea la imagen segun las dimensiones dadas
			imagecopyresized($img_destino, $img_origen, 0, 0, 0, 0, $ancho_origen, $alto_origen, imagesx($img_origen), imagesy($img_origen));
			imagegif($img_destino,$destino);//Se guarda la nueva foto
		}
	}
}
echo "1";
?>