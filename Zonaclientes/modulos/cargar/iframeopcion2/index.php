<?php
error_reporting(0);
session_start();
include('../../../data/Conexion.php');
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_SESSION['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];

$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
$dato = mysqli_fetch_array($con);
$descripcionperfil = $dato['prf_descripcion'];
$claveperfil = $dato['prf_clave_int'];
$claveusuario = $dato['usu_clave_int'];
$aprueba = $dato['prf_sw_aprobar'];
$ultimaobra = $dato['obr_clave_int'];
$ultimoestado = $dato['usu_ultimo_estado'];
if($_GET['mostrarultimoarchivo'] == 'si')
{
	?>
	<fieldset name="Group1">
		<legend><strong>ULTIMO INFORME CARGADO</strong></legend>
		<table style="width: 80%">
			<tr>
				<td><strong>TIPO INFORME</strong></td>
				<td><strong>NOMBRE ARCHIVO</strong></td>
				<td><strong>OBRA</strong></td>
				<td><strong>COMENTARIOS</strong></td>
			</tr>
			<?php 
			$con = mysqli_query($conectar,"select ca.caa_clave_int,tii_nombre,caa_nombre,o.obr_nombre,caa_comentarios from carga_archivo ca inner join tipo_informe ti on (ti.tii_clave_int = ca.tii_clave_int) inner join obra o on (o.obr_clave_int = ca.obr_clave_int) where ca.caa_usu_actualiz = '".$usuario."' order by caa_clave_int DESC LIMIT 1");
			$dato = mysqli_fetch_array($con);
			$clavearc = $dato['caa_clave_int'];
			$ti = $dato['tii_nombre'];
			$nom = $dato['caa_nombre'];
			$obr = $dato['obr_nombre'];
			$com = $dato['caa_comentarios'];
			?>
			<input name="clavearchivo" id="clavearchivo" value="<?php echo $clavearc; ?>" type="hidden" />
			<tr>
				<td><?php echo $ti; ?></td>
				<td><?php echo $nom; ?></td>
				<td><?php echo $obr; ?></td>
				<td><?php echo $com; ?></td>
			</tr>
		</table>
	</fieldset>
	<?php
	exit();
}
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
<!-- Force latest IE rendering engine or ChromeFrame if installed -->
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<![endif]-->
<meta charset="utf-8">
<title>jQuery File Upload Demo</title>
<meta name="description" content="File Upload widget with multiple file selection, drag&amp;drop support, progress bars, validation and preview images, audio and video for jQuery. Supports cross-domain, chunked and resumable file uploads and client-side image resizing. Works with any server-side platform (PHP, Python, Ruby on Rails, Java, Node.js, Go etc.) that supports standard HTML form file uploads.">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Bootstrap styles -->
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<!-- Generic page styles -->
<link rel="stylesheet" href="css/style.css">
<!-- blueimp Gallery styles -->
<link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="css/jquery.fileupload.css">
<link rel="stylesheet" href="css/jquery.fileupload-ui.css">
<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href="css/jquery.fileupload-noscript.css"></noscript>
<noscript><link rel="stylesheet" href="css/jquery.fileupload-ui-noscript.css"></noscript>
</head>
<body>
<div class="container">
	<table style="width: 100%">
		<tr>
			<td style="width: 50%"><strong>ARCHIVO</strong></td>
			<td style="width: 50%"><strong>ANEXOS</strong></td>
		</tr>
		<tr>
			<td style="width: 50%">
			<!-- The file upload form used as target for the file upload widget -->
			<form id="fileupload" action="//jquery-file-upload.appspot.com/" method="POST" enctype="multipart/form-data">
				<!-- Redirect browsers with JavaScript disabled to the origin page -->
				<noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
				<div style="float:left"><input type="text" placeholder="Nombre archivo" spellcheck="false" style="width:200px;height:45px" name="nombre1" id="nombre1" class="form-control" /></div> 
				<div style="float:left;margin-left:10px"><textarea name="comentario1" id="comentario1" style="width:200px;height:45px" placeholder="Comentarios" class="form-control"></textarea></div>
				<br><br><br>
				<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
				<div class="row fileupload-buttonbar">
					<div class="col-lg-7">
					<!-- The fileinput-button span is used to style the file input field as button -->
						<table style="width: 100%">
							<tr>
								<td>
								<?php echo '<span class="btn btn-success fileinput-button">
									<i class="glyphicon glyphicon-plus"></i>
									<span>Examinar</span>
									<input type="file" name="files[]">
								</span>'; ?>
								</td>
								<td>&nbsp;
								</td>
								<td>
								<button type="submit" class="btn btn-primary start">
									<i class="glyphicon glyphicon-upload"></i>
									<span>Iniciar carga</span>
								</button>
								</td>
								<td>&nbsp;
								</td>
								<td>
								<button type="reset" class="btn btn-warning cancel">
									<i class="glyphicon glyphicon-ban-circle"></i>
									<span>Cancelar carga</span>
								</button>
								</td>
							</tr>
							<tr>
								<td colspan="5" style="width:100%;">
								<!-- The global file processing state -->
								<span class="fileupload-process" style="width:100%;"></span>
								<!-- The global progress state -->
								<div class="col-lg-5 fileupload-progress fade" style="width:100%;">
									<!-- The global progress bar -->
									<div style="width:100%;" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
										<div class="progress-bar progress-bar-success" style="width:0%;"></div>
									</div>
									<!-- The extended global progress state -->
									<div style="width:100%;" class="progress-extended">&nbsp;</div>
								</div>
								</td>
							</tr>
						</table>
					</div>
				</div>
				<!-- The table listing the files available for upload/download -->
				<table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
			    
			</form>
			</td>
			<td style="width: 50%">
			<form id="fileupload1" action="//jquery-file-upload.appspot.com/" method="POST" enctype="multipart/form-data">
				<!-- Redirect browsers with JavaScript disabled to the origin page -->
				<noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
				<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
				<div class="row fileupload-buttonbar">
					<div class="col-lg-7">
					<!-- The fileinput-button span is used to style the file input field as button -->
						<table style="width: 100%">
							<tr>
								<td>
								<?php echo '<span class="btn btn-success fileinput-button">
									<i class="glyphicon glyphicon-plus"></i>
									<span>Examinar</span>
									<input type="file" name="files[]" multiple style="right: 0; top: 0">
								</span>'; ?>
								</td>
								<td>&nbsp;
								</td>
								<td>
								<button type="submit" class="btn btn-primary start">
									<i class="glyphicon glyphicon-upload"></i>
									<span>Iniciar carga</span>
								</button>
								</td>
								<td>&nbsp;
								</td>
								<td>
								<button type="reset" class="btn btn-warning cancel">
									<i class="glyphicon glyphicon-ban-circle"></i>
									<span>Cancelar carga</span>
								</button>
								</td>
							</tr>
							<tr>
								<td colspan="5" style="width:100%;">
								<!-- The global file processing state -->
								<span class="fileupload-process" style="width:100%;"></span>
								<!-- The global progress state -->
								<div class="col-lg-5 fileupload-progress fade" style="width:100%;">
									<!-- The global progress bar -->
									<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:100%;">
										<div class="progress-bar progress-bar-success" style="width:0%;"></div>
									</div>
									<!-- The extended global progress state -->
									<div class="progress-extended" style="width:100%;">&nbsp;</div>
								</div>
								</td>
							</tr>
						</table>
					</div>
					
				</div>
				<!-- The table listing the files available for upload/download -->
				<table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>	
			</form>
			</td>
		</tr>
		<tr>
			<td colspan="2">
			<div id="ultimoregistro">
			<fieldset name="Group1">
				<legend><strong>ULTIMO INFORME CARGADO</strong></legend>
				<table style="width: 80%">
					<tr>
						<td><strong>TIPO INFORME</strong></td>
						<td><strong>NOMBRE ARCHIVO</strong></td>
						<td><strong>OBRA</strong></td>
						<td><strong>COMENTARIOS</strong></td>
					</tr>
					<?php 
					$con = mysqli_query($conectar,"select ca.caa_clave_int,tii_nombre,caa_nombre,o.obr_nombre,caa_comentarios from carga_archivo ca inner join tipo_informe ti on (ti.tii_clave_int = ca.tii_clave_int) inner join obra o on (o.obr_clave_int = ca.obr_clave_int) where ca.caa_usu_actualiz = '".$usuario."' order by caa_clave_int DESC LIMIT 1");
					$dato = mysqli_fetch_array($con);
					$clavearc = $dato['caa_clave_int'];
					$ti = $dato['tii_nombre'];
					$nom = $dato['caa_nombre'];
					$obr = $dato['obr_nombre'];
					$com = $dato['caa_comentarios'];
					?>
					<input name="clavearchivo" id="clavearchivo" value="<?php echo $clavearc; ?>" type="hidden" />
					<tr>
						<td><?php echo $ti; ?></td>
						<td><?php echo $nom; ?></td>
						<td><?php echo $obr; ?></td>
						<td><?php echo $com; ?></td>
					</tr>
				</table>
			</fieldset>
			</div>
			</td>
		</tr>
		<tr>
			<td>&nbsp;
			
			</td>
			<td>&nbsp;
			</td>
		</tr>
	</table>
</div>
<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Processing...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">

</script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="js/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<!-- blueimp Gallery script -->
<script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="js/jquery.fileupload.js?<?php echo time();?>"></script>
<!-- The File Upload processing plugin -->
<script src="js/jquery.fileupload-process.js?<?php echo time();?>"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="js/jquery.fileupload-image.js?<?php echo time();?>"></script>
<!-- The File Upload audio preview plugin -->
<script src="js/jquery.fileupload-audio.js?<?php echo time();?>"></script>
<!-- The File Upload video preview plugin -->
<script src="js/jquery.fileupload-video.js?<?php echo time();?>"></script>
<!-- The File Upload validation plugin -->
<script src="js/jquery.fileupload-validate.js?<?php echo time();?>"></script>
<!-- The File Upload user interface plugin -->
<script src="js/jquery.fileupload-ui.js?<?php echo time();?>"></script>
<!-- The main application script -->
<script src="js/main3.js?<?php echo time();?>"></script>
<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
<!--[if (gte IE 8)&(lt IE 10)]>
<script src="js/cors/jquery.xdr-transport.js"></script>
<![endif]-->
</body> 
</html>
