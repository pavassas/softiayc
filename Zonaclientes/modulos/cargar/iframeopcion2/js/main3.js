/*
 * jQuery File Upload Plugin JS Example 8.9.1
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */
/* global $, window */
$(function () {
    'use strict';
    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: 'server/php/',//../iframecargar/
        maxFileSize: 16000000,
        maxNumberOfFiles: 1,
        processfail: function (e, data) {
	        alert(data.files[data.index].name + "\n" + data.files[data.index].error);
	    }
    }).bind('fileuploadprogressall', function (e, data) {
        if(data.loaded==data.total) {
        	alert("CARGA REALIZADA CORRECTAMENTE");
        	parent.ULTIMOARCHIVO();
        	$("#nombre1").val('');
	    	$("#comentario1").val('');
        }
	});
    
    $('#fileupload').bind('fileuploadsubmit', function (e, data) {
	    // The example input, doesn't have to be part of the upload form:
	    var input1 = $("#obra", window.parent.document);
	    var input2 = $("#tipoinforme", window.parent.document);
	    var input3 = $("#nombre1");
	    var input4 = $("#comentario1");
	    var input5 = $("#usuario", window.parent.document);
	    data.formData = {obra: input1.val(),tipoinforme: input2.val(),nombre: input3.val(),comentario: input4.val(),usuario: input5.val()};
	    if (!data.formData.obra) {
	      data.context.find('button').prop('disabled', false);
	      alert("Debe elegir la obra");
	      input1.focus();
	      return false;
	    }
	    else
	    if (!data.formData.tipoinforme) {
	      data.context.find('button').prop('disabled', false);
	      alert("Debe elegir el tipo de informe");
	      input2.focus();
	      return false;
	    }
	    else
	    if (!data.formData.nombre) {
	      data.context.find('button').prop('disabled', false);
	      alert("Debe ingresar el nombre del archivo");
	      input3.focus();
	      return false;
	    }
	});
	
	$('#fileupload1').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: 'serveranexos/php/',//../iframecargar/
        maxFileSize: 14000000
    }).bind('fileuploadprogress', function (e, data) {
	        if(data.loaded==data.total) {
	        	var filess= data.files[0];
            	var filenam = filess.name;
	        	var nomane = prompt("Agregar nombre al anexo: "+filenam);
				if(nomane != ''){ parent.NOMBREANEXO('',nomane); setTimeout("parent.NOMBREANEXO('','"+nomane+"');",1000); setTimeout("parent.NOMBREANEXO('','"+nomane+"');",2000); setTimeout("parent.NOMBREANEXO('','"+nomane+"');",3000); setTimeout("parent.NOMBREANEXO('','"+nomane+"');",4000); setTimeout("parent.NOMBREANEXO('','"+nomane+"');",5000); setTimeout("parent.NOMBREANEXO('','"+nomane+"');",6000); setTimeout("parent.NOMBREANEXO('','"+nomane+"');",7000); setTimeout("parent.NOMBREANEXO('','"+nomane+"');",8000); setTimeout("parent.NOMBREANEXO('','"+nomane+"');",9000); setTimeout("parent.NOMBREANEXO('','"+nomane+"');",10000); }
	        }
	});
    
    $('#fileupload1').bind('fileuploadsubmit', function (e, data) {
	    // The example input, doesn't have to be part of the upload form:
	    var input = $("#usuario", window.parent.document);
	    var input1 = $("#clavearchivo");
	    data.formData = {usuario: input.val(),clavearc: input1.val()};
	    if (!data.formData.clavearc) {
	      data.context.find('button').prop('disabled', false);
	      alert("No hay un archivo seleccionado para agregar anexos");
	      return false;
	    }
	});

    // Enable iframe cross-domain access via redirect option:
    $('#fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );

    if (window.location.hostname === 'blueimp.github.io') {
        // Demo settings:
        $('#fileupload').fileupload('option', {
            url: '//jquery-file-upload.appspot.com/',
            // Enable image resizing, except for Android and Opera,
            // which actually support image resizing, but fail to
            // send Blob objects via XHR requests:
            disableImageResize: /Android(?!.*Chrome)|Opera/
                .test(window.navigator.userAgent),
            maxFileSize: 14000000,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
        });
        // Upload server status check for browsers with CORS support:
        if ($.support.cors) {
            $.ajax({
                url: '//jquery-file-upload.appspot.com/',
                type: 'HEAD'
            }).fail(function () {
                $('<div class="alert alert-danger"/>')
                    .text('Upload server currently unavailable - ' +
                            new Date())
                    .appendTo('#fileupload');
            });
        }
    } else {
        // Load existing files:
        $('#fileupload').addClass('fileupload-processing');
        $.ajax({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            url: $('#fileupload').fileupload('option', 'url'),
            dataType: 'json',
            context: $('#fileupload')[0]
        }).always(function () {
            $(this).removeClass('fileupload-processing');
        }).done(function (result) {
            $(this).fileupload('option', 'done')
                .call(this, $.Event('done'), {result: result});
        });
    }

});
