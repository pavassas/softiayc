<?php
/*
 * jQuery File Upload Plugin PHP Example 5.14
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */
ini_set('max_execution_time', 600);
ini_set('upload_max_filesize', '300M');
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
header("Content-Type: text/html;charset=utf-8");
date_default_timezone_set('America/Bogota');
$options = array(
    'delete_type' => 'POST',
    'db_host' => 'localhost',
    'db_user' => 'usrpavas',
    'db_pass' => '9A12)WHFy$2p4v4s',
    'db_name' => 'iayc',
    'db_table' => 'anexos_archivo',
    'upload_dir'=>'../../../iframecargar/uploads/'
);
error_reporting(0);
require('UploadHandler.php');

class CustomUploadHandler extends UploadHandler {

    protected function initialize() {
        $this->db = new mysqli(
            $this->options['db_host'],
            $this->options['db_user'],
            $this->options['db_pass'],
            $this->options['db_name']
        );
        parent::initialize();
        $this->db->close();
    }

    protected function handle_form_data($file, $index) {
        /*$file->obra = $_REQUEST['obra'];
        $file->tipoinforme = $_REQUEST['tipoinforme'];
        $file->nombre = $_REQUEST['nombre'];
        $file->comentario = $_REQUEST['comentario'];*/
        $file->usuario = $_REQUEST['usuario'];
    }
    
    protected function handle_file_upload($uploaded_file, $name, $size, $type, $error,
            $index = null, $content_range = null) {
        $file = parent::handle_file_upload(
            $uploaded_file, $name, $size, $type, $error, $index, $content_range
        );
        if (empty($file->error)) {

            $conectar = mysqli_connect("localhost", "usrpavas","9A12)WHFy$2p4v4s", "iayc");
             mysqli_query($conectar,"SET NAMES 'utf8'");
        	//$link=mysql_connect("localhost","usrpavas","9A12)WHFy$2p4v4s"); //abro la conexion
			//mysql_select_db("iayc",$link); //selecciono mi base de datos 
						
			$ruta = "uploads/".$file->nameoriginal;
        	
        	$fecha=date("Y/m/d H:i:s");
        	$file->fecha = $fecha;
        	$file->ruta = $ruta;
        	$file->null = '';
        	
        	$con = mysqli_query($conectar,"select caa_clave_int from carga_archivo where caa_usu_actualiz = '".utf8_decode($file->usuario)."' order by caa_clave_int DESC LIMIT 1");
			$dato = mysqli_fetch_array($con);
			$clacaa = $dato['caa_clave_int'];
        	$file->clave = $clacaa;
        	
            $sql = 'INSERT INTO `'.$this->options['db_table']
                .'` (`caa_clave_int`, `ana_ruta_original`, `ana_ruta`, `ana_nombre`, `ana_usu_actualiz`, `ana_fec_actualiz`)'
                .' VALUES (?, ?, ?, ?, ?, ?)';
            $query = $this->db->prepare($sql);
            $query->bind_param(
                'ssssss',
                $file->clave,
                $file->nameoriginal,
                $file->ruta,
                $file->null,
                utf8_decode($file->usuario),
                $file->fecha
            );
            $query->execute();
            $file->id = $this->db->insert_id;
        }
        return $file;
    }
}

$upload_handler = new CustomUploadHandler($options);