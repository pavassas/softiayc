<?php
/*
 * jQuery File Upload Plugin PHP Example 5.14
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */
ini_set('max_execution_time', 600);
ini_set('upload_max_filesize', '300M');
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
header("Content-Type: text/html;charset=utf-8");
date_default_timezone_set('America/Bogota');
require_once('../../../../../Classes/PHPMailer-master/class.phpmailer.php');
$options = array(
    'delete_type' => 'POST',
    'db_host' => 'localhost',
    'db_user' => 'usrpavas',
    'db_pass' => '9A12)WHFy$2p4v4s',
    'db_name' => 'iayc',
    'db_table' => 'carga_archivo',
    'upload_dir'=>'../../../iframecargar/uploads/'
);
error_reporting(0);
require('UploadHandler.php');

function MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomtii,$nomarc)
{
	$mail = new PHPMailer();						
	$mail->Body = '';
	
	$mail->From = "adminpavas@pavas.com.co";
	$mail->FromName = "I, A & C";
	$mail->AddAddress($ema, "Destino");
	$mail->Subject = $asu;
	
	// Cuerpo del mensaje
	$mail->Body .= "Hola ".$nomusu."!\n\n";	
	$mail->Body .= $men;
	$mail->Body .= "Tipo Informe: ".$nomtii."\n";
	$mail->Body .= "Nombre Archivo: ".$nomarc."\n";
	$mail->Body .= "Fecha Carga: ".date("d/m/Y H:m:s")."\n\n";
	$mail->Body .= "Este mensaje es generado automáticamente por ZONA CLIENTES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Zonaclientes \n";
	
	if(!$mail->Send())
	{
		//echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
		//echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
	}
	else
	{
	}
}

class CustomUploadHandler extends UploadHandler {

    protected function initialize() {
        $this->db = new mysqli(
            $this->options['db_host'],
            $this->options['db_user'],
            $this->options['db_pass'],
            $this->options['db_name']
        );
        parent::initialize();
        $this->db->close();
    }

    protected function handle_form_data($file, $index) {
        $file->obra = $_REQUEST['obra'];
        $file->tipoinforme = $_REQUEST['tipoinforme'];
        $file->nombre = $_REQUEST['nombre'];
        $file->comentario = $_REQUEST['comentario'];
        $file->usuario = $_REQUEST['usuario'];
    }
    
    protected function handle_file_upload($uploaded_file, $name, $size, $type, $error,
            $index = null, $content_range = null) {
        $file = parent::handle_file_upload(
            $uploaded_file, $name, $size, $type, $error, $index, $content_range
        );
        if (empty($file->error)) {
        	$array_nombre = explode('.',$file->name);
			$cuenta_arr_nombre = count($array_nombre);
			$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
	
			$conectar = mysqli_connect("localhost", "usrpavas","9A12)WHFy$2p4v4s", "iayc");
			 mysqli_query($conectar,"SET NAMES 'utf8'");
        	//$link=mysql_connect("localhost","usrpavas","9A12)WHFy$2p4v4s"); //abro la conexion
			//mysql_select_db("iayc",$link); //selecciono mi base de datos 
			
			$con = mysqli_query($conectar,"select caa_clave_int from carga_archivo order by caa_clave_int DESC LIMIT 1");
			$dato = mysqli_fetch_array($con);
			$clacaa = $dato['caa_clave_int'];
			
			if($clacaa == '' or $clacaa == 0)
			{
				$clacaa = 1;
			}
			else
			{
				$clacaa = $clacaa+1;
			}
			$ruta = "uploads/archivo".$clacaa.".".$extension;
        	
        	$fecha=date("Y/m/d H:i:s");
        	$file->fecha = $fecha;
        	$file->estado = 0;
        	$file->ruta = $ruta;
        	
            $sql = 'INSERT INTO `'.$this->options['db_table']
                .'` (`obr_clave_int`, `tii_clave_int`, `caa_nombre`, `caa_ruta`, `caa_ruta_original`, `caa_comentarios`, `caa_estado`, `caa_fecha_creacion`, `caa_usu_creacion`, `caa_usu_actualiz`, `caa_fec_actualiz`)'
                .' VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
            $query = $this->db->prepare($sql);
            $query->bind_param(
                'sisssssssss',
                $file->obra,
                $file->tipoinforme,
                $file->nombre,
                $file->ruta,
                $file->nameoriginal,
                $file->comentario,
                $file->estado,
                $file->fecha,
                utf8_decode($file->usuario),
                utf8_decode($file->usuario),
                $file->fecha
            );
            $query->execute();
            $file->id = $this->db->insert_id;
			
			$contii = mysqli_query($conectar,"select tii_nombre from tipo_informe where tii_clave_int = '".$file->tipoinforme."'");
			$datoti = mysqli_fetch_array($contii);
			$nomti = $datoti['tii_nombre'];
			
			$conobr = mysqli_query($conectar,"select obr_nombre from obra where obr_clave_int = ".$file->obra."");
			$datoobr = mysqli_fetch_array($conobr);
			$nomobr = $datoobr['obr_nombre'];
			
			$con = mysqli_query($conectar,"select * from usuario where usu_usuario = '".utf8_decode($file->usuario)."'");
			$dato = mysqli_fetch_array($con);
			$nombreusuario = $dato['usu_nombre'];
	
            $con = mysqli_query($conectar,"select * from notificar_h where not_tipo = 1");
			$dato = mysqli_fetch_array($con);
			$clanot = $dato['not_clave_int'];
			$swtodobr = $dato['not_sw_todas_obras'];
			$swtodusuper = $dato['not_sw_usuario_perfil'];
			$swact = $dato['not_sw_activo'];
			$asu = "NUEVO ARCHIVO. OBRA: ".$nomobr;
			$men = "El usuario ".$nombreusuario.", ha subido el archivo ".$file->nombre." correspondiente al proyecto ".$nomobr.". Pendiente aprobación por parte del coordinador.\n";
			
			//Si notificacion es activa Y Si son obras especificas Y Si son todos los usuarios OOO Si son Todos los perfiles
			if($swact == 1 and $swtodobr == 0 and ($swtodusuper == 1 || $swtodusuper == 3))
			{
				$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) where uo.obr_clave_int = (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_registro = '".$file->obra."' and nod_tipo = 1) and u.usu_usuario <> '".utf8_decode($file->usuario)."'");
				$num = mysqli_num_rows($con);
				
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$nomusu = $dato['usu_nombre'];
					$ema = $dato['usu_email'];
					MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomti,$file->nombre);
				}
			}
			else
			//Si notificacion es activa Y Si son obras especificas Y Si son usuarios especificos
			if($swact == 1 and $swtodobr == 0 and $swtodusuper == 2)
			{
				$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) where uo.obr_clave_int = (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_registro = '".$file->obra."' and nod_tipo = 1) and u.usu_clave_int IN (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_tipo = 2) and u.usu_usuario <> '".utf8_decode($file->usuario)."'");
				$num = mysqli_num_rows($con);
				
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$nomusu = $dato['usu_nombre'];
					$ema = $dato['usu_email'];
					MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomti,$file->nombre);
				}
			}
			else
			//Si notificacion es activa Y Si son obras especificas Y Si son perfiles especificos
			if($swact == 1 and $swtodobr == 0 and $swtodusuper == 4)
			{
				$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) where uo.obr_clave_int = (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_registro = '".$file->obra."' and nod_tipo = 1) and u.prf_clave_int IN (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_tipo = 3) and u.usu_usuario <> '".utf8_decode($file->usuario)."'");
				$num = mysqli_num_rows($con);
				
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$nomusu = $dato['usu_nombre'];
					$ema = $dato['usu_email'];
					MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomti,$file->nombre);
				}
			}
			else
			//Si notificacion es activa Y Si son todas las obras Y Si son todos los usuarios OOO Si son Todos los perfiles
			if($swact == 1 and $swtodobr == 1 and ($swtodusuper == 1 || $swtodusuper == 3))
			{
				$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) where uo.obr_clave_int = '".$file->obra."' and u.usu_usuario <> '".$file->usuario."'");
				$num = mysqli_num_rows($con);
				
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$nomusu = $dato['usu_nombre'];
					$ema = $dato['usu_email'];
					MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomti,$file->nombre);
				}
			}
			else
			//Si notificacion es activa Y Si son todas las obras Y Si son usuarios especificos
			if($swact == 1 and $swtodobr == 1 and $swtodusuper == 2)
			{
				$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) where uo.obr_clave_int = '".$file->obra."' and u.usu_clave_int IN (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_tipo = 2) and u.usu_usuario <> '".$file->usuario."'");
				$num = mysqli_num_rows($con);
				
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$nomusu = $dato['usu_nombre'];
					$ema = $dato['usu_email'];
					MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomti,$file->nombre);
				}
			}
			else
			//Si notificacion es activa Y Si son todas las obras Y Si son perfiles especificos
			if($swact == 1 and $swtodobr == 1 and $swtodusuper == 4)
			{
				$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) where uo.obr_clave_int = '".$file->obra."' and u.prf_clave_int IN (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_tipo = 3) and u.usu_usuario <> '".$file->usuario."'");
				$num = mysqli_num_rows($con);
				
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$nomusu = $dato['usu_nombre'];
					$ema = $dato['usu_email'];
					MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomti,$file->nombre);
				}
			}
            
            /*$con = mysqli_query($conectar,"select * from notificar_h where obr_clave_int = '".$file->obra."' and not_tipo = 1",$link);
			$dato = mysqli_fetch_array($con);
			$swtod = $dato['not_sw_todos'];
			$swact = $dato['not_sw_activo'];
			
			$contii = mysqli_query($conectar,"select tii_nombre from tipo_informe where tii_clave_int = '".$file->tipoinforme."'",$link);
			$datoti = mysqli_fetch_array($contii);
			$nomti = $datoti['tii_nombre'];
			
			if($swtod == 1 and $swact == 1)
			{
				$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) inner join obra o on (o.obr_clave_int = uo.obr_clave_int) where uo.obr_clave_int = '".$file->obra."' and u.usu_usuario <> '".$file->usuario."'",$link);
				$num = mysqli_num_rows($con);
				
				for($i = 0; $i < $num; $i++)
				{
					$mail = new PHPMailer();
					
					$dato = mysqli_fetch_array($con);
					$mail->Body = '';
					$ema = '';
					$nomusu = $dato['usu_nombre'];
					$ema = $dato['usu_email'];
					$nomobr = $dato['obr_nombre'];
					
					$mail->From = "adminpavas@pavas.com.co";
					$mail->FromName = "I, A & C";
					$mail->AddAddress($ema, "Destino");
					$mail->Subject = "NUEVO ".$nomti.". OBRA: ".$nomobr;
					
					// Cuerpo del mensaje
					$mail->Body .= "Hola ".$nomusu."!\n\n";	
					$mail->Body .= "ZONA CLIENTES registra que se ha publicado un nuevo ".$nomti." de su obra: ".$nomobr.".\n";
					$mail->Body .= "NOMBRE ARCHIVO: ".$file->nombre."\n";
					$mail->Body .= date("d/m/Y H:m:s")."\n\n";
					$mail->Body .= "Este mensaje es generado automáticamente por ZONA CLIENTES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Zonaclientes \n";
					
					if(!$mail->Send())
					{
						//echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
						//echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
					}
					else
					{
					}
				}
			}
			else
			if($swact == 1)
			{
				$con = mysqli_query($conectar,"select * from notificar_h nh inner join notificar_d nd on (nd.not_clave_int = nh.not_clave_int) inner join usuario u on (u.usu_clave_int = nd.usu_clave_int) inner join obra o on (o.obr_clave_int = nh.obr_clave_int) where nh.obr_clave_int = '".$file->obra."' and nh.not_tipo = 1 and u.usu_usuario <> '".$file->usuario."'",$link);
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$mail = new PHPMailer();
					
					$dato = mysqli_fetch_array($con);
					$mail->Body = '';
					$ema = '';
					$nomusu = $dato['usu_nombre'];
					$ema = $dato['usu_email'];
					$nomobr = $dato['obr_nombre'];
					
					$mail->From = "adminpavas@pavas.com.co";
					$mail->FromName = "I, A & C";
					$mail->AddAddress($ema, "Destino");
					$mail->Subject = "NUEVO ".$nomti.". OBRA: ".$nomobr;
					
					// Cuerpo del mensaje
					$mail->Body .= "Hola ".$nomusu."!\n\n";	
					$mail->Body .= "ZONA CLIENTES registra que se ha publicado un nuevo ".$nomti." de su obra: ".$nomobr.".\n";
					$mail->Body .= "NOMBRE ARCHIVO: ".$file->nombre."\n";
					$mail->Body .= date("d/m/Y H:m:s")."\n\n";
					$mail->Body .= "Este mensaje es generado automáticamente por ZONA CLIENTES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Zonaclientes \n";
					
					if(!$mail->Send())
					{
						//echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
						//echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
					}
					else
					{
					}
				}
			}*/
        }
        return $file;
    }
}

$upload_handler = new CustomUploadHandler($options);