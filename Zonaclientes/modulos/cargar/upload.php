<?php
define('WP_MEMORY_LIMIT', '128M');
ini_set('memory_limit', '128M');
include('../../data/Conexion.php');
session_start();
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_SESSION['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];

$fecha=date("Y/m/d H:i:s");
$i = $_GET['i'];
$nom = $_GET['nom'];
$obr = $_GET['obr'];
$ti = $_GET['ti'];
$com = $_GET['com'];
$act = $_GET['act'];
$ci = $_GET['ci'];

if(is_array($_FILES)) 
{
	if(is_uploaded_file($_FILES['adjunto'.$i]['tmp_name'])) 
	{
		$file = $_FILES["adjunto".$i]["name"];
		$sourcePath = $_FILES['adjunto'.$i]['tmp_name'];
		$archivo = basename($_FILES['adjunto'.$i]['name']);
		
		$array_nombre = explode('.',$archivo);
		$cuenta_arr_nombre = count($array_nombre);
		$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
		
		if($act == 'SI')
		{
			$clacaa = $ci;
		}
		else
		{
			$con = mysqli_query($conectar,"select caa_clave_int from carga_archivo where caa_usu_actualiz = '".$usuario."' order by caa_clave_int DESC LIMIT 1");
			$dato = mysqli_fetch_array($con);
			$clacaa = $dato['caa_clave_int'];
			
			if($clacaa == '' or $clacaa == 0)
			{
				$clacaa = 1;
			}
			else
			{
				$clacaa = $clacaa+1;
			}
		}
		
		$destino =  "iframecargar/uploads/archivo".$clacaa.".".$extension;
		$ruta = "uploads/archivo".$clacaa.".".$extension;
		
		if(move_uploaded_file($sourcePath,$destino)) 
		{
			if($act == 'SI')
			{
				mysqli_query($conectar,"update carga_archivo set obr_clave_int = '".$obr."', tii_clave_int = '".$ti."', caa_nombre = '".$nom."', caa_ruta_original = '".$file."', caa_comentarios = '".$com."', caa_usu_actualiz = '".$usuario."', caa_fec_actualiz = '".$fecha."' where caa_clave_int = '".$ci."'");
			}
			else
			{
				mysqli_query($conectar,"INSERT INTO carga_archivo(caa_clave_int,obr_clave_int,tii_clave_int,caa_nombre,caa_ruta,caa_ruta_original,caa_comentarios,caa_fecha_creacion,caa_usu_creacion,caa_usu_actualiz,caa_fec_actualiz) values(null,'".$obr."','".$ti."','".$nom."','".$ruta."','".$file."','".$com."','".$fecha."','".$usuario."','".$usuario."','".$fecha."')");
			}
			if($extension == 'jpg' or $extension == 'jpeg' or $extension == 'JPG' or $extension == 'JPEG')
			{
				//Reduzco el tamano de la imagen
				$img_origen = imagecreatefromjpeg($destino);
				$ancho_origen = imagesx($img_origen);//Se obtiene el ancho de la imagen
				$alto_origen = imagesy($img_origen);//Se obtiene el alto de la imagen
				$ancho_limite = 850;
				
				if($ancho_origen > $ancho_limite)//Para foto horizontal
				{
					$ancho_origen = $ancho_limite;
					$alto_origen = $ancho_limite*imagesy($img_origen)/imagesx($img_origen);
				}
				else//Para fotos verticales
				{
					$alto_origen = $ancho_limite;
					$ancho_origen = $ancho_limite*imagesx($img_origen)/imagesy($img_origen);
				}
				$img_destino = imagecreatetruecolor($ancho_origen, $alto_origen);//Se crea la imagen segun las dimensiones dadas
				imagecopyresized($img_destino, $img_origen, 0, 0, 0, 0, $ancho_origen, $alto_origen, imagesx($img_origen), imagesy($img_origen));
				imagejpeg($img_destino,$destino);//Se guarda la nueva foto
			}
			else
			if($extension == 'png' or $extension == 'PNG')
			{
				//Reduzco el tamano de la imagen
				$img_origen = imagecreatefrompng($destino);
				$ancho_origen = imagesx($img_origen);//Se obtiene el ancho de la imagen
				$alto_origen = imagesy($img_origen);//Se obtiene el alto de la imagen
				$ancho_limite = 850;
				
				if($ancho_origen > $ancho_limite)//Para foto horizontal
				{
					$ancho_origen = $ancho_limite;
					$alto_origen = $ancho_limite*imagesy($img_origen)/imagesx($img_origen);
				}
				else//Para fotos verticales
				{
					$alto_origen = $ancho_limite;
					$ancho_origen = $ancho_limite*imagesx($img_origen)/imagesy($img_origen);
				}
				$img_destino = imagecreatetruecolor($ancho_origen, $alto_origen);//Se crea la imagen segun las dimensiones dadas
				imagecopyresized($img_destino, $img_origen, 0, 0, 0, 0, $ancho_origen, $alto_origen, imagesx($img_origen), imagesy($img_origen));
				imagepng($img_destino,$destino);//Se guarda la nueva foto
			}
			else
			if($extension == 'gif' or $extension == 'GIF')
			{
				//Reduzco el tamano de la imagen
				$img_origen = imagecreatefromgif($destino);
				$ancho_origen = imagesx($img_origen);//Se obtiene el ancho de la imagen
				$alto_origen = imagesy($img_origen);//Se obtiene el alto de la imagen
				$ancho_limite = 850;
				
				if($ancho_origen > $ancho_limite)//Para foto horizontal
				{
					$ancho_origen = $ancho_limite;
					$alto_origen = $ancho_limite*imagesy($img_origen)/imagesx($img_origen);
				}
				else//Para fotos verticales
				{
					$alto_origen = $ancho_limite;
					$ancho_origen = $ancho_limite*imagesx($img_origen)/imagesy($img_origen);
				}
				$img_destino = imagecreatetruecolor($ancho_origen, $alto_origen);//Se crea la imagen segun las dimensiones dadas
				imagecopyresized($img_destino, $img_origen, 0, 0, 0, 0, $ancho_origen, $alto_origen, imagesx($img_origen), imagesy($img_origen));
				imagegif($img_destino,$destino);//Se guarda la nueva foto
			}
		}
	}
}
?>