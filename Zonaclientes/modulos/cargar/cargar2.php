<?php
	error_reporting(0);
	include('../../data/Conexion.php');
	require_once('../../Classes/PHPMailer-master/class.phpmailer.php');
	?>
	<div style="display:none">
	<?php
	session_start();
	?>
	</div>
	<?php
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_SESSION['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
		
	// verifica si no se ha loggeado
	if(!isset($_SESSION["persona"]))
	{
	  session_destroy();
	  header("LOCATION:index.php");
	}else{
	}
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");
	
	$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
	$dato = mysqli_fetch_array($con);
	$descripcionperfil = $dato['prf_descripcion'];
	$claveperfil = $dato['prf_clave_int'];
	$claveusuario = $dato['usu_clave_int'];
	$aprueba = $dato['prf_sw_aprobar'];
	$ultimaobra = $dato['obr_clave_int'];
	$ultimoestado = $dato['usu_ultimo_estado'];
	
	if($_GET['nuevacarga'] == 'si')
	{
		$fecha=date("Y/m/d H:i:s");
		$nom = $_GET['nom'];
		$obr = $_GET['obr'];
		$tii = $_GET['tii'];
		
		if($obr == '')
		{
			echo "<div class='validaciones'>Debe elegir la Obra</div>";
		}
		else
		if($tii == '')
		{
			echo "<div class='validaciones'>Debe elegir el tipo de informe</div>";
		}
		else
		if($nom == '')
		{
			echo "<div class='validaciones'>Debe ingresar el Nombre</div>";
		}
		else
		{
			$sql = mysqli_query($conectar,"insert into carga(car_clave_int,car_nombre,obr_clave_int,tii_clave_int,car_fecha_creacion,car_usu_creacion,car_usu_actualiz,car_fec_actualiz) values(null,'".$nom."','".$obr."','".$tii."','".$fecha."','".$usuario."','".$usuario."','".$fecha."')");
			if($sql > 0)
			{
				$conobr = mysqli_query($conectar,"select obr_nombre from obra where obr_clave_int = ".$obr."");
				$datoobr = mysqli_fetch_array($conobr);
				$nomobr = $datoobr['obr_nombre'];
				mysqli_query($conectar,"update usuario set car_clave_int = 0 where usu_usuario = '".$usuario."'");
				echo "<div style='font-size:medium' class='ok' align='left'>Agregar imagenes a la obra: ".$nomobr."</div>";
				?>
				<iframe src="iframecargar/index.php" frameborder="1" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen style="width: 100%; height:360px; border-style:dashed;overflow:hidden; overflow-y:hidden; overflow-x:hidden;" scrolling=no></iframe>
				<br><br>
				<div style="float:left" id="fotosagregadas">
				</div>
				<?php
				$con = mysqli_query($conectar,"select * from notificar_h where obr_clave_int = '".$obr."' and not_tipo = 1");
				$dato = mysqli_fetch_array($con);
				$swtod = $dato['not_sw_todos'];
				$swact = $dato['not_sw_activo'];
				
				if($swtod == 1 and $swact == 1)
				{
					$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) where uo.obr_clave_int = '".$obr."' and u.usu_clave_int <> '".$claveusuario."'");
					$num = mysqli_num_rows($con);
					
					for($i = 0; $i < $num; $i++)
					{
						$mail = new PHPMailer();
						
						$dato = mysqli_fetch_array($con);
						$mail->Body = '';
						$ema = '';
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						
						$mail->From = "adminpavas@pavas.com.co";
						$mail->FromName = "I, A & C";
						$mail->AddAddress($ema, "Destino");
						$mail->Subject = "NUEVO REGISTRO FOTOGRAFICO. OBRA: ".$nomobr;
						
						// Cuerpo del mensaje
						$mail->Body .= "Hola ".$nomusu."!\n\n";	
						$mail->Body .= "ZONA CLIENTES registra que se ha publicado un nuevo REGISTRO FOTOGRAFICO de su obra: ".$nomobr.".\n";
						$mail->Body .= "NOMBRE ARCHIVO: ".$nom."\n";
						$mail->Body .= date("d/m/Y H:m:s")."\n\n";
						$mail->Body .= "Este mensaje es generado automáticamente por ZONA CLIENTES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Zonaclientes \n";
						
						if(!$mail->Send())
						{
							//echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
							//echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
						}
						else
						{
						}
					}
				}
				else
				if($swact == 1)
				{
					$con = mysqli_query($conectar,"select * from notificar_h nh inner join notificar_d nd on (nd.not_clave_int = nh.not_clave_int) inner join usuario u on (u.usu_clave_int = nd.usu_clave_int) where nh.obr_clave_int = '".$obr."' and nh.not_tipo = 1 and u.usu_clave_int <> '".$claveusuario."'");
					$num = mysqli_num_rows($con);
					for($i = 0; $i < $num; $i++)
					{
						$mail = new PHPMailer();
						
						$dato = mysqli_fetch_array($con);
						$mail->Body = '';
						$ema = '';
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						
						$mail->From = "adminpavas@pavas.com.co";
						$mail->FromName = "I, A & C";
						$mail->AddAddress($ema, "Destino");
						$mail->Subject = "NUEVO REGISTRO FOTOGRAFICO. OBRA: ".$nomobr;
						
						// Cuerpo del mensaje
						$mail->Body .= "Hola ".$nomusu."!\n\n";	
						$mail->Body .= "ZONA CLIENTES registra que se ha publicado un nuevo REGISTRO FOTOGRAFICO de su obra: ".$nomobr.".\n";
						$mail->Body .= "NOMBRE ARCHIVO: ".$nom."\n";
						$mail->Body .= date("d/m/Y H:m:s")."\n\n";
						$mail->Body .= "Este mensaje es generado automáticamente por ZONA CLIENTES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Zonaclientes \n";
						
						if(!$mail->Send())
						{
							//echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
							//echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
						}
						else
						{
						}
					}
				}
			}
			else
			{
				echo "<div class='validaciones'>Hubo un problema al guardar los datos, porfavor intentelo de nuevo o contacte a su proveedor.</div>";
			}
		}
		exit();
	}
	if($_GET['refrescartodos'] == 'si')
	{
		?>
		<table style="width: 100%">
			<tr>
				<td style="width: 90px; cursor:pointer;<?php if($ultimoestado == 'NUEVO'){ echo 'background-color:#092451;color:#ffffff'; } ?>" class="section" onclick="NUEVACARGA()" onmouseover="this.style.backgroundColor='#092451';this.style.color='#ffffff';"  onmouseout="<?php if($ultimoestado <> 'NUEVO'){ echo "this.style.backgroundColor='#ffffff';this.style.color='#000000'"; } ?>">
				Nuevo
				</td>
				<td style="width: 90px; cursor:pointer;<?php if($ultimoestado == 'TODOS'){ echo 'background-color:#092451;color:#ffffff'; } ?>" class="section" onclick="VERTODOS('T')" onmouseover="this.style.backgroundColor='#092451';this.style.color='#ffffff';"  onmouseout="<?php if($ultimoestado <> 'TODOS'){ echo "this.style.backgroundColor='#ffffff';this.style.color='#000000'"; } ?>">
				<div id="todos">
				Todos 
				<?php 
				$con = mysqli_query($conectar,"select * from carga where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.")");
				$num = mysqli_num_rows($con);
				$con = mysqli_query($conectar,"select * from carga_archivo where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.")");
				$num1 = mysqli_num_rows($con);
				echo $num+$num1;
				?>
				</div>
				</td>
				<td style="width: 160px; cursor:pointer;<?php if($ultimoestado == 'PENDIENTES'){ echo 'background-color:#092451;color:#ffffff'; } ?>" class="section" onclick="VERTODOS('0')" onmouseover="this.style.backgroundColor='#092451';this.style.color='#ffffff';"  onmouseout="<?php if($ultimoestado <> 'PENDIENTES'){ echo "this.style.backgroundColor='#ffffff';this.style.color='#000000'"; } ?>">
				<div id="todos">
				Pendiente coordinador 
				<?php 
				$con = mysqli_query($conectar,"select * from carga where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and car_estado = 0");
				$num = mysqli_num_rows($con);
				$con = mysqli_query($conectar,"select * from carga_archivo where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and caa_estado = 0");
				$num1 = mysqli_num_rows($con);
				echo $num+$num1;
				?>
				</div>
				</td>
				<td style="width: 100px; cursor:pointer;<?php if($ultimoestado == 'APROBADOS'){ echo 'background-color:#092451;color:#ffffff'; } ?>" class="section" onclick="VERTODOS('1')" onmouseover="this.style.backgroundColor='#092451';this.style.color='#ffffff';"  onmouseout="<?php if($ultimoestado <> 'APROBADOS'){ echo "this.style.backgroundColor='#ffffff';this.style.color='#000000'"; } ?>">
				<div id="todos">
				Aprobados 
				<?php 
				$con = mysqli_query($conectar,"select * from carga where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and car_estado = 1");
				$num = mysqli_num_rows($con);
				$con = mysqli_query($conectar,"select * from carga_archivo where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and caa_estado = 1");
				$num1 = mysqli_num_rows($con);
				echo $num+$num1;
				?>
				</div>
				</td>
				<td class="auto-style2" style="width: 600px"><strong>CARGAR </strong> </td>
				<td>&nbsp;</td>
			</tr>
		</table>
		<?php
		exit();
	}
	if($_GET['crearnuevacarga'] == 'si')
	{
		mysqli_query($conectar,"update usuario set usu_ultimo_estado = 'NUEVO' where usu_usuario = '".$usuario."'");
		?>
		<table style="width: 100%" class="section">
			<tr>
				<td align="center" style="text-align:center">
				<table style="width: 45%" align="center">
					<tr>
						<td class="auto-style1"><strong>Obra:</strong></td>
						<td class="auto-style1">
						<select name="obra" id="obra" class="inputs" style="width: 210px">
						<option value="">-Seleccione-</option>
						<?php
							$con = mysqli_query($conectar,"select * from obra where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = '".$claveusuario."') order by obr_nombre");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['obr_clave_int'];
								$nombre = $dato['obr_nombre'];
						?>
							<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
						<?php
							}
						?>
						</select>	
						</td>
						<td class="auto-style1" rowspan="3">
						<div id="guardar">
						<input name="botonguardar" id="botonguardar" class="inputs" type="button" value="GUARDAR" onclick="NUEVO()" style="width: 100px; height: 60px; cursor:pointer" />
						</div>
						</td>
						<td class="auto-style1" rowspan="3">
						<div id="estadoactualizar"></div>
						</td>
					</tr>
					<tr>
						<td class="auto-style1"><strong>Tipo informe:</strong></td>
						<td class="auto-style1">
						<select name="tipoinforme" id="tipoinforme" onchange="VALIDAR(this.value)" class="inputs" style="width: 210px">
						<option value="">-Seleccione-</option>
						<?php
							$con = mysqli_query($conectar,"select * from tipo_informe order by tii_nombre");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['tii_clave_int'];
								$nombre = $dato['tii_nombre'];
						?>
							<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
						<?php
							}
						?>
						</select>
						</td>
					</tr>
					<tr>
						<td class="auto-style1"><strong>Nombre:</strong></td>
						<td class="auto-style1">
						<input name="nombre" id="nombre" spellcheck="false" class="inputs" type="text" style="width: 200px" />
						</td>
					</tr>
					</table>
				
				</td>
			</tr>
			<tr>
				<td align="center" style="text-align:center">
				<hr/>
				</td>
			</tr>
			<tr>
				<td>
				
				<table style="width: 100%">
					<tr>
						<td>
						<div id="opcioncarga1" class="inputs" style="background-color:#092451;width:10%;color:white;cursor:pointer;float:left;display:none;text-align:center" onclick="VALIDAR('')">METODO 1</div> 
						<div id="opcioncarga2" class="inputs" style="background-color:#092451;width:10%;color:white;cursor:pointer;float:left;display:none;margin-left:10px;text-align:center" onclick="OPCION2()">METODO 2</div>
						</td>
					</tr>
					<tr>
						<td>
						<div id="fotos">
						<div style="width: 100%; height:360px; border-style:dashed;overflow:hidden;cursor:pointer" onclick="ALERTA()"></div>
						</div>
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		<?php
		exit();
	}
	if($_GET['vertodos'] == 'si')
	{
		$est = $_GET['est'];
		if($est == 'T'){ $ue = 'TODOS'; }elseif($est == '0'){ $ue = 'PENDIENTES'; }elseif($est == '1'){ $ue = 'APROBADOS'; }
		mysqli_query($conectar,"update usuario set usu_ultimo_estado = '".$ue."' where usu_usuario = '".$usuario."'");
		
		$sql1 = mysqli_query($conectar,"select * from carga");
		$num1 = mysqli_num_rows($sql1);
		$sql2 = mysqli_query($conectar,"select * from carga_archivo");
		$num2 = mysqli_num_rows($sql2);
		$total = $num1+$num2;
		?>
			<fieldset name="Group1">
				<legend><strong>FILTRO</strong> <img src="../../images/buscar.png" alt="" height="18" width="15" /></legend>
				<table style="width: 100%" class="filtros">
				<tr>
					<td align="left">
					<table style="width: 100%">
						<tr>
							<td><strong>OBRA:</strong></td>
							<td><strong>TIPO INFORME:</strong></td>
							<td><strong>NOMBRE ARCHIVO:</strong></td>
						</tr>
						<tr>
							<td>
							<select multiple="multiple" onchange="BUSCAR()" name="busobra" id="busobra" style="width:315px">
							<?php
								$con = mysqli_query($conectar,"select * from obra where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = '".$claveusuario."') order by obr_nombre");
								$num = mysqli_num_rows($con);
								for($i = 0; $i < $num; $i++)
								{
									$dato = mysqli_fetch_array($con);
									$clave = $dato['obr_clave_int'];
									$nombre = $dato['obr_nombre'];
							?>
								<option value="<?php echo $clave; ?>" selected="selected"><?php echo $nombre; ?></option>
							<?php
								}
							?>
							</select>
						</td>
						<td>
						<select multiple="multiple" onchange="BUSCAR()" name="bustipoinforme" id="bustipoinforme" style="width:315px" name="D1">
						<?php
							$con = mysqli_query($conectar,"select * from tipo_informe order by tii_nombre");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['tii_clave_int'];
								$nombre = $dato['tii_nombre'];
						?>
							<option value="<?php echo $clave; ?>" selected="selected"><?php echo $nombre; ?></option>
						<?php
							}
						?>
						</select>
						</td>
						<td>
							<input name="busnombrearchivo" id="busnombrearchivo" onkeyup="BUSCAR()" type="text" class="inputs" style="width: 315px">
						</td>
						</tr>
						<tr>
							<td style="height: 3px">
							<strong>NOMBRE ANEXO:
							</strong>
							</td>
							<td style="height: 3px">
							<strong>LEYENDA:</strong></td>
							<td style="height: 3px">
							<strong>COMENTARIOS:</strong></td>
						</tr>
						<tr>
							<td>
							<input name="busnombreanexo" id="busnombreanexo" onkeyup="BUSCAR()" type="text" class="inputs" style="width: 315px; height: 36px;"></td>
							<td>
							<input name="busleyenda" id="busleyenda" onkeyup="BUSCAR()" type="text" class="inputs" style="width: 315px; height: 36px;"></td>
							<td>
							<textarea cols="20" name="buscomentario" id="buscomentario" onkeyup="BUSCAR()" class="inputs" style="width: 315px; height: 36px;"></textarea>
							</td>
						</tr>
					</table>
					</td>
				</tr>
				</table>
			</fieldset>
			<div id="resultadobusqueda">
			<div class="inputs" style="background-color:#E6E8E8">
				<table style="width: 10%">
					<tr>
						<td>&nbsp;</td>
						<td><input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<?php echo $total; ?>,this);" /></td>
						<td>
						<?php
						if($est == '0' and $aprueba == 1)
						{
						?>
						<img style="cursor:pointer" src="../../images/aprobar.png" title="Aprobar" onclick="APROBARINFORMESMASIVO()" height="30" width="29"></td>
						<?php
						}
						?>
						<td>
						<img style="cursor:pointer" src="../../images/delete.png" height="30" width="29" title="ELIMINAR" onclick="ELIMINARINFORMESMASIVO()">
						</td>
						<td><div id="estadoaprobacion" style="width: 500px"></div></td>
					</tr>
				</table>
			</div>
			<fieldset name="Group1">
				<legend><strong>COMITES DE OBRA</strong></legend>
			<table style="width: 100%" class="section">
			<tr>
				<td class="auto-style4">
				
				</td>
				<td align="left" style="text-align:left">
				
				</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4" style="width: 37px">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
			</tr>
			<tr>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4"><strong>NOMBRE DEL ARCHIVO</strong></td>
				<td class="auto-style4"><strong>OBRA</strong></td>
				<td class="auto-style4"><strong>COMENTARIOS</strong></td>
				<td class="auto-style4"><strong>FECHA CREACIÓN</strong></td>
				<td class="auto-style4"><strong>ACTUALIZADO POR</strong></td>
				<td></td>
				<td style="width: 37px">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="13">
				<hr>
				</td>
			</tr>
			<?php
			$contador=0;
			if($est == 'T')
			{
				$con = mysqli_query($conectar,"select * from carga_archivo ca inner join obra o on (o.obr_clave_int = ca.obr_clave_int) inner join tipo_informe ti on (ti.tii_clave_int = ca.tii_clave_int) where ca.obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and ca.tii_clave_int = 2 order by DATE(SUBSTRING(ca.caa_nombre,1,8)) DESC");
			}
			else
			{
				$con = mysqli_query($conectar,"select * from carga_archivo ca inner join obra o on (o.obr_clave_int = ca.obr_clave_int) inner join tipo_informe ti on (ti.tii_clave_int = ca.tii_clave_int) where ca.obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and caa_estado = '".$est."' and ca.tii_clave_int = 2 order by DATE(SUBSTRING(ca.caa_nombre,1,8)) DESC");
			}
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clacaa = $dato['caa_clave_int'];
				$nom = $dato['caa_nombre'];
				$obr = $dato['obr_nombre'];
				$tii = $dato['tii_nombre'];
				$rut = $dato['caa_ruta'];
				$com = $dato['caa_comentarios'];
				$estado = $dato['caa_estado'];
				$fec = $dato['caa_fecha_creacion'];
				$usuact = $dato['caa_usu_actualiz'];
				$fecact = $dato['caa_fec_actualiz'];
				
				$array_nombre = explode('.',$rut);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
								
				$conane = mysqli_query($conectar,"select * from anexos_archivo where caa_clave_int = '".$clacaa."'");
				$numane = mysqli_num_rows($conane);
				$contador=$contador+1;
			?>
				<tr style="<?php if($i % 2 == 0){ echo 'background-color:silver'; } ?>" id="obra<?php echo $clacaa; ?>">
					<td class="auto-style1">
					<input onclick="contadorVals(this);" type="checkbox" name="idcatid" id="idcat<?php echo $contador; ?>" value="<?php echo "OINFORME".$dato['caa_clave_int'];?>" />
					</td>
					<td class="auto-style1"><?php echo $nom.".".$extension; ?></td>
					<td class="auto-style1"><?php echo $obr; ?></td>
					<td class="auto-style1"><div style="cursor:pointer" onmouseover="javascript: VentanaFlotante('<?php echo $com; ?>', 300, 120)" onmouseout="javascript: quitarDiv();"><?php echo substr($com,0,50); ?></div></td>
					<td class="auto-style1"><?php echo $fec; ?></td>
					<td class="auto-style1"><?php echo $usuact; ?></td>
					<td>
					<?php
					if(($estado == '1' and strtoupper($descripcionperfil) == strtoupper('Administrador')) or ($estado == '0'))
					{
					?>
					<img src="../../images/editar.png" alt="" height="30" width="29" onclick="EDITARARCHIVO('<?php echo $clacaa; ?>')" style="cursor:pointer" title="EDITAR" />
					<?php
					}
					?>
					</td>
					<td class="fila" style="background-color:#092451;color:white;width:37px;cursor:pointer">
					<a <?php if($rut != ''){ echo "href='iframecargar/$rut'"; }else{ echo "onclick='ALERTASINADJUNTO()'"; } ?> target='_blank' style="color:white;text-decoration:none;text-shadow: 1px 1px 1px #aaa">
					VER ONLINE</a>
					</td>
					<td <?php if($numane > 0){ echo 'class="fila"'; echo 'style="background-color:#092451;color:white;width:37px;cursor:pointer"'; } ?> onclick="MOSTRARMOVIMIENTOANEXO('<?php echo $clacaa; ?>');OCULTARSCROLL()">
					<?php
					if($numane > 0)
					{
					?>
					<div id="verocultaranexo<?php echo $clacaa; ?>" style="text-shadow: 1px 1px 1px #aaa">VER ANEXOS</div><input name="ocultoanexos<?php echo $clacaa; ?>" id="ocultoanexos<?php echo $clacaa; ?>" value="0" type="hidden" />
					<?php
					}
					?>
					</td>
					<td class="fila" style="background-color:#092451;color:white;width:37px;cursor:pointer">
					<a href='descargar.php?clacaa=<?php echo $clacaa; ?>' target='_blank' style="color:white;text-decoration:none;text-shadow: 1px 1px 1px #aaa">
					DESCARGAR</a>
					</td>
					<td>
					<a href='descargarcomitezip.php?clacaa=<?php echo $clacaa; ?>' target='_blank'>
					<img style="cursor:pointer" src="../../images/zip.png" title="Descargar todo" height="30" width="35"></a></td>
					<td>
					<?php
					if($est == '0' and $aprueba == 1)
					{
					?>
					<img style="cursor:pointer" src="../../images/aprobar.png" title="Aprobar" onclick="APROBARCOMITE('<?php echo $clacaa; ?>','obra')" height="30" width="29">
					<?php
					}
					?>
					</td>
					<td>
					<?php
					if(($estado == '1' and strtoupper($descripcionperfil) == strtoupper('Administrador')) or ($estado == '0'))
					{
					?>
					<img style="cursor:pointer" src="../../images/delete.png" height="30" width="29" title="ELIMINAR" onclick="ELIMINARARCHIVO('<?php echo $clacaa; ?>','LISTA','<?php echo $i; ?>','obra')">
					<?php
					}
					?>
					</td>
				</tr>
				<tr>
					<td class="auto-style1">
					&nbsp;</td>
					<td class="auto-style1" colspan="12">
					<div id="movimientoanexo<?php echo $clacaa; ?>"></div>
					</td>
				</tr>
			<?php
			}
			?>
			</table>
			</fieldset>
			<fieldset name="Group1">
				<legend><strong>COMITES TECNICOS</strong></legend>
			<table style="width: 100%" class="section">
			<tr>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4" style="width: 37px">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
			</tr>
			<tr>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4"><strong>NOMBRE DEL ARCHIVO</strong></td>
				<td class="auto-style4"><strong>OBRA</strong></td>
				<td class="auto-style4"><strong>COMENTARIOS</strong></td>
				<td class="auto-style4"><strong>FECHA CREACIÓN</strong></td>
				<td class="auto-style4"><strong>ACTUALIZADO POR</strong></td>
				<td></td>
				<td style="width: 37px">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="14">
				<hr>
				</td>
			</tr>
			<?php
			if($est == 'T')
			{
				$con = mysqli_query($conectar,"select * from carga_archivo ca inner join obra o on (o.obr_clave_int = ca.obr_clave_int) inner join tipo_informe ti on (ti.tii_clave_int = ca.tii_clave_int) where ca.obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and ca.tii_clave_int = 3 order by DATE(SUBSTRING(ca.caa_nombre,1,8)) DESC");
			}
			else
			{
				$con = mysqli_query($conectar,"select * from carga_archivo ca inner join obra o on (o.obr_clave_int = ca.obr_clave_int) inner join tipo_informe ti on (ti.tii_clave_int = ca.tii_clave_int) where ca.obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and caa_estado = '".$est."' and ca.tii_clave_int = 3 order by DATE(SUBSTRING(ca.caa_nombre,1,8)) DESC");
			}
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clacaa = $dato['caa_clave_int'];
				$nom = $dato['caa_nombre'];
				$obr = $dato['obr_nombre'];
				$tii = $dato['tii_nombre'];
				$rut = $dato['caa_ruta'];
				$com = $dato['caa_comentarios'];
				$estado = $dato['caa_estado'];
				$fec = $dato['caa_fecha_creacion'];
				$usuact = $dato['caa_usu_actualiz'];
				$fecact = $dato['caa_fec_actualiz'];
				
				$array_nombre = explode('.',$rut);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
								
				$conane = mysqli_query($conectar,"select * from anexos_archivo where caa_clave_int = '".$clacaa."'");
				$numane = mysqli_num_rows($conane);
				$contador=$contador+1;
			?>
				<tr style="<?php if($i % 2 == 0){ echo 'background-color:silver'; } ?>" id="tecnico<?php echo $clacaa; ?>">
					<td class="auto-style1">
					<input onclick="contadorVals(this);" type="checkbox" name="idcatid" id="idcat<?php echo $contador; ?>" value="<?php echo "CINFORME".$dato['caa_clave_int'];?>" />
					</td>
					<td class="auto-style1"><?php echo $nom.".".$extension; ?></td>
					<td class="auto-style1"><?php echo $obr; ?></td>
					<td class="auto-style1"><div style="cursor:pointer" onmouseover="javascript: VentanaFlotante('<?php echo $com; ?>', 300, 120)" onmouseout="javascript: quitarDiv();"><?php echo substr($com,0,50); ?></div></td>
					<td class="auto-style1"><?php echo $fec; ?></td>
					<td class="auto-style1"><?php echo $usuact; ?></td>
					<td>
					<?php
					if(($estado == '1' and strtoupper($descripcionperfil) == strtoupper('administrador')) or ($estado == '0'))
					{
					?>
					<img src="../../images/editar.png" alt="" height="30" width="29" onclick="EDITARARCHIVO('<?php echo $clacaa; ?>')" style="cursor:pointer" title="EDITAR" />
					<?php
					}
					?>
					</td>
					<td class="fila" style="background-color:#092451;color:white;width:37px;cursor:pointer">
					<a <?php if($rut != ''){ echo "href='iframecargar/$rut'"; }else{ echo "onclick='ALERTASINADJUNTO()'"; } ?> target='_blank' style="color:white;text-decoration:none;text-shadow: 1px 1px 1px #aaa">
					VER ONLINE</a>
					</td>
					<td <?php if($numane > 0){ echo 'class="fila"'; echo 'style="background-color:#092451;color:white;width:37px;cursor:pointer"'; } ?> onclick="MOSTRARMOVIMIENTOANEXO('<?php echo $clacaa; ?>');OCULTARSCROLL()">
					<?php
					if($numane > 0)
					{
					?>
					<div id="verocultaranexo<?php echo $clacaa; ?>" style="text-shadow: 1px 1px 1px #aaa">VER ANEXOS</div><input name="ocultoanexos<?php echo $clacaa; ?>" id="ocultoanexos<?php echo $clacaa; ?>" value="0" type="hidden" />
					<?php
					}
					?>
					</td>
					<td class="fila" style="background-color:#092451;color:white;width:37px;cursor:pointer">
					<a href='descargar.php?clacaa=<?php echo $clacaa; ?>' target='_blank' style="color:white;text-decoration:none;text-shadow: 1px 1px 1px #aaa">
					DESCARGAR</a>
					</td>
					<td>
					<a href='descargarcomitezip.php?clacaa=<?php echo $clacaa; ?>' target='_blank'>
					<img style="cursor:pointer" src="../../images/zip.png" title="Descargar todo" height="30" width="35"></a></td>
					<td>
					<?php
					if($est == '0' and $aprueba == 1)
					{
					?>
					<img style="cursor:pointer" src="../../images/aprobar.png" title="Aprobar" onclick="APROBARCOMITE('<?php echo $clacaa; ?>','tecnico')" height="30" width="29">
					<?php
					}
					?>
					</td>
					<td>
					<?php
					if(($estado == '1' and strtoupper($descripcionperfil) == strtoupper('administrador')) or ($estado == '0'))
					{
					?>
					<img style="cursor:pointer" src="../../images/delete.png" height="30" width="29" title="ELIMINAR" onclick="ELIMINARARCHIVO('<?php echo $clacaa; ?>','LISTA','<?php echo $i; ?>','tecnico')">
					<?php
					}
					?>
					</td>
				</tr>
				<tr>
					<td class="auto-style1">
					&nbsp;</td>
					<td class="auto-style1" colspan="12">
					<div id="movimientoanexo<?php echo $clacaa; ?>"></div>
					</td>
					<td class="auto-style1">
					&nbsp;</td>
				</tr>
			<?php
			}
			?>
			</table>
			</fieldset>
			<fieldset name="Group1">
				<legend><strong>INFORMES MENSUALES</strong></legend>
			<table style="width: 100%" class="section">
			<tr>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4" style="width: 37px">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
			</tr>
			<tr>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4"><strong>NOMBRE DEL ARCHIVO</strong></td>
				<td class="auto-style4"><strong>OBRA</strong></td>
				<td class="auto-style4"><strong>COMENTARIOS</strong></td>
				<td class="auto-style4"><strong>FECHA CREACIÓN</strong></td>
				<td class="auto-style4"><strong>ACTUALIZADO POR</strong></td>
				<td></td>
				<td style="width: 37px">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="14">
				<hr>
				</td>
			</tr>
			<?php
			if($est == 'T')
			{
				$con = mysqli_query($conectar,"select * from carga_archivo ca inner join obra o on (o.obr_clave_int = ca.obr_clave_int) inner join tipo_informe ti on (ti.tii_clave_int = ca.tii_clave_int) where ca.obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and ca.tii_clave_int = 4 order by DATE(SUBSTRING(ca.caa_nombre,1,8)) DESC");
			}
			else
			{
				$con = mysqli_query($conectar,"select * from carga_archivo ca inner join obra o on (o.obr_clave_int = ca.obr_clave_int) inner join tipo_informe ti on (ti.tii_clave_int = ca.tii_clave_int) where ca.obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and caa_estado = '".$est."' and ca.tii_clave_int = 4 order by DATE(SUBSTRING(ca.caa_nombre,1,8)) DESC");
			}
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clacaa = $dato['caa_clave_int'];
				$nom = $dato['caa_nombre'];
				$obr = $dato['obr_nombre'];
				$tii = $dato['tii_nombre'];
				$rut = $dato['caa_ruta'];
				$com = $dato['caa_comentarios'];
				$estado = $dato['caa_estado'];
				$fec = $dato['caa_fecha_creacion'];
				$usuact = $dato['caa_usu_actualiz'];
				$fecact = $dato['caa_fec_actualiz'];
				
				$array_nombre = explode('.',$rut);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
								
				$conane = mysqli_query($conectar,"select * from anexos_archivo where caa_clave_int = '".$clacaa."'");
				$numane = mysqli_num_rows($conane);
				$contador=$contador+1;
			?>
				<tr style="<?php if($i % 2 == 0){ echo 'background-color:silver'; } ?>" id="mensual<?php echo $clacaa; ?>">
					<td class="auto-style1">
					<input onclick="contadorVals(this);" type="checkbox" name="idcatid" id="idcat<?php echo $contador; ?>" value="<?php echo "IINFORME".$dato['caa_clave_int'];?>" />
					</td>
					<td class="auto-style1"><?php echo $nom.".".$extension; ?></td>
					<td class="auto-style1"><?php echo $obr; ?></td>
					<td class="auto-style1"><div style="cursor:pointer" onmouseover="javascript: VentanaFlotante('<?php echo $com; ?>', 300, 120)" onmouseout="javascript: quitarDiv();"><?php echo substr($com,0,50); ?></div></td>
					<td class="auto-style1"><?php echo $fec; ?></td>
					<td class="auto-style1"><?php echo $usuact; ?></td>
					<td>
					<?php
					if(($estado == '1' and strtoupper($descripcionperfil) == strtoupper('administrador')) or ($estado == '0'))
					{
					?>
					<img src="../../images/editar.png" alt="" height="30" width="29" onclick="EDITARARCHIVO('<?php echo $clacaa; ?>')" style="cursor:pointer" title="EDITAR" />
					<?php
					}
					?>
					</td>
					<td class="fila" style="background-color:#092451;color:white;width:37px;cursor:pointer">
					<a <?php if($rut != ''){ echo "href='iframecargar/$rut'"; }else{ echo "onclick='ALERTASINADJUNTO()'"; } ?> target='_blank' style="color:white;text-decoration:none;text-shadow: 1px 1px 1px #aaa">
					VER ONLINE</a>
					</td>
					<td <?php if($numane > 0){ echo 'class="fila"'; echo 'style="background-color:#092451;color:white;width:37px;cursor:pointer"'; } ?> onclick="MOSTRARMOVIMIENTOANEXO('<?php echo $clacaa; ?>');OCULTARSCROLL()">
					<?php
					if($numane > 0)
					{
					?>
					<div id="verocultaranexo<?php echo $clacaa; ?>" style="text-shadow: 1px 1px 1px #aaa">VER ANEXOS</div><input name="ocultoanexos<?php echo $clacaa; ?>" id="ocultoanexos<?php echo $clacaa; ?>" value="0" type="hidden" />
					<?php
					}
					?>
					</td>
					<td class="fila" style="background-color:#092451;color:white;width:37px;cursor:pointer">
					<a href='descargar.php?clacaa=<?php echo $clacaa; ?>' target='_blank' style="color:white;text-decoration:none;text-shadow: 1px 1px 1px #aaa">
					DESCARGAR</a>
					</td>
					<td>
					<a href='descargarcomitezip.php?clacaa=<?php echo $clacaa; ?>' target='_blank'>
					<img style="cursor:pointer" src="../../images/zip.png" title="Descargar todo" height="30" width="35"></a></td>
					<td>
					<?php
					if($est == '0' and $aprueba == 1)
					{
					?>
					<img style="cursor:pointer" src="../../images/aprobar.png" title="Aprobar" onclick="APROBARCOMITE('<?php echo $clacaa; ?>','mensual')" height="30" width="29">
					<?php
					}
					?>
					</td>
					<td>
					<?php
					if(($estado == '1' and strtoupper($descripcionperfil) == strtoupper('administrador')) or ($estado == '0'))
					{
					?>
					<img style="cursor:pointer" src="../../images/delete.png" height="30" width="29" title="ELIMINAR" onclick="ELIMINARARCHIVO('<?php echo $clacaa; ?>','LISTA','<?php echo $i; ?>','mensual')">
					<?php
					}
					?>
					</td>
				</tr>
				<tr>
					<td class="auto-style1">
					&nbsp;</td>
					<td class="auto-style1" colspan="12">
					<div id="movimientoanexo<?php echo $clacaa; ?>"></div>
					</td>
					<td class="auto-style1">
					&nbsp;</td>
				</tr>
			<?php
			}
			?>
			</table>
			</fieldset>
			<fieldset name="Group1">
				<legend><strong>REGISTRO FOTOGRAFICO</strong></legend>
		<table style="width: 100%" class="section">
			<tr>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
			</tr>
			<tr>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4"><strong>NOMBRE DEL ARCHIVO</strong></td>
				<td class="auto-style4"><strong>OBRA</strong></td>
				<td class="auto-style4"><strong>FECHA CREACIÓN</strong></td>
				<td class="auto-style4"><strong>ACTUALIZADO POR</strong></td>
				<td></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="10">
				<hr>
				</td>
			</tr>
			<?php
			if($est == 'T')
			{
				$con = mysqli_query($conectar,"select * from carga c inner join obra o on (o.obr_clave_int = c.obr_clave_int) inner join tipo_informe ti on (ti.tii_clave_int = c.tii_clave_int) where c.obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") order by CAST(car_nombre AS SIGNED INTEGER) DESC");
			}
			else
			{
				$con = mysqli_query($conectar,"select * from carga c inner join obra o on (o.obr_clave_int = c.obr_clave_int) inner join tipo_informe ti on (ti.tii_clave_int = c.tii_clave_int) where c.obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and car_estado = '".$est."' order by CAST(car_nombre AS SIGNED INTEGER) DESC");
			}
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clacar = $dato['car_clave_int'];
				$nom = $dato['car_nombre'];
				$obr = $dato['obr_nombre'];
				$tii = $dato['tii_nombre'];
				$fec = $dato['car_fecha_creacion'];
				$estado = $dato['car_estado'];
				$usuact = $dato['car_usu_actualiz'];
				$fecact = $dato['car_fec_actualiz'];
				$contador=$contador+1;
			?>
				<tr style="<?php if($i % 2 == 0){ echo 'background-color:silver'; } ?>" id="imagenes<?php echo $clacar; ?>">
					<td class="auto-style1">
					<input onclick="contadorVals(this);" type="checkbox" name="idcatid" id="idcat<?php echo $contador; ?>" value="<?php echo "FOTO".$dato['car_clave_int'];?>" />
					</td>
					<td class="auto-style1"><?php echo $nom; ?></td>
					<td class="auto-style1"><?php echo $obr; ?></td>
					<td class="auto-style1"><?php echo $fec; ?></td>
					<td class="auto-style1"><?php echo $usuact; ?></td>
					<td>
					<?php
					if(($estado == '1' and strtoupper($descripcionperfil) == strtoupper('administrador')) or ($estado == '0'))
					{
					?>
					<img src="../../images/editar.png" alt="" height="30" width="29" onclick="EDITAR('<?php echo $clacar; ?>')" style="cursor:pointer" title="EDITAR" />
					<?php
					}
					?>
					</td>
					<td class="fila" style="background-color:#092451;color:white;width:37px;cursor:pointer" onclick="parent.mostrarVentana('<?php echo $clacar; ?>');OCULTARSCROLL()">
					<div id="verocultofotos<?php echo $clacar; ?>" style="text-shadow: 1px 1px 1px #aaa">VER FOTOS</div><input name="ocultofotos<?php echo $clacar; ?>" id="ocultofotos<?php echo $clacar; ?>" value="0" type="hidden" />
					</td>
					<td>
					<a href='descargarregistrofotograficozip.php?clacar=<?php echo $clacar; ?>' target='_blank'>
					<img style="cursor:pointer" src="../../images/zip.png" title="Descargar todo" height="30" width="35"></a>
					</td>
					<td>
					<?php
					if($est == '0' and $aprueba == 1)
					{
					?>
					<img style="cursor:pointer" src="../../images/aprobar.png" title="Aprobar" onclick="APROBARREGISTROFOTOGRAFICO('<?php echo $clacar; ?>')" height="30" width="29">
					<?php
					}
					?>
					</td>
					<td>
					<?php
					if(($estado == '1' and strtoupper($descripcionperfil) == strtoupper('administrador')) or ($estado == '0'))
					{
					?>
					<img style="cursor:pointer" src="../../images/delete.png" height="30" width="29" title="ELIMINAR" onclick="ELIMINARCARGA('<?php echo $clacar; ?>','LISTA','<?php echo $i; ?>')">
					<?php
					}
					?>
					</td>
				</tr>
				<tr>
					<td class="auto-style1">
					&nbsp;</td>
					<td class="auto-style1" colspan="9">
					<div id="movimiento<?php echo $clacar; ?>"></div>
					</td>
				</tr>
			<?php
			}
			?>
			</table>
			</fieldset>
			</div>
			<input name="contador1" id="contador1" value="<?php echo $contador; ?>" type="hidden" />
		<?
		exit();
	}
	if($_GET['editarcarga'] == 'si')
	{
		$clacar = $_GET['clacar'];
		$con = mysqli_query($conectar,"select car_nombre,obr_clave_int,tii_clave_int from carga where car_clave_int = '".$clacar."'");
		$dato = mysqli_fetch_array($con);
		$nom = $dato['car_nombre'];
		$obr = $dato['obr_clave_int'];
		$tii = $dato['tii_clave_int'];
		
		mysqli_query($conectar,"update usuario set car_clave_int = ".$clacar." where usu_usuario = '".$usuario."'");
		?>
		<table style="width: 100%" class="section">
			<tr>
				<td align="center" style="text-align:center">
				<table style="width: 45%" align="center">
					<tr>
						<td class="auto-style1"><strong>Obra:</strong></td>
						<td class="auto-style1">
						<select name="obra1" id="obra1" class="inputs" style="width: 210px">
						<option value="">-Seleccione-</option>
						<?php
							$con = mysqli_query($conectar,"select * from obra where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = '".$claveusuario."') order by obr_nombre");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['obr_clave_int'];
								$nombre = $dato['obr_nombre'];
						?>
							<option value="<?php echo $clave; ?>" <?php if($clave == $obr){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
						<?php
							}
						?>
						</select>
						</td>
						<td class="auto-style1" rowspan="3">
						<input name="Button1" class="inputs" type="button" value="ACTUALIZAR" onclick="ACTUALIZARCARGA('<?php echo $clacar; ?>')" style="width: 100px; height: 60px; cursor:pointer" />
						</td>
						<td class="auto-style1" rowspan="3">
						<input name="Button1" class="inputs" type="button" value="ELIMINAR" onclick="ELIMINARCARGA('<?php echo $clacar; ?>','','')" style="width: 100px; height: 60px; cursor:pointer" />
						</td>
						<td class="auto-style1" rowspan="3">
						<div id="estadoactualizar"></div>
						</td>
					</tr>
					<tr>
						<td class="auto-style1"><strong>Tipo informe:</strong></td>
						<td class="auto-style1">
						<select name="tipoinforme1" id="tipoinforme1" disabled="disabled" class="inputs" style="width: 210px">
						<option value="">-Seleccione-</option>
						<?php
							$con = mysqli_query($conectar,"select * from tipo_informe where tii_clave_int = 1 order by tii_nombre");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['tii_clave_int'];
								$nombre = $dato['tii_nombre'];
						?>
							<option value="<?php echo $clave; ?>" <?php if($clave == $tii){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
						<?php
							}
						?>
						</select>
						</td>
					</tr>
					<tr>
						<td class="auto-style1"><strong>Nombre:</strong></td>
						<td class="auto-style1">
						<input name="nombre1" id="nombre1" value="<?php echo $nom; ?>" spellcheck="false" class="inputs" type="text" style="width: 200px" />
						</td>
					</tr>
					</table>
				
				</td>
			</tr>
			<tr>
				<td align="center" style="text-align:center">
				<hr/>
				</td>
			</tr>
			<tr>
				<td>
				<div id="fotos">
				<iframe src="iframecargar/index.php" frameborder="1" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen style="width: 100%; height:360px; border-style:dashed;overflow:hidden; overflow-y:hidden; overflow-x:hidden;" scrolling=no></iframe>
				<br><br>
				<div style="float:left" id="fotosagregadas">
				<div class="inputs" style="overflow:auto;width:100%">
				<?php
				$con = mysqli_query($conectar,"select * from carga_foto where car_clave_int = ".$clacar."");
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$clacaf = $dato['caf_clave_int'];
					$fot = $dato['caf_nombre'];
					$ley = $dato['caf_leyenda'];
					?>
					<div style="float:left" class="service_list" id="service<?php echo $clacaf; ?>" data="<?php echo $clacaf; ?>">
					<table style="width: 10%">
						<tr>
							<td class="auto-style2"><?php echo $i+1; ?></td>
						</tr>
						<tr>
							<td class="auto-style2"><img src="iframecargar/<?php echo $fot; ?>" height="80" width="80" style="border-radius:3px;-webkit-border-radius:10px;-moz-border-radius:3px;border:1px solid black" /></td>
						</tr>
						<tr>
							<td class="auto-style2">
							<textarea cols="20" class="inputs" name="TextArea1" onkeyup="GUARDARLEYENDA('<?php echo $clacaf; ?>',this.value)" rows="2"><?php echo $ley; ?></textarea>
							</td>
						</tr>
						<tr>
							<td class="auto-style2">
							<img src="../../images/delete.png" onclick="eliminar('<?php echo $clacaf; ?>')" height="26" width="26" style="cursor:pointer"></td>
						</tr>
					</table>
					</div>
					<?php
				}
				?>
				</div>
				</div>
				</div>
				</td>
			</tr>
		</table>
		<?php
		exit();
	}
	if($_GET['editarcargaarchivo'] == 'si')
	{
		$clacaa = $_GET['clacaa'];
		$con = mysqli_query($conectar,"select caa_comentarios,caa_nombre,caa_ruta_original,obr_clave_int,tii_clave_int from carga_archivo where caa_clave_int = '".$clacaa."'");
		$dato = mysqli_fetch_array($con);
		$com = $dato['caa_comentarios'];
		$nom = $dato['caa_nombre'];
		$rutori = $dato['caa_ruta_original'];
		$obr = $dato['obr_clave_int'];
		$tii = $dato['tii_clave_int'];
		?>
		<table style="width: 100%" class="section">
			<tr>
				<td align="center" style="text-align:center">
				<table style="width: 45%" align="center">
					<tr>
						<td class="auto-style1"><strong>Obra:</strong></td>
						<td class="auto-style1">
						<select name="obra1" id="obra1" class="inputs" style="width: 210px">
						<option value="">-Seleccione-</option>
						<?php
							$con = mysqli_query($conectar,"select * from obra where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = '".$claveusuario."') order by obr_nombre");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['obr_clave_int'];
								$nombre = $dato['obr_nombre'];
						?>
							<option value="<?php echo $clave; ?>" <?php if($clave == $obr){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
						<?php
							}
						?>
						</select>
						</td>
						<td class="auto-style1" rowspan="3">
						<input name="Button1" class="inputs" type="button" value="ACTUALIZAR" onclick="ADJUNTAR('1','SI','<?php echo $clacaa; ?>')" style="width: 100px; height: 60px; cursor:pointer" />
						</td>
						<td class="auto-style1" rowspan="3">
						<input name="Button1" class="inputs" type="button" value="ELIMINAR" onclick="ELIMINARARCHIVO('<?php echo $clacaa; ?>','','')" style="width: 100px; height: 60px; cursor:pointer" />
						</td>
						<td class="auto-style1" rowspan="3">
						<div id="estadoactualizar"></div>
						</td>
					</tr>
					<tr>
						<td class="auto-style1"><strong>Tipo informe:</strong></td>
						<td class="auto-style1">
						<select name="tipoinforme1" id="tipoinforme1" class="inputs" style="width: 210px">
						<option value="">-Seleccione-</option>
						<?php
							$con = mysqli_query($conectar,"select * from tipo_informe where tii_clave_int <> 1 order by tii_nombre");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['tii_clave_int'];
								$nombre = $dato['tii_nombre'];
						?>
							<option value="<?php echo $clave; ?>" <?php if($clave == $tii){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
						<?php
							}
						?>
						</select>
						</td>
					</tr>
					<tr>
						<td class="auto-style1"><strong>Nombre:</strong></td>
						<td class="auto-style1">
						<input name="nombre11" id="nombre11" value="" disabled="disabled" spellcheck="false" class="inputs" type="text" style="width: 200px" />
						</td>
					</tr>
					</table>
				
				</td>
			</tr>
			<tr>
				<td align="center" style="text-align:center">
				<hr/>
				</td>
			</tr>
			<tr>
				<td>
				<div id="archivos">
				<div><br><table style="width: 50%" align="center"><tr><td style="height: 130px"><fieldset name="Group1"><legend align="center"><strong>INFORME</strong></legend><table align="center"><tr><td align="center" style="text-align:center"><input type="text" class="inputs" placeholder="" spellcheck="false" style="width:180px" name="nombre1" id="nombre1" value="<?php echo $nom; ?>" /></td></tr><tr><td align="center" style="text-align:center"><input name="fileUpload1" id="fileUpload1" type="file" /></td></tr><tr><td><div id="resultadoadjunto1" style="width:100%"><div class='ok1' style='width: 100%' align='center'><?php echo $rutori; ?></div></div></td></tr></table></fieldset></td><td style="height: 130px"><fieldset name="Group1"><legend align="center"><strong>ANEXOS</strong></legend><table align="center"><tr><td>&nbsp;</td></tr><tr><td onmousemove="OCULTOSELECCIONADO('1')"><input name="fileUpload2" id="fileUpload2" type="file" /></td></tr><tr><td></td></tr><tr><td></td></tr></table></fieldset></td><td style="height: 130px"><fieldset name="Group1" style="height:98px"><legend align="center"><strong>COMENTARIOS</strong></legend><table align="center"><tr><td><textarea class="inputs" name="comentario1" id="comentario1" style="height: 50px; width: 250px;"><?php echo $com; ?></textarea></td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td></tr></table></fieldset></td></tr><tr><td colspan="3"><div id="ocultarbotonsubir1"><a><input type="button" onclick="ADJUNTAR('1','SI','<?php echo $clacaa; ?>')" onmousemove="OCULTOSELECCIONADO('1')" value="ACTUALIZAR" style="width:100%;cursor:pointer" /><div id="resultadoactualizar"></div><input name="ocultoanexo1" id="ocultoanexo1" value="0" type="hidden" /></a></div></td></tr><tr>
				<td colspan="3"><br>
				<div id="veranexos">
				<fieldset name="Group1" style="width:100%">
				<legend align="center">
				<strong>ANEXOS
				</strong>
				</legend>
				<table style="width: 100%" class="section" align="center">
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td style="width: 85px">&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td style="text-align:left"><strong>NOMBRE DEL ANEXO</strong></td>
						<td style="text-align:left"><strong>VER</strong></td>
						<td style="text-align:left; width: 85px;"><strong>DESCARGAR</strong></td>
						<td style="text-align:left"><strong>ELIMINAR</strong></td>
					</tr>
					<tr>
						<td colspan="4">
						<hr>
						</td>
					</tr>
					<?php
					$con = mysqli_query($conectar,"select * from anexos_archivo where caa_clave_int = '".$clacaa."' order by ana_nombre");
					$num = mysqli_num_rows($con);
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$claana = $dato['ana_clave_int'];
						$nom = $dato['ana_nombre'];
						$rut = $dato['ana_ruta'];
					?>
						<tr style="<?php if($i % 2 == 0){ echo 'background-color:silver'; } ?>;cursor:pointer" id="service<?php echo $claana; ?>" data="<?php echo $claana; ?>" onmouseover="this.style.backgroundColor='#A7A7A7';this.style.color='#000000';" onmouseout="this.style.backgroundColor='<?php if($i % 2 == 0){ echo "#BDBDBD"; } ?>';this.style.color='#000000';" onclick="MOSTRARMOVIMIENTO('<?php echo $clacar; ?>');OCULTARSCROLL()">
							<td class="auto-style1" style="text-align:left">
							<input type="text" class="inputs" placeholder="Nombre anexo" style="width:98%" onkeyup="GUARDARNOMBREANEXOAUTOMATICO('<?php echo $claana; ?>',this.value)" name="nombreanexo1" id="nombreanexo1" value="<?php echo $nom; ?>" />
							</td>
							<td class="fila" style="background-color:#092451;color:white;width:37px;cursor:pointer">
							<a <?php if($rut != ''){ echo "href='iframecargar/$rut'"; }else{ echo "onclick='ALERTASINADJUNTO()'"; } ?> target='_blank' style="color:white;text-decoration:none;text-shadow: 1px 1px 1px #aaa">
							VER ONLINE
							</a>
							</td>
							<td class="fila" style="background-color:#092451;color:white;width:37px;cursor:pointer">
							<a href='descargar.php?claana=<?php echo $claana; ?>' target='_blank' style="color:white;text-decoration:none;text-shadow: 1px 1px 1px #aaa">
							DESCARGAR</a>
							</td>
							<td class="auto-style1">
							<img style="cursor:pointer" src="../../images/delete.png" height="30" width="29" onclick="eliminaranexo('<?php echo $claana; ?>')">
							</td>
						</tr>
					<?php
					}
					?>
					</table>
				</fieldset>
				</div>
				</td></tr></table></div>
				</div>
				</td>
			</tr>
		</table>
		<?php
		exit();
	}
	if($_GET['actualizarcarga'] == 'si')
	{
		$nom = $_GET['nom'];
		$obr = $_GET['obr'];
		$tii = $_GET['tii'];
		$cc = $_GET['cc'];
		
		if($obr == '')
		{
			echo "<div class='validaciones'>Debe elegir la Obra</div>";
		}
		else
		if($tii == '')
		{
			echo "<div class='validaciones'>Debe elegir el tipo de informe</div>";
		}
		else
		if($nom == '')
		{
			echo "<div class='validaciones'>Debe ingresar el Nombre</div>";
		}
		else
		{
			$sql = mysqli_query($conectar,"update carga set car_nombre = '".$nom."', obr_clave_int = '".$obr."', tii_clave_int = '".$tii."' where car_clave_int = ".$cc."");
			if($sql > 0)
			{
				$con = mysqli_query($conectar,"select * from notificar_h where obr_clave_int = '".$obr."' and not_tipo = 2");
				$dato = mysqli_fetch_array($con);
				$swtod = $dato['not_sw_todos'];
				$swact = $dato['not_sw_activo'];
				
				if($swtod == 1 and $swact == 1)
				{
					$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) inner join obra o on (o.obr_clave_int = uo.obr_clave_int) where uo.obr_clave_int = '".$obr."' and u.usu_clave_int <> '".$claveusuario."'");
					$num = mysqli_num_rows($con);
					
					for($i = 0; $i < $num; $i++)
					{
						$mail = new PHPMailer();
						
						$dato = mysqli_fetch_array($con);
						$mail->Body = '';
						$ema = '';
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						$nomobr = $dato['obr_nombre'];
						
						$mail->From = "adminpavas@pavas.com.co";
						$mail->FromName = "I, A & C";
						$mail->AddAddress($ema, "Destino");
						$mail->Subject = "ACTUALIZACION REGISTRO FOTOGRAFICO. OBRA: ".$nomobr;
						
						// Cuerpo del mensaje
						$mail->Body .= "Hola ".$nomusu."!\n\n";	
						$mail->Body .= "ZONA CLIENTES registra que se ha actualizado un REGISTRO FOTOGRAFICO de su obra: ".$nomobr.".\n";
						$mail->Body .= "NOMBRE ARCHIVO: ".$nom."\n";
						$mail->Body .= date("d/m/Y H:m:s")."\n\n";
						$mail->Body .= "Este mensaje es generado automáticamente por ZONA CLIENTES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Zonaclientes \n";
						
						if(!$mail->Send())
						{
							//echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
							//echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
						}
						else
						{
						}
					}
				}
				else
				if($swact == 1)
				{
					$con = mysqli_query($conectar,"select * from notificar_h nh inner join notificar_d nd on (nd.not_clave_int = nh.not_clave_int) inner join usuario u on (u.usu_clave_int = nd.usu_clave_int) inner join obra o on (o.obr_clave_int = nh.obr_clave_int) where nh.obr_clave_int = '".$obr."' and nh.not_tipo = 2 and u.usu_clave_int <> '".$claveusuario."'");
					$num = mysqli_num_rows($con);
					for($i = 0; $i < $num; $i++)
					{
						$mail = new PHPMailer();
						
						$dato = mysqli_fetch_array($con);
						$mail->Body = '';
						$ema = '';
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						$nomobr = $dato['obr_nombre'];
						
						$mail->From = "adminpavas@pavas.com.co";
						$mail->FromName = "I, A & C";
						$mail->AddAddress($ema, "Destino");
						$mail->Subject = "ACTUALIZACION REGISTRO FOTOGRAFICO. OBRA: ".$nomobr;
						
						// Cuerpo del mensaje
						$mail->Body .= "Hola ".$nomusu."!\n\n";	
						$mail->Body .= "ZONA CLIENTES registra que se ha actualizado un REGISTRO FOTOGRAFICO de su obra: ".$nomobr.".\n";
						$mail->Body .= "NOMBRE ARCHIVO: ".$nom."\n";
						$mail->Body .= date("d/m/Y H:m:s")."\n\n";
						$mail->Body .= "Este mensaje es generado automáticamente por ZONA CLIENTES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Zonaclientes \n";
						
						if(!$mail->Send())
						{
							//echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
							//echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
						}
						else
						{
						}
					}
				}
				echo "<div class='ok'>Datos actualizados correctamente</div>";
			}
			else
			{
				echo "<div class='validaciones'>ERROR</div>";
			}
		}
		exit();
	}
	if($_GET['actualizararchivo'] == 'si')
	{
		$obr = $_GET['obr'];
		$tii = $_GET['tii'];
		$cc = $_GET['cc'];
		
		if($obr == '')
		{
			echo "<div class='validaciones'>Debe elegir la Obra</div>";
		}
		else
		if($tii == '')
		{
			echo "<div class='validaciones'>Debe elegir el tipo de informe</div>";
		}
		else
		{
			$sql = mysqli_query($conectar,"update carga_archivo set obr_clave_int = '".$obr."', tii_clave_int = '".$tii."' where caa_clave_int = ".$cc."");
			if($sql > 0)
			{
				echo "<div class='ok'>OK</div>";
			}
			else
			{
				echo "<div class='validaciones'>ERROR</div>";
			}
		}
		exit();
	}
	if($_GET['actualizararchivoeditado'] == 'si')
	{
		$nom = $_GET['nom'];
		$obr = $_GET['obr'];
		$tii = $_GET['ti'];
		$com = $_GET['com'];
		$ci = $_GET['ci'];
		
		if($obr == '')
		{
			echo "<div class='validaciones'>Debe elegir la Obra</div>";
		}
		else
		if($tii == '')
		{
			echo "<div class='validaciones'>Debe elegir el tipo de informe</div>";
		}
		else
		if($nom == '')
		{
			echo "<div class='validaciones'>Debe ingresar el nombre del adjunto</div>";
		}
		else
		{
			$sql = mysqli_query($conectar,"update carga_archivo set caa_nombre = '".$nom."', obr_clave_int = '".$obr."', tii_clave_int = '".$tii."', caa_comentarios = '".$com."' where caa_clave_int = ".$ci."");
			if($sql > 0)
			{
				$con = mysqli_query($conectar,"select * from notificar_h where obr_clave_int = '".$obr."' and not_tipo = 2");
				$dato = mysqli_fetch_array($con);
				$swtod = $dato['not_sw_todos'];
				$swact = $dato['not_sw_activo'];
				
				$contii = mysqli_query($conectar,"select tii_nombre from tipo_informe where tii_clave_int = '".$tii."'");
				$datoti = mysqli_fetch_array($contii);
				$nomti = $datoti['tii_nombre'];
				
				if($swtod == 1 and $swact == 1)
				{
					$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) inner join obra o on (o.obr_clave_int = uo.obr_clave_int) where uo.obr_clave_int = '".$obr."' and u.usu_usuario <> '".$usuario."'");
					$num = mysqli_num_rows($con);
					
					for($i = 0; $i < $num; $i++)
					{
						$mail = new PHPMailer();
						
						$dato = mysqli_fetch_array($con);
						$mail->Body = '';
						$ema = '';
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						$nomobr = $dato['obr_nombre'];
						
						$mail->From = "adminpavas@pavas.com.co";
						$mail->FromName = "I, A & C";
						$mail->AddAddress($ema, "Destino");
						$mail->Subject = "ACTUALIZACION ".$nomti.". OBRA: ".$nomobr;
						
						// Cuerpo del mensaje
						$mail->Body .= "Hola ".$nomusu."!\n\n";	
						$mail->Body .= "ZONA CLIENTES registra que se ha actualizado un ".$nomti." de su obra: ".$nomobr.".\n";
						$mail->Body .= "NOMBRE ARCHIVO: ".$nom."\n";
						$mail->Body .= date("d/m/Y H:m:s")."\n\n";
						$mail->Body .= "Este mensaje es generado automáticamente por ZONA CLIENTES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Zonaclientes \n";
						
						if(!$mail->Send())
						{
							//echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
							//echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
						}
						else
						{
						}
					}
				}
				else
				if($swact == 1)
				{
					$con = mysqli_query($conectar,"select * from notificar_h nh inner join notificar_d nd on (nd.not_clave_int = nh.not_clave_int) inner join usuario u on (u.usu_clave_int = nd.usu_clave_int) inner join obra o on (o.obr_clave_int = nh.obr_clave_int) where nh.obr_clave_int = '".$obr."' and nh.not_tipo = 2 and u.usu_usuario <> '".$usuario."'");
					$num = mysqli_num_rows($con);
					for($i = 0; $i < $num; $i++)
					{
						$mail = new PHPMailer();
						
						$dato = mysqli_fetch_array($con);
						$mail->Body = '';
						$ema = '';
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						$nomobr = $dato['obr_nombre'];
						
						$mail->From = "adminpavas@pavas.com.co";
						$mail->FromName = "I, A & C";
						$mail->AddAddress($ema, "Destino");
						$mail->Subject = "ACTUALIZACION ".$nomti.". OBRA: ".$nomobr;
						
						// Cuerpo del mensaje
						$mail->Body .= "Hola ".$nomusu."!\n\n";	
						$mail->Body .= "ZONA CLIENTES registra que se ha actualizado un ".$nomti." de su obra: ".$nomobr.".\n";
						$mail->Body .= "NOMBRE ARCHIVO: ".$nom."\n";
						$mail->Body .= date("d/m/Y H:m:s")."\n\n";
						$mail->Body .= "Este mensaje es generado automáticamente por ZONA CLIENTES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Zonaclientes \n";
						
						if(!$mail->Send())
						{
							//echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
							//echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
						}
						else
						{
						}
					}
				}
				echo "<div class='ok'>Datos actualizados correctamente</div>";
			}
			else
			{
				echo "<div class='validaciones'>Hubo un error al actualizar los datos, intentelo nuevamente</div>";
			}
		}
		exit();
	}
	if($_GET['botonguardar'] == 'si')
	{
		$con = mysqli_query($conectar,"select car_clave_int from carga where car_usu_creacion = '".$usuario."' order by car_clave_int DESC LIMIT 1");
		$dato = mysqli_fetch_array($con);
		$clacar = $dato['car_clave_int'];
		?>
		<input name="Button1" class="inputs" type="button" value="ACTUALIZAR" onclick="ACTUALIZARCARGAGUARDADA('<?php echo $clacar; ?>')" style="width: 100px; height: 60px; cursor:pointer" />
		<?php
		exit();
	}
	if($_GET['tipoinforme'] == 'si')
	{
		$tipinf = $_GET['tipinf'];
		if($tipinf == 1 or $tipinf == '')
		{
		?>
			<div style="width: 100%; height:360px; border-style:dashed;overflow:hidden;cursor:pointer" onclick="ALERTA()"></div>
		<?php
		}
		else
		{
		?>
			<div id="estadoactualizar">
			<img src="../../images/agg.png" style="height:80px;width:80px" onclick="AGREGAR()">
			</div>
		<?php
		}
		exit();
	}
	if($_GET['mostrarruta'] == 'si')
	{
		$nomadj = $_GET['nomadj'];
		echo "<div class='ok1' style='width: 100%' align='center'>$nomadj</div>";
		exit();
	}
	if($_GET['estadoadjunto'] == 'si')
	{
	?>
		<div class='ok1' style='width: 100%' align='center'>CORRECTO</div>
	<?php
		exit();
	}
	if($_GET['guardarnombreanexo'] == 'si')
	{
		$con = mysqli_query($conectar,"select caa_clave_int from carga_archivo where caa_usu_actualiz = '".$usuario."' order by caa_clave_int DESC LIMIT 1");
		$dato = mysqli_fetch_array($con);
		$clacaa = $dato['caa_clave_int'];
		
		$nomane = $_GET['nomane'];
		$seleccionadosnomane = explode(',',$nomane);
		//$num = count($seleccionadosnomane);
		$nombres = array();
		
		$con = mysqli_query($conectar,"select ana_clave_int from anexos_archivo where caa_clave_int = '".$clacaa."' and (ana_nombre = '' or ana_nombre IS NULL)");
		$num = mysqli_num_rows($con);
		for($i = 0; $i < $num; $i++)
		{
			$dato = mysqli_fetch_array($con);
			$claana = $dato['ana_clave_int'];
			$nombres[$i]=$seleccionadosnomane[$i];
			mysqli_query($conectar,"update anexos_archivo set ana_nombre = '".$nombres[$i]."' where ana_clave_int = ".$claana."");
		}
		exit();
	}
	if($_GET['guardarnombreanexoactualizado'] == 'si')
	{		
		$nomane = $_GET['nomane'];
		$ci = $_GET['ci'];
		$clacaa = $ci;
		
		$seleccionadosnomane = explode(',',$nomane);
		//$num = count($seleccionadosnomane);
		$nombres = array();
		
		$con = mysqli_query($conectar,"select ana_clave_int from anexos_archivo where caa_clave_int = '".$clacaa."' and (ana_nombre = '' or ana_nombre IS NULL)");
		$num = mysqli_num_rows($con);
		for($i = 0; $i < $num; $i++)
		{
			$dato = mysqli_fetch_array($con);
			$claana = $dato['ana_clave_int'];
			$nombres[$i]=$seleccionadosnomane[$i];
			mysqli_query($conectar,"update anexos_archivo set ana_nombre = '".$nombres[$i]."' where ana_clave_int = ".$claana."");
		}
		exit();
	}
	if($_GET['mostrarmovimientoanexo'] == 'si')
	{
		$clacaa = $_GET['clacaa'];
		?>
		<fieldset name="Group1" style="width:50%">
		<legend align="center">
		<strong>ANEXOS
		</strong>
		</legend>
		<table style="width: 100%" class="section" align="center">
			<tr>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
			</tr>
			<tr>
				<td class="auto-style4"><strong>NOMBRE DEL ANEXO</strong></td>
				<td class="auto-style4"><strong>VER</strong></td>
				<td class="auto-style4"><strong>DESCARGAR</strong></td>
			</tr>
			<tr>
				<td colspan="3">
				<hr>
				</td>
			</tr>
			<?php
			$con = mysqli_query($conectar,"select * from anexos_archivo where caa_clave_int = '".$clacaa."' order by ana_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$claana = $dato['ana_clave_int'];
				$nom = $dato['ana_nombre'];
				$rut = $dato['ana_ruta'];
			?>
				<tr style="<?php if($i % 2 == 0){ echo 'background-color:silver'; } ?>;cursor:pointer" onmouseover="this.style.backgroundColor='#A7A7A7';this.style.color='#000000';" onmouseout="this.style.backgroundColor='<?php if($i % 2 == 0){ echo "#BDBDBD"; } ?>';this.style.color='#000000';" onclick="MOSTRARMOVIMIENTO('<?php echo $clacar; ?>');OCULTARSCROLL()">
					<td class="auto-style1"><?php echo $nom; ?></td>
					<td class="fila" style="background-color:#092451;color:white;width:37px;cursor:pointer">
					<a <?php if($rut != ''){ echo "href='iframecargar/$rut'"; }else{ echo "onclick='ALERTASINADJUNTO()'"; } ?> target='_blank' style="color:white;text-decoration:none;text-shadow: 1px 1px 1px #aaa">
					VER ONLINE
					</a>
					</td>
					<td class="fila" style="background-color:#092451;color:white;width:37px;cursor:pointer">
					<a href='descargar.php?claana=<?php echo $claana; ?>' target='_blank' style="color:white;text-decoration:none;text-shadow: 1px 1px 1px #aaa">
					DESCARGAR
					</a>
					</td>
				</tr>
			<?php
			}
			?>
			</table>
		</fieldset>
		<?php
		exit();
	}
	if($_GET['aprobarregistrofotografico'] == 'si')
	{
		$clacar = $_GET['clacar'];
		$con = mysqli_query($conectar,"select * from carga where car_clave_int = '".$clacar."'");
		$dato = mysqli_fetch_array($con);
		$obr = $dato['obr_clave_int'];
		$tii = $dato['tii_clave_int'];
		$nom = $dato['car_nombre'];
		
		$sql = mysqli_query($conectar,"update carga set car_estado = 1 where car_clave_int = '".$clacar."'");
		
		if($sql > 0)
		{
			$con = mysqli_query($conectar,"select * from notificar_h where obr_clave_int = '".$obr."' and not_tipo = 4");
			$dato = mysqli_fetch_array($con);
			$swtod = $dato['not_sw_todos'];
			$swact = $dato['not_sw_activo'];
			
			$contii = mysqli_query($conectar,"select tii_nombre from tipo_informe where tii_clave_int = '".$tii."'");
			$datoti = mysqli_fetch_array($contii);
			$nomti = $datoti['tii_nombre'];
			
			if($swtod == 1 and $swact == 1)
			{
				$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) inner join obra o on (o.obr_clave_int = uo.obr_clave_int) where uo.obr_clave_int = '".$obr."' and u.usu_usuario <> '".$usuario."'");
				$num = mysqli_num_rows($con);
				
				for($i = 0; $i < $num; $i++)
				{
					$mail = new PHPMailer();
					
					$dato = mysqli_fetch_array($con);
					$mail->Body = '';
					$ema = '';
					$nomusu = $dato['usu_nombre'];
					$ema = $dato['usu_email'];
					$nomobr = $dato['obr_nombre'];
					
					$mail->From = "adminpavas@pavas.com.co";
					$mail->FromName = "I, A & C";
					$mail->AddAddress($ema, "Destino");
					$mail->Subject = "INFORME APROBADO: ".$nomti.". OBRA: ".$nomobr;
					
					// Cuerpo del mensaje
					$mail->Body .= "Hola ".$nomusu."!\n\n";	
					$mail->Body .= "ZONA CLIENTES registra que se ha aprobado un ".$nomti." de su obra: ".$nomobr.".\n";
					$mail->Body .= "NOMBRE ARCHIVO: ".$nom."\n";
					$mail->Body .= date("d/m/Y H:m:s")."\n\n";
					$mail->Body .= "Este mensaje es generado automáticamente por ZONA CLIENTES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Zonaclientes \n";
					
					if(!$mail->Send())
					{
						//echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
						//echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
					}
					else
					{
					}
				}
			}
			else
			if($swact == 1)
			{
				$con = mysqli_query($conectar,"select * from notificar_h nh inner join notificar_d nd on (nd.not_clave_int = nh.not_clave_int) inner join usuario u on (u.usu_clave_int = nd.usu_clave_int) inner join obra o on (o.obr_clave_int = nh.obr_clave_int) where nh.obr_clave_int = '".$obr."' and nh.not_tipo = 4 and u.usu_usuario <> '".$usuario."'");
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$mail = new PHPMailer();
					
					$dato = mysqli_fetch_array($con);
					$mail->Body = '';
					$ema = '';
					$nomusu = $dato['usu_nombre'];
					$ema = $dato['usu_email'];
					$nomobr = $dato['obr_nombre'];
					
					$mail->From = "adminpavas@pavas.com.co";
					$mail->FromName = "I, A & C";
					$mail->AddAddress($ema, "Destino");
					$mail->Subject = "INFORME APROBADO: ".$nomti.". OBRA: ".$nomobr;
					
					// Cuerpo del mensaje
					$mail->Body .= "Hola ".$nomusu."!\n\n";	
					$mail->Body .= "ZONA CLIENTES registra que se ha aprobado un ".$nomti." de su obra: ".$nomobr.".\n";
					$mail->Body .= "NOMBRE ARCHIVO: ".$nom."\n";
					$mail->Body .= date("d/m/Y H:m:s")."\n\n";
					$mail->Body .= "Este mensaje es generado automáticamente por ZONA CLIENTES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Zonaclientes \n";
					
					if(!$mail->Send())
					{
						//echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
						//echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
					}
					else
					{
					}
				}
			}
		}
		?>
		<table style="width: 100%">
			<tr>
				<td style="width: 90px; cursor:pointer;<?php if($ultimoestado == 'NUEVO'){ echo 'background-color:#092451;color:#ffffff'; } ?>" class="section" onclick="NUEVACARGA()" onmouseover="this.style.backgroundColor='#092451';this.style.color='#ffffff';"  onmouseout="<?php if($ultimoestado <> 'NUEVO'){ echo "this.style.backgroundColor='#ffffff';this.style.color='#000000'"; } ?>">
				Nuevo
				</td>
				<td style="width: 90px; cursor:pointer;<?php if($ultimoestado == 'TODOS'){ echo 'background-color:#092451;color:#ffffff'; } ?>" class="section" onclick="VERTODOS('T')" onmouseover="this.style.backgroundColor='#092451';this.style.color='#ffffff';"  onmouseout="<?php if($ultimoestado <> 'TODOS'){ echo "this.style.backgroundColor='#ffffff';this.style.color='#000000'"; } ?>">
				<div id="todos">
				Todos 
				<?php 
				$con = mysqli_query($conectar,"select * from carga where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.")");
				$num = mysqli_num_rows($con);
				$con = mysqli_query($conectar,"select * from carga_archivo where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.")");
				$num1 = mysqli_num_rows($con);
				echo $num+$num1;
				?>
				</div>
				</td>
				<td style="width: 160px; cursor:pointer;<?php if($ultimoestado == 'PENDIENTES'){ echo 'background-color:#092451;color:#ffffff'; } ?>" class="section" onclick="VERTODOS('0')" onmouseover="this.style.backgroundColor='#092451';this.style.color='#ffffff';"  onmouseout="<?php if($ultimoestado <> 'PENDIENTES'){ echo "this.style.backgroundColor='#ffffff';this.style.color='#000000'"; } ?>">
				<div id="todos">
				Pendiente coordinador 
				<?php 
				$con = mysqli_query($conectar,"select * from carga where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and car_estado = 0");
				$num = mysqli_num_rows($con);
				$con = mysqli_query($conectar,"select * from carga_archivo where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and caa_estado = 0");
				$num1 = mysqli_num_rows($con);
				echo $num+$num1;
				?>
				</div>
				</td>
				<td style="width: 100px; cursor:pointer;<?php if($ultimoestado == 'APROBADOS'){ echo 'background-color:#092451;color:#ffffff'; } ?>" class="section" onclick="VERTODOS('1')" onmouseover="this.style.backgroundColor='#092451';this.style.color='#ffffff';"  onmouseout="<?php if($ultimoestado <> 'APROBADOS'){ echo "this.style.backgroundColor='#ffffff';this.style.color='#000000'"; } ?>">
				<div id="todos">
				Aprobados 
				<?php 
				$con = mysqli_query($conectar,"select * from carga where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and car_estado = 1");
				$num = mysqli_num_rows($con);
				$con = mysqli_query($conectar,"select * from carga_archivo where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and caa_estado = 1");
				$num1 = mysqli_num_rows($con);
				echo $num+$num1;
				?>
				</div>
				</td>
				<td class="auto-style2" style="width: 600px"><strong>CARGAR </strong> </td>
				<td>&nbsp;</td>
			</tr>
		</table>
		<?php
		exit();
	}
	if($_GET['aprobarcomite'] == 'si')
	{
		$clacaa = $_GET['clacaa'];
		$con = mysqli_query($conectar,"select * from carga_archivo where caa_clave_int = '".$clacaa."'");
		$dato = mysqli_fetch_array($con);
		$obr = $dato['obr_clave_int'];
		$tii = $dato['tii_clave_int'];
		$nom = $dato['caa_nombre'];
		
		$sql = mysqli_query($conectar,"update carga_archivo set caa_estado = 1 where caa_clave_int = '".$clacaa."'");
		if($sql > 0)
		{
			$con = mysqli_query($conectar,"select * from notificar_h where obr_clave_int = '".$obr."' and not_tipo = 4");
			$dato = mysqli_fetch_array($con);
			$swtod = $dato['not_sw_todos'];
			$swact = $dato['not_sw_activo'];
			
			$contii = mysqli_query($conectar,"select tii_nombre from tipo_informe where tii_clave_int = '".$tii."'");
			$datoti = mysqli_fetch_array($contii);
			$nomti = $datoti['tii_nombre'];
			
			if($swtod == 1 and $swact == 1)
			{
				$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) inner join obra o on (o.obr_clave_int = uo.obr_clave_int) where uo.obr_clave_int = '".$obr."' and u.usu_usuario <> '".$usuario."'");
				$num = mysqli_num_rows($con);
				
				for($i = 0; $i < $num; $i++)
				{
					$mail = new PHPMailer();
					
					$dato = mysqli_fetch_array($con);
					$mail->Body = '';
					$ema = '';
					$nomusu = $dato['usu_nombre'];
					$ema = $dato['usu_email'];
					$nomobr = $dato['obr_nombre'];
					
					$mail->From = "adminpavas@pavas.com.co";
					$mail->FromName = "I, A & C";
					$mail->AddAddress($ema, "Destino");
					$mail->Subject = "INFORME APROBADO: ".$nomti.". OBRA: ".$nomobr;
					
					// Cuerpo del mensaje
					$mail->Body .= "Hola ".$nomusu."!\n\n";	
					$mail->Body .= "ZONA CLIENTES registra que se ha aprobado un ".$nomti." de su obra: ".$nomobr.".\n";
					$mail->Body .= "NOMBRE ARCHIVO: ".$nom."\n";
					$mail->Body .= date("d/m/Y H:m:s")."\n\n";
					$mail->Body .= "Este mensaje es generado automáticamente por ZONA CLIENTES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Zonaclientes \n";
					
					if(!$mail->Send())
					{
						//echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
						//echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
					}
					else
					{
					}
				}
			}
			else
			if($swact == 1)
			{
				$con = mysqli_query($conectar,"select * from notificar_h nh inner join notificar_d nd on (nd.not_clave_int = nh.not_clave_int) inner join usuario u on (u.usu_clave_int = nd.usu_clave_int) inner join obra o on (o.obr_clave_int = nh.obr_clave_int) where nh.obr_clave_int = '".$obr."' and nh.not_tipo = 4 and u.usu_usuario <> '".$usuario."'");
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$mail = new PHPMailer();
					
					$dato = mysqli_fetch_array($con);
					$mail->Body = '';
					$ema = '';
					$nomusu = $dato['usu_nombre'];
					$ema = $dato['usu_email'];
					$nomobr = $dato['obr_nombre'];
					
					$mail->From = "adminpavas@pavas.com.co";
					$mail->FromName = "I, A & C";
					$mail->AddAddress($ema, "Destino");
					$mail->Subject = "INFORME APROBADO: ".$nomti.". OBRA: ".$nomobr;
					
					// Cuerpo del mensaje
					$mail->Body .= "Hola ".$nomusu."!\n\n";	
					$mail->Body .= "ZONA CLIENTES registra que se ha aprobado un ".$nomti." de su obra: ".$nomobr.".\n";
					$mail->Body .= "NOMBRE ARCHIVO: ".$nom."\n";
					$mail->Body .= date("d/m/Y H:m:s")."\n\n";
					$mail->Body .= "Este mensaje es generado automáticamente por ZONA CLIENTES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Zonaclientes \n";
					
					if(!$mail->Send())
					{
						//echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
						//echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
					}
					else
					{
					}
				}
			}
		}
		?>
		<table style="width: 100%">
			<tr>
				<td style="width: 90px; cursor:pointer;<?php if($ultimoestado == 'NUEVO'){ echo 'background-color:#092451;color:#ffffff'; } ?>" class="section" onclick="NUEVACARGA()" onmouseover="this.style.backgroundColor='#092451';this.style.color='#ffffff';"  onmouseout="<?php if($ultimoestado <> 'NUEVO'){ echo "this.style.backgroundColor='#ffffff';this.style.color='#000000'"; } ?>">
				Nuevo
				</td>
				<td style="width: 90px; cursor:pointer;<?php if($ultimoestado == 'TODOS'){ echo 'background-color:#092451;color:#ffffff'; } ?>" class="section" onclick="VERTODOS('T')" onmouseover="this.style.backgroundColor='#092451';this.style.color='#ffffff';"  onmouseout="<?php if($ultimoestado <> 'TODOS'){ echo "this.style.backgroundColor='#ffffff';this.style.color='#000000'"; } ?>">
				<div id="todos">
				Todos 
				<?php 
				$con = mysqli_query($conectar,"select * from carga where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.")");
				$num = mysqli_num_rows($con);
				$con = mysqli_query($conectar,"select * from carga_archivo where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.")");
				$num1 = mysqli_num_rows($con);
				echo $num+$num1;
				?>
				</div>
				</td>
				<td style="width: 160px; cursor:pointer;<?php if($ultimoestado == 'PENDIENTES'){ echo 'background-color:#092451;color:#ffffff'; } ?>" class="section" onclick="VERTODOS('0')" onmouseover="this.style.backgroundColor='#092451';this.style.color='#ffffff';"  onmouseout="<?php if($ultimoestado <> 'PENDIENTES'){ echo "this.style.backgroundColor='#ffffff';this.style.color='#000000'"; } ?>">
				<div id="todos">
				Pendiente coordinador 
				<?php 
				$con = mysqli_query($conectar,"select * from carga where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and car_estado = 0");
				$num = mysqli_num_rows($con);
				$con = mysqli_query($conectar,"select * from carga_archivo where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and caa_estado = 0");
				$num1 = mysqli_num_rows($con);
				echo $num+$num1;
				?>
				</div>
				</td>
				<td style="width: 100px; cursor:pointer;<?php if($ultimoestado == 'APROBADOS'){ echo 'background-color:#092451;color:#ffffff'; } ?>" class="section" onclick="VERTODOS('1')" onmouseover="this.style.backgroundColor='#092451';this.style.color='#ffffff';"  onmouseout="<?php if($ultimoestado <> 'APROBADOS'){ echo "this.style.backgroundColor='#ffffff';this.style.color='#000000'"; } ?>">
				<div id="todos">
				Aprobados 
				<?php 
				$con = mysqli_query($conectar,"select * from carga where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and car_estado = 1");
				$num = mysqli_num_rows($con);
				$con = mysqli_query($conectar,"select * from carga_archivo where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and caa_estado = 1");
				$num1 = mysqli_num_rows($con);
				echo $num+$num1;
				?>
				</div>
				</td>
				<td class="auto-style2" style="width: 600px"><strong>CARGAR </strong> </td>
				<td>&nbsp;</td>
			</tr>
		</table>
		<?php
		exit();
	}
	if($_GET['guardarleyenda'] == 'si')
	{
		$clacaf = $_GET['clacaf'];
		$ley = $_GET['ley'];
		mysqli_query($conectar,"update carga_foto set caf_leyenda = '".$ley."' where caf_clave_int = ".$clacaf."");
		exit();
	}
	if($_GET['mostrarfotosagregadas'] == 'si')
	{
		$clacar = $_GET['clacar'];
		?>
		<div class="inputs" style="overflow:auto;width:100%">
		<?php
		$con = mysqli_query($conectar,"select * from carga_foto where car_clave_int = ".$clacar."");
		$num = mysqli_num_rows($con);
		for($i = 0; $i < $num; $i++)
		{
			$dato = mysqli_fetch_array($con);
			$clacaf = $dato['caf_clave_int'];
			$fot = $dato['caf_nombre'];
			$ley = $dato['caf_leyenda'];
			?>
			<div style="float:left" class="service_list" id="service<?php echo $clacaf; ?>" data="<?php echo $clacaf; ?>">
			<table style="width: 10%">
				<tr>
					<td class="auto-style2"><?php echo $i+1; ?></td>
				</tr>
				<tr>
					<td class="auto-style2"><img src="iframecargar/<?php echo $fot; ?>" height="80" width="80" style="border-radius:3px;-webkit-border-radius:10px;-moz-border-radius:3px;border:1px solid black" /></td>
				</tr>
				<tr>
					<td class="auto-style2">
					<textarea cols="20" class="inputs" name="TextArea1" onkeyup="GUARDARLEYENDA('<?php echo $clacaf; ?>',this.value)" rows="2"><?php echo $ley; ?></textarea>
					</td>
				</tr>
				<tr>
					<td class="auto-style2">
					<img src="../../images/delete.png" onclick="eliminar('<?php echo $clacaf; ?>')" height="26" width="26" style="cursor:pointer"></td>
				</tr>
			</table>
			</div>
			<?php
		}
		?>
		</div>
		<?php
		exit();
	}
	if($_GET['guardarnombreanexoautomatico'] == 'si')
	{
		$claana = $_GET['claana'];
		$nomane = $_GET['nomane'];
		if($nomane <> '')
		{
			mysqli_query($conectar,"update anexos_archivo set ana_nombre = '".$nomane."' where ana_clave_int = '".$claana."'");
		}
		exit();
	}
	if($_GET['veranexos'] == 'si')
	{
		$file = $_GET['file'];
		$nomane = $_GET['nomane'];
		?>
		<fieldset name="Group1" style="width:100%">
		<legend align="center">
		<strong>ANEXOS
		</strong>
		</legend>
		<table style="width: 100%" class="section" align="center">
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td style="width: 85px">&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align:left"><strong>NOMBRE DEL ANEXO</strong></td>
				<td style="text-align:left"><strong>VER</strong></td>
				<td style="text-align:left; width: 85px;"><strong>DESCARGAR</strong></td>
				<td style="text-align:left"><strong>ELIMINAR</strong></td>
			</tr>
			<tr>
				<td colspan="4">
				<hr>
				</td>
			</tr>
			<?php
			$con = mysqli_query($conectar,"select * from anexos_archivo where caa_clave_int = (select caa_clave_int from anexos_archivo where ana_usu_actualiz = '".$usuario."' and (ana_nombre = '' or ana_nombre IS NULL or ana_nombre = '".$nomane."') order by ana_clave_int DESC LIMIT 1) order by ana_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$claana = $dato['ana_clave_int'];
				$nom = $dato['ana_nombre'];
				$rut = $dato['ana_ruta'];
			?>
				<tr style="<?php if($i % 2 == 0){ echo 'background-color:silver'; } ?>;cursor:pointer" id="service<?php echo $claana; ?>" data="<?php echo $claana; ?>" onmouseover="this.style.backgroundColor='#A7A7A7';this.style.color='#000000';" onmouseout="this.style.backgroundColor='<?php if($i % 2 == 0){ echo "#BDBDBD"; } ?>';this.style.color='#000000';" onclick="MOSTRARMOVIMIENTO('<?php echo $clacar; ?>');OCULTARSCROLL()">
					<td class="auto-style1" style="text-align:left">
					<input type="text" class="inputs" placeholder="Nombre anexo" style="width:98%" onkeyup="GUARDARNOMBREANEXOAUTOMATICO('<?php echo $claana; ?>',this.value)" name="nombreanexo1" id="nombreanexo1" value="<?php echo $nom; ?>" />
					</td>
					<td class="fila" style="background-color:#092451;color:white;width:37px;cursor:pointer">
					<a <?php if($rut != ''){ echo "href='iframecargar/$rut'"; }else{ echo "onclick='ALERTASINADJUNTO()'"; } ?> target='_blank' style="color:white;text-decoration:none;text-shadow: 1px 1px 1px #aaa">
					VER ONLINE</a>
					</td>
					<td class="fila" style="background-color:#092451;color:white;width:37px;cursor:pointer">
					<a href='descargar.php?claana=<?php echo $claana; ?>' target='_blank' style="color:white;text-decoration:none;text-shadow: 1px 1px 1px #aaa">
					DESCARGAR</a>
					</td>
					<td class="auto-style1">
					<img style="cursor:pointer" src="../../images/delete.png" height="30" width="29" onclick="eliminaranexo('<?php echo $claana; ?>')">
					</td>
				</tr>
			<?php
			}
			?>
			</table>
		</fieldset>
		<?php
		exit();
	}
	if($_GET['agregarnombreanexo'] == 'si')
	{
		$file = $_GET['file'];
		$nomane = $_GET['nomane'];
		mysqli_query($conectar,"update anexos_archivo set ana_nombre = '".$nomane."' where ana_usu_actualiz = '".$usuario."' and (ana_nombre = '' or ana_nombre IS NULL) order by ana_clave_int DESC LIMIT 1");
		exit();
	}
	if($_GET['eliminarcarga'] == 'si')
	{
		$clacar = $_GET['clacar'];
		$con = mysqli_query($conectar,"select * from carga where car_clave_int = '".$clacar."'");
		$dato = mysqli_fetch_array($con);
		$obr = $dato['obr_clave_int'];
		$tii = $dato['tii_clave_int'];
		$nom = $dato['car_nombre'];
		
		$con = mysqli_query($conectar,"delete from carga where car_clave_int = '".$clacar."'");
		mysqli_query($conectar,"delete from carga_foto where car_clave_int = '".$clacar."'");
		if($con > 0)
		{
			$con = mysqli_query($conectar,"select * from notificar_h where obr_clave_int = '".$obr."' and not_tipo = 3");
			$dato = mysqli_fetch_array($con);
			$swtod = $dato['not_sw_todos'];
			$swact = $dato['not_sw_activo'];
			
			$contii = mysqli_query($conectar,"select tii_nombre from tipo_informe where tii_clave_int = '".$tii."'");
			$datoti = mysqli_fetch_array($contii);
			$nomti = $datoti['tii_nombre'];
			
			if($swtod == 1 and $swact == 1)
			{
				$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) inner join obra o on (o.obr_clave_int = uo.obr_clave_int) where uo.obr_clave_int = '".$obr."' and u.usu_usuario <> '".$usuario."'");
				$num = mysqli_num_rows($con);
				
				for($i = 0; $i < $num; $i++)
				{
					$mail = new PHPMailer();
					
					$dato = mysqli_fetch_array($con);
					$mail->Body = '';
					$ema = '';
					$nomusu = $dato['usu_nombre'];
					$ema = $dato['usu_email'];
					$nomobr = $dato['obr_nombre'];
					
					$mail->From = "adminpavas@pavas.com.co";
					$mail->FromName = "I, A & C";
					$mail->AddAddress($ema, "Destino");
					$mail->Subject = "REGISTRO ELIMINADO: ".$nomti.". OBRA: ".$nomobr;
					
					// Cuerpo del mensaje
					$mail->Body .= "Hola ".$nomusu."!\n\n";	
					$mail->Body .= "ZONA CLIENTES registra que se ha eliminado un ".$nomti." de su obra: ".$nomobr.".\n";
					$mail->Body .= "NOMBRE ARCHIVO: ".$nom."\n";
					$mail->Body .= date("d/m/Y H:m:s")."\n\n";
					$mail->Body .= "Este mensaje es generado automáticamente por ZONA CLIENTES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Zonaclientes \n";
					
					if(!$mail->Send())
					{
						//echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
						//echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
					}
					else
					{
					}
				}
			}
			else
			if($swact == 1)
			{
				$con = mysqli_query($conectar,"select * from notificar_h nh inner join notificar_d nd on (nd.not_clave_int = nh.not_clave_int) inner join usuario u on (u.usu_clave_int = nd.usu_clave_int) inner join obra o on (o.obr_clave_int = nh.obr_clave_int) where nh.obr_clave_int = '".$obr."' and nh.not_tipo = 3 and u.usu_usuario <> '".$usuario."'");
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$mail = new PHPMailer();
					
					$dato = mysqli_fetch_array($con);
					$mail->Body = '';
					$ema = '';
					$nomusu = $dato['usu_nombre'];
					$ema = $dato['usu_email'];
					$nomobr = $dato['obr_nombre'];
					
					$mail->From = "adminpavas@pavas.com.co";
					$mail->FromName = "I, A & C";
					$mail->AddAddress($ema, "Destino");
					$mail->Subject = "REGISTRO ELIMINADO: ".$nomti.". OBRA: ".$nomobr;
					
					// Cuerpo del mensaje
					$mail->Body .= "Hola ".$nomusu."!\n\n";	
					$mail->Body .= "ZONA CLIENTES registra que se ha eliminado un ".$nomti." de su obra: ".$nomobr.".\n";
					$mail->Body .= "NOMBRE ARCHIVO: ".$nom."\n";
					$mail->Body .= date("d/m/Y H:m:s")."\n\n";
					$mail->Body .= "Este mensaje es generado automáticamente por ZONA CLIENTES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Zonaclientes \n";
					
					if(!$mail->Send())
					{
						//echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
						//echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
					}
					else
					{
					}
				}
			}
			echo "<div class='ok'>Eliminado</div>";
		}
		else
		{
			echo "<div class='validaciones'>Error</div>";
		}
		exit();
	}
	if($_GET['eliminararchivo'] == 'si')
	{
		$clacaa = $_GET['clacaa'];
		$con = mysqli_query($conectar,"select * from carga_archivo where caa_clave_int = '".$clacaa."'");
		$dato = mysqli_fetch_array($con);
		$obr = $dato['obr_clave_int'];
		$tii = $dato['tii_clave_int'];
		$nom = $dato['caa_nombre'];
		
		$con = mysqli_query($conectar,"delete from carga_archivo where caa_clave_int = '".$clacaa."'");
		mysqli_query($conectar,"delete from anexos_archivo where caa_clave_int = '".$clacaa."'");
		if($con > 0)
		{
			$con = mysqli_query($conectar,"select * from notificar_h where obr_clave_int = '".$obr."' and not_tipo = 3");
			$dato = mysqli_fetch_array($con);
			$swtod = $dato['not_sw_todos'];
			$swact = $dato['not_sw_activo'];
			
			$contii = mysqli_query($conectar,"select tii_nombre from tipo_informe where tii_clave_int = '".$tii."'");
			$datoti = mysqli_fetch_array($contii);
			$nomti = $datoti['tii_nombre'];
			
			if($swtod == 1 and $swact == 1)
			{
				$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) inner join obra o on (o.obr_clave_int = uo.obr_clave_int) where uo.obr_clave_int = '".$obr."' and u.usu_usuario <> '".$usuario."'");
				$num = mysqli_num_rows($con);
				
				for($i = 0; $i < $num; $i++)
				{
					$mail = new PHPMailer();
					
					$dato = mysqli_fetch_array($con);
					$mail->Body = '';
					$ema = '';
					$nomusu = $dato['usu_nombre'];
					$ema = $dato['usu_email'];
					$nomobr = $dato['obr_nombre'];
					
					$mail->From = "adminpavas@pavas.com.co";
					$mail->FromName = "I, A & C";
					$mail->AddAddress($ema, "Destino");
					$mail->Subject = "REGISTRO ELIMINADO: ".$nomti.". OBRA: ".$nomobr;
					
					// Cuerpo del mensaje
					$mail->Body .= "Hola ".$nomusu."!\n\n";	
					$mail->Body .= "ZONA CLIENTES registra que se ha eliminado un ".$nomti." de su obra: ".$nomobr.".\n";
					$mail->Body .= "NOMBRE ARCHIVO: ".$nom."\n";
					$mail->Body .= date("d/m/Y H:m:s")."\n\n";
					$mail->Body .= "Este mensaje es generado automáticamente por ZONA CLIENTES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Zonaclientes \n";
					
					if(!$mail->Send())
					{
						//echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
						//echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
					}
					else
					{
					}
				}
			}
			else
			if($swact == 1)
			{
				$con = mysqli_query($conectar,"select * from notificar_h nh inner join notificar_d nd on (nd.not_clave_int = nh.not_clave_int) inner join usuario u on (u.usu_clave_int = nd.usu_clave_int) inner join obra o on (o.obr_clave_int = nh.obr_clave_int) where nh.obr_clave_int = '".$obr."' and nh.not_tipo = 3 and u.usu_usuario <> '".$usuario."'");
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$mail = new PHPMailer();
					
					$dato = mysqli_fetch_array($con);
					$mail->Body = '';
					$ema = '';
					$nomusu = $dato['usu_nombre'];
					$ema = $dato['usu_email'];
					$nomobr = $dato['obr_nombre'];
					
					$mail->From = "adminpavas@pavas.com.co";
					$mail->FromName = "I, A & C";
					$mail->AddAddress($ema, "Destino");
					$mail->Subject = "REGISTRO ELIMINADO: ".$nomti.". OBRA: ".$nomobr;
					
					// Cuerpo del mensaje
					$mail->Body .= "Hola ".$nomusu."!\n\n";	
					$mail->Body .= "ZONA CLIENTES registra que se ha eliminado un ".$nomti." de su obra: ".$nomobr.".\n";
					$mail->Body .= "NOMBRE ARCHIVO: ".$nom."\n";
					$mail->Body .= date("d/m/Y H:m:s")."\n\n";
					$mail->Body .= "Este mensaje es generado automáticamente por ZONA CLIENTES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Zonaclientes \n";
					
					if(!$mail->Send())
					{
						//echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
						//echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
					}
					else
					{
					}
				}
			}
			echo "<div class='ok'>Eliminado</div>";
		}
		else
		{
			echo "<div class='validaciones'>Error</div>";
		}
		exit();
	}
	if($_GET['buscar'] == 'si')
	{
		$con = mysqli_query($conectar,"select usu_ultimo_estado from usuario where usu_usuario = '".$usuario."'");
		$dato = mysqli_fetch_array($con);
		$ultimoestado = $dato['usu_ultimo_estado'];
		
		$sql1 = mysqli_query($conectar,"select * from carga");
		$num1 = mysqli_num_rows($sql1);
		$sql2 = mysqli_query($conectar,"select * from carga_archivo");
		$num2 = mysqli_num_rows($sql2);
		$total = $num1+$num2;
	
		$obr = $_GET['obras'];
		$inf = $_GET['informes'];
		$nomarc = $_GET['nomarc'];
		$nomane = $_GET['nomane'];
		$nomley = $_GET['nomley'];
		$nomcom = $_GET['nomcom'];
		if($ultimoestado == 'TODOS'){ $est = ''; }elseif($ultimoestado == 'PENDIENTES'){ $est = '0'; }elseif($ultimoestado == 'APROBADOS'){ $est = '1'; }
		$seleccionados = explode(',',$obr);
		$num = count($seleccionados);
		$obras = array();
		for($i = 0; $i < $num; $i++)
		{
			if($seleccionados[$i] != '')
			{
				$obras[$i]=$seleccionados[$i];
			}
		}
		$listaobras=implode(',',$obras);
		
		$seleccionados = explode(',',$inf);
		$num = count($seleccionados);
		$informes = array();
		for($i = 0; $i < $num; $i++)
		{
			if($seleccionados[$i] != '')
			{
				$informes[$i]=$seleccionados[$i];
			}
		}
		$contador=0;
		?>
		<div class="inputs" style="background-color:#E6E8E8">
			<table style="width: 10%">
				<tr>
					<td>&nbsp;</td>
					<td><input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<?php echo $total; ?>,this);" /></td>
					<td>
					<?php
					if($est == '0' and $aprueba == 1)
					{
					?>
					<img style="cursor:pointer" src="../../images/aprobar.png" title="Aprobar" onclick="APROBARINFORMESMASIVO()" height="30" width="29"></td>
					<?php
					}
					?>
					<td>
					<img style="cursor:pointer" src="../../images/delete.png" height="30" width="29" title="ELIMINAR" onclick="ELIMINARINFORMESMASIVO()">
					</td>
					<td><div id="estadoaprobacion" style="width: 500px"></div></td>
				</tr>
			</table>
		</div>
		<?php
		if($informes[0] == 2 or $informes[1] == 2 or $informes[2] == 2 or $informes[3] == 2)
		{
		?>
		<fieldset name="Group1">
			<legend><strong>COMITES DE OBRA</strong></legend>
			<table style="width: 100%" class="section">
			<tr>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4" style="width: 37px">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4">&nbsp;</td>
			</tr>
			<tr>
				<td class="auto-style4">&nbsp;</td>
				<td class="auto-style4"><strong>NOMBRE DEL ARCHIVO</strong></td>
				<td class="auto-style4"><strong>OBRA</strong></td>
				<td class="auto-style4"><strong>COMENTARIOS</strong></td>
				<td class="auto-style4"><strong>FECHA CREACIÓN</strong></td>
				<td class="auto-style4"><strong>ACTUALIZADO POR</strong></td>
				<td></td>
				<td style="width: 37px">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="13">
				<hr>
				</td>
			</tr>
			<?php
			if($listaobras <> '')
			{
				if($nomane <> '')
				{
					$con = mysqli_query($conectar,"select * from carga_archivo ca inner join obra o on (o.obr_clave_int = ca.obr_clave_int) inner join tipo_informe ti on (ti.tii_clave_int = ca.tii_clave_int) where ca.obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and ca.tii_clave_int = 2 and ca.obr_clave_int in (".$listaobras.") and (ca.caa_nombre LIKE REPLACE('%".$nomarc."%',' ','%') OR '".$nomarc."' IS NULL OR '".$nomarc."' = '') and ca.caa_clave_int in (select caa_clave_int from anexos_archivo where ana_nombre LIKE REPLACE('%".$nomane."%',' ','%') OR '".$nomane."' IS NULL OR '".$nomane."' = '') and (ca.caa_comentarios LIKE REPLACE('%".$nomcom."%',' ','%') OR '".$nomcom."' IS NULL OR '".$nomcom."' = '') and (ca.caa_estado = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') order by DATE(SUBSTRING(ca.caa_nombre,1,8)) DESC");
				}
				else
				{
					$con = mysqli_query($conectar,"select * from carga_archivo ca inner join obra o on (o.obr_clave_int = ca.obr_clave_int) inner join tipo_informe ti on (ti.tii_clave_int = ca.tii_clave_int) where ca.obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and ca.tii_clave_int = 2 and ca.obr_clave_int in (".$listaobras.") and (ca.caa_nombre LIKE REPLACE('%".$nomarc."%',' ','%') OR '".$nomarc."' IS NULL OR '".$nomarc."' = '') and (ca.caa_comentarios LIKE REPLACE('%".$nomcom."%',' ','%') OR '".$nomcom."' IS NULL OR '".$nomcom."' = '') and (ca.caa_estado = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') order by DATE(SUBSTRING(ca.caa_nombre,1,8)) DESC");
				}
			}
			else
			{
				if($nomane <> '')
				{
					$con = mysqli_query($conectar,"select * from carga_archivo ca inner join obra o on (o.obr_clave_int = ca.obr_clave_int) inner join tipo_informe ti on (ti.tii_clave_int = ca.tii_clave_int) where ca.obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and ca.tii_clave_int = 2 and (ca.caa_nombre LIKE REPLACE('%".$nomarc."%',' ','%') OR '".$nomarc."' IS NULL OR '".$nomarc."' = '') and ca.caa_clave_int in (select caa_clave_int from anexos_archivo where ana_nombre LIKE REPLACE('%".$nomane."%',' ','%') OR '".$nomane."' IS NULL OR '".$nomane."' = '') and (ca.caa_comentarios LIKE REPLACE('%".$nomcom."%',' ','%') OR '".$nomcom."' IS NULL OR '".$nomcom."' = '') and (ca.caa_estado = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') order by DATE(SUBSTRING(ca.caa_nombre,1,8)) DESC");
				}
				else
				{
					$con = mysqli_query($conectar,"select * from carga_archivo ca inner join obra o on (o.obr_clave_int = ca.obr_clave_int) inner join tipo_informe ti on (ti.tii_clave_int = ca.tii_clave_int) where ca.obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and ca.tii_clave_int = 2 and (ca.caa_nombre LIKE REPLACE('%".$nomarc."%',' ','%') OR '".$nomarc."' IS NULL OR '".$nomarc."' = '') and (ca.caa_comentarios LIKE REPLACE('%".$nomcom."%',' ','%') OR '".$nomcom."' IS NULL OR '".$nomcom."' = '') and (ca.caa_estado = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') order by DATE(SUBSTRING(ca.caa_nombre,1,8)) DESC");
				}
			}
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clacaa = $dato['caa_clave_int'];
				$nom = $dato['caa_nombre'];
				$obr = $dato['obr_nombre'];
				$tii = $dato['tii_nombre'];
				$rut = $dato['caa_ruta'];
				$com = $dato['caa_comentarios'];
				$fec = $dato['caa_fecha_creacion'];
				$usuact = $dato['caa_usu_actualiz'];
				$fecact = $dato['caa_fec_actualiz'];
				
				$array_nombre = explode('.',$rut);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
								
				$conane = mysqli_query($conectar,"select * from anexos_archivo where caa_clave_int = '".$clacaa."'");
				$numane = mysqli_num_rows($conane);
				$contador=$contador+1;
			?>
				<tr style="<?php if($i % 2 == 0){ echo 'background-color:silver'; } ?>" id="obra<?php echo $clacaa; ?>">
					<td class="auto-style1">
					<input onclick="contadorVals(this);" type="checkbox" name="idcatid" id="idcat<?php echo $contador; ?>" value="<?php echo "OINFORME".$dato['caa_clave_int'];?>" />
					</td>
					<td class="auto-style1"><?php echo $nom.".".$extension; ?></td>
					<td class="auto-style1"><?php echo $obr; ?></td>
					<td class="auto-style1"><div style="cursor:pointer" onmouseover="javascript: VentanaFlotante('<?php echo $com; ?>', 300, 120)" onmouseout="javascript: quitarDiv();"><?php echo substr($com,0,50); ?></div></td>
					<td class="auto-style1"><?php echo $fec; ?></td>
					<td class="auto-style1"><?php echo $usuact; ?></td>
					<td><img src="../../images/editar.png" alt="" height="30" width="29" onclick="EDITARARCHIVO('<?php echo $clacaa; ?>')" style="cursor:pointer" title="EDITAR" /></td>
					<td class="fila" style="background-color:#092451;color:white;width:37px;cursor:pointer">
					<a <?php if($rut != ''){ echo "href='iframecargar/$rut'"; }else{ echo "onclick='ALERTASINADJUNTO()'"; } ?> target='_blank' style="color:white;text-decoration:none;text-shadow: 1px 1px 1px #aaa">
					VER ONLINE</a>
					</td>
					<td <?php if($numane > 0){ echo 'class="fila"'; echo 'style="background-color:#092451;color:white;width:37px;cursor:pointer"'; } ?> onclick="MOSTRARMOVIMIENTOANEXO('<?php echo $clacaa; ?>');OCULTARSCROLL()">
					<?php
					if($numane > 0)
					{
					?>
					<div id="verocultaranexo<?php echo $clacaa; ?>" style="text-shadow: 1px 1px 1px #aaa">VER ANEXOS</div><input name="ocultoanexos<?php echo $clacaa; ?>" id="ocultoanexos<?php echo $clacaa; ?>" value="0" type="hidden" />
					<?php
					}
					?>
					</td>
					<td class="fila" style="background-color:#092451;color:white;width:37px;cursor:pointer">
					<a href='descargar.php?clacaa=<?php echo $clacaa; ?>' target='_blank' style="color:white;text-decoration:none;text-shadow: 1px 1px 1px #aaa">
					DESCARGAR</a>
					</td>
					<td>
					<a href='descargarcomitezip.php?clacaa=<?php echo $clacaa; ?>' target='_blank'>
					<img style="cursor:pointer" src="../../images/zip.png" title="Descargar todo" height="30" width="35"></a></td>
					<td>
					<?php
					if($est == '0' and $aprueba == 1)
					{
					?>
					<img style="cursor:pointer" src="../../images/aprobar.png" title="Aprobar" onclick="APROBARCOMITE('<?php echo $clacaa; ?>')" height="30" width="29">
					<?php
					}
					?>
					</td>
					<td>
					<img style="cursor:pointer" src="../../images/delete.png" height="30" width="29" title="ELIMINAR" onclick="ELIMINARARCHIVO('<?php echo $clacaa; ?>','LISTA','<?php echo $i; ?>','obra')">
					</td>
				</tr>
				<tr>
					<td class="auto-style1">
					&nbsp;</td>
					<td class="auto-style1" colspan="12">
					<div id="movimientoanexo<?php echo $clacaa; ?>"></div>
					</td>
				</tr>
			<?php
			}
			?>
			</table>
			</fieldset>
			<?php
			}
			if($informes[0] == 3 or $informes[1] == 3 or $informes[2] == 3 or $informes[3] == 3)
			{
			?>
			<fieldset name="Group1">
				<legend><strong>COMITES TECNICOS</strong></legend>
				<table style="width: 100%" class="section">
				<tr>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4" style="width: 37px">&nbsp;</td>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4">&nbsp;</td>
				</tr>
				<tr>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4"><strong>NOMBRE DEL ARCHIVO</strong></td>
					<td class="auto-style4"><strong>OBRA</strong></td>
					<td class="auto-style4"><strong>COMENTARIOS</strong></td>
					<td class="auto-style4"><strong>FECHA CREACIÓN</strong></td>
					<td class="auto-style4"><strong>ACTUALIZADO POR</strong></td>
					<td></td>
					<td style="width: 37px">&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="14">
					<hr>
					</td>
				</tr>
				<?php
				if($listaobras <> '')
				{
					if($nomane <> '')
					{
						$con = mysqli_query($conectar,"select * from carga_archivo ca inner join obra o on (o.obr_clave_int = ca.obr_clave_int) inner join tipo_informe ti on (ti.tii_clave_int = ca.tii_clave_int) where ca.obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and ca.tii_clave_int = 3 and ca.obr_clave_int in (".$listaobras.") and (ca.caa_nombre LIKE REPLACE('%".$nomarc."%',' ','%') OR '".$nomarc."' IS NULL OR '".$nomarc."' = '') and ca.caa_clave_int in (select caa_clave_int from anexos_archivo where ana_nombre LIKE REPLACE('%".$nomane."%',' ','%') OR '".$nomane."' IS NULL OR '".$nomane."' = '') and (ca.caa_comentarios LIKE REPLACE('%".$nomcom."%',' ','%') OR '".$nomcom."' IS NULL OR '".$nomcom."' = '') and (ca.caa_estado = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') order by DATE(SUBSTRING(ca.caa_nombre,1,8)) DESC");
					}
					else
					{
						$con = mysqli_query($conectar,"select * from carga_archivo ca inner join obra o on (o.obr_clave_int = ca.obr_clave_int) inner join tipo_informe ti on (ti.tii_clave_int = ca.tii_clave_int) where ca.obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and ca.tii_clave_int = 3 and ca.obr_clave_int in (".$listaobras.") and (ca.caa_nombre LIKE REPLACE('%".$nomarc."%',' ','%') OR '".$nomarc."' IS NULL OR '".$nomarc."' = '') and (ca.caa_comentarios LIKE REPLACE('%".$nomcom."%',' ','%') OR '".$nomcom."' IS NULL OR '".$nomcom."' = '') and (ca.caa_estado = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') order by DATE(SUBSTRING(ca.caa_nombre,1,8)) DESC");
					}
				}
				else
				{
					if($nomane <> '')
					{
						$con = mysqli_query($conectar,"select * from carga_archivo ca inner join obra o on (o.obr_clave_int = ca.obr_clave_int) inner join tipo_informe ti on (ti.tii_clave_int = ca.tii_clave_int) where ca.obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and ca.tii_clave_int = 3 and (ca.caa_nombre LIKE REPLACE('%".$nomarc."%',' ','%') OR '".$nomarc."' IS NULL OR '".$nomarc."' = '') and ca.caa_clave_int in (select caa_clave_int from anexos_archivo where ana_nombre LIKE REPLACE('%".$nomane."%',' ','%') OR '".$nomane."' IS NULL OR '".$nomane."' = '') and (ca.caa_comentarios LIKE REPLACE('%".$nomcom."%',' ','%') OR '".$nomcom."' IS NULL OR '".$nomcom."' = '') and (ca.caa_estado = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') order by DATE(SUBSTRING(ca.caa_nombre,1,8)) DESC");
					}
					else
					{
						$con = mysqli_query($conectar,"select * from carga_archivo ca inner join obra o on (o.obr_clave_int = ca.obr_clave_int) inner join tipo_informe ti on (ti.tii_clave_int = ca.tii_clave_int) where ca.obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and ca.tii_clave_int = 3 and (ca.caa_nombre LIKE REPLACE('%".$nomarc."%',' ','%') OR '".$nomarc."' IS NULL OR '".$nomarc."' = '') and (ca.caa_comentarios LIKE REPLACE('%".$nomcom."%',' ','%') OR '".$nomcom."' IS NULL OR '".$nomcom."' = '') and (ca.caa_estado = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') order by DATE(SUBSTRING(ca.caa_nombre,1,8)) DESC");
					}
				}
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$clacaa = $dato['caa_clave_int'];
					$nom = $dato['caa_nombre'];
					$obr = $dato['obr_nombre'];
					$tii = $dato['tii_nombre'];
					$rut = $dato['caa_ruta'];
					$com = $dato['caa_comentarios'];
					$fec = $dato['caa_fecha_creacion'];
					$usuact = $dato['caa_usu_actualiz'];
					$fecact = $dato['caa_fec_actualiz'];
					
					$array_nombre = explode('.',$rut);
					$cuenta_arr_nombre = count($array_nombre);
					$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
										
					$conane = mysqli_query($conectar,"select * from anexos_archivo where caa_clave_int = '".$clacaa."'");
					$numane = mysqli_num_rows($conane);
					$contador=$contador+1;
				?>
					<tr style="<?php if($i % 2 == 0){ echo 'background-color:silver'; } ?>" id="tecnico<?php echo $clacaa; ?>">
						<td class="auto-style1">
						<input onclick="contadorVals(this);" type="checkbox" name="idcatid" id="idcat<?php echo $contador; ?>" value="<?php echo "CINFORME".$dato['caa_clave_int'];?>" />
						</td>
						<td class="auto-style1"><?php echo $nom.".".$extension; ?></td>
						<td class="auto-style1"><?php echo $obr; ?></td>
						<td class="auto-style1"><div style="cursor:pointer" onmouseover="javascript: VentanaFlotante('<?php echo $com; ?>', 300, 120)" onmouseout="javascript: quitarDiv();"><?php echo substr($com,0,50); ?></div></td>
						<td class="auto-style1"><?php echo $fec; ?></td>
						<td class="auto-style1"><?php echo $usuact; ?></td>
						<td><img src="../../images/editar.png" alt="" height="30" width="29" onclick="EDITARARCHIVO('<?php echo $clacaa; ?>')" style="cursor:pointer" title="EDITAR" /></td>
						<td class="fila" style="background-color:#092451;color:white;width:37px;cursor:pointer">
						<a <?php if($rut != ''){ echo "href='iframecargar/$rut'"; }else{ echo "onclick='ALERTASINADJUNTO()'"; } ?> target='_blank' style="color:white;text-decoration:none;text-shadow: 1px 1px 1px #aaa">
						VER ONLINE</a>
						</td>
						<td <?php if($numane > 0){ echo 'class="fila"'; echo 'style="background-color:#092451;color:white;width:37px;cursor:pointer"'; } ?> onclick="MOSTRARMOVIMIENTOANEXO('<?php echo $clacaa; ?>');OCULTARSCROLL()">
						<?php
						if($numane > 0)
						{
						?>
						<div id="verocultaranexo<?php echo $clacaa; ?>" style="text-shadow: 1px 1px 1px #aaa">VER ANEXOS</div><input name="ocultoanexos<?php echo $clacaa; ?>" id="ocultoanexos<?php echo $clacaa; ?>" value="0" type="hidden" />
						<?php
						}
						?>
						</td>
						<td class="fila" style="background-color:#092451;color:white;width:37px;cursor:pointer">
						<a href='descargar.php?clacaa=<?php echo $clacaa; ?>' target='_blank' style="color:white;text-decoration:none;text-shadow: 1px 1px 1px #aaa">
						DESCARGAR</a>
						</td>
						<td>
						<a href='descargarcomitezip.php?clacaa=<?php echo $clacaa; ?>' target='_blank'>
						<img style="cursor:pointer" src="../../images/zip.png" title="Descargar todo" height="30" width="35"></a></td>
						<td>
						<?php
						if($est == '0' and $aprueba == 1)
						{
						?>
						<img style="cursor:pointer" src="../../images/aprobar.png" title="Aprobar" onclick="APROBARCOMITE('<?php echo $clacaa; ?>')" height="30" width="29">
						<?php
						}
						?>
						</td>
						<td>
						<img style="cursor:pointer" src="../../images/delete.png" height="30" width="29" title="ELIMINAR" onclick="ELIMINARARCHIVO('<?php echo $clacaa; ?>','LISTA','<?php echo $i; ?>','tecnico')">
						</td>
					</tr>
					<tr>
						<td class="auto-style1">
						&nbsp;</td>
						<td class="auto-style1" colspan="12">
						<div id="movimientoanexo<?php echo $clacaa; ?>"></div>
						</td>
						<td class="auto-style1">
						&nbsp;</td>
					</tr>
				<?php
				}
				?>
				</table>
				</fieldset>
				<?php
				}
				if($informes[0] == 4 or $informes[1] == 4 or $informes[2] == 4 or $informes[3] == 4)
				{
				?>
				<fieldset name="Group1">
					<legend><strong>INFORMES MENSUALES</strong></legend>
				<table style="width: 100%" class="section">
				<tr>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4" style="width: 37px">&nbsp;</td>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4">&nbsp;</td>
				</tr>
				<tr>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4"><strong>NOMBRE DEL ARCHIVO</strong></td>
					<td class="auto-style4"><strong>OBRA</strong></td>
					<td class="auto-style4"><strong>COMENTARIOS</strong></td>
					<td class="auto-style4"><strong>FECHA CREACIÓN</strong></td>
					<td class="auto-style4"><strong>ACTUALIZADO POR</strong></td>
					<td></td>
					<td style="width: 37px">&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="14">
					<hr>
					</td>
				</tr>
				<?php
				if($listaobras <> '')
				{
					if($nomane <> '')
					{
						$con = mysqli_query($conectar,"select * from carga_archivo ca inner join obra o on (o.obr_clave_int = ca.obr_clave_int) inner join tipo_informe ti on (ti.tii_clave_int = ca.tii_clave_int) where ca.obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and ca.tii_clave_int = 4 and ca.obr_clave_int in (".$listaobras.") and (ca.caa_nombre LIKE REPLACE('%".$nomarc."%',' ','%') OR '".$nomarc."' IS NULL OR '".$nomarc."' = '') and ca.caa_clave_int in (select caa_clave_int from anexos_archivo where ana_nombre LIKE REPLACE('%".$nomane."%',' ','%') OR '".$nomane."' IS NULL OR '".$nomane."' = '') and (ca.caa_comentarios LIKE REPLACE('%".$nomcom."%',' ','%') OR '".$nomcom."' IS NULL OR '".$nomcom."' = '') and (ca.caa_estado = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') order by DATE(SUBSTRING(ca.caa_nombre,1,8)) DESC");
					}
					else
					{
						$con = mysqli_query($conectar,"select * from carga_archivo ca inner join obra o on (o.obr_clave_int = ca.obr_clave_int) inner join tipo_informe ti on (ti.tii_clave_int = ca.tii_clave_int) where ca.obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and ca.tii_clave_int = 4 and ca.obr_clave_int in (".$listaobras.") and (ca.caa_nombre LIKE REPLACE('%".$nomarc."%',' ','%') OR '".$nomarc."' IS NULL OR '".$nomarc."' = '') and (ca.caa_comentarios LIKE REPLACE('%".$nomcom."%',' ','%') OR '".$nomcom."' IS NULL OR '".$nomcom."' = '') and (ca.caa_estado = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') order by DATE(SUBSTRING(ca.caa_nombre,1,8)) DESC");
					}
				}
				else
				{
					if($nomane <> '')
					{
						$con = mysqli_query($conectar,"select * from carga_archivo ca inner join obra o on (o.obr_clave_int = ca.obr_clave_int) inner join tipo_informe ti on (ti.tii_clave_int = ca.tii_clave_int) where ca.obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and ca.tii_clave_int = 4 and (ca.caa_nombre LIKE REPLACE('%".$nomarc."%',' ','%') OR '".$nomarc."' IS NULL OR '".$nomarc."' = '') and ca.caa_clave_int in (select caa_clave_int from anexos_archivo where ana_nombre LIKE REPLACE('%".$nomane."%',' ','%') OR '".$nomane."' IS NULL OR '".$nomane."' = '') and (ca.caa_comentarios LIKE REPLACE('%".$nomcom."%',' ','%') OR '".$nomcom."' IS NULL OR '".$nomcom."' = '') and (ca.caa_estado = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') order by DATE(SUBSTRING(ca.caa_nombre,1,8)) DESC");
					}
					else
					{
						$con = mysqli_query($conectar,"select * from carga_archivo ca inner join obra o on (o.obr_clave_int = ca.obr_clave_int) inner join tipo_informe ti on (ti.tii_clave_int = ca.tii_clave_int) where ca.obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and ca.tii_clave_int = 4 and (ca.caa_nombre LIKE REPLACE('%".$nomarc."%',' ','%') OR '".$nomarc."' IS NULL OR '".$nomarc."' = '') and (ca.caa_comentarios LIKE REPLACE('%".$nomcom."%',' ','%') OR '".$nomcom."' IS NULL OR '".$nomcom."' = '') and (ca.caa_estado = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') order by DATE(SUBSTRING(ca.caa_nombre,1,8)) DESC");
					}
				}
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$clacaa = $dato['caa_clave_int'];
					$nom = $dato['caa_nombre'];
					$obr = $dato['obr_nombre'];
					$tii = $dato['tii_nombre'];
					$rut = $dato['caa_ruta'];
					$com = $dato['caa_comentarios'];
					$fec = $dato['caa_fecha_creacion'];
					$usuact = $dato['caa_usu_actualiz'];
					$fecact = $dato['caa_fec_actualiz'];
					
					$array_nombre = explode('.',$rut);
					$cuenta_arr_nombre = count($array_nombre);
					$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
										
					$conane = mysqli_query($conectar,"select * from anexos_archivo where caa_clave_int = '".$clacaa."'");
					$numane = mysqli_num_rows($conane);
					$contador=$contador+1;
				?>
					<tr style="<?php if($i % 2 == 0){ echo 'background-color:silver'; } ?>" id="mensual<?php echo $clacaa; ?>">
						<td class="auto-style1">
						<input onclick="contadorVals(this);" type="checkbox" name="idcatid" id="idcat<?php echo $contador; ?>" value="<?php echo "IINFORME".$dato['caa_clave_int'];?>" />
						</td>
						<td class="auto-style1"><?php echo $nom.".".$extension; ?></td>
						<td class="auto-style1"><?php echo $obr; ?></td>
						<td class="auto-style1"><div style="cursor:pointer" onmouseover="javascript: VentanaFlotante('<?php echo $com; ?>', 300, 120)" onmouseout="javascript: quitarDiv();"><?php echo substr($com,0,50); ?></div></td>
						<td class="auto-style1"><?php echo $fec; ?></td>
						<td class="auto-style1"><?php echo $usuact; ?></td>
						<td><img src="../../images/editar.png" alt="" height="30" width="29" onclick="EDITARARCHIVO('<?php echo $clacaa; ?>')" style="cursor:pointer" title="EDITAR" /></td>
						<td class="fila" style="background-color:#092451;color:white;width:37px;cursor:pointer">
						<a <?php if($rut != ''){ echo "href='iframecargar/$rut'"; }else{ echo "onclick='ALERTASINADJUNTO()'"; } ?> target='_blank' style="color:white;text-decoration:none;text-shadow: 1px 1px 1px #aaa">
						VER ONLINE</a>
						</td>
						<td <?php if($numane > 0){ echo 'class="fila"'; echo 'style="background-color:#092451;color:white;width:37px;cursor:pointer"'; } ?> onclick="MOSTRARMOVIMIENTOANEXO('<?php echo $clacaa; ?>');OCULTARSCROLL()">
						<?php
						if($numane > 0)
						{
						?>
						<div id="verocultaranexo<?php echo $clacaa; ?>" style="text-shadow: 1px 1px 1px #aaa">VER ANEXOS</div><input name="ocultoanexos<?php echo $clacaa; ?>" id="ocultoanexos<?php echo $clacaa; ?>" value="0" type="hidden" />
						<?php
						}
						?>
						</td>
						<td class="fila" style="background-color:#092451;color:white;width:37px;cursor:pointer">
						<a href='descargar.php?clacaa=<?php echo $clacaa; ?>' target='_blank' style="color:white;text-decoration:none;text-shadow: 1px 1px 1px #aaa">
						DESCARGAR</a>
						</td>
						<td>
						<a href='descargarcomitezip.php?clacaa=<?php echo $clacaa; ?>' target='_blank'>
						<img style="cursor:pointer" src="../../images/zip.png" title="Descargar todo" height="30" width="35"></a></td>
						<td>
						<?php
						if($est == '0' and $aprueba == 1)
						{
						?>
						<img style="cursor:pointer" src="../../images/aprobar.png" title="Aprobar" onclick="APROBARCOMITE('<?php echo $clacaa; ?>')" height="30" width="29">
						<?php
						}
						?>
						</td>
						<td>
						<img style="cursor:pointer" src="../../images/delete.png" height="30" width="29" title="ELIMINAR" onclick="ELIMINARARCHIVO('<?php echo $clacaa; ?>','LISTA','<?php echo $i; ?>','mensual')">
						</td>
					</tr>
					<tr>
						<td class="auto-style1">
						&nbsp;</td>
						<td class="auto-style1" colspan="12">
						<div id="movimientoanexo<?php echo $clacaa; ?>"></div>
						</td>
						<td class="auto-style1">
						&nbsp;</td>
					</tr>
				<?php
				}
				?>
				</table>
				</fieldset>
				<?php
				}
				if($informes[0] == 1 or $informes[1] == 1 or $informes[2] == 1 or $informes[3] == 1)
				{
				?>
				<fieldset name="Group1">
					<legend><strong>REGISTRO FOTOGRAFICO</strong></legend>
				<table style="width: 100%" class="section">
				<tr>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4">&nbsp;</td>
				</tr>
				<tr>
					<td class="auto-style4">&nbsp;</td>
					<td class="auto-style4"><strong>NOMBRE DEL ARCHIVO</strong></td>
					<td class="auto-style4"><strong>OBRA</strong></td>
					<td class="auto-style4"><strong>FECHA CREACIÓN</strong></td>
					<td class="auto-style4"><strong>ACTUALIZADO POR</strong></td>
					<td></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="10">
					<hr>
					</td>
				</tr>
				<?php
				if($listaobras <> '')
				{
					if($nomley <> '')
					{
						$con = mysqli_query($conectar,"select * from carga c inner join obra o on (o.obr_clave_int = c.obr_clave_int) inner join tipo_informe ti on (ti.tii_clave_int = c.tii_clave_int) where c.obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and c.obr_clave_int in (".$listaobras.") and (c.car_nombre LIKE REPLACE('%".$nomarc."%',' ','%') OR '".$nomarc."' IS NULL OR '".$nomarc."' = '') and c.car_clave_int in (select car_clave_int from carga_foto where caf_leyenda LIKE REPLACE('%".$nomley."%',' ','%') OR '".$nomley."' IS NULL OR '".$nomley."' = '') and (c.car_estado = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') order by CAST(car_nombre AS SIGNED INTEGER) DESC");
					}
					else
					{
						$con = mysqli_query($conectar,"select * from carga c inner join obra o on (o.obr_clave_int = c.obr_clave_int) inner join tipo_informe ti on (ti.tii_clave_int = c.tii_clave_int) where c.obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and c.obr_clave_int in (".$listaobras.") and (c.car_nombre LIKE REPLACE('%".$nomarc."%',' ','%') OR '".$nomarc."' IS NULL OR '".$nomarc."' = '') and (c.car_estado = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') order by CAST(car_nombre AS SIGNED INTEGER) DESC");
					}
				}
				else
				{
					if($nomley <> '')
					{
						$con = mysqli_query($conectar,"select * from carga c inner join obra o on (o.obr_clave_int = c.obr_clave_int) inner join tipo_informe ti on (ti.tii_clave_int = c.tii_clave_int) where c.obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and (c.car_nombre LIKE REPLACE('%".$nomarc."%',' ','%') OR '".$nomarc."' IS NULL OR '".$nomarc."' = '') and c.car_clave_int in (select car_clave_int from carga_foto where caf_leyenda LIKE REPLACE('%".$nomley."%',' ','%') OR '".$nomley."' IS NULL OR '".$nomley."' = '') and (c.car_estado = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') order by CAST(car_nombre AS SIGNED INTEGER) DESC");
					}
					else
					{
						$con = mysqli_query($conectar,"select * from carga c inner join obra o on (o.obr_clave_int = c.obr_clave_int) inner join tipo_informe ti on (ti.tii_clave_int = c.tii_clave_int) where c.obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and (c.car_nombre LIKE REPLACE('%".$nomarc."%',' ','%') OR '".$nomarc."' IS NULL OR '".$nomarc."' = '') and (c.car_estado = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') order by CAST(car_nombre AS SIGNED INTEGER) DESC");
					}
				}
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$clacar = $dato['car_clave_int'];
					$nom = $dato['car_nombre'];
					$obr = $dato['obr_nombre'];
					$tii = $dato['tii_nombre'];
					$fec = $dato['car_fecha_creacion'];
					$usuact = $dato['car_usu_actualiz'];
					$fecact = $dato['car_fec_actualiz'];
					$contador=$contador+1;
				?>
					<tr style="<?php if($i % 2 == 0){ echo 'background-color:silver'; } ?>" id="imagenes<?php echo $clacar; ?>">
						<td class="auto-style1">
						<input onclick="contadorVals(this);" type="checkbox" name="idcatid" id="idcat<?php echo $contador; ?>" value="<?php echo "FOTO".$dato['car_clave_int'];?>" />
						</td>
						<td class="auto-style1"><?php echo $nom; ?></td>
						<td class="auto-style1"><?php echo $obr; ?></td>
						<td class="auto-style1"><?php echo $fec; ?></td>
						<td class="auto-style1"><?php echo $usuact; ?></td>
						<td><img src="../../images/editar.png" alt="" height="30" width="29" onclick="EDITAR('<?php echo $clacar; ?>')" style="cursor:pointer" title="EDITAR" /></td>
						<td class="fila" style="background-color:#092451;color:white;width:37px;cursor:pointer" onclick="parent.mostrarVentana('<?php echo $clacar; ?>');OCULTARSCROLL()">
						<div id="verocultofotos<?php echo $clacar; ?>" style="text-shadow: 1px 1px 1px #aaa">VER FOTOS</div><input name="ocultofotos<?php echo $clacar; ?>" id="ocultofotos<?php echo $clacar; ?>" value="0" type="hidden" />
						</td>
						<td>
						<a href='descargarregistrofotograficozip.php?clacar=<?php echo $clacar; ?>' target='_blank'>
						<img style="cursor:pointer" src="../../images/zip.png" title="Descargar todo" height="30" width="35"></a>
						</td>
						<td>
						<?php
						if($est == '0' and $aprueba == 1)
						{
						?>
						<img style="cursor:pointer" src="../../images/aprobar.png" title="Aprobar" onclick="APROBARREGISTROFOTOGRAFICO('<?php echo $clacar; ?>')" height="30" width="29">
						<?php
						}
						?>
						</td>
						<td>
						<img style="cursor:pointer" src="../../images/delete.png" height="30" width="29" title="ELIMINAR" onclick="ELIMINARCARGA('<?php echo $clacar; ?>','LISTA','<?php echo $i; ?>')">
						</td>
					</tr>
					<tr>
						<td class="auto-style1">
						&nbsp;</td>
						<td class="auto-style1" colspan="9">
						<div id="movimiento<?php echo $clacar; ?>"></div>
						</td>
					</tr>
				<?php
				}
				?>
				</table>
			</fieldset>
		<?php
			}
		?>
		<input name="contador1" id="contador1" value="<?php echo $contador; ?>" type="hidden" />
		<?php
		exit();
	}
	if($_GET['insrtardatos'] == 'si')
	{
		$fecha=date("Y/m/d H:i:s");
		$nom = $_GET['nom'];
		$obr = $_GET['obr'];
		$ti = $_GET['ti'];
		$com = $_GET['com'];
		mysqli_query($conectar,"INSERT INTO carga_archivo(caa_clave_int,obr_clave_int,tii_clave_int,caa_nombre,caa_ruta,caa_ruta_original,caa_comentarios,caa_fecha_creacion,caa_usu_creacion,caa_usu_actualiz,caa_fec_actualiz) values(null,'".$obr."','".$ti."','".$nom."','','','".$com."','".$fecha."','".$usuario."','".$usuario."','".$fecha."')");
		exit();
	}
	if($_GET['mostraropcion2'] == 'si')
	{
		?>
		<iframe src="iframeopcion2/index.php" name="iframeopc2" id="iframeopc2" frameborder="1" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen style="width: 100%; height:1000px; border-style:dashed; overflow-y:auto; overflow-x:hidden;"></iframe>
		<?php
		exit();
	}
	if($_GET['aprobarinformesmasivo'] == 'si')
	{
		$inf = $_GET['inf'];

		$seleccionados = explode(',',$inf);
		$num = count($seleccionados);
		for($i = 0; $i < $num; $i++)
		{
			if($seleccionados[$i] != '')
			{
				if(substr($seleccionados[$i], 1, 7) == "INFORME")
				{
					$clave = substr($seleccionados[$i], 8, 4);
					mysqli_query($conectar,"update carga_archivo set caa_estado = 1 where caa_clave_int = '".$clave."'");
				}
				else
				if(substr($seleccionados[$i], 0, 4) == "FOTO")
				{
					$clave = substr($seleccionados[$i], 4, 4);
					mysqli_query($conectar,"update carga set car_estado = 1 where car_clave_int = '".$clave."'");
				}
			}
		}
		echo "<div class='ok'>Los informes seleccionados han sido aprobados correctamente</div>";
		echo "<style onload='VERCANTIDADES();'></style>";
		exit();
	}
	if($_GET['vercantidades'] == 'si')
	{
		?>
		<table style="width: 100%">
			<tr>
				<td style="width: 90px; cursor:pointer;<?php if($ultimoestado == 'NUEVO'){ echo 'background-color:#092451;color:#ffffff'; } ?>" class="section" onclick="NUEVACARGA()" onmouseover="this.style.backgroundColor='#092451';this.style.color='#ffffff';"  onmouseout="<?php if($ultimoestado <> 'NUEVO'){ echo "this.style.backgroundColor='#ffffff';this.style.color='#000000'"; } ?>">
				Nuevo
				</td>
				<td style="width: 90px; cursor:pointer;<?php if($ultimoestado == 'TODOS'){ echo 'background-color:#092451;color:#ffffff'; } ?>" class="section" onclick="VERTODOS('T')" onmouseover="this.style.backgroundColor='#092451';this.style.color='#ffffff';"  onmouseout="<?php if($ultimoestado <> 'TODOS'){ echo "this.style.backgroundColor='#ffffff';this.style.color='#000000'"; } ?>">
				<div id="todos">
				Todos 
				<?php 
				$con = mysqli_query($conectar,"select * from carga where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.")");
				$num = mysqli_num_rows($con);
				$con = mysqli_query($conectar,"select * from carga_archivo where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.")");
				$num1 = mysqli_num_rows($con);
				echo $num+$num1;
				?>
				</div>
				</td>
				<td style="width: 160px; cursor:pointer;<?php if($ultimoestado == 'PENDIENTES'){ echo 'background-color:#092451;color:#ffffff'; } ?>" class="section" onclick="VERTODOS('0')" onmouseover="this.style.backgroundColor='#092451';this.style.color='#ffffff';"  onmouseout="<?php if($ultimoestado <> 'PENDIENTES'){ echo "this.style.backgroundColor='#ffffff';this.style.color='#000000'"; } ?>">
				<div id="todos">
				Pendiente coordinador 
				<?php 
				$con = mysqli_query($conectar,"select * from carga where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and car_estado = 0");
				$num = mysqli_num_rows($con);
				$con = mysqli_query($conectar,"select * from carga_archivo where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and caa_estado = 0");
				$num1 = mysqli_num_rows($con);
				echo $num+$num1;
				?>
				</div>
				</td>
				<td style="width: 100px; cursor:pointer;<?php if($ultimoestado == 'APROBADOS'){ echo 'background-color:#092451;color:#ffffff'; } ?>" class="section" onclick="VERTODOS('1')" onmouseover="this.style.backgroundColor='#092451';this.style.color='#ffffff';"  onmouseout="<?php if($ultimoestado <> 'APROBADOS'){ echo "this.style.backgroundColor='#ffffff';this.style.color='#000000'"; } ?>">
				<div id="todos">
				Aprobados 
				<?php 
				$con = mysqli_query($conectar,"select * from carga where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and car_estado = 1");
				$num = mysqli_num_rows($con);
				$con = mysqli_query($conectar,"select * from carga_archivo where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and caa_estado = 1");
				$num1 = mysqli_num_rows($con);
				echo $num+$num1;
				?>
				</div>
				</td>
				<td class="auto-style2" style="width: 600px"><strong>CARGAR </strong> </td>
				<td>&nbsp;</td>
			</tr>
		</table>
		<?php
		exit();
	}
	if($_GET['eliminarinformesmasivo'] == 'si')
	{
		$inf = $_GET['inf'];

		$seleccionados = explode(',',$inf);
		$num = count($seleccionados);
		for($i = 0; $i < $num; $i++)
		{
			if($seleccionados[$i] != '')
			{
				if(substr($seleccionados[$i], 1, 7) == "INFORME")
				{
					$clave = substr($seleccionados[$i], 8, 4);
					mysqli_query($conectar,"delete from carga_archivo where caa_clave_int = '".$clave."'");
					mysqli_query($conectar,"delete from anexos_archivo where caa_clave_int = '".$clave."'");
				}
				else
				if(substr($seleccionados[$i], 0, 4) == "FOTO")
				{
					$clave = substr($seleccionados[$i], 4, 4);
					mysqli_query($conectar,"delete from carga where car_clave_int = '".$clave."'");
					mysqli_query($conectar,"delete from carga_foto where car_clave_int = '".$clave."'");
				}
			}
		}
		echo "<div class='ok'>Los informes seleccionados han sido eliminados correctamente</div>";
		echo "<style onload='VERCANTIDADES();'></style>";
		exit();
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title>Cargar</title>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="llamadas11.js"></script>
<link rel="stylesheet" href="css/style.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../js/jsNotifications/ext/jboesch-Gritter/css/jquery.gritter.css">
<script type="text/javascript" src="../../js/jsNotifications/ext/jboesch-Gritter/js/jquery.gritter.min.js"></script>
<script type="text/javascript" src="../../js/jsNotifications/jsNotifications.js"></script>
<script type="text/javascript" language="javascript">
function VERINFO()
{
	$(function(){
		//create a new instance of jsNotifications class and set up the general settings 
		var objInstanceName=new jsNotifications({
			autoCloseTime : 5,
			showAlerts: true,
			title: 'INFORMACION'
		});
		objInstanceName.show('info','Recuerde que el tamaño máximo permitido es de <strong>8MB</strong> por archivo.');
	});
}
</script>
<script language="javascript">
function ADJUNTAR(v,act,ci)
{
	var nom = $('#nombre'+v).val();
	if(act == 'SI')
	{
		var obr = $('#obra1').val();
		var ti = $('#tipoinforme1').val();
	}
	else
	{
		var obr = $('#obra').val();
		var ti = $('#tipoinforme').val();
	}
	var adj = $(".fileName").text();
	var com = $('#comentario'+v).val();
	
	if(obr == '')
	{
		alert("Debe elegir la obra");
	}
	else
	if(ti == '')
	{
		alert("Debe elegir el tipo de informe");
	}
	else
	if(nom == '')
	{
		alert("Debe ingresar el nombre del archivo");
	}
	else
	if(act == 'SI')
	{
		ACTUALIZARARCHIVO(nom,obr,ti,com,ci);
		javascript:$('#fileUpload'+v).fileUploadStart();
	}
	else
	if(adj == '')
	{
		alert("Debe elegir el archivo");
	}
	else
	{
		//INSERTARDATOS(nom,obr,ti,com);//Ya se guarda la informacion directamente en el archivo 
		//donde se suben los aarchivos para evitar una mala carga
		//RESULTADOADJUNTO(v);
		REFRESCARTODOS();
		javascript:$('#fileUpload1'+v).fileUploadStart();
		if(act == 'NO')
		{
			var div = document.getElementById('ocultarbotonsubir'+v);
			div.style.display = 'none';
		}
		if(act == 'NO')
		{
			AGREGAR();
		}
	}
}
function ALERTA()
{
	alert("Debe guardar para agregar imagenes");
}
</script>
<script type="text/javascript" src="js/jquery.uploadify3.js"></script>
<link type="text/css" rel="stylesheet" href="css/uploadify.css"  />
<script type="text/javascript">
function REFRESCARBOTON(v)
{
	$("#fileUpload"+v).fileUpload({
			'uploader': 'images/uploader.swf',
			'cancelImg': 'images/cancel.png',
			'folder': 'iframecargar/uploads/',
			'buttonText': 'Seleccionar',
			'checkScript': 'check.php',
			'script': 'uploadmultiple.php',
			'multi': true,
			'simUploadLimit': 2,
			'onComplete': function(event, queueID, fileObj, response, data) {
                 var nomane = prompt("Agregar nombre al anexo: "+fileObj['name'], "");
				 if(nomane != ''){ AGREGARNOMBREANEXO(fileObj['name'],nomane); }
				 VERANEXOS(fileObj['name'],nomane);
				 $(".fileName").html("");
             }
		});
}
function REFRESCARBOTON1(v)
{
	$("#fileUpload1"+v).fileUpload({
			'uploader': 'images/uploader.swf',
			'cancelImg': 'images/cancel.png',
			'folder': 'iframecargar/uploads/',
			'buttonText': 'Seleccionar',
			'checkScript': 'check.php',
			'script': 'uploadarchivo.php',
			'multi': false,
			'simUploadLimit': 1,
			'sizeLimit':'8100000',//8.1MB
			'scriptData':{'nombrearchivo':$('#nombre'+v).val(), 'obra':$('#obra').val(), 'tipoinforme':$('#tipoinforme').val(), 'comentario':$('#comentario'+v).val()},
			'onComplete': function(event, queueID, fileObj, response, data) {
				//$("#estadoactualizar").html("Terminando... <img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />");
				//Mostrar progreso
                 /*var nomane = prompt("Agregar nombre al anexo: "+fileObj['name'], "");
				 if(nomane != ''){ AGREGARNOMBREANEXO(fileObj['name'],nomane); }
				 VERANEXOS(fileObj['name'],nomane);*/
             },
             onError: function (event, queueID ,fileObj, errorObj) {
		     },
             onAllComplete: function(event, data){
             	javascript:$('#fileUpload'+v).fileUploadStart();
				alert("CARGA REALIZADA CORRECTAMENTE");
			}
		});
	$('#nombre'+v).bind('change', function(){
		$('#fileUpload1'+v).fileUploadSettings('scriptData','&nombrearchivo='+$(this).val()+'&obra='+$('#obra').val()+'&tipoinforme='+$('#tipoinforme').val()+'&comentario='+$('#comentario'+v).val());
	});
	$('#obra').bind('change', function(){
		$('#fileUpload1'+v).fileUploadSettings('scriptData','&nombrearchivo='+$('#nombre'+v).val()+'&obra='+$(this).val()+'&tipoinforme='+$('#tipoinforme').val()+'&comentario='+$('#comentario'+v).val());
	});
	$('#tipoinforme').bind('change', function(){
		$('#fileUpload1'+v).fileUploadSettings('scriptData','&nombrearchivo='+$('#nombre'+v).val()+'&obra='+$('#obra').val()+'&tipoinforme='+$(this).val()+'&comentario='+$('#comentario'+v).val());
	});
	$('#comentario'+v).bind('change', function(){
		$('#fileUpload1'+v).fileUploadSettings('scriptData','&nombrearchivo='+$('#nombre'+v).val()+'&obra='+$('#obra').val()+'&tipoinforme='+$('#tipoinforme').val()+'&comentario='+$(this).val());
	});
}
function REFRESCARBOTONACTUALIZAR1(v,ci)
{
	$("#fileUpload"+v).fileUpload({
		'uploader': 'images/uploader.swf',
		'cancelImg': 'images/cancel.png',
		'folder': 'iframecargar/uploads/',
		'buttonText': 'Seleccionar',
		'checkScript': 'check.php',
		'script': 'uploadarchivo.php?ci='+ci,
		'multi': true,
		'sizeLimit':'8100000',//8.1MB
		'simUploadLimit': 2,
		onError: function (event, queueID ,fileObj, errorObj) {
	     },
		'onComplete': function(event, queueID, fileObj, response, data) {
             javascript:$('#fileUpload2').fileUploadStart();
         }
	});
}
function REFRESCARBOTONACTUALIZAR2(v,ci)
{
	$("#fileUpload"+v).fileUpload({
		'uploader': 'images/uploader.swf',
		'cancelImg': 'images/cancel.png',
		'folder': 'iframecargar/uploads/',
		'buttonText': 'Seleccionar',
		'checkScript': 'check.php',
		'script': 'uploadmultiple.php?ci='+ci,
		'multi': true,
		'sizeLimit':'8100000',//8.1MB
		'simUploadLimit': 2,
		onError: function (event, queueID ,fileObj, errorObj) {
	     },
		'onComplete': function(event, queueID, fileObj, response, data) {
             var nomane = prompt("Agregar nombre al anexo: "+fileObj['name'], "");
			 if(nomane != ''){ AGREGARNOMBREANEXO(fileObj['name'],nomane); }
			 VERANEXOS(fileObj['name'],nomane);
         }
	});
}
function ACTUALIZARFOTOS(v)
{
	VERFOTOSAGREGADAS(v);
}
</script>
<script type="text/javascript" language="javascript">
function OCULTARSCROLL()
{
	setTimeout("parent.autoResize('iframe6')",500);
	setTimeout("parent.autoResize('iframe6')",1000);
	setTimeout("parent.autoResize('iframe6')",2000);
	setTimeout("parent.autoResize('iframe6')",3000);
	setTimeout("parent.autoResize('iframe6')",4000);
	setTimeout("parent.autoResize('iframe6')",5000);
	setTimeout("parent.autoResize('iframe6')",6000);
	setTimeout("parent.autoResize('iframe6')",7000);
	setTimeout("parent.autoResize('iframe6')",8000);
	setTimeout("parent.autoResize('iframe6')",9000);
	setTimeout("parent.autoResize('iframe6')",10000);
	setTimeout("parent.autoResize('iframe6')",11000);
	setTimeout("parent.autoResize('iframe6')",12000);
	setTimeout("parent.autoResize('iframe6')",13000);
	setTimeout("parent.autoResize('iframe6')",14000);
	setTimeout("parent.autoResize('iframe6')",15000);
	setTimeout("parent.autoResize('iframe6')",16000);
	setTimeout("parent.autoResize('iframe6')",17000);
	setTimeout("parent.autoResize('iframe6')",18000);
	setTimeout("parent.autoResize('iframe6')",19000);
	setTimeout("parent.autoResize('iframe6')",20000);
}
setTimeout("parent.autoResize('iframe6')",500);
setTimeout("parent.autoResize('iframe6')",1000);
setTimeout("parent.autoResize('iframe6')",2000);
setTimeout("parent.autoResize('iframe6')",3000);
setTimeout("parent.autoResize('iframe6')",4000);
setTimeout("parent.autoResize('iframe6')",5000);
setTimeout("parent.autoResize('iframe6')",6000);
setTimeout("parent.autoResize('iframe6')",7000);
setTimeout("parent.autoResize('iframe6')",8000);
setTimeout("parent.autoResize('iframe6')",9000);
setTimeout("parent.autoResize('iframe6')",10000);
setTimeout("parent.autoResize('iframe6')",11000);
setTimeout("parent.autoResize('iframe6')",12000);
setTimeout("parent.autoResize('iframe6')",13000);
setTimeout("parent.autoResize('iframe6')",14000);
setTimeout("parent.autoResize('iframe6')",15000);
setTimeout("parent.autoResize('iframe6')",16000);
setTimeout("parent.autoResize('iframe6')",17000);
setTimeout("parent.autoResize('iframe6')",18000);
setTimeout("parent.autoResize('iframe6')",19000);
setTimeout("parent.autoResize('iframe6')",20000);

var IE = document.all ? true : false;
if (!IE) {
    document.captureEvents(Event.MOUSEMOVE);
}
document.onmousemove = getMouseXY;
var tempX = 0;
var tempY = 0;
//esta funcion no necesitas entender solo asigna la posicion del raton a tempX y tempY
function getMouseXY(e){
    if (IE) { //para IE
        tempX = event.clientX + document.body.scrollLeft;
        tempY = event.clientY + document.body.scrollTop;
    }
    else { //para netscape
        tempX = e.pageX;
        tempY = e.pageY;
    }
    if (tempX < 0) {
        tempX = 0;
    }
    if (tempY < 0) {
        tempY = 0;
    }
    return true;
}
function VentanaFlotante(mensaje, x, y){
        //creo el objeto div
        var div_fl = document.createElement('DIV');
        //le asigno que su posicion sera abosoluta
        div_fl.style.position = 'absolute';
        //le asigno el ide a la ventana
        div_fl.id = 'Miventana';
        //digo en que posicion left y top se creara a partir de la posicion del raton tempx tempy
        div_fl.style.left = tempX + 'px';
        div_fl.style.top = tempY + 'px';
        //asigno el ancho del div pasado por parametro
        div_fl.style.width = x + 'px';
        //asigno el alto del div pasado por parametro
        div_fl.style.height = y + 'px';
        //digo con css que el borde sera de grosor 1px solido y de color negro
        div_fl.style.border = "1px solid #000000";
        //asigno el color de fondo
        div_fl.style.backgroundColor = "#cccccc";
        //el objeto añado a la estrutura principal el document.body
        document.body.appendChild(div_fl);
        //el mensaje pasado por parametro muestro dentro del div
        div_fl.innerHTML = mensaje;
}
function quitarDiv()
{
//creo el objeto del div
var mv = document.getElementById('Miventana');
//elimino el objeto
document.body.removeChild(mv);
}
function SUBIRSCROLL()
{
	parent.SUBIRSCROLL();
}
function ALERTASINADJUNTO()
{
	alert('No se encuentra el archivo');
}
function NOMBREANEXO(v,nom)
{
	AGREGARNOMBREANEXO(v,nom);
}
function ULTIMOARCHIVO()
{
	MOSTRARULTIMOARCHIVO();
}
selecteds=0;

function CheckUncheck(total,check){
	checkbox=null;
	for(i=1;i<=total;i++){
		checkbox=document.getElementById("idcat"+i);
		//alert(checkbox.value);
		checkbox.checked=check.checked;
	}
	
	if(check.checked){
		selecteds=total;
	}else{
		selecteds=0;
	}
	
}

function contadorVals(check){
	if(check.checked){
		selecteds=selecteds+1;
	}else{
		selecteds=selecteds-1;
	}
}

function selectedVals(){
	if(selecteds==0){
		//alert("Seleccione al menos un registro.");
		return false;
	}else{
		return true;
	}
}
</script>
<?php //********ESTAS LIBRERIAS JS Y CSS SIRVEN PARA HACER LA BUSQUEDA DINAMICA CON CHECKLIST************//?>
<link rel="stylesheet" type="text/css" href="../../css/checklist/jquery.multiselect.css" />
<link rel="stylesheet" type="text/css" href="../../css/checklist/jquery.multiselect.filter.css" />
<link rel="stylesheet" type="text/css" href="../../css/checklist/styleselect.css" />
<link rel="stylesheet" type="text/css" href="../../css/checklist/prettify.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script type="text/javascript" src="../../js/checklist/jquery.multiselect.js"></script>
<script type="text/javascript" src="../../js/checklist/jquery.multiselect.filter.js"></script>
<script type="text/javascript" src="../../js/checklist/prettify.js"></script>
<?php //**************************************************************************************************//?>
</head>
<body style="font-family:Arial, Helvetica, sans-serif">
<?php
$sql1 = mysqli_query($conectar,"select * from carga");
$num1 = mysqli_num_rows($sql1);
$sql2 = mysqli_query($conectar,"select * from carga_archivo");
$num2 = mysqli_num_rows($sql2);
$total = $num1+$num2;
?>
<form name="form1" id="form1" method="post" enctype="multipart/form-data">
<div id="estados">
<table style="width: 100%">
	<tr>
		<td style="width: 90px; cursor:pointer;<?php if($ultimoestado == 'NUEVO'){ echo 'background-color:#092451;color:#ffffff'; } ?>" class="section" onclick="NUEVACARGA()" onmouseover="this.style.backgroundColor='#092451';this.style.color='#ffffff';"  onmouseout="<?php if($ultimoestado <> 'NUEVO'){ echo "this.style.backgroundColor='#ffffff';this.style.color='#000000'"; } ?>">
		Nuevo
		</td>
		<td style="width: 90px; cursor:pointer;<?php if($ultimoestado == 'TODOS'){ echo 'background-color:#092451;color:#ffffff'; } ?>" class="section" onclick="VERTODOS('T')" onmouseover="this.style.backgroundColor='#092451';this.style.color='#ffffff';"  onmouseout="<?php if($ultimoestado <> 'TODOS'){ echo "this.style.backgroundColor='#ffffff';this.style.color='#000000'"; } ?>">
		<div id="todos">
		Todos 
		<?php 
		$con = mysqli_query($conectar,"select * from carga where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.")");
		$num = mysqli_num_rows($con);
		$con = mysqli_query($conectar,"select * from carga_archivo where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.")");
		$num1 = mysqli_num_rows($con);
		echo $num+$num1;
		?>
		</div>
		</td>
		<td style="width: 160px; cursor:pointer;<?php if($ultimoestado == 'PENDIENTES'){ echo 'background-color:#092451;color:#ffffff'; } ?>" class="section" onclick="VERTODOS('0')" onmouseover="this.style.backgroundColor='#092451';this.style.color='#ffffff';"  onmouseout="<?php if($ultimoestado <> 'PENDIENTES'){ echo "this.style.backgroundColor='#ffffff';this.style.color='#000000'"; } ?>">
		<div id="todos">
		Pendiente coordinador 
		<?php 
		$con = mysqli_query($conectar,"select * from carga where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and car_estado = 0");
		$num = mysqli_num_rows($con);
		$con = mysqli_query($conectar,"select * from carga_archivo where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and caa_estado = 0");
		$num1 = mysqli_num_rows($con);
		echo $num+$num1;
		?>
		</div>
		</td>
		<td style="width: 100px; cursor:pointer;<?php if($ultimoestado == 'APROBADOS'){ echo 'background-color:#092451;color:#ffffff'; } ?>" class="section" onclick="VERTODOS('1')" onmouseover="this.style.backgroundColor='#092451';this.style.color='#ffffff';"  onmouseout="<?php if($ultimoestado <> 'APROBADOS'){ echo "this.style.backgroundColor='#ffffff';this.style.color='#000000'"; } ?>">
		<div id="todos">
		Aprobados 
		<?php 
		$con = mysqli_query($conectar,"select * from carga where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and car_estado = 1");
		$num = mysqli_num_rows($con);
		$con = mysqli_query($conectar,"select * from carga_archivo where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = ".$claveusuario.") and caa_estado = 1");
		$num1 = mysqli_num_rows($con);
		echo $num+$num1;
		?>
		</div>
		</td>
		<td class="auto-style2" style="width: 600px"><strong>CARGAR </strong> </td>
		<td>&nbsp;</td>
	</tr>
</table>
</div>
<div id="opcion">
<table style="width: 100%" class="section">
	<tr>
		<td align="center" style="text-align:center">
		<table style="width: 45%" align="center">
			<tr>
				<td class="auto-style1"><strong>Obra:</strong></td>
				<td class="auto-style1">
				<select name="obra" id="obra" class="inputs" style="width: 210px">
				<option value="">-Seleccione-</option>
				<?php
					$con = mysqli_query($conectar,"select * from obra where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = '".$claveusuario."') order by obr_nombre");
					$num = mysqli_num_rows($con);
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$clave = $dato['obr_clave_int'];
						$nombre = $dato['obr_nombre'];
				?>
					<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
				<?php
					}
				?>
				</select>
				</td>
				<td class="auto-style1" rowspan="3">
				<div id="guardar">
				<input name="botonguardar" id="botonguardar" class="inputs" type="button" value="GUARDAR" onclick="NUEVO()" style="width: 100px; height: 60px; cursor:pointer" />
				</div>
				</td>
				<td class="auto-style1" rowspan="3">
				<div id="estadoactualizar"></div>
				</td>
			</tr>
			<tr>
				<td class="auto-style1"><strong>Tipo informe:</strong></td>
				<td class="auto-style1">
				<select name="tipoinforme" id="tipoinforme" onchange="VALIDAR(this.value)" class="inputs" style="width: 210px">
				<option value="">-Seleccione-</option>
				<?php
					$con = mysqli_query($conectar,"select * from tipo_informe order by tii_nombre");
					$num = mysqli_num_rows($con);
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$clave = $dato['tii_clave_int'];
						$nombre = $dato['tii_nombre'];
				?>
					<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
				<?php
					}
				?>
				</select>
				</td>
			</tr>
			<tr>
				<td class="auto-style1"><strong>Nombre:</strong></td>
				<td class="auto-style1">
				<input name="nombre" id="nombre" spellcheck="false" class="inputs" type="text" style="width: 200px" />
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" style="text-align:center">
		<hr/>
		</td>
	</tr>
	<tr>
		<td>
		
		<table style="width: 100%">
			<tr>
				<td>
				<div id="opcioncarga1" class="inputs" style="background-color:#092451;width:10%;color:white;cursor:pointer;float:left;display:none;text-align:center" onclick="VALIDAR('')">METODO 1</div> 
				<div id="opcioncarga2" class="inputs" style="background-color:#092451;width:10%;color:white;cursor:pointer;float:left;display:none;margin-left:10px;text-align:center" onclick="OPCION2()">METODO 2</div>
				</td>
			</tr>
			<tr>
				<td>
				<div id="fotos">
				<div style="width: 100%; height:360px; border-style:dashed;overflow:hidden;cursor:pointer" onclick="ALERTA()">
				</div>
				</div>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</div>
<input name="usuario" id="usuario" value="<?php echo $usuario; ?>" type="hidden" />
<input name="agregado" id="agregado" value="0" type="hidden" />
<input name="ocultoseleccionado" id="ocultoseleccionado" value="0" type="hidden" />
<input name="ocultoeliminados" id="ocultoeliminados" value="0" type="hidden" />
<script type="text/javascript" language="javascript">
<?php if($ultimoestado == 'NUEVO'){ ?>NUEVACARGA();<?php }elseif($ultimoestado == 'TODOS'){ ?>VERTODOS('T');<?php }elseif($ultimoestado == 'PENDIENTES'){ ?>VERTODOS('0');<?php }elseif($ultimoestado == 'APROBADOS'){ ?>VERTODOS('1');<?php } ?>
function VERFILTRO()
{
	$("#busobra").multiselect().multiselectfilter();
	$("#bustipoinforme").multiselect().multiselectfilter();
}
</script>
</form>
</body>
</html>