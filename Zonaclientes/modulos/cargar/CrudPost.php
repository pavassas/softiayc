<?php
	session_start();
	error_reporting(0);
	include('../../data/Conexion.php');
	require_once('../../Classes/PHPMailer-master/class.phpmailer.php');
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_SESSION['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
		

	
	$con = mysqli_query($conectar,"select usu_clave_int from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
	$dato = mysqli_fetch_array($con);
	$claveusuario = $dato['usu_clave_int'];
	
	if($_POST['aprobarmasivo'] == 'si')
	{
		$id = $_POST['id'];
		$ti = $_POST['ti'];

		if($ti == "COMITEOBRA" || $ti == "COMITETECNICO" || $ti == "INFORMEMENSUAL")
		{
			$con = mysqli_query($conectar,"update carga_archivo set caa_estado = 1 where caa_clave_int = '".$id."'");
		}
		else
		if($ti == "REGISTROFOTOGRAFICO")
		{
			$con = mysqli_query($conectar,"update carga set car_estado = 1 where car_clave_int = '".$id."'");
		}
		if($con > 0)
		{
			echo 1;
		}
	}
	else
	if($_POST['eliminarmasivo'] == 'si')
	{
		$id = $_POST['id'];
		$ti = $_POST['ti'];

		if($ti == "COMITEOBRA" || $ti == "COMITETECNICO" || $ti == "INFORMEMENSUAL")
		{
			$con = mysqli_query($conectar,"delete from carga_archivo where caa_clave_int = '".$id."'");
			mysqli_query($conectar,"delete from anexos_archivo where caa_clave_int = '".$id."'");
		}
		else
		if($ti == "REGISTROFOTOGRAFICO")
		{
			$con = mysqli_query($conectar,"delete from carga where car_clave_int = '".$id."'");
			mysqli_query($conectar,"delete from carga_foto where car_clave_int = '".$id."'");
		}
		if($con > 0)
		{
			echo 1;
		}
	}
	else
	if($_POST['aprobarcomite'] == 'si')
	{
		$clacaa = $_POST['clacaa'];
		$ti = $_POST['ti'];

		if($ti == "COMITEOBRA" || $ti == "COMITETECNICO" || $ti == "INFORMEMENSUAL")
		{
			$con = mysqli_query($conectar,"select * from carga_archivo where caa_clave_int = '".$clacaa."'");
			$dato = mysqli_fetch_array($con);
			$obr = $dato['obr_clave_int'];
			$tii = $dato['tii_clave_int'];
			$nom = $dato['caa_nombre'];
			
			$sql = mysqli_query($conectar,"update carga_archivo set caa_estado = 1 where caa_clave_int = '".$clacaa."'");
			if($sql > 0)
			{
				$conobr = mysqli_query($conectar,"select obr_nombre from obra where obr_clave_int = ".$obr."");
				$datoobr = mysqli_fetch_array($conobr);
				$nomobr = $datoobr['obr_nombre'];
				
				$contii = mysqli_query($conectar,"select tii_nombre from tipo_informe where tii_clave_int = '".$tii."'");
				$datoti = mysqli_fetch_array($contii);
				$nomti = $datoti['tii_nombre'];
				
				$con = mysqli_query($conectar,"select * from notificar_h where not_tipo = 4");
				$dato = mysqli_fetch_array($con);
				$clanot = $dato['not_clave_int'];
				$swtodobr = $dato['not_sw_todas_obras'];
				$swtodusuper = $dato['not_sw_usuario_perfil'];
				$swact = $dato['not_sw_activo'];
				$asu = "INFORME APROBADO: ".$nomti.". OBRA: ".$nomobr;
				$men = "El usuario ".$nombreusuario.", ha subido el archivo ".$nom." correspondiente al proyecto ".$nomobr.".\n";
				
				//Si notificacion es activa Y Si son obras especificas Y Si son todos los usuarios OOO Si son Todos los perfiles
				if($swact == 1 and $swtodobr == 0 and ($swtodusuper == 1 || $swtodusuper == 3))
				{
					$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) where uo.obr_clave_int = (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_registro = '".$obr."' and nod_tipo = 1) and u.usu_clave_int <> '".$claveusuario."'");
					$num = mysqli_num_rows($con);
					
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomti,$nom);
					}
				}
				else
				//Si notificacion es activa Y Si son obras especificas Y Si son usuarios especificos
				if($swact == 1 and $swtodobr == 0 and $swtodusuper == 2)
				{
					$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) where uo.obr_clave_int = (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_registro = '".$obr."' and nod_tipo = 1) and u.usu_clave_int IN (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_tipo = 2) and u.usu_clave_int <> '".$claveusuario."'");
					$num = mysqli_num_rows($con);
					
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomti,$nom);
					}
				}
				else
				//Si notificacion es activa Y Si son obras especificas Y Si son perfiles especificos
				if($swact == 1 and $swtodobr == 0 and $swtodusuper == 4)
				{
					$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) where uo.obr_clave_int = (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_registro = '".$obr."' and nod_tipo = 1) and u.prf_clave_int IN (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_tipo = 3) and u.usu_clave_int <> '".$claveusuario."'");
					$num = mysqli_num_rows($con);
					
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomti,$nom);
					}
				}
				else
				//Si notificacion es activa Y Si son todas las obras Y Si son todos los usuarios OOO Si son Todos los perfiles
				if($swact == 1 and $swtodobr == 1 and ($swtodusuper == 1 || $swtodusuper == 3))
				{
					$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) where uo.obr_clave_int = '".$obr."' and u.usu_clave_int <> '".$claveusuario."'");
					$num = mysqli_num_rows($con);
					
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomti,$nom);
					}
				}
				else
				//Si notificacion es activa Y Si son todas las obras Y Si son usuarios especificos
				if($swact == 1 and $swtodobr == 1 and $swtodusuper == 2)
				{
					$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) where uo.obr_clave_int = '".$obr."' and u.usu_clave_int IN (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_tipo = 2) and u.usu_clave_int <> '".$claveusuario."'");
					$num = mysqli_num_rows($con);
					
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomti,$nom);
					}
				}
				else
				//Si notificacion es activa Y Si son todas las obras Y Si son perfiles especificos
				if($swact == 1 and $swtodobr == 1 and $swtodusuper == 4)
				{
					$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) where uo.obr_clave_int = '".$obr."' and u.prf_clave_int IN (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_tipo = 3) and u.usu_clave_int <> '".$claveusuario."'");
					$num = mysqli_num_rows($con);
					
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomti,$nom);
					}
				}
			}
		}
		else
		if($ti == "FOTO")
		{
			$clacar = $_POST['clacaa'];
			$con = mysqli_query($conectar,"select * from carga where car_clave_int = '".$clacar."'");
			$dato = mysqli_fetch_array($con);
			$obr = $dato['obr_clave_int'];
			$tii = $dato['tii_clave_int'];
			$nom = $dato['car_nombre'];
			
			$sql = mysqli_query($conectar,"update carga set car_estado = 1 where car_clave_int = '".$clacar."'");
			
			if($sql > 0)
			{
				$conobr = mysqli_query($conectar,"select obr_nombre from obra where obr_clave_int = ".$obr."");
				$datoobr = mysqli_fetch_array($conobr);
				$nomobr = $datoobr['obr_nombre'];
				
				$contii = mysqli_query($conectar,"select tii_nombre from tipo_informe where tii_clave_int = '".$tii."'");
				$datoti = mysqli_fetch_array($contii);
				$nomti = $datoti['tii_nombre'];
				
				$con = mysqli_query($conectar,"select * from notificar_h where not_tipo = 4");
				$dato = mysqli_fetch_array($con);
				$clanot = $dato['not_clave_int'];
				$swtodobr = $dato['not_sw_todas_obras'];
				$swtodusuper = $dato['not_sw_usuario_perfil'];
				$swact = $dato['not_sw_activo'];
				$asu = "INFORME APROBADO: ".$nomti.". OBRA: ".$nomobr;
				$men = "El usuario ".$nombreusuario.", ha subido el archivo ".$nom." correspondiente al proyecto ".$nomobr.".\n";
				
				//Si notificacion es activa Y Si son obras especificas Y Si son todos los usuarios OOO Si son Todos los perfiles
				if($swact == 1 and $swtodobr == 0 and ($swtodusuper == 1 || $swtodusuper == 3))
				{
					$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) where uo.obr_clave_int = (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_registro = '".$obr."' and nod_tipo = 1) and u.usu_clave_int <> '".$claveusuario."'");
					$num = mysqli_num_rows($con);
					
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomti,$nom);
					}
				}
				else
				//Si notificacion es activa Y Si son obras especificas Y Si son usuarios especificos
				if($swact == 1 and $swtodobr == 0 and $swtodusuper == 2)
				{
					$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) where uo.obr_clave_int = (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_registro = '".$obr."' and nod_tipo = 1) and u.usu_clave_int IN (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_tipo = 2) and u.usu_clave_int <> '".$claveusuario."'");
					$num = mysqli_num_rows($con);
					
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomti,$nom);
					}
				}
				else
				//Si notificacion es activa Y Si son obras especificas Y Si son perfiles especificos
				if($swact == 1 and $swtodobr == 0 and $swtodusuper == 4)
				{
					$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) where uo.obr_clave_int = (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_registro = '".$obr."' and nod_tipo = 1) and u.prf_clave_int IN (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_tipo = 3) and u.usu_clave_int <> '".$claveusuario."'");
					$num = mysqli_num_rows($con);
					
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomti,$nom);
					}
				}
				else
				//Si notificacion es activa Y Si son todas las obras Y Si son todos los usuarios OOO Si son Todos los perfiles
				if($swact == 1 and $swtodobr == 1 and ($swtodusuper == 1 || $swtodusuper == 3))
				{
					$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) where uo.obr_clave_int = '".$obr."' and u.usu_clave_int <> '".$claveusuario."'");
					$num = mysqli_num_rows($con);
					
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomti,$nom);
					}
				}
				else
				//Si notificacion es activa Y Si son todas las obras Y Si son usuarios especificos
				if($swact == 1 and $swtodobr == 1 and $swtodusuper == 2)
				{
					$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) where uo.obr_clave_int = '".$obr."' and u.usu_clave_int IN (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_tipo = 2) and u.usu_clave_int <> '".$claveusuario."'");
					$num = mysqli_num_rows($con);
					
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomti,$nom);
					}
				}
				else
				//Si notificacion es activa Y Si son todas las obras Y Si son perfiles especificos
				if($swact == 1 and $swtodobr == 1 and $swtodusuper == 4)
				{
					$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) where uo.obr_clave_int = '".$obr."' and u.prf_clave_int IN (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_tipo = 3) and u.usu_clave_int <> '".$claveusuario."'");
					$num = mysqli_num_rows($con);
					
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomti,$nom);
					}
				}
			}
		}
		if($sql > 0)
		{
			echo 1;
		}
	}
	else
	if($_POST['eliminararchivo'] == 'si')
	{
		$clacaa = $_POST['clacaa'];
		$ti = $_POST['ti'];
		if($ti == "COMITEOBRA" || $ti == "COMITETECNICO" || $ti == "INFORMEMENSUAL")
		{
			$con = mysqli_query($conectar,"select * from carga_archivo where caa_clave_int = '".$clacaa."'");
			$dato = mysqli_fetch_array($con);
			$obr = $dato['obr_clave_int'];
			$tii = $dato['tii_clave_int'];
			$nom = $dato['caa_nombre'];
			
			$sql = mysqli_query($conectar,"delete from carga_archivo where caa_clave_int = '".$clacaa."'");
			mysqli_query($conectar,"delete from anexos_archivo where caa_clave_int = '".$clacaa."'");
			if($sql > 0)
			{
				$conobr = mysqli_query($conectar,"select obr_nombre from obra where obr_clave_int = ".$obr."");
				$datoobr = mysqli_fetch_array($conobr);
				$nomobr = $datoobr['obr_nombre'];
					
				$contii = mysqli_query($conectar,"select tii_nombre from tipo_informe where tii_clave_int = '".$tii."'");
				$datoti = mysqli_fetch_array($contii);
				$nomti = $datoti['tii_nombre'];
				
				$con = mysqli_query($conectar,"select * from notificar_h where not_tipo = 3");
				$dato = mysqli_fetch_array($con);
				$clanot = $dato['not_clave_int'];
				$swtodobr = $dato['not_sw_todas_obras'];
				$swtodusuper = $dato['not_sw_usuario_perfil'];
				$swact = $dato['not_sw_activo'];
				$asu = "REGISTRO ELIMINADO: ".$nomti.". OBRA: ".$nomobr;
				$men = "El usuario ".$nombreusuario.", ha eliminado el archivo ".$nom." correspondiente al proyecto ".$nomobr.". Por favor revisar y realizar correcciones.\n";
				
				//Si notificacion es activa Y Si son obras especificas Y Si son todos los usuarios OOO Si son Todos los perfiles
				if($swact == 1 and $swtodobr == 0 and ($swtodusuper == 1 || $swtodusuper == 3))
				{
					$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) where uo.obr_clave_int = (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_registro = '".$obr."' and nod_tipo = 1) and u.usu_clave_int <> '".$claveusuario."'");
					$num = mysqli_num_rows($con);
					
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomti,$nom);
					}
				}
				else
				//Si notificacion es activa Y Si son obras especificas Y Si son usuarios especificos
				if($swact == 1 and $swtodobr == 0 and $swtodusuper == 2)
				{
					$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) where uo.obr_clave_int = (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_registro = '".$obr."' and nod_tipo = 1) and u.usu_clave_int IN (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_tipo = 2) and u.usu_clave_int <> '".$claveusuario."'");
					$num = mysqli_num_rows($con);
					
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomti,$nom);
					}
				}
				else
				//Si notificacion es activa Y Si son obras especificas Y Si son perfiles especificos
				if($swact == 1 and $swtodobr == 0 and $swtodusuper == 4)
				{
					$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) where uo.obr_clave_int = (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_registro = '".$obr."' and nod_tipo = 1) and u.prf_clave_int IN (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_tipo = 3) and u.usu_clave_int <> '".$claveusuario."'");
					$num = mysqli_num_rows($con);
					
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomti,$nom);
					}
				}
				else
				//Si notificacion es activa Y Si son todas las obras Y Si son todos los usuarios OOO Si son Todos los perfiles
				if($swact == 1 and $swtodobr == 1 and ($swtodusuper == 1 || $swtodusuper == 3))
				{
					$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) where uo.obr_clave_int = '".$obr."' and u.usu_clave_int <> '".$claveusuario."'");
					$num = mysqli_num_rows($con);
					
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomti,$nom);
					}
				}
				else
				//Si notificacion es activa Y Si son todas las obras Y Si son usuarios especificos
				if($swact == 1 and $swtodobr == 1 and $swtodusuper == 2)
				{
					$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) where uo.obr_clave_int = '".$obr."' and u.usu_clave_int IN (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_tipo = 2) and u.usu_clave_int <> '".$claveusuario."'");
					$num = mysqli_num_rows($con);
					
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomti,$nom);
					}
				}
				else
				//Si notificacion es activa Y Si son todas las obras Y Si son perfiles especificos
				if($swact == 1 and $swtodobr == 1 and $swtodusuper == 4)
				{
					$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) where uo.obr_clave_int = '".$obr."' and u.prf_clave_int IN (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_tipo = 3) and u.usu_clave_int <> '".$claveusuario."'");
					$num = mysqli_num_rows($con);
					
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomti,$nom);
					}
				}
			}
		}
		else
		if($ti == "FOTO")
		{
			$clacar = $_POST['clacaa'];
			$con = mysqli_query($conectar,"select * from carga where car_clave_int = '".$clacar."'");
			$dato = mysqli_fetch_array($con);
			$obr = $dato['obr_clave_int'];
			$tii = $dato['tii_clave_int'];
			$nom = $dato['car_nombre'];
			
			$sql = mysqli_query($conectar,"delete from carga where car_clave_int = '".$clacar."'");
			mysqli_query($conectar,"delete from carga_foto where car_clave_int = '".$clacar."'");
			if($sql > 0)
			{
				$conobr = mysqli_query($conectar,"select obr_nombre from obra where obr_clave_int = ".$obr."");
				$datoobr = mysqli_fetch_array($conobr);
				$nomobr = $datoobr['obr_nombre'];
					
				$contii = mysqli_query($conectar,"select tii_nombre from tipo_informe where tii_clave_int = '".$tii."'");
				$datoti = mysqli_fetch_array($contii);
				$nomti = $datoti['tii_nombre'];
				
				$con = mysqli_query($conectar,"select * from notificar_h where not_tipo = 3");
				$dato = mysqli_fetch_array($con);
				$clanot = $dato['not_clave_int'];
				$swtodobr = $dato['not_sw_todas_obras'];
				$swtodusuper = $dato['not_sw_usuario_perfil'];
				$swact = $dato['not_sw_activo'];
				$asu = "REGISTRO ELIMINADO: ".$nomti.". OBRA: ".$nomobr;
				$men = "El usuario ".$nombreusuario.", ha eliminado el archivo ".$nom." correspondiente al proyecto ".$nomobr.". Por favor revisar y realizar correcciones.\n";
				
				//Si notificacion es activa Y Si son obras especificas Y Si son todos los usuarios OOO Si son Todos los perfiles
				if($swact == 1 and $swtodobr == 0 and ($swtodusuper == 1 || $swtodusuper == 3))
				{
					$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) where uo.obr_clave_int = (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_registro = '".$obr."' and nod_tipo = 1) and u.usu_clave_int <> '".$claveusuario."'");
					$num = mysqli_num_rows($con);
					
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomti,$nom);
					}
				}
				else
				//Si notificacion es activa Y Si son obras especificas Y Si son usuarios especificos
				if($swact == 1 and $swtodobr == 0 and $swtodusuper == 2)
				{
					$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) where uo.obr_clave_int = (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_registro = '".$obr."' and nod_tipo = 1) and u.usu_clave_int IN (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_tipo = 2) and u.usu_clave_int <> '".$claveusuario."'");
					$num = mysqli_num_rows($con);
					
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomti,$nom);
					}
				}
				else
				//Si notificacion es activa Y Si son obras especificas Y Si son perfiles especificos
				if($swact == 1 and $swtodobr == 0 and $swtodusuper == 4)
				{
					$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) where uo.obr_clave_int = (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_registro = '".$obr."' and nod_tipo = 1) and u.prf_clave_int IN (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_tipo = 3) and u.usu_clave_int <> '".$claveusuario."'");
					$num = mysqli_num_rows($con);
					
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomti,$nom);
					}
				}
				else
				//Si notificacion es activa Y Si son todas las obras Y Si son todos los usuarios OOO Si son Todos los perfiles
				if($swact == 1 and $swtodobr == 1 and ($swtodusuper == 1 || $swtodusuper == 3))
				{
					$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) where uo.obr_clave_int = '".$obr."' and u.usu_clave_int <> '".$claveusuario."'");
					$num = mysqli_num_rows($con);
					
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomti,$nom);
					}
				}
				else
				//Si notificacion es activa Y Si son todas las obras Y Si son usuarios especificos
				if($swact == 1 and $swtodobr == 1 and $swtodusuper == 2)
				{
					$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) where uo.obr_clave_int = '".$obr."' and u.usu_clave_int IN (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_tipo = 2) and u.usu_clave_int <> '".$claveusuario."'");
					$num = mysqli_num_rows($con);
					
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomti,$nom);
					}
				}
				else
				//Si notificacion es activa Y Si son todas las obras Y Si son perfiles especificos
				if($swact == 1 and $swtodobr == 1 and $swtodusuper == 4)
				{
					$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) where uo.obr_clave_int = '".$obr."' and u.prf_clave_int IN (select nod_registro from notificar_d where not_clave_int = '".$clanot."' and nod_tipo = 3) and u.usu_clave_int <> '".$claveusuario."'");
					$num = mysqli_num_rows($con);
					
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomti,$nom);
					}
				}
			}
		}
		if($sql > 0)
		{
			echo 1;
		}
	}
	else if($_POST['editarcargaarchivo']=="si")
	{
		$clacaa = $_POST['clacaa'];
		$con = mysqli_query($conectar,"select caa_comentarios,caa_nombre,caa_ruta_original,obr_clave_int,tii_clave_int from carga_archivo where caa_clave_int = '".$clacaa."'");
		$dato = mysqli_fetch_array($con);
		$com = $dato['caa_comentarios'];
		$nom = $dato['caa_nombre'];
		$rutori = $dato['caa_ruta_original'];
		$obr = $dato['obr_clave_int'];
		$tii = $dato['tii_clave_int'];	
		?>
		<table style="width: 100%" class="section">
			<tr>
				<td align="center" style="text-align:center">
				<table style="width: 45%" align="center">
					<tr>
						<td class="auto-style1"><strong>Obra:</strong></td>
						<td class="auto-style1">
						<select name="obra1" id="obra1" class="inputs" style="width: 210px">
						<option value="">-Seleccione-</option>
						<?php
							$con = mysqli_query($conectar,"select * from obra where obr_clave_int in (select obr_clave_int from usuario_obra where usu_clave_int = '".$claveusuario."') and (obr_sw_activo = 1 or obr_clave_int = '".$obr."') order by obr_nombre");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['obr_clave_int'];
								$nombre = $dato['obr_nombre'];
						?>
							<option value="<?php echo $clave; ?>" <?php if($clave == $obr){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
						<?php
							}
						?>
						</select>
						</td>
						<td class="auto-style1" rowspan="3">
						<input name="Button1" class="inputs" type="button" value="ACTUALIZAR" onClick="ADJUNTAR('1','SI','<?php echo $clacaa; ?>')" style="width: 100px; height: 60px; cursor:pointer" />
						</td>
						<td class="auto-style1" rowspan="3">
						<input name="Button1" class="inputs" type="button" value="ELIMINAR" onClick="ELIMINARARCHIVO('<?php echo $clacaa; ?>','','')" style="width: 100px; height: 60px; cursor:pointer" />
						</td>
						<td class="auto-style1" rowspan="3">
						<div id="estadoactualizar"></div>
						</td>
					</tr>
					<tr>
						<td class="auto-style1"><strong>Tipo informe:</strong></td>
						<td class="auto-style1">
						<select name="tipoinforme1" id="tipoinforme1" class="inputs" style="width: 210px">
						<option value="">-Seleccione-</option>
						<?php
							$con = mysqli_query($conectar,"select * from tipo_informe where tii_clave_int <> 1 order by tii_nombre");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['tii_clave_int'];
								$nombre = $dato['tii_nombre'];
						?>
							<option value="<?php echo $clave; ?>" <?php if($clave == $tii){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
						<?php
							}
						?>
						</select>
						</td>
					</tr>
					<tr>
						<td class="auto-style1"><strong>Nombre:</strong></td>
						<td class="auto-style1">
						<input name="nombre11" id="nombre11" value="" disabled="disabled" spellcheck="false" class="inputs" type="text" style="width: 200px" />
						</td>
					</tr>
					</table>
				
				</td>
			</tr>
			<tr>
				<td align="center" style="text-align:center">
				<hr/>
				</td>
			</tr>
			<tr>
				<td>
				<div id="archivos">
				<div><br><table style="width: 50%" align="center"><tr><td style="height: 130px"><fieldset name="Group1"><legend align="center"><strong>INFORME</strong></legend><table align="center"><tr><td align="center" style="text-align:center"><input type="text" class="inputs" placeholder="" spellcheck="false" style="width:180px" name="nombre1" id="nombre1" value="<?php echo $nom; ?>" /></td></tr><tr><td align="center" style="text-align:center"><input name="fileUpload1" id="fileUpload1" type="file" /></td></tr><tr><td><div id="resultadoadjunto1" style="width:100%"><div class='ok1' style='width: 100%' align='center'><?php echo $rutori; ?></div></div></td></tr></table></fieldset></td><td style="height: 130px"><fieldset name="Group1"><legend align="center"><strong>ANEXOS</strong></legend><table align="center"><tr><td>&nbsp;</td></tr><tr><td onMouseMove="OCULTOSELECCIONADO('1')"><input name="fileUpload2" id="fileUpload2" type="file" /></td></tr><tr><td></td></tr><tr><td></td></tr></table></fieldset></td><td style="height: 130px"><fieldset name="Group1" style="height:98px"><legend align="center"><strong>COMENTARIOS</strong></legend><table align="center"><tr><td><textarea class="inputs" name="comentario1" id="comentario1" style="height: 50px; width: 250px;"><?php echo $com; ?></textarea></td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td></tr></table></fieldset></td></tr><tr><td colspan="3"><div id="ocultarbotonsubir1"><a><input type="button" onClick="ADJUNTAR('1','SI','<?php echo $clacaa; ?>')" onMouseMove="OCULTOSELECCIONADO('1')" value="ACTUALIZAR" style="width:100%;cursor:pointer" /><div id="resultadoactualizar"></div><input name="ocultoanexo1" id="ocultoanexo1" value="0" type="hidden" /></a></div></td></tr><tr>
				<td colspan="3"><br>
				<div id="veranexos">
				<fieldset name="Group1" style="width:100%">
				<legend align="center">
				<strong>ANEXOS
				</strong>
				</legend>
				<table style="width: 100%" class="section" align="center">
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td style="width: 85px">&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td style="text-align:left"><strong>NOMBRE DEL ANEXO</strong></td>
						<td style="text-align:left"><strong>VER</strong></td>
						<td style="text-align:left; width: 85px;"><strong>DESCARGAR</strong></td>
						<td style="text-align:left"><strong>ELIMINAR</strong></td>
					</tr>
					<tr>
						<td colspan="4">
						<hr>
						</td>
					</tr>
					<?php
					$con = mysqli_query($conectar,"select * from anexos_archivo where caa_clave_int = '".$clacaa."' order by ana_nombre");
					$num = mysqli_num_rows($con);
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$claana = $dato['ana_clave_int'];
						$nom = $dato['ana_nombre'];
						$rut = $dato['ana_ruta'];
					?>
						<tr style="<?php if($i % 2 == 0){ echo 'background-color:silver'; } ?>;cursor:pointer" id="service<?php echo $claana; ?>" data="<?php echo $claana; ?>" onMouseOver="this.style.backgroundColor='#A7A7A7';this.style.color='#000000';" onMouseOut="this.style.backgroundColor='<?php if($i % 2 == 0){ echo "#BDBDBD"; } ?>';this.style.color='#000000';" onClick="MOSTRARMOVIMIENTO('<?php echo $clacar; ?>');OCULTARSCROLL()">
							<td class="auto-style1" style="text-align:left">
							<input type="text" class="inputs" placeholder="Nombre anexo" style="width:98%" onKeyUp="GUARDARNOMBREANEXOAUTOMATICO('<?php echo $claana; ?>',this.value)" name="nombreanexo1" id="nombreanexo1" value="<?php echo $nom; ?>" />
							</td>
							<td class="fila" style="background-color:#092451;color:white;width:37px;cursor:pointer">
							<a <?php if($rut != ''){ echo "href='iframecargar/$rut'"; }else{ echo "onclick='ALERTASINADJUNTO()'"; } ?> target='_blank' style="color:white;text-decoration:none;text-shadow: 1px 1px 1px #aaa">
							VER ONLINE
							</a>
							</td>
							<td class="fila" style="background-color:#092451;color:white;width:37px;cursor:pointer">
							<a href='descargar.php?claana=<?php echo $claana; ?>' target='_blank' style="color:white;text-decoration:none;text-shadow: 1px 1px 1px #aaa">
							DESCARGAR</a>
							</td>
							<td class="auto-style1">
							<img style="cursor:pointer" src="../../images/delete.png" height="30" width="29" onClick="eliminaranexo('<?php echo $claana; ?>')">
							</td>
						</tr>
					<?php
					}
					?>
					</table>
				</fieldset>
				</div>
				</td></tr></table></div>
				</div>
				</td>
			</tr>
		</table>
		<?php
		echo "<script>REFRESCARBOTONACTUALIZAR1('1','".$clacaa."');</script>";
		echo "<script>REFRESCARBOTONACTUALIZAR2('2','".$clacaa."');</script>";
		echo "<script>OCULTARSCROLL();</script>";		
		echo "<script>VERINFO();</script>";
		echo "<script>SUBIRSCROLL();</script>";
	}
	
	function MensajeNuevaCarga($nomusu,$ema,$asu,$men,$nomtii,$nomarc)
	{
		$mail = new PHPMailer();						
		$mail->Body = '';
		
		$mail->From = "adminpavas@pavas.com.co";
		$mail->FromName = "I, A & C";
		$mail->AddAddress($ema, "Destino");
		$mail->Subject = $asu;
		
		// Cuerpo del mensaje
		$mail->Body .= "Hola ".$nomusu."!\n\n";	
		$mail->Body .= $men;
		$mail->Body .= "Tipo Informe: ".$nomtii."\n";
		$mail->Body .= "Nombre Archivo: ".$nomarc."\n";
		$mail->Body .= "Fecha Carga: ".date("d/m/Y H:m:s")."\n\n";
		$mail->Body .= "Este mensaje es generado automáticamente por ZONA CLIENTES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Zonaclientes \n";
		
		if(!$mail->Send())
		{
			//echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
			//echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
		}
		else
		{
		}
	}
?>