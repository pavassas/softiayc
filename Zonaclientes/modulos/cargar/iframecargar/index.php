﻿<?php
error_reporting(0);
include('../../../data/Conexion.php');
echo "<div style='display:none'>";
session_start();
echo "</div>";
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_SESSION['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];

// verifica si no se ha loggeado
if(!isset($_SESSION["persona"]))
{
  session_destroy();
  header("LOCATION:index.php");
}else{
}
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");

$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
$dato = mysqli_fetch_array($con);
$claveperfil = $dato['prf_clave_int'];
$claveusuario = $dato['usu_clave_int'];
$ediclacar = $dato['car_clave_int'];

$con = mysqli_query($conectar,"select car_clave_int from carga where car_usu_creacion = '".$usuario."' order by car_clave_int DESC LIMIT 1");
$dato = mysqli_fetch_array($con);
$clacar = $dato['car_clave_int'];

if($ediclacar <> '' and $ediclacar <> 0)
{
	$clacar = $ediclacar;
}

$con = mysqli_query($conectar,"select * from carga c inner join tipo_informe ti on (ti.tii_clave_int = c.tii_clave_int) where c.car_clave_int = ".$clacar."");
$dato = mysqli_fetch_array($con);
$png = $dato['tii_png'];
$jpg = $dato['tii_jpg'];
$jpeg = $dato['tii_jpeg'];
$gif = $dato['tii_gif'];
$pdf = $dato['tii_pdf'];
$docx = $dato['tii_docx'];
$xls = $dato['tii_xls'];
$xlsx = $dato['tii_xlsx'];

if($png == '1'){ $png = '.png'; }elseif($png == '0'){ $png = ''; }
if($jpg == '1'){ $jpg = '.jpg'; }elseif($jpg == '0'){ $jpg = ''; }
if($jpeg == '1'){ $jpeg = '.jpeg'; }elseif($jpeg == '0'){ $jpeg = ''; }
if($gif == '1'){ $gif = '.gif'; }elseif($gif == '0'){ $gif = ''; }
if($pdf == '1'){ $pdf = '.pdf'; }elseif($pdf == '0'){ $pdf = ''; }
if($docx == '1'){ $docx = '.docx'; }elseif($docx == '0'){ $docx = ''; }
if($xls == '1'){ $xls = '.xls'; }elseif($xls == '0'){ $xls = ''; }
if($xlsx == '1'){ $xlsx = '.xlsx'; }elseif($xlsx == '0'){ $xlsx = ''; }

if($_GET['guardarleyenda'] == 'si')
{
	$na = $_GET['na'];
	$le = $_GET['le'];
	$con = mysqli_query($conectar,"update carga_foto set caf_leyenda = '".$le."' where caf_usu_actualiz = '".$usuario."' and caf_nombre_original = '".$na."' order by caf_clave_int DESC LIMIT 1");
	exit();	
}
if($_GET['bajarpeso'] == 'si')
{
	$na = $_GET['na'];
	$le = $_GET['le'];
	$con = mysqli_query($conectar,"select caf_nombre from carga_foto where caf_usu_actualiz = '".$usuario."' and caf_nombre_original = '".$na."' LIMIT 1");
	$dato = mysqli_fetch_array($con);
	$rut = $dato['caf_nombre'];
	$destino = $rut;
	$array_nombre = explode('.',$rut);
	$cuenta_arr_nombre = count($array_nombre);
	$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
	
	if($extension == 'jpg' or $extension == 'jpeg' or $extension == 'JPG' or $extension == 'JPEG')
	{
		//Reduzco el tamano de la imagen
		$img_origen = imagecreatefromjpeg($destino);
		$ancho_origen = imagesx($img_origen);//Se obtiene el ancho de la imagen
		$alto_origen = imagesy($img_origen);//Se obtiene el alto de la imagen
		$ancho_limite = 850;
		
		if($ancho_origen > $ancho_limite)//Para foto horizontal
		{
			$ancho_origen = $ancho_limite;
			$alto_origen = $ancho_limite*imagesy($img_origen)/imagesx($img_origen);
		}
		else//Para fotos verticales
		{
			$alto_origen = $ancho_limite;
			$ancho_origen = $ancho_limite*imagesx($img_origen)/imagesy($img_origen);
		}
		$img_destino = imagecreatetruecolor($ancho_origen, $alto_origen);//Se crea la imagen segun las dimensiones dadas
		imagecopyresized($img_destino, $img_origen, 0, 0, 0, 0, $ancho_origen, $alto_origen, imagesx($img_origen), imagesy($img_origen));
		imagejpeg($img_destino,$destino);//Se guarda la nueva foto
	}
	else
	if($extension == 'png' or $extension == 'PNG')
	{
		//Reduzco el tamano de la imagen
		$img_origen = imagecreatefrompng($destino);
		$ancho_origen = imagesx($img_origen);//Se obtiene el ancho de la imagen
		$alto_origen = imagesy($img_origen);//Se obtiene el alto de la imagen
		$ancho_limite = 850;
		
		if($ancho_origen > $ancho_limite)//Para foto horizontal
		{
			$ancho_origen = $ancho_limite;
			$alto_origen = $ancho_limite*imagesy($img_origen)/imagesx($img_origen);
		}
		else//Para fotos verticales
		{
			$alto_origen = $ancho_limite;
			$ancho_origen = $ancho_limite*imagesx($img_origen)/imagesy($img_origen);
		}
		$img_destino = imagecreatetruecolor($ancho_origen, $alto_origen);//Se crea la imagen segun las dimensiones dadas
		imagecopyresized($img_destino, $img_origen, 0, 0, 0, 0, $ancho_origen, $alto_origen, imagesx($img_origen), imagesy($img_origen));
		imagepng($img_destino,$destino);//Se guarda la nueva foto
	}
	else
	if($extension == 'gif' or $extension == 'GIF')
	{
		//Reduzco el tamano de la imagen
		$img_origen = imagecreatefromgif($destino);
		$ancho_origen = imagesx($img_origen);//Se obtiene el ancho de la imagen
		$alto_origen = imagesy($img_origen);//Se obtiene el alto de la imagen
		$ancho_limite = 850;
		
		if($ancho_origen > $ancho_limite)//Para foto horizontal
		{
			$ancho_origen = $ancho_limite;
			$alto_origen = $ancho_limite*imagesy($img_origen)/imagesx($img_origen);
		}
		else//Para fotos verticales
		{
			$alto_origen = $ancho_limite;
			$ancho_origen = $ancho_limite*imagesx($img_origen)/imagesy($img_origen);
		}
		$img_destino = imagecreatetruecolor($ancho_origen, $alto_origen);//Se crea la imagen segun las dimensiones dadas
		imagecopyresized($img_destino, $img_origen, 0, 0, 0, 0, $ancho_origen, $alto_origen, imagesx($img_origen), imagesy($img_origen));
		imagegif($img_destino,$destino);//Se guarda la nueva foto
	}
	exit();	
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Subir archivos con dropzone</title>
	<meta charset="utf-8">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	
	<script type="text/javascript" src="bower_components/dropzone/downloads/dropzone.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function()
	{
		Dropzone.autoDiscover = false;
		$("#dropzone").dropzone({
			url: "uploads.php",
			addRemoveLinks: true,
			maxFileSize: 20000000,
			maxfilesexceeded: function(file)
			{
			alert('You have uploaded more than 1 Image. Only the first file will be uploaded!');
			},
			dictResponseError: "Ha ocurrido un error en el server",
			acceptedFiles: '<?php echo $png; ?>,<?php echo $jpg; ?>,<?php echo $jpeg; ?>,<?php echo $gif; ?>,<?php echo $pdf; ?>,<?php echo $docx; ?>,<?php echo $xls; ?>,<?php echo $xlsx; ?>',
			complete: function(file)
			{
				if(file.status == "success")
				{
					//alert("El siguiente archivo ha subido correctamentee: " + file.name);
					var leyenda = prompt("Desea agregar una leyenda a esta imagen?", "");
					if(leyenda != ''){ AGREGARLEYENDA(file.name,leyenda); }
					setTimeout("parent.ACTUALIZARFOTOS('<?php echo $clacar; ?>');",1000);
					//setTimeout("BAJARPESO('"+file.name+"');",1000);
				}
			},
			error: function(file)
			{
				alert("No se puede cargar el tipo de archivo seleccionado: " + file.name);
			},
			removedfile: function(file, serverFileName)
			{
				var name = file.name;
				$.ajax({
					type: "POST",
					url: "uploads.php?delete=true",
					data: "filename="+name,
					success: function(data)
					{
						var json = JSON.parse(data);
						if(json.res == true)
						{
							var element;
							(element = file.previewElement) != null ? 
							element.parentNode.removeChild(file.previewElement) : 
							false;
							alert("El elemento fué eliminado: " + name); 
						}
					}
				});
			}
		});
	});
	</script>
	<script type="text/javascript" src="llamadas1.js"></script>
	<link rel="stylesheet" type="text/css" href="bower_components/dropzone/downloads/css/dropzone.css">
</head>
<body>
	<div id="dropzone" class="dropzone"></div>
</body>
</html>