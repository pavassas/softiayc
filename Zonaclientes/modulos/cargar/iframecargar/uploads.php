<?php
define('WP_MEMORY_LIMIT', '128M');
ini_set('memory_limit', '128M');
error_reporting(0);
include('../../../data/Conexion.php');
require_once('../../../Classes/PHPMailer-master/class.phpmailer.php');
session_start();
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_SESSION['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];

// verifica si no se ha loggeado
if(!isset($_SESSION["persona"]))
{
  session_destroy();
  header("LOCATION:index.php");
}else{
}
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");

$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
$dato = mysqli_fetch_array($con);
$claveperfil = $dato['prf_clave_int'];
$claveusuario = $dato['usu_clave_int'];
$ediclacar = $dato['car_clave_int'];

$con = mysqli_query($conectar,"select car_clave_int from carga where car_usu_creacion = '".$usuario."' order by car_clave_int DESC LIMIT 1");
$dato = mysqli_fetch_array($con);
$clacar = $dato['car_clave_int'];

if($ediclacar <> '' and $ediclacar <> 0)
{
	$clacar = $ediclacar;
}
			
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
{
	if(isset($_GET["delete"]) && $_GET["delete"] == true)
	{
		$name = $_POST["filename"];
		/*if(file_exists('./uploads/'.$name))
		{*/
			//unlink('./uploads/'.$name);
			mysqli_query($conectar,"DELETE FROM carga_foto WHERE caf_nombre_original = '$name' and car_clave_int = ".$clacar." LIMIT 1");
			echo json_encode(array("res" => true));
		/*}
		else
		{
			echo json_encode(array("res" => false));
		}*/
	}
	else
	{
		$file = $_FILES["file"]["name"];
		$archivo = basename($_FILES['file']['name']);
						
		$array_nombre = explode('.',$archivo);
		$cuenta_arr_nombre = count($array_nombre);
		$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				
		$filetype = $_FILES["file"]["type"];
		$filesize = $_FILES["file"]["size"];

		if(!is_dir("uploads/"))
			mkdir("uploads/", 0777);
		
		$con = mysqli_query($conectar,"select caf_clave_int from carga_foto order by caf_clave_int DESC LIMIT 1");
		$dato = mysqli_fetch_array($con);
		$clacaf = $dato['caf_clave_int'];
		
		if($clacaf == '' or $clacaf == 0)
		{
			$clacaf = 1;
		}
		else
		{
			$clacaf = $clacaf+1;
		}
		
		$destino =  "uploads/".$clacaf.".".$extension;
		
		if($file && move_uploaded_file($_FILES["file"]["tmp_name"], $destino))
		{
			mysqli_query($conectar,"INSERT INTO carga_foto(caf_clave_int,car_clave_int,caf_nombre,caf_nombre_original,caf_tipo,caf_tamano,caf_usu_actualiz,caf_fec_actualiz) values(null,'".$clacar."','".$destino."','".$file."','".$filetype."','".$filesize."','".$usuario."','".$fecha."')");
			
			if($ediclacar <> '' and $ediclacar <> 0)
			{
				$con1 = mysqli_query($conectar,"select obr_clave_int,car_nombre from carga where car_clave_int = '".$clacar."'");
				$dato1 = mysqli_fetch_array($con1);
				$obr = $dato1['obr_clave_int'];
				$nom = $dato1['car_nombre'];

				$con = mysqli_query($conectar,"select * from notificar_h where obr_clave_int = '".$obr."' and not_tipo = 2");
				$dato = mysqli_fetch_array($con);
				$swtod = $dato['not_sw_todos'];
				$swact = $dato['not_sw_activo'];
				
				if($swtod == 1 and $swact == 1)
				{
					$con = mysqli_query($conectar,"select * from usuario u inner join usuario_obra uo on (uo.usu_clave_int = u.usu_clave_int) inner join obra o on (o.obr_clave_int = uo.obr_clave_int) where uo.obr_clave_int = '".$obr."' and u.usu_clave_int <> '".$claveusuario."'");
					$num = mysqli_num_rows($con);
					
					for($i = 0; $i < $num; $i++)
					{
						$mail = new PHPMailer();
						
						$dato = mysqli_fetch_array($con);
						$mail->Body = '';
						$ema = '';
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						$nomobr = $dato['obr_nombre'];
						
						$mail->From = "adminpavas@pavas.com.co";
						$mail->FromName = "I, A & C";
						$mail->AddAddress($ema, "Destino");
						$mail->Subject = "ACTUALIZACION REGISTRO FOTOGRAFICO. OBRA: ".$nomobr;
						
						// Cuerpo del mensaje
						$mail->Body .= "Hola ".$nomusu."!\n\n";	
						$mail->Body .= "ZONA CLIENTES registra que se ha actualizado un REGISTRO FOTOGRAFICO de su obra: ".$nomobr.".\n";
						$mail->Body .= "NOMBRE ARCHIVO: ".$nom."\n";
						$mail->Body .= date("d/m/Y H:m:s")."\n\n";
						$mail->Body .= "Este mensaje es generado automáticamente por ZONA CLIENTES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Zonaclientes \n";
						
						if(!$mail->Send())
						{
							//echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
							//echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
						}
						else
						{
						}
					}
				}
				else
				if($swact == 1)
				{
					$con = mysqli_query($conectar,"select * from notificar_h nh inner join notificar_d nd on (nd.not_clave_int = nh.not_clave_int) inner join usuario u on (u.usu_clave_int = nd.usu_clave_int) inner join obra o on (o.obr_clave_int = nh.obr_clave_int) where nh.obr_clave_int = '".$obr."' and nh.not_tipo = 2 and u.usu_clave_int <> '".$claveusuario."'");
					$num = mysqli_num_rows($con);
					for($i = 0; $i < $num; $i++)
					{
						$mail = new PHPMailer();
						
						$dato = mysqli_fetch_array($con);
						$mail->Body = '';
						$ema = '';
						$nomusu = $dato['usu_nombre'];
						$ema = $dato['usu_email'];
						$nomobr = $dato['obr_nombre'];
						
						$mail->From = "adminpavas@pavas.com.co";
						$mail->FromName = "I, A & C";
						$mail->AddAddress($ema, "Destino");
						$mail->Subject = "ACTUALIZACION REGISTRO FOTOGRAFICO. OBRA: ".$nomobr;
						
						// Cuerpo del mensaje
						$mail->Body .= "Hola ".$nomusu."!\n\n";	
						$mail->Body .= "ZONA CLIENTES registra que se ha actualizado un REGISTRO FOTOGRAFICO de su obra: ".$nomobr.".\n";
						$mail->Body .= "NOMBRE ARCHIVO: ".$nom."\n";
						$mail->Body .= date("d/m/Y H:m:s")."\n\n";
						$mail->Body .= "Este mensaje es generado automáticamente por ZONA CLIENTES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Zonaclientes \n";
						
						if(!$mail->Send())
						{
							//echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
							//echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
						}
						else
						{
						}
					}
				}
			}

			if($extension == 'jpg' or $extension == 'jpeg' or $extension == 'JPG' or $extension == 'JPEG')
			{
				//Reduzco el tamano de la imagen
				$img_origen = imagecreatefromjpeg($destino);
				$ancho_origen = imagesx($img_origen);//Se obtiene el ancho de la imagen
				$alto_origen = imagesy($img_origen);//Se obtiene el alto de la imagen
				$ancho_limite = 850;
				
				if($ancho_origen > $ancho_limite)//Para foto horizontal
				{
					$ancho_origen = $ancho_limite;
					$alto_origen = $ancho_limite*imagesy($img_origen)/imagesx($img_origen);
				}
				else//Para fotos verticales
				{
					$alto_origen = $ancho_limite;
					$ancho_origen = $ancho_limite*imagesx($img_origen)/imagesy($img_origen);
				}
				$img_destino = imagecreatetruecolor($ancho_origen, $alto_origen);//Se crea la imagen segun las dimensiones dadas
				imagecopyresized($img_destino, $img_origen, 0, 0, 0, 0, $ancho_origen, $alto_origen, imagesx($img_origen), imagesy($img_origen));
				imagejpeg($img_destino,$destino);//Se guarda la nueva foto
			}
			else
			if($extension == 'png' or $extension == 'PNG')
			{
				//Reduzco el tamano de la imagen
				$img_origen = imagecreatefrompng($destino);
				$ancho_origen = imagesx($img_origen);//Se obtiene el ancho de la imagen
				$alto_origen = imagesy($img_origen);//Se obtiene el alto de la imagen
				$ancho_limite = 850;
				
				if($ancho_origen > $ancho_limite)//Para foto horizontal
				{
					$ancho_origen = $ancho_limite;
					$alto_origen = $ancho_limite*imagesy($img_origen)/imagesx($img_origen);
				}
				else//Para fotos verticales
				{
					$alto_origen = $ancho_limite;
					$ancho_origen = $ancho_limite*imagesx($img_origen)/imagesy($img_origen);
				}
				$img_destino = imagecreatetruecolor($ancho_origen, $alto_origen);//Se crea la imagen segun las dimensiones dadas
				imagecopyresized($img_destino, $img_origen, 0, 0, 0, 0, $ancho_origen, $alto_origen, imagesx($img_origen), imagesy($img_origen));
				imagepng($img_destino,$destino);//Se guarda la nueva foto
			}
			else
			if($extension == 'gif' or $extension == 'GIF')
			{
				//Reduzco el tamano de la imagen
				$img_origen = imagecreatefromgif($destino);
				$ancho_origen = imagesx($img_origen);//Se obtiene el ancho de la imagen
				$alto_origen = imagesy($img_origen);//Se obtiene el alto de la imagen
				$ancho_limite = 850;
				
				if($ancho_origen > $ancho_limite)//Para foto horizontal
				{
					$ancho_origen = $ancho_limite;
					$alto_origen = $ancho_limite*imagesy($img_origen)/imagesx($img_origen);
				}
				else//Para fotos verticales
				{
					$alto_origen = $ancho_limite;
					$ancho_origen = $ancho_limite*imagesx($img_origen)/imagesy($img_origen);
				}
				$img_destino = imagecreatetruecolor($ancho_origen, $alto_origen);//Se crea la imagen segun las dimensiones dadas
				imagecopyresized($img_destino, $img_origen, 0, 0, 0, 0, $ancho_origen, $alto_origen, imagesx($img_origen), imagesy($img_origen));
				imagegif($img_destino,$destino);//Se guarda la nueva foto
			}
		}
	}
}
?>