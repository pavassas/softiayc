<?php
	include('../../data/Conexion.php');
	require_once('../../Classes/PHPMailer-master/class.phpmailer.php');
	date_default_timezone_set('America/Bogota');
	session_start();
	$usuario= $_SESSION['usuario'];
	$fecha=date("Y/m/d H:i:s");
	
	$con = mysqli_query($conectar,"select usu_clave_int from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
	$dato = mysqli_fetch_array($con);
	$claveusuario = $dato['usu_clave_int'];
	
	$service = $_POST['id'];
	$conimg = mysqli_query($conectar,"select caf_leyenda from carga_foto where caf_clave_int = '".$service."'");
	$datoimg = mysqli_fetch_array($conimg);
	$ley = $datoimg['caf_leyenda'];
	
	$con = mysqli_query($conectar,"select usu_nombre,usu_email,tii_nombre,o.obr_nombre,car_nombre from carga c inner join usuario_obra uo on (uo.obr_clave_int = c.obr_clave_int) inner join usuario u on (u.usu_clave_int = uo.usu_clave_int) inner join obra o on (o.obr_clave_int = c.obr_clave_int) inner join tipo_informe ti on (ti.tii_clave_int = c.tii_clave_int) where c.car_clave_int = (select car_clave_int from carga_foto where caf_clave_int = '".$service."') and c.car_estado = 1 and u.usu_clave_int <> '".$claveusuario."'");
	$num = mysqli_num_rows($con);
	for($i = 0; $i < $num; $i++)
	{
		$mail = new PHPMailer();
		
		$dato1 = mysqli_fetch_array($con);
		$mail->Body = '';
		$ema = '';
		$nom = $dato1['usu_nombre'];
		$ema = $dato1['usu_email'];
		$tii = $dato1['tii_nombre'];
		$obr = $dato1['obr_nombre'];
		$nomarc = $dato1['car_nombre'];
		
		$mail->From = "adminpavas@pavas.com.co";
		$mail->FromName = "I,A & C";
		$mail->AddAddress($ema, "Destino");
		$mail->Subject = "Imagen eliminada del registro fotografico. Obra: ".$obr;
		
		// Cuerpo del mensaje
		$mail->Body .= "Hola ".$nom."!\n\n";	
		$mail->Body .= "ZONA CLIENTES registra que se ha eliminado una imagen del REGISTRO FOTOGRAFICO de su obra: ".$obr.".\n";
		$mail->Body .= "NOMBRE ARCHIVO: ".$nomarc."\n";
		$mail->Body .= "LEYENDA IMAGEN: ".$ley."\n";
		$mail->Body .= date("d/m/Y H:m:s")."\n\n";
		$mail->Body .= "Este mensaje es generado automáticamente por ZONA CLIENTES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Zonaclientes \n";
		
		if(!$mail->Send())
		{
			//echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
			//echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
		}
		else
		{
		}
	}
	mysqli_query($conectar,"DELETE FROM carga_foto WHERE caf_clave_int='".$service."'");
?>