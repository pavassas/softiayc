<?php
	include('../../data/Conexion.php');
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_SESSION['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
		
	// verifica si no se ha loggeado
	if(!isset($_SESSION["persona"]))
	{
	  session_destroy();
	  header("LOCATION:index.php");
	}else{
	}
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");
	
	$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
	$dato = mysqli_fetch_array($con);
	$perfil = $dato['prf_descripcion'];
	$claveusuario = $dato['usu_clave_int'];
	$clave = $dato['usu_clave'];
	$ven = $dato['ven_clave_int'];
	$ultimaobra = $dato['obr_clave_int'];
		
	if($_GET['cambiarcon'] == 'si')
	{
		sleep(1);
		$ant = $_GET['ant'];
		$nue = $_GET['nue'];
		$conf = $_GET['conf'];
		$fecha=date("Y/m/d H:i:s");
		
		function decrypt($string, $key)
		{
			$result = "";
			$string = base64_decode($string);
			for($i=0; $i<strlen($string); $i++) 
			{
				$char = substr($string, $i, 1);
				$keychar = substr($key, ($i % strlen($key))-1, 1);
				$char = chr(ord($char)-ord($keychar));
				$result.=$char;
			}
			return $result;
		}
				
		if(decrypt($clave,"p4v4svasquez") <> $ant)
		{
			echo "<div class='validaciones'>La contraseña anterior no es válida</div>";
		}
		else		
		if($nue <> $conf)
		{
			echo "<div class='validaciones'>Las contraseñas no coinciden</div>";
		}
		else
		{
			//$nue = hash_hmac('sha512', 'salt' . $nue, 'p4v4svasquez');
			function encrypt($string, $key) 
			{
				$result = "";	
				for($i=0; $i<strlen($string); $i++) 
				{
					$char = substr($string, $i, 1);
					$keychar = substr($key, ($i % strlen($key))-1, 1);
					$char = chr(ord($char)+ord($keychar));
					$result.=$char;
				}
				return base64_encode($result);
			}
			$sql = mysqli_query($conectar,"update usuario set usu_clave = '".encrypt($nue,"p4v4svasquez")."',usu_primerinicio = 1 where usu_clave_int = '".$claveusuario."'");
			
			if($sql >= 1)
			{
				mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,ash_clave_int,asd_clave_int,art_clave_int,obr_clave_int,loa_usu_actualiz,loa_fec_actualiz) values(null,'".$ven."',6,'','','','".$ultimaobra."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 6=Cambio Contraseña
				echo "<div class='ok'>Contraseña Cambiada Correctamente</div>";
			}
			else
			{
				echo "<div class='validaciones'>No se han podido guardar los datos</div>";
			}
		}
		exit();
	}
?>
<!DOCTYPE HTML>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

	
<title>CONTROL DE OBRAS</title>
<meta name="description" content="Service Desk">
<meta name="author" content="InvGate S.R.L.">

<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
<link rel="stylesheet" href="css/style.css" type="text/css" />

<?php //VALIDACIONES ?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="llamadas.js"></script>

<?php //VENTANA EMERGENTE ?>
<link rel="stylesheet" href="../../css/reveal.css" />
<script type="text/javascript" src="../../js/jquery.reveal.js"></script>

<?php //CALENDARIO ?>
<link type="text/css" rel="stylesheet" href="../../css/dhtmlgoodies_calendar.css?random=20051112" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../../js/dhtmlgoodies_calendar.js?random=20060118"></script>
</head>


<body style="overflow:hidden; overflow-y:hidden; overflow-x:hidden;">
<form name="form1" id="form1" action="informes/programacionimp.php" method="post" onsubmit="return selectedVals();">
<!--[if lte IE 7]>
<div class="ieWarning">Este navegador no es compatible con el sistema. Por favor, use Chrome, Safari, Firefox o Internet Explorer 8 o superior.</div>
<![endif]-->
<table style="width: 100%; height: 271px;">
	<tr>
		<td class="auto-style2">
		<div id="editarprogramacion" class="reveal-modal" style="left: 57%; top: 50px; height: 370px; width: 350px;"></div>
		<div id="configuraciones">
			<br>
			<table align="center" style="width: 39%; border:1px solid" class="auto-style5">
				<tr>
					<td colspan="2" class="auto-style9" style="background-color:#092451;color:white"><strong>Cambiar Contraseña</strong></td>
				</tr>
				<tr>
					<td class="auto-style11"><strong>Contraseña Anterior:</strong></td>
					<td class="auto-style3">
					<input name="anterior" id="anterior" type="password" class="inputs"></td>
				</tr>
				<tr>
					<td class="auto-style11"><strong>Nueva Contraseña:</strong></td>
					<td class="auto-style3">
					<input name="nueva" id="nueva" type="password" class="inputs"></td>
				</tr>
				<tr>
					<td class="auto-style11"><strong>Confirmar Contraseña:</strong></td>
					<td class="auto-style6">
					<input name="confirmar" id="confirmar" type="password" class="inputs"></td>
				</tr>
				<tr>
					<td class="auto-style12" colspan="2">
					<hr>
					<input name="Button1" type="button" value="Cambiar" onclick="CAMBIAR()" style="width: 257px; cursor:pointer"></td>
				</tr>
				<tr>
					<td class="auto-style8" colspan="2">
					<div id="datos">
					</div>
					</td>
				</tr>
				</table>
		</div>
		</td>
	</tr>
	</table>
</form>
</body>
</html>