<?php
include('../../data/Conexion.php');
	header("Cache-Control: no-store, no-cache, must-revalidate");
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");
	session_start();
	$usuario= $_SESSION['usuario'];
	$opcion = $_POST['opcion'];
	if($opcion=="BUSCAR")
	{
		?>
		<script src="../../js/datatable/jslistactividades.js"></script>
		<table style="width: 100%" id="tblistaactividades" class="table table-striped table-condensed">
			<thead>
			<tr>
			<th class="dt-head-center bg-info" style="height: 23px; width: 200px;"><strong>Actividad</strong></th>
			<th class="dt-head-center bg-info" style="height: 23px; width: 200px;"><strong>Ventana</strong></th>
			<th class="dt-head-center bg-info" style="height: 23px; width: 200px;"><strong>Registro</strong></th>
			<th class="dt-head-center bg-info" style="width: 170px; height: 23px;"><strong>Creado Por</strong></th>
			<th class="dt-head-center bg-info" style="width: 170px; height: 23px;"><strong>Fecha Creación</strong></th>
			</tr>
			</thead>
			<tfoot>
			<tr>
			<th class="dt-head-center bg-info" style="height: 23px; width: 200px;"><strong></strong></th>
			<th class="dt-head-center bg-info" style="height: 23px; width: 200px;"><strong></strong></th>
			<th class="dt-head-center bg-info" style="height: 23px; width: 200px;"><strong></strong></th>
			<th class="dt-head-center bg-info" style="width: 170px; height: 23px;"><strong></strong></th>
			<th class="dt-head-center bg-info" style="width: 170px; height: 23px;"><strong></strong></th>
			</tr>
			</tfoot>
		</table>
		<?php
	}