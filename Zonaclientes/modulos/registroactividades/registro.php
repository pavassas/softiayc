<?php
	include('../../data/Conexion.php');
	header("Cache-Control: no-store, no-cache, must-revalidate");
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");
?>
	<div style="display:none">
	<?php
	session_start();
	$usuario= $_SESSION['usuario'];
	require_once("lib/Zebra_Pagination.php");
	?>
	</div>
<?php
	$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
	$dato = mysqli_fetch_array($con);
	$perfil = $dato['prf_descripcion'];
	$claveusuario = $dato['usu_clave_int'];
	$ultimaobra = $dato['obr_clave_int'];
?>
<?php
	if($_GET['buscarmat'] == 'si')
	{
		$act = $_GET['act'];
		$usu = $_GET['usu'];
		$ven = $_GET["ven"];
		$fi = $_GET["fi"];
		$ff = $_GET["ff"];
		
		$seleccionados = explode(',',$act);
		$num = count($seleccionados);
		$actividades = array();
		for($i = 0; $i < $num; $i++)
		{
			if($seleccionados[$i] != '')
			{
				$actividades[$i]=$seleccionados[$i];
			}
		}
		$listaactividades=implode(',',$actividades);
		
		$seleccionados1 = explode(',',$usu);
		$num = count($seleccionados1);
		$usuarios = array();
		for($i = 0; $i < $num; $i++)
		{
			if($seleccionados1[$i] != '')
			{
				$usuarios[$i]=$seleccionados1[$i];
			}
		}
		$listausuarios=implode(',',$usuarios);
				
		$seleccionados3 = explode(',',$ven);
		$num = count($seleccionados3);
		$ventanas = array();
		for($i = 0; $i < $num; $i++)
		{
			if($seleccionados3[$i] != '')
			{
				$ventanas[$i]=$seleccionados3[$i];
			}
		}
		$listaventanas=implode(',',$ventanas);
?>
		<table style="width: 95%">
			<tr>
			<td class="auto-style15" style="height: 23px; width: 200px;"><strong>Actividad</strong></td>
			<td class="auto-style15" style="height: 23px; width: 200px;"><strong>Ventana</strong></td>
			<td class="auto-style15" style="height: 23px; width: 200px;"><strong>Registro</strong></td>
			<td class="auto-style15" style="width: 170px; height: 23px;">
			<strong>
			Creado Por</strong></td>
			<td class="auto-style15" style="width: 170px; height: 23px;">
			<strong>Fecha Creación</strong></td>
			</tr>
		<?php
			$contador=0;
			if($listaactividades != ''){ if($sql1 == ''){	$sql1 = "ta.tia_clave_int in (".$listaactividades.")"; }else{ $sql1 = $sql1." and ta.tia_clave_int in (".$listaactividades.")"; } }
			if($listausuarios != ''){ if($sql1 == ''){	$sql1 = "u.usu_clave_int in (".$listausuarios.")"; }else{ $sql1 = $sql1." and u.usu_clave_int in (".$listausuarios.")"; } }
			if($listaventanas != ''){ if($sql1 == ''){	$sql1 = "v.ven_clave_int in (".$listaventanas.")"; }else{ $sql1 = $sql1." and v.ven_clave_int in (".$listaventanas.")"; } }
			if($sql1 == ''){ $sql1 = "((la.loa_fec_actualiz BETWEEN '".$fi." 00:00:00' AND '".$ff." 23:59:59') or ('".$fi."' Is Null and '".$ff."' Is Null) or ('".$fi."' = '' and '".$ff."' = ''))"; }else{ $sql1 = $sql1." and ((la.loa_fec_actualiz BETWEEN '".$fi." 00:00:00' AND '".$ff." 23:59:59') or ('".$fi."' Is Null and '".$ff."' Is Null) or ('".$fi."' = '' and '".$ff."' = ''))"; }
			
			if(strtoupper($perfil) == strtoupper('Administrador'))
			{
				$query = mysqli_query($conectar,"select * from log_actividades la inner join usuario u on (u.usu_usuario = la.loa_usu_actualiz) inner join tipo_actividad ta on (ta.tia_clave_int = la.tia_clave_int) left outer join ventana v on (v.ven_clave_int = la.ven_clave_int) where ".$sql1."");
			}
			else
			{
				$query = mysqli_query($conectar,"select * from log_actividades la inner join usuario u on (u.usu_usuario = la.loa_usu_actualiz) inner join tipo_actividad ta on (ta.tia_clave_int = la.tia_clave_int) left outer join ventana v on (v.ven_clave_int = la.ven_clave_int) where ".$sql1." and la.loa_usu_actualiz = '".$usuario."'");
			}
			//$res = $con->query($query);
			$num_registros = mysqli_num_rows($query);
			
			$resul_x_pagina = 100;
			//Paginar:
			$paginacion = new Zebra_Pagination();
			$paginacion->records($num_registros);
			$paginacion->records_per_page($resul_x_pagina);
			
			if(strtoupper($perfil) == strtoupper('Administrador'))
			{
				$con = mysqli_query($conectar,"select * from log_actividades la inner join usuario u on (u.usu_usuario = la.loa_usu_actualiz) inner join tipo_actividad ta on (ta.tia_clave_int = la.tia_clave_int) left outer join ventana v on (v.ven_clave_int = la.ven_clave_int) where ".$sql1." order by la.loa_fec_actualiz DESC, ta.tia_nombre, v.ven_nombre, la.loa_usu_actualiz LIMIT ".(($paginacion->get_page() - 1) * $resul_x_pagina). ',' .$resul_x_pagina);
			}
			else
			{
				$con = mysqli_query($conectar,"select * from log_actividades la inner join usuario u on (u.usu_usuario = la.loa_usu_actualiz) inner join tipo_actividad ta on (ta.tia_clave_int = la.tia_clave_int) left outer join ventana v on (v.ven_clave_int = la.ven_clave_int) where ".$sql1." and la.loa_usu_actualiz = '".$usuario."' order by la.loa_fec_actualiz DESC, ta.tia_nombre, v.ven_nombre, la.loa_usu_actualiz LIMIT ".(($paginacion->get_page() - 1) * $resul_x_pagina). ',' .$resul_x_pagina);
			}
			$num = mysqli_num_rows($con);
			
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$ven = $dato['ven_nombre'];
				$act = $dato['tia_nombre'];
				$reg = $dato['loa_registro'];
				$usu = $dato['loa_usu_actualiz'];
				$fec = $dato['loa_fec_actualiz'];
				
				$contador=$contador+1;
		?>
		<tr>
			<td class="auto-style21" style="width: 200px"><?php echo $act; ?></td>
			<td class="auto-style21" style="width: 200px"><?php echo $ven; ?></td>
			<td class="auto-style21" style="width: 200px"><?php echo $reg; ?></td>
			<td class="auto-style21" style="width: 170px"><?php echo $usu; ?></td>
			<td class="auto-style21" style="width: 170px"><?php echo $fec; ?></td>
		</tr>
		<tr>
			<td class="auto-style3" colspan="5">
			<hr>
			</td>
		</tr>
		<?php
			}
		?>
	</table>
<?php
		$paginacion->render('');
		exit();
	}
?>
<!DOCTYPE HTML>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

	
<title>CONTROL DE OBRAS</title>
<meta name="description" content="Service Desk">
<meta name="author" content="InvGate S.R.L.">

<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
<link rel="stylesheet" href="css/style.css" type="text/css" />

<script type="text/javascript" language="javascript">
	/*selecteds=0;
	
	function CheckUncheck(total,check){
		checkbox=null;
		for(i=1;i<=total;i++){
			checkbox=document.getElementById("idcat"+i);
			//alert(checkbox.value);
			checkbox.checked=check.checked;
		}
		
		if(check.checked){
			selecteds=total;
		}else{
			selecteds=0;
		}
		
	}
	
	function contadorVals(check){
		if(check.checked){
			selecteds=selecteds+1;
		}else{
			selecteds=selecteds-1;
		}
	}
	
	function selectedVals(){
		if(selecteds==0){
			alert("Seleccione al menos un registro.");
			return false;
		}else{
			return true;
		}
	}*/
	function OCULTARSCROLL()
	{
		setTimeout("parent.autoResize('iframe4')",500);
		setTimeout("parent.autoResize('iframe4')",1000);
		setTimeout("parent.autoResize('iframe4')",2000);
		setTimeout("parent.autoResize('iframe4')",3000);
		setTimeout("parent.autoResize('iframe4')",4000);
		setTimeout("parent.autoResize('iframe4')",5000);
		setTimeout("parent.autoResize('iframe4')",6000);
		setTimeout("parent.autoResize('iframe4')",7000);
		setTimeout("parent.autoResize('iframe4')",8000);
		setTimeout("parent.autoResize('iframe4')",9000);
		setTimeout("parent.autoResize('iframe4')",10000);
		setTimeout("parent.autoResize('iframe4')",11000);
		setTimeout("parent.autoResize('iframe4')",12000);
		setTimeout("parent.autoResize('iframe4')",13000);
		setTimeout("parent.autoResize('iframe4')",14000);
		setTimeout("parent.autoResize('iframe4')",15000);
		setTimeout("parent.autoResize('iframe4')",16000);
		setTimeout("parent.autoResize('iframe4')",17000);
		setTimeout("parent.autoResize('iframe4')",18000);
		setTimeout("parent.autoResize('iframe4')",19000);
		setTimeout("parent.autoResize('iframe4')",20000);
	}
	setTimeout("parent.autoResize('iframe4')",500);
	setTimeout("parent.autoResize('iframe4')",1000);
	setTimeout("parent.autoResize('iframe4')",2000);
	setTimeout("parent.autoResize('iframe4')",3000);
	setTimeout("parent.autoResize('iframe4')",4000);
	setTimeout("parent.autoResize('iframe4')",5000);
	setTimeout("parent.autoResize('iframe4')",6000);
	setTimeout("parent.autoResize('iframe4')",7000);
	setTimeout("parent.autoResize('iframe4')",8000);
	setTimeout("parent.autoResize('iframe4')",9000);
	setTimeout("parent.autoResize('iframe4')",10000);
	setTimeout("parent.autoResize('iframe4')",11000);
	setTimeout("parent.autoResize('iframe4')",12000);
	setTimeout("parent.autoResize('iframe4')",13000);
	setTimeout("parent.autoResize('iframe4')",14000);
	setTimeout("parent.autoResize('iframe4')",15000);
	setTimeout("parent.autoResize('iframe4')",16000);
	setTimeout("parent.autoResize('iframe4')",17000);
	setTimeout("parent.autoResize('iframe4')",18000);
	setTimeout("parent.autoResize('iframe4')",19000);
	setTimeout("parent.autoResize('iframe4')",20000);
</script>
<?php //VALIDACIONES ?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="llamadas.js?<?php echo time();?>"></script>

<?php //CALENDARIO ?>
<link type="text/css" rel="stylesheet" href="../../css/dhtmlgoodies_calendar.css?random=20051112" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../../js/dhtmlgoodies_calendar.js?random=20060118"></script>

<script type="text/javascript" src="../../js/jquery.searchabledropdown-1.0.8.min.js"></script>
<script type="text/javascript">
/*$(document).ready(function() {
	$("select").searchable();
});*/
</script>
<?php //********ESTAS LIBRERIAS JS Y CSS SIRVEN PARA HACER LA BUSQUEDA DINAMICA CON CHECKLIST************//?>
<link rel="stylesheet" type="text/css" href="../../css/checklist/jquery.multiselect.css" />
<link rel="stylesheet" type="text/css" href="../../css/checklist/jquery.multiselect.filter.css" />
<link rel="stylesheet" type="text/css" href="../../css/checklist/styleselect.css" />
<link rel="stylesheet" type="text/css" href="../../css/checklist/prettify.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script type="text/javascript" src="../../js/checklist/jquery.multiselect.js"></script>
<script type="text/javascript" src="../../js/checklist/jquery.multiselect.filter.js"></script>
<script type="text/javascript" src="../../js/checklist/prettify.js"></script>
<link rel="stylesheet" href="../../Classes/datatables/dataTables.bootstrap.css">
<script src="../../Classes/datatables/jquery.dataTables.min.js"></script>
<script src="../../Classes/datatables/dataTables.bootstrap.min.js"></script>

<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="../../Classes/bootstrap/css/bootstrap.min.css">
<script src="../../Classes/bootstrap/js/bootstrap.min.js"></script>
<?php //**************************************************************************************************//?>

</head>
<body>
<form name="form1" id="form1" method="post">
<!--[if lte IE 7]>
<div class="ieWarning">Este navegador no es compatible con el sistema. Por favor, use Chrome, Safari, Firefox o Internet Explorer 8 o superior.</div>
<![endif]-->
<table style="width: 100%">
	<tr>
		<td style="cursor:pointer" colspan="8" align="center">
		<strong>REGISTRO DE ACTIVIDADES DE USUARIOS</strong></td>
	</tr>
	<tr>
		<td class="auto-style2" style="cursor:pointer" colspan="8">
		<div id="filtro">
			<table style="width: 95%">
				<tr>
					<td class="auto-style1" style="width: 132px"><strong>
					<span class="auto-style13">Filtro:</span><img src="../../images/buscar.png" alt="" height="18" width="15" /></strong></td>
					<td align="left">
					<table style="width: 100%">
						<tr>
							<td>Actividades:</td>
							<td>Usuarios:</td>
							<td>Ventana:</td>
						</tr>
						<tr>
							<td>
					<select multiple="multiple" onchange="BUSCAR('')" name="busactividad" id="busactividad" style="width:315px">
					<?php
						$con = mysqli_query($conectar,"select * from tipo_actividad order by tia_nombre");
						$num = mysqli_num_rows($con);
						for($i = 0; $i < $num; $i++)
						{
							$dato = mysqli_fetch_array($con);
							$clave = $dato['tia_clave_int'];
							$nombre = $dato['tia_nombre'];
					?>
						<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
					<?php
						}
					?>
					</select></td>
							<td>
					<select multiple="multiple" onchange="BUSCAR('')" name="bususuario" id="bususuario" style="width:315px" name="D1">
					<?php
						if(strtoupper($perfil) == strtoupper('Administrador'))
						{
							$con = mysqli_query($conectar,"select * from usuario order by usu_nombre");
						}
						else
						{
							$con = mysqli_query($conectar,"select * from usuario where usu_usuario = '".$usuario."' order by usu_nombre");
						}
						$num = mysqli_num_rows($con);
						for($i = 0; $i < $num; $i++)
						{
							$dato = mysqli_fetch_array($con);
							$clave = $dato['usu_clave_int'];
							$usu = $dato['usu_usuario'];
							$nombre = $dato['usu_nombre'];
					?>
						<option value="<?php echo $clave; ?>"><?php echo $nombre." - ".$usu; ?></option>
					<?php
						}
					?>
					</select></td>
						<td>
						<select multiple="multiple" onchange="BUSCAR('')" name="busventana" id="busventana" style="width:315px">
							<?php
								$con = mysqli_query($conectar,"select * from ventana order by ven_nombre");
								$num = mysqli_num_rows($con);
								for($i = 0; $i < $num; $i++)
								{
									$dato = mysqli_fetch_array($con);
									$clave = $dato['ven_clave_int'];
									$nombre = $dato['ven_nombre'];
							?>
								<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
							<?php
								}
							?>
							</select>
						</td>
						</tr>
						<tr>
							<td>
							<input class="inputs" onchange="BUSCAR('')" onkeyup="BUSCAR('')" name="busfecini" id="busfecini" onclick="displayCalendar(this,'yyyy-mm-dd',this)" maxlength="70" type="text" placeholder="Fecha Inicial" style="width: 158px" />
							</td>
							<td>
							<input class="inputs" onchange="BUSCAR('')" onkeyup="BUSCAR('')" name="busfecfin" id="busfecfin" onclick="displayCalendar(this,'yyyy-mm-dd',this)" maxlength="70" type="text" placeholder="Fecha Final" style="width: 158px" />
							</td>
						<td>
						&nbsp;</td>
						</tr>
					</table>
					</td>
				</tr>
				</table>
		</div>
		</td>
	</tr>
	<tr>
		<td colspan="8" class="auto-style2">
		<form name="formulario" method="post">
		<div id="asignados" style="width:100%">
				<table style="width: 100%" align="center">
					<tr>
						<td class="auto-style3" colspan="11">
						<fieldset name="Group1">
						<legend align="center">LOG ACTIVIDADES <br>
						<button name="Accion" value="excel" title="Exportar a Excel" onclick="EXPORTAR()" type="button" style="cursor:pointer;">
						<img src="../../images/excel.png" height="25" width="25"></button>
						</legend>
						
						<table style="width: 100%">
							<tr>
								<td class="auto-style1">
								<div class='ok' id="materialesagg" style="width:100%">
									<table style="width: 95%">
										<tr>
										<td class="auto-style15" style="height: 23px; width: 200px;"><strong>Actividad</strong></td>
										<td class="auto-style15" style="height: 23px; width: 200px;"><strong>Ventana</strong></td>
										<td class="auto-style15" style="height: 23px; width: 200px;"><strong>Registro</strong></td>
										<td class="auto-style15" style="width: 170px; height: 23px;">
										<strong>
										Creado Por</strong></td>
										<td class="auto-style15" style="width: 170px; height: 23px;">
										<strong>Fecha Creación</strong></td>
										</tr>
									<?php
										$contador=0;
										if(strtoupper($perfil) == strtoupper('Administrador'))
										{
											$query = mysqli_query($conectar,"select * from log_actividades la inner join tipo_actividad ta on (ta.tia_clave_int = la.tia_clave_int) left outer join ventana v on (v.ven_clave_int = la.ven_clave_int)");
										}
										else
										{
											$query = mysqli_query($conectar,"select * from log_actividades la inner join tipo_actividad ta on (ta.tia_clave_int = la.tia_clave_int) left outer join ventana v on (v.ven_clave_int = la.ven_clave_int) where la.loa_usu_actualiz = '".$usuario."'");
										}
										//$res = $con->query($query);
										$num_registros = mysqli_num_rows($query);
										
										$resul_x_pagina = 100;
										//Paginar:
										$paginacion = new Zebra_Pagination();
										$paginacion->records($num_registros);
										$paginacion->records_per_page($resul_x_pagina);
											
										if(strtoupper($perfil) == strtoupper('Administrador'))
										{
											$con = mysqli_query($conectar,"select * from log_actividades la inner join tipo_actividad ta on (ta.tia_clave_int = la.tia_clave_int) left outer join ventana v on (v.ven_clave_int = la.ven_clave_int) order by la.loa_fec_actualiz DESC, ta.tia_nombre, v.ven_nombre, la.loa_usu_actualiz LIMIT ".(($paginacion->get_page() - 1) * $resul_x_pagina). ',' .$resul_x_pagina);
										}
										else
										{
											$con = mysqli_query($conectar,"select * from log_actividades la inner join tipo_actividad ta on (ta.tia_clave_int = la.tia_clave_int) left outer join ventana v on (v.ven_clave_int = la.ven_clave_int) where la.loa_usu_actualiz = '".$usuario."' order by la.loa_fec_actualiz DESC, ta.tia_nombre, v.ven_nombre, la.loa_usu_actualiz LIMIT ".(($paginacion->get_page() - 1) * $resul_x_pagina). ',' .$resul_x_pagina);
										}
										$num = mysqli_num_rows($con);
										
										for($i = 0; $i < $num; $i++)
										{
											$dato = mysqli_fetch_array($con);
											$ven = $dato['ven_nombre'];
											$act = $dato['tia_nombre'];
											$reg = $dato['loa_registro'];
											$usu = $dato['loa_usu_actualiz'];
											$fec = $dato['loa_fec_actualiz'];
											$contador=$contador+1;
									?>
									<tr>
										<td class="auto-style21" style="width: 200px"><?php echo $act; ?></td>
										<td class="auto-style21" style="width: 200px"><?php echo $ven; ?></td>
										<td class="auto-style21" style="width: 200px"><?php echo $reg; ?></td>
										<td class="auto-style21" style="width: 170px"><?php echo $usu; ?></td>
										<td class="auto-style21" style="width: 170px"><?php echo $fec; ?></td>
									</tr>
									<tr>
										<td class="auto-style3" colspan="5">
										<hr>
										</td>
									</tr>
									<?php
										}
									?>
									</table>
									<?php $paginacion->render(''); ?>
								</div>
								</td>
							</tr>
						</table>
						
						</fieldset>
					</td>
					</tr>
					<tr>
						<td class="auto-style3" style="width: 150px; height: 23px;">
						&nbsp;</td>
						<td class="auto-style3" style="width: 149px; height: 23px;">
						&nbsp;</td>
						<td class="auto-style3" style="width: 150px; height: 23px;">
						&nbsp;</td>
						<td class="auto-style3" style="width: 150px; height: 23px;">
						&nbsp;</td>
						<td class="auto-style3" style="width: 150px; height: 23px;">
						&nbsp;</td>
						<td class="auto-style3" style="width: 150px; height: 23px;">
						&nbsp;</td>
						<td class="auto-style3" style="width: 100px; height: 23px;">
						&nbsp;</td>
						<td class="auto-style3" style="width: 100px; height: 23px;">
						&nbsp;</td>
						<td class="auto-style3" style="width: 61px; height: 23px;">
						&nbsp;</td>
						<td class="auto-style3" style="width: 252px; height: 23px;">
						&nbsp;</td>
						<td class="auto-style3" style="width: 382px; height: 23px;">
						&nbsp;</td>
					</tr>
					<tr>
						<td class="auto-style3" style="width: 150px; height: 23px;">
							&nbsp;</td>
						<td class="auto-style3" style="width: 150px; height: 23px;">
							&nbsp;</td>
						<td class="auto-style3" style="width: 150px; height: 23px;">
							&nbsp;</td>
						<td class="auto-style3" style="width: 150px; height: 23px;">
							&nbsp;</td>
						<td class="auto-style3" style="width: 150px; height: 23px;">
							&nbsp;</td>
						<td class="auto-style3" style="width: 150px; height: 23px;">
							&nbsp;</td>
						<td class="auto-style3" style="width: 100px; height: 23px;">
							&nbsp;</td>
						<td class="auto-style3" style="width: 100px; height: 23px;">
							&nbsp;</td>
						<td class="auto-style3" style="width: 61px; height: 23px;">
							&nbsp;</td>
						<td class="auto-style3" style="width: 252px; height: 23px;">
							&nbsp;</td>
						<td class="auto-style3" style="width: 382px; height: 23px;">
							&nbsp;</td>
					</tr>
					</table>
		</div>
		</form>
		</td>
	</tr>
	<tr>
		<td style="width: 100px">&nbsp;</td>
		<td style="width: 80px">&nbsp;</td>
		<td style="width: 90px">&nbsp;</td>
		<td style="width: 100px">&nbsp;</td>
		<td style="width: 80px">&nbsp;</td>
		<td style="width: 100px">&nbsp;</td>
		<td style="width: 50px">&nbsp;</td>
		<td style="width: 49px">&nbsp;</td>
	</tr>
	<tr>
		<td style="width: 100px">&nbsp;</td>
		<td style="width: 80px">&nbsp;</td>
		<td style="width: 90px">&nbsp;</td>
		<td style="width: 100px">&nbsp;</td>
		<td style="width: 80px">&nbsp;</td>
		<td style="width: 100px">&nbsp;</td>
		<td style="width: 50px">&nbsp;</td>
		<td style="width: 49px">&nbsp;</td>
	</tr>
</table>
<script type="text/javascript" language="javascript">
	$(document).ready(function() {
		BUSCAR('');	
	});
    $("#asignados").on("click",".expand",function(){
        $(this).toggle();
        $(this).next().toggle();
        $(this).parent().parent().children().last().toggle();
    });
    $("#asignados").on("click",".collapse",function(){
        $(this).toggle();
        $(this).prev().toggle();
        $(this).parent().parent().children().last().toggle();
    });

    $("input[type='checkbox']").click(function () {
        if ($(this).attr("checked") == false) {
            $(this).parent().parent().find("input[type='checkbox']").each(function () {
                $(this).removeAttr("checked");
            });
        }
        else {
            $(this).parent().parent().find("input[type='checkbox']").each(function () {
                $(this).attr("checked", "checked");
            });
        }
    });
    $("#busactividad").multiselect().multiselectfilter();
    $("#bususuario").multiselect().multiselectfilter();
    $("#busventana").multiselect().multiselectfilter();
</script>
</form>
</body>
</html>