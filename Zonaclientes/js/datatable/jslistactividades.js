$(document).ready(function() {
	var selected = [];
	var actividades = "";
	var objCBarray = document.getElementsByName('multiselect_busactividad');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	actividades += objCBarray[i].value + ",";
	    }
	}
	
	var usuarios = "";
	var objCBarray = document.getElementsByName('multiselect_bususuario');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	usuarios += objCBarray[i].value + ",";
	    }
	}
	
	var articulos = "";
	var objCBarray = document.getElementsByName('multiselect_busarticulo');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	articulos += objCBarray[i].value + ",";
	    }
	}
	
	var ventanas = "";
	var objCBarray = document.getElementsByName('multiselect_busventana');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	ventanas += objCBarray[i].value + ",";
	    }
	}
	
	var fi = form1.busfecini.value;
	var ff = form1.busfecfin.value;

	var table = $('#tblistaactividades').DataTable( {  
		"searching":false,
	"ordering": true,
	"info": true,
	"autoWidth": false,
	"pagingType": "simple_numbers",
	"lengthMenu": [[50,100,200,-1 ], [50,100,200,"Todos" ]],
	"language": {
		"lengthMenu": "Ver _MENU_",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "Anterior","next":"Siguiente"}
	},		
	"processing": true,
	"serverSide": true,
	"ajax": {
	            "url": "../../json/jsonlistaactividades.php",
	            "data": {act:actividades,usu:usuarios,art:articulos,ven:ventanas,fi:fi,ff:ff},
	            "type":"POST"
			},
	"columns": [
		{ "data": "Actividad" },
		{ "data": "Ventana" },
		{ "data": "Registro" },
		{ "data": "Creado" },
		{ "data": "FechaCreacion" }
	],
	"order": [[4, 'desc']],
	"rowCallback": function( row, data){
		if( $.inArray(data.DT_RowId, selected) !== -1 )
		{
			$(row).addClass('selected');
		}
	}
	} );

	/*$('#tbcomiteobra tfoot th').each( function () {
	var title = $('#tbcomiteobra tfoot th').eq( $(this).index() ).text();
	if(title==""){$(this).html('');}else{
	$(this).html( '<div class="input-group"><input type="text" style="width:100%" class="form-control" placeholder="'+title+'" /></div>' );}
	} );*/

	// DataTable
	var table = $('#tblistaactividades').DataTable();

	// Apply the search
	table.columns().every( function () {
	var that = this;

	$( 'input', this.footer() ).on( 'keyup change', function () {
	    table.search( this.value ).draw();
	} );
	} );

	$('#tblistaactividades tbody').on('click', 'tr', function () {
		var id = this.id;
		var index = $.inArray(id, selected);

		if ( index === -1 ) {
		    selected.push( id );
		} else {
		    selected.splice( index, 1 );
		}

		$(this).toggleClass('selected');
	} );
	    
});	