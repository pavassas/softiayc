// JavaScript Document
// JavaScript Document
function CARGARTABLA(tipo)
{
	var obras = "";
	var objCBarray = document.getElementsByName('multiselect_busobra');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	obras += objCBarray[i].value + ",";
	    }
	}
	
	var informes = "";
	var objCBarray = document.getElementsByName('multiselect_bustipoinforme');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	informes += objCBarray[i].value + ",";
	    }
	}

	var nomarc = $('#busnombrearchivo').val();
	var nomane = $('#busnombreanexo').val();
	var nomley = $('#busleyenda').val();
	var nomcom = $('#buscomentario').val();
	
	if(tipo == 'COMITEOBRA')
	{
		var selected = [];
		var table = $('#tbcomiteobra').DataTable( {       
	        
			"ordering": true,
			"info": true,
			"autoWidth": false,
			"pagingType": "simple_numbers",
			"lengthMenu": [[50,100,200,-1 ], [50,100,200,"Todos" ]],
			"language": {
			"lengthMenu": "Ver _MENU_ registros",
			"zeroRecords": "No se encontraron datos",
			"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
			"infoEmpty": "No se encontraron datos",
			"sSearch": "Buscar:",
			"infoFiltered": ""
			},
			"processing": true,
	        "serverSide": true,
	        "ajax": {
	                    "url": "../../json/jsoncomiteobra.php",
	                    "data": { obras: obras,informes: informes,nomarc:nomarc,nomane:nomane,nomley:nomley,nomcom:nomcom  }
					},
			"columns": [
				{ "data": "NOMBRE" },
				{ "data": "OBRA" },
				{ "data": "COMENTARIOS" },
				{ "data": "FECHACREACION" },
				{ "data": "USUARIOACTUALIZ" },
				{ "data": "FECHAACTUALIZ" },
				{ "orderable": false, "data": "EDITAR" },
				{ "orderable": false, "data": "VERONLINE" },
				{ 
					"class":          "details-control",
					"orderable": false,
					"data": "VERANEXOS" 
				},
				{ "orderable": false, "data": "DESCARGAR" },
				{ "orderable": false, "data": "ZIP" },
				{ "orderable": false, "data": "APROBAR" },
				{ "orderable": false, "data": "ELIMINAR" }
			],
			"order": [[0, 'desc']],
			"rowCallback": function( row, data){
				if( $.inArray(data.DT_RowId, selected) !== -1 )
				{
					$(row).addClass('selected');
				}
			}
	    } );
	     
		  /*$('#tbcomiteobra tfoot th').each( function () {
	        var title = $('#tbcomiteobra tfoot th').eq( $(this).index() ).text();
			if(title==""){$(this).html('');}else{
	        $(this).html( '<div class="input-group"><input type="text" style="width:100%" class="form-control" placeholder="'+title+'" /></div>' );}
	    } );*/
	     
	    // DataTable
	    var table = $('#tbcomiteobra').DataTable();
	 
	    // Apply the search
	    table.columns().every( function () {
	        var that = this;
	 
	        $( 'input', this.footer() ).on( 'keyup change', function () {
	            table.search( this.value ).draw();
	        } );
	    } );
	    
	    $('#tbcomiteobra tbody').on('click', 'tr', function () {
	        var id = this.id;
	        var index = $.inArray(id, selected);
	 
	        if ( index === -1 ) {
	            selected.push( id );
	        } else {
	            selected.splice( index, 1 );
	        }
	 
	        $(this).toggleClass('selected');
	    } );
	    
	    $('#aprobarmasivocomiteobra').on('click', function() 
		{
		    var sel = [];
		    var conf = confirm("Realmente desea aprobar los informes seleccionados?");
			if(conf==true)
			{
				for(var i = 0; i<selected.length;i++)
				{
					if(selected[i]=="" || selected[i]==null)
					{
					}
					else
					{
						var id = selected[i];
						var n=id.split("row_");
						sel.push(n[1]);
						APROBARMASIVO(id.replace('row_',''),'COMITEOBRA');
					}
				}
				$('#estadoaprobacioncomiteobra').html('<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button>Los informes seleccionados han sido aprobados correctamente</div>');
				sel.length=0; selected.length = 0;
			}
			else
			{
			  return false;
			}
	    });
	    
	    $('#eliminarmasivocomiteobra').on('click', function() 
		{
		    var sel = [];
		    var conf = confirm("Realmente desea eliminar los informes seleccionados?");
			if(conf==true)
			{
				for(var i = 0; i<selected.length;i++)
				{
					if(selected[i]=="" || selected[i]==null)
					{
					}
					else
					{
						var id = selected[i];
						var n=id.split("row_");
						sel.push(n[1]);
						ELIMINARMASIVO(id.replace('row_',''),'COMITEOBRA');
					}
				}
				$('#estadoaprobacioncomiteobra').html('<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button>Los informes seleccionados han sido eliminados correctamente</div>');
				sel.length=0; selected.length = 0;
			}
			else
			{
			  return false;
			}
	    });
	    
	    var detailRows = [];
	 
	    $('#tbcomiteobra tbody').on( 'click', 'tr td.details-control', function () {
	        var tr = $(this).closest('tr');
	        var row = table.row( tr );
			var dat = row.data();
			var id = dat.CLAVE;		
	
	        if ( row.child.isShown()) 
	        {
	            tr.removeClass( 'details' );
	            row.child.hide();
	            $("#verocultaranexo"+id).html("VER ANEXOS");
	        }
	        else 
	        {
	        	$("#verocultaranexo"+id).html("OCULTAR ANEXOS");
	            // Open this row
				var table1 = "<div id='divchil"+id+"'><div class='col-xs-1'></div><div id='no-more-tables'><table style='font-size:12px;width:40%' class='table table-condensed table-striped' id='tbanexos"+id+"'>";
				table1+="<thead><tr><th>NOMBRE DEL ANEXO</th><th>VER</th><th>DESCARGAR</th></tr></thead><tbody>";
				              
				table1+="</tbody><tfoot><tr><th></th><th></th><th></th></tr></tfoot></table></div></div>";
				row.child(table1).show();
				convertirdata(id);
	            tr.addClass('details');
			}
			
		    });
		    
			  // On each draw, loop over the `detailRows` array and show any child rows
			table.on( 'draw', function () {
		        $.each( detailRows, function ( i, id ) {
		            $('#'+id+' td.details-control').trigger( 'click' );
		     });
	    });
	}
	else
	if(tipo == 'COMITETECNICO')
	{
		var selected = [];
		var table = $('#tbcomitetecnico').DataTable( {       
	        
			"ordering": true,
			"info": true,
			"autoWidth": false,
			"pagingType": "simple_numbers",
			"lengthMenu": [[50,100,200,-1 ], [50,100,200,"Todos" ]],
			"language": {
			"lengthMenu": "Ver _MENU_ registros",
			"zeroRecords": "No se encontraron datos",
			"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
			"infoEmpty": "No se encontraron datos",
			"sSearch": "Buscar:",
			"infoFiltered": ""
			},
			"processing": true,
	        "serverSide": true,
	        "ajax": {
	                    "url": "../../json/jsoncomitetecnico.php",
	                    "data": { obras: obras,informes: informes,nomarc:nomarc,nomane:nomane,nomley:nomley,nomcom:nomcom }
					},
			"columns": [
				{ "data": "NOMBRE" },
				{ "data": "OBRA" },
				{ "data": "COMENTARIOS" },
				{ "data": "FECHACREACION" },
				{ "data": "USUARIOACTUALIZ" },
				{ "data": "FECHAACTUALIZ" },
				{ "orderable": false, "data": "EDITAR" },
				{ "orderable": false, "data": "VERONLINE" },
				{ 
					"class":          "details-control",
					"orderable":      false,
					"data": "VERANEXOS" 
				},
				{ "orderable": false, "data": "DESCARGAR" },
				{ "orderable": false, "data": "ZIP" },
				{ "orderable": false, "data": "APROBAR" },
				{ "orderable": false, "data": "ELIMINAR" }
			],
			"order": [[0, 'desc']],
			"rowCallback": function( row, data){
				if( $.inArray(data.DT_RowId, selected) !== -1 )
				{
					$(row).addClass('selected');
				}
			}
	    } );
	     
		  /*$('#tbcomiteobra tfoot th').each( function () {
	        var title = $('#tbcomiteobra tfoot th').eq( $(this).index() ).text();
			if(title==""){$(this).html('');}else{
	        $(this).html( '<div class="input-group"><input type="text" style="width:100%" class="form-control" placeholder="'+title+'" /></div>' );}
	    } );*/
	     
	    // DataTable
	    var table = $('#tbcomitetecnico').DataTable();
	 
	    // Apply the search
	    table.columns().every( function () {
	        var that = this;
	 
	        $( 'input', this.footer() ).on( 'keyup change', function () {
	            table.search( this.value ).draw();
	        } );
	    } );
	    
	    $('#tbcomitetecnico tbody').on('click', 'tr', function () {
	        var id = this.id;
	        var index = $.inArray(id, selected);
	 
	        if ( index === -1 ) {
	            selected.push( id );
	        } else {
	            selected.splice( index, 1 );
	        }
	 
	        $(this).toggleClass('selected');
	    } );
	    
	    $('#aprobarmasivocomitetecnico').on('click', function() 
		{
		    var sel = [];
		    var conf = confirm("Realmente desea aprobar los informes seleccionados?");
			if(conf==true)
			{
				for(var i = 0; i<selected.length;i++)
				{
					if(selected[i]=="" || selected[i]==null)
					{
					}
					else
					{
						var id = selected[i];
						var n=id.split("row_");
						sel.push(n[1]);
						APROBARMASIVO(id.replace('row_',''),'COMITETECNICO');
					}
				}
				$('#estadoaprobacioncomitetecnico').html('<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button>Los informes seleccionados han sido aprobados correctamente</div>');
				sel.length=0; selected.length = 0;
			}
			else
			{
			  return false;
			}
	    });
	    
	    $('#eliminarmasivocomitetecnico').on('click', function() 
		{
		    var sel = [];
		    var conf = confirm("Realmente desea eliminar los informes seleccionados?");
			if(conf==true)
			{
				for(var i = 0; i<selected.length;i++)
				{
					if(selected[i]=="" || selected[i]==null)
					{
					}
					else
					{
						var id = selected[i];
						var n=id.split("row_");
						sel.push(n[1]);
						ELIMINARMASIVO(id.replace('row_',''),'COMITETECNICO');
					}
				}
				$('#estadoaprobacioncomitetecnico').html('<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button>Los informes seleccionados han sido eliminados correctamente</div>');
				sel.length=0; selected.length = 0;
			}
			else
			{
			  return false;
			}
	    });
	    
	    var detailRows = [];
	 
	    $('#tbcomitetecnico tbody').on( 'click', 'tr td.details-control', function () {
	        var tr = $(this).closest('tr');
	        var row = table.row( tr );
			var dat = row.data();
			var id = dat.CLAVE;		
	
	        if ( row.child.isShown()) 
	        {
	            tr.removeClass( 'details' );
	            row.child.hide();
	            $("#verocultaranexo"+id).html("VER ANEXOS");
	        }
	        else 
	        {
	        	$("#verocultaranexo"+id).html("OCULTAR ANEXOS");
	            // Open this row
				var table1 = "<div id='divchil"+id+"'><div class='col-xs-1'></div><div id='no-more-tables'><table style='font-size:12px;width:40%' class='table table-condensed table-striped' id='tbanexos"+id+"'>";
				table1+="<thead><tr><th>NOMBRE DEL ANEXO</th><th>VER</th><th>DESCARGAR</th></tr></thead><tbody>";
				              
				table1+="</tbody><tfoot><tr><th></th><th></th><th></th></tr></tfoot></table></div></div>";
				row.child(table1).show();
				convertirdata(id);
	            tr.addClass('details');
			}
			
		    });
		    
			  // On each draw, loop over the `detailRows` array and show any child rows
			table.on( 'draw', function () {
		        $.each( detailRows, function ( i, id ) {
		            $('#'+id+' td.details-control').trigger( 'click' );
		     });
	    });
	}
	else
	if(tipo == 'INFORMEMENSUAL')
	{
		var selected = [];
		var table = $('#tbinformemensual').DataTable( {       
	        
			"ordering": true,
			"info": true,
			"autoWidth": false,
			"pagingType": "simple_numbers",
			"lengthMenu": [[50,100,200,-1 ], [50,100,200,"Todos" ]],
			"language": {
			"lengthMenu": "Ver _MENU_ registros",
			"zeroRecords": "No se encontraron datos",
			"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
			"infoEmpty": "No se encontraron datos",
			"sSearch": "Buscar:",
			"infoFiltered": ""
			},
			"processing": true,
	        "serverSide": true,
	        "ajax": {
	                    "url": "../../json/jsoninformemensual.php",
	                    "data": { obras: obras,informes: informes,nomarc:nomarc,nomane:nomane,nomley:nomley,nomcom:nomcom }
					},
			"columns": [
				{ "data": "NOMBRE" },
				{ "data": "OBRA" },
				{ "data": "COMENTARIOS" },
				{ "data": "FECHACREACION" },
				{ "data": "USUARIOACTUALIZ" },
				{ "data": "FECHAACTUALIZ" },
				{ "orderable": false, "data": "EDITAR" },
				{ "orderable": false, "data": "VERONLINE" },
				{ 
					"class":          "details-control",
					"orderable":      false,
					"data": "VERANEXOS" 
				},
				{ "orderable": false, "data": "DESCARGAR" },
				{ "orderable": false, "data": "ZIP" },
				{ "orderable": false, "data": "APROBAR" },
				{ "orderable": false, "data": "ELIMINAR" }
			],
			"order": [[0, 'desc']],
			"rowCallback": function( row, data){
				if( $.inArray(data.DT_RowId, selected) !== -1 )
				{
					$(row).addClass('selected');
				}
			}
	    } );
	     
		  /*$('#tbcomiteobra tfoot th').each( function () {
	        var title = $('#tbcomiteobra tfoot th').eq( $(this).index() ).text();
			if(title==""){$(this).html('');}else{
	        $(this).html( '<div class="input-group"><input type="text" style="width:100%" class="form-control" placeholder="'+title+'" /></div>' );}
	    } );*/
	     
	    // DataTable
	    var table = $('#tbinformemensual').DataTable();
	 
	    // Apply the search
	    table.columns().every( function () {
	        var that = this;
	 
	        $( 'input', this.footer() ).on( 'keyup change', function () {
	            table.search( this.value ).draw();
	        } );
	    } );
	    
	    $('#tbinformemensual tbody').on('click', 'tr', function () {
	        var id = this.id;
	        var index = $.inArray(id, selected);
	 
	        if ( index === -1 ) {
	            selected.push( id );
	        } else {
	            selected.splice( index, 1 );
	        }
	 
	        $(this).toggleClass('selected');
	    } );
	    
	    $('#aprobarmasivoinformemensual').on('click', function() 
		{
		    var sel = [];
		    var conf = confirm("Realmente desea aprobar los informes seleccionados?");
			if(conf==true)
			{
				for(var i = 0; i<selected.length;i++)
				{
					if(selected[i]=="" || selected[i]==null)
					{
					}
					else
					{
						var id = selected[i];
						var n=id.split("row_");
						sel.push(n[1]);
						APROBARMASIVO(id.replace('row_',''),'INFORMEMENSUAL');
					}
				}
				$('#estadoaprobacioninformemensual').html('<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button>Los informes seleccionados han sido aprobados correctamente</div>');
				sel.length=0; selected.length = 0;
			}
			else
			{
			  return false;
			}
	    });
	    
	    $('#eliminarmasivoinformemensual').on('click', function() 
		{
		    var sel = [];
		    var conf = confirm("Realmente desea eliminar los informes seleccionados?");
			if(conf==true)
			{
				for(var i = 0; i<selected.length;i++)
				{
					if(selected[i]=="" || selected[i]==null)
					{
					}
					else
					{
						var id = selected[i];
						var n=id.split("row_");
						sel.push(n[1]);
						ELIMINARMASIVO(id.replace('row_',''),'INFORMEMENSUAL');
					}
				}
				$('#estadoaprobacioninformemensual').html('<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button>Los informes seleccionados han sido eliminados correctamente</div>');
				sel.length=0; selected.length = 0;
			}
			else
			{
			  return false;
			}
	    });
	    
	    var detailRows = [];
	 
	    $('#tbinformemensual tbody').on( 'click', 'tr td.details-control', function () {
	        var tr = $(this).closest('tr');
	        var row = table.row( tr );
			var dat = row.data();
			var id = dat.CLAVE;		
	
	        if ( row.child.isShown()) 
	        {
	            tr.removeClass( 'details' );
	            row.child.hide();
	            $("#verocultaranexo"+id).html("VER ANEXOS");
	        }
	        else 
	        {
	        	$("#verocultaranexo"+id).html("OCULTAR ANEXOS");
	            // Open this row
				var table1 = "<div id='divchil"+id+"'><div class='col-xs-1'></div><div id='no-more-tables'><table style='font-size:12px;width:40%' class='table table-condensed table-striped' id='tbanexos"+id+"'>";
				table1+="<thead><tr><th>NOMBRE DEL ANEXO</th><th>VER</th><th>DESCARGAR</th></tr></thead><tbody>";
				              
				table1+="</tbody><tfoot><tr><th></th><th></th><th></th></tr></tfoot></table></div></div>";
				row.child(table1).show();
				convertirdata(id);
	            tr.addClass('details');
			}
			
		    });
		    
			  // On each draw, loop over the `detailRows` array and show any child rows
			table.on( 'draw', function () {
		        $.each( detailRows, function ( i, id ) {
		            $('#'+id+' td.details-control').trigger( 'click' );
		     });
	    });
	}
	else
	if(tipo == 'REGISTROFOTOGRAFICO')
	{
		var selected = [];
		var table = $('#tbregistrofotografico').DataTable( {       
	        
			"ordering": true,
			"info": true,
			"autoWidth": false,
			"pagingType": "simple_numbers",
			"lengthMenu": [[50,100,200,-1 ], [50,100,200,"Todos" ]],
			"language": {
			"lengthMenu": "Ver _MENU_ registros",
			"zeroRecords": "No se encontraron datos",
			"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
			"infoEmpty": "No se encontraron datos",
			"sSearch": "Buscar:",
			"infoFiltered": ""
			},
			"processing": true,
	        "serverSide": true,
	        "ajax": {
	                    "url": "../../json/jsonregistrofotografico.php",
	                    "data": { obras: obras,informes: informes,nomarc:nomarc,nomane:nomane,nomley:nomley,nomcom:nomcom }
					},
			"columns": [
				{ "data": "NOMBRE" },
				{ "data": "OBRA" },
				{ "data": "FECHACREACION" },
				{ "data": "USUARIOACTUALIZ" },
				{ "data": "FECHAACTUALIZ" },
				{ "orderable": false, "data": "EDITAR" },
				{ "orderable": false, "data": "VERFOTOS" },
				{ "orderable": false, "data": "ZIP" },
				{ "orderable": false, "data": "APROBAR" },
				{ "orderable": false, "data": "ELIMINAR" }
			],
			"order": [[0, 'desc']],
			"rowCallback": function( row, data){
				if( $.inArray(data.DT_RowId, selected) !== -1 )
				{
					$(row).addClass('selected');
				}
			}
	    } );
	     
		  /*$('#tbcomiteobra tfoot th').each( function () {
	        var title = $('#tbcomiteobra tfoot th').eq( $(this).index() ).text();
			if(title==""){$(this).html('');}else{
	        $(this).html( '<div class="input-group"><input type="text" style="width:100%" class="form-control" placeholder="'+title+'" /></div>' );}
	    } );*/
	     
	    // DataTable
	    var table = $('#tbregistrofotografico').DataTable();
	 
	    // Apply the search
	    table.columns().every( function () {
	        var that = this;
	 
	        $( 'input', this.footer() ).on( 'keyup change', function () {
	            table.search( this.value ).draw();
	        } );
	    } );
	    
	    $('#tbregistrofotografico tbody').on('click', 'tr', function () {
	        var id = this.id;
	        var index = $.inArray(id, selected);
	 
	        if ( index === -1 ) {
	            selected.push( id );
	        } else {
	            selected.splice( index, 1 );
	        }
	 
	        $(this).toggleClass('selected');
	    } );
	    
	    $('#aprobarmasivoregistrofotografico').on('click', function() 
		{
		    var sel = [];
		    var conf = confirm("Realmente desea aprobar los informes seleccionados?");
			if(conf==true)
			{
				for(var i = 0; i<selected.length;i++)
				{
					if(selected[i]=="" || selected[i]==null)
					{
					}
					else
					{
						var id = selected[i];
						var n=id.split("row_");
						sel.push(n[1]);
						APROBARMASIVO(id.replace('row_',''),'REGISTROFOTOGRAFICO');
					}
				}
				$('#estadoaprobacionregistrofotografico').html('<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button>Los informes seleccionados han sido aprobados correctamente</div>');
				sel.length=0; selected.length = 0;
			}
			else
			{
			  return false;
			}
	    });
	    
	    $('#eliminarmasivoregistrofotografico').on('click', function() 
		{
		    var sel = [];
		    var conf = confirm("Realmente desea eliminar los informes seleccionados?");
			if(conf==true)
			{
				for(var i = 0; i<selected.length;i++)
				{
					if(selected[i]=="" || selected[i]==null)
					{
					}
					else
					{
						var id = selected[i];
						var n=id.split("row_");
						sel.push(n[1]);
						ELIMINARMASIVO(id.replace('row_',''),'REGISTROFOTOGRAFICO');
					}
				}
				$('#estadoaprobacionregistrofotografico').html('<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button>Los informes seleccionados han sido eliminados correctamente</div>');
				sel.length=0; selected.length = 0;
			}
			else
			{
			  return false;
			}
	    });
	}
}
function convertirdata(d)
{
	var selected = [];
	var obras = "";
	var objCBarray = document.getElementsByName('multiselect_busobra');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	obras += objCBarray[i].value + ",";
	    }
	}
	
	var informes = "";
	var objCBarray = document.getElementsByName('multiselect_bustipoinforme');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	informes += objCBarray[i].value + ",";
	    }
	}
	
	var nomarc = $('#busnombrearchivo').val();
	var nomane = $('#busnombreanexo').val();
	var nomley = $('#busleyenda').val();
	var nomcom = $('#buscomentario').val();
   var table = $('#tbanexos'+d).DataTable(
   {   
        "ordering": true,
		"info": true,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,20,30,-1 ], [10,20,30,"Todos" ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "Anterior","next":"siguiente"}
		},
		"processing": true,
        "serverSide": true,
        "ajax": { 
        			"url": "../../json/jsonanexos.php", 
        			"data": { id:d }
        		},
		"columns": [ 
			{ "data": "NOMBREARCHIVO" },
			{ "data": "VERONLINE" },
			{ "data": "DESCARGAR" }
		],
		"order": [[0, 'asc']],
		"rowCallback": function( row, data){
				if( $.inArray(data.DT_RowId, selected) !== -1 )
				{
					$(row).addClass('selected');
				}
			}
    } );
     
	$('table #tbanexos'+d+' thead tr th').each(function(index,element){
	    index += 1;
	    $('tr td:nth-child('+index+')').attr('data-title',$(this).attr('data-title'));
	});
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
}